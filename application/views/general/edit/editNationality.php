<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo lang('create_tautybe'); ?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">

        <?php
        foreach ($resultsEditNationality as $row) {
            echo validation_errors();
            /* Tautybė */
            echo form_fieldset('Tautybė');
            echo form_open('/general/nationalitys/edit_nationality/' . $row->id);
            $dataNationality = array(
                'name' => 'nationality',
                'id' => 'nationality',
                'size' => '100',
                'value' => $row->tautybe,
                'class' => 'form-control'
            );
            echo form_input($dataNationality);
            echo form_fieldset_close();
            echo br(1);
            $buttonSubmit = array(
                'name' => 'mysubmit',
                'id' => 'submit',
                'class' => 'btn btn-success',
                'value' => 'Išsaugoti'
            );
            echo form_submit($buttonSubmit);
            ?>
                <?php echo anchor("general/nationalitys/seeNationality", "Grįžti atgal",
                    array(  'name' => 'backSeeNationality',
                            'id' => 'backSeeNationality',
                            'class'=> 'btn btn-default'))
                ?>
            <?php
            echo form_close();
        }
        ?>
    </div>
                        <!-- /.col-lg-12 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
