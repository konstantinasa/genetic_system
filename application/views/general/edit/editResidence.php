<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo lang('edit_item'); ?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">

        <?php
        foreach ($resultsEditResidence as $row) {
            echo validation_errors();
            /* Gyvenviete */
            echo form_fieldset('Gyvenamoji vieta (miestas ar kaimas)');
            echo form_open('/general/residences/edit_residence/' . $row->id);
            $dataResidence = array(
                'name' => 'residence',
                'id' => 'residence',
                'size' => '100',
                'value' => $row->miestasarkaimas,
                'class' => 'form-control'
            );
            echo form_input($dataResidence);
            echo br(2);
            
            /* Savivaldybe */
            echo form_label('Savivaldybė', 'savivaldybe');
            $dataSavivaldybe = [ '0' => '-'];
            foreach($resultsSavivaldybe as $row2){
                $dataSavivaldybe[$row2->id] = $row2->gyvsavivaldybe . ', ' . $row2->gyvapskritis;
            }
            echo form_dropdown('savivaldybe',$dataSavivaldybe, $row->savivaldybeid, 'id="dropDownSearchResidence"', 'class="form-control"');
            echo br(2);
            
            
            
            
            
            echo form_fieldset_close();
            echo br(1);
            $buttonSubmit = array(
                'name' => 'mysubmit',
                'id' => 'submit',
                'class' => 'btn btn-success',
                'value' => 'Išsaugoti'
            );
            echo form_submit($buttonSubmit);
            ?>
                <?php echo anchor("general/residences/seeResidence", "Grįžti atgal",
                    array(  'name' => 'backSeeResidence',
                            'id' => 'backSeeResidence',
                            'class'=> 'btn btn-default'))
                ?>
            <?php
            echo form_close();
        }
        ?>
    </div>
                        <!-- /.col-lg-12 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
