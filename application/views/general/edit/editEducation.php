<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo lang('edit_item'); ?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">

        <?php
        foreach ($resultsEditEducation as $row) {
            echo validation_errors();
            /* Tautybė */
            echo form_fieldset('Išsilavinimas');
            echo form_open('/general/educations/edit_education/' . $row->id);
            $dataEducation = array(
                'name' => 'education',
                'id' => 'education',
                'size' => '100',
                'value' => $row->issilavinimas,
                'class' => 'form-control'
            );
            echo form_input($dataEducation);
            echo form_fieldset_close();
            echo br(1);
            $buttonSubmit = array(
                'name' => 'mysubmit',
                'id' => 'submit',
                'class' => 'btn btn-success',
                'value' => 'Išsaugoti'
            );
            echo form_submit($buttonSubmit);
            ?>
                <?php echo anchor("general/educations/seeEducation", "Grįžti atgal",
                    array(  'name' => 'backSeeEducation',
                            'id' => 'backSeeEducation',
                            'class'=> 'btn btn-default'))
                ?>
            <?php
            echo form_close();
        }
        ?>
    </div>
                        <!-- /.col-lg-12 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
