<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo lang('edit_item'); ?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">

        <?php
        foreach ($resultsEditDisease as $row) {
            echo validation_errors();
            /* Tautybė */
            echo form_fieldset('Ligos pavadinimas');
            echo form_open('/general/diseases/edit_disease/' . $row->id);
            $dataDisease = array(
                'name' => 'disease',
                'id' => 'disease',
                'size' => '100',
                'value' => $row->pavadinimas,
                'class' => 'form-control'
            );
            echo form_input($dataDisease);
            echo form_fieldset_close();
            echo br(1);
            $buttonSubmit = array(
                'name' => 'mysubmit',
                'id' => 'submit',
                'class' => 'btn btn-success',
                'value' => 'Išsaugoti'
            );
            echo form_submit($buttonSubmit);
            ?>
                <?php echo anchor("general/diseases/seeDisease", "Grįžti atgal",
                    array(  'name' => 'backSeeDisease',
                            'id' => 'backSeeDisease',
                            'class'=> 'btn btn-default'))
                ?>
            <?php
            echo form_close();
        }
        ?>
    </div>
                        <!-- /.col-lg-12 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
