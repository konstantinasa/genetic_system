<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo lang('create_alergiskaskam'); ?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
        <?php
        echo validation_errors();
        /* Tautybė */
        echo form_fieldset('Alergenas');
        echo form_open('/general/allergys/createAllergy');
        $dataAllergy = array(
            'name' => 'allergy',
            'id' => 'allergy',
            'size' => '100',
            'value' => set_value('allergy'),
            'class' => 'form-control'
        );
        echo form_input($dataAllergy);
        echo br();
        echo form_fieldset_close();
        $buttonSubmit = array(
            'name' => 'mysubmit',
            'id' => 'submit',
            'class'=> 'btn btn-success',
            'value' => 'Išsaugoti'
        );
        echo form_submit($buttonSubmit);
        ?>
            <?php echo anchor("general/allergys/seeAllergy", "Grįžti atgal",
                array(  'name' => 'backSeeAllergy',
                        'id' => 'backSeeAllergy',
			'class'=> 'btn btn-default'))
            ?>
        <?php
        echo form_close();
        ?>
    </div>
                        <!-- /.col-lg-12 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
