<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo lang('create_srginfo'); ?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
        <?php
        echo validation_errors();
        
        /* Liga */
        echo form_fieldset('Liga');
        echo form_open('/general/sicks/createSick');
        $dataSick = array(
            'name' => 'sick',
            'id' => 'sick',
            'size' => '100',
            'value' => set_value('sick'),
            'class' => 'form-control'
        );
        echo form_input($dataSick);
        echo br();
        
        /*ar pagr kat*/
        echo form_label('Ar pagrindinė kategorija', 'kategorija');
        $dataKategoroja = [ '0' => '-'];
        foreach($resultReiksme as $row){
            $dataKategoroja[$row->id] = $row->reiksme;
        }
        echo form_dropdown('kategorija',$dataKategoroja, '', 'class="form-control"');
        echo br(2);
        
        
        echo form_fieldset_close();
        $buttonSubmit = array(
            'name' => 'mysubmit',
            'id' => 'submit',
            'class'=> 'btn btn-success',
            'value' => 'Išsaugoti'
        );
        echo form_submit($buttonSubmit);
        ?>
            <?php echo anchor("general/sicks/seeSick", "Grįžti atgal",
                array(  'name' => 'backSeeSick',
                        'id' => 'backSeeSick',
			'class'=> 'btn btn-default'))
            ?>
        <?php
        echo form_close();
        ?>
    </div>
                        <!-- /.col-lg-12 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
