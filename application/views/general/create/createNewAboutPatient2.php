<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo lang('create_patient_heading'); ?> 2 žingsnis</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
    
    <?php
    // --- Progres bar -------
    $done = array(
        'first' => 'progtrckr-done',
        'second' => 'progtrckr-done',
        'third' => 'progtrckr-todo',
        'fourth' => 'progtrckr-todo',
        'fifth' => 'progtrckr-todo'
    );
    $this->load->view('include/progress_bar', $done);
    echo br(1);
        
        echo validation_errors();
        echo form_open('/general/aboutPatients/createNewAboutPatient2');
        
        echo anchor("general/aboutPatients/createNewAboutPatient", "Grįžti atgal",
        array(  'name' => 'backSeeAboutPatient',
                'id' => 'backSeeAboutPatient',
                'class'=> 'btn btn-default'));
        echo nbs(3);

        $buttonSubmit = array(
        'name' => 'mysubmit',
        'id' => 'submit',
        'value' => 'Testi',
        'class'=> 'btn btn btn-primary'
         );
        echo form_submit($buttonSubmit);
        echo nbs(3);

        ?>
        <div class="pull-right">
            <?php
            echo anchor("general/aboutPatients/clear_form", "Išvalyti anketą",
                    array('id' => 'clear_form',
                        'name' => 'clear_form',
                        'type' => 'button',
                        'width' => '200',
                        'height' => '200',
                        'class'=> 'btn btn-danger btn-xs'));  
           ?>
        </div>
        <?php 
       
        echo br(2);
        
        
         /* Ugis */
        echo form_label('Ūgis(cm)', 'ugis');
        $dataUgis = array(
            'name' => 'ugis',
            'id' => 'ugis',
            'size' => '100',
            'value' => set_value('ugis') === null ? set_value('ugis') : isset($this->session->userdata['step_2']['ugis']) ? $this->session->userdata['step_2']['ugis'] : set_value('ugis'),
            'class' => 'form-control',
            'placeholder' => 'pvz: 167'
        );
        echo form_input($dataUgis);
        echo br(1);
        
        /* Svoris*/
        echo form_label('Svoris(kg)', 'svoris');
        $dataSvoris = array(
            'name' => 'svoris',
            'id' => 'svoris',
            'size' => '100',
            'value' => set_value('svoris') === null ? set_value('svoris') : isset($this->session->userdata['step_2']['svoris']) ? $this->session->userdata['step_2']['svoris'] : set_value('svoris'),
            'class' => 'form-control',
            'placeholder' => 'pvz: 77.137'
        );
        echo form_input($dataSvoris);
        echo br(1);
        
         /* Tiriamojo liemes apimtis*/
        echo form_label('Liemens apimtis(cm)', 'tiriamliemapim');
        $dataTirLiemApimt = array(
            'name' => 'tiriamliemapim',
            'id' => 'tiriamliemapim',
            'size' => '100',
            'value' => set_value('tiriamliemapim') === null ? set_value('tiriamliemapim') : isset($this->session->userdata['step_2']['tiriamliemapim']) ? $this->session->userdata['step_2']['tiriamliemapim'] : set_value('tiriamliemapim'),
            'class' => 'form-control',
            'placeholder' => 'pvz: 95'
        );
        echo form_input($dataTirLiemApimt);
        echo br(1);
        
        /* Gimimo svoris*/
        echo form_label('Gimimo svoris(kg)', 'gimimosvoris');
        $dataGimSvor = array(
            'name' => 'gimimosvoris',
            'id' => 'gimimosvoris',
            'size' => '100',
            'value' => set_value('gimimosvoris') === null ? set_value('gimimosvoris') : isset($this->session->userdata['step_2']['gimimosvoris']) ? $this->session->userdata['step_2']['gimimosvoris'] : set_value('gimimosvoris'),
            'class' => 'form-control',
            'placeholder' => 'pvz: 7.137'
        );
        echo form_input($dataGimSvor);
        echo br(1);
        
         /* Gimimo ugis*/
        echo form_label('Gimimo ūgis(cm)', 'gimimougis');
        $dataGimUg = array(
            'name' => 'gimimougis',
            'id' => 'gimimougis',
            'size' => '100',
            'value' => set_value('gimimougis') === null ? set_value('gimimougis') : isset($this->session->userdata['step_2']['gimimougis']) ? $this->session->userdata['step_2']['gimimougis'] : set_value('gimimougis'),
            'class' => 'form-control',
            'placeholder' => 'pvz: 57'
            
        );
        echo form_input($dataGimUg);
        echo br(1);
        
        /*Ar kito svoris?*/
        /* KG*/
        echo "<h4>Ar buvo svorio augimas/kritimas per pastaruosius metus?</h4>";
        echo form_label('Priaugau(kg)', 'kg');
        $dataKg = array(
            'name' => 'kg',
            'id' => 'kg',
            'size' => '100',
            'value' => set_value('kg') === null ? set_value('kg') : isset($this->session->userdata['step_2']['kg']) ? $this->session->userdata['step_2']['kg'] : set_value('kg'),
            'class' => 'form-control',
            'placeholder' => 'pvz: 7.137'
        );
        echo form_input($dataKg);
        echo br(1);
        
        /* Per men*/
        echo form_label('Per mėnesius:', 'perkiekmen');
        $dataPerMen = array(
            'name' => 'perkiekmen',
            'id' => 'perkiekmen',
            'size' => '100',
            'value' => set_value('perkiekmen') === null ? set_value('perkiekmen') : isset($this->session->userdata['step_2']['perkiekmen']) ? $this->session->userdata['step_2']['perkiekmen'] : set_value('perkiekmen'),
            'class' => 'form-control',
            'placeholder' => 'pvz: 3'
        );
        echo form_input($dataPerMen);
        echo br(1);
        
        /* KG_2*/
        echo form_label('Numečiau(kg)' ,'kg_2');
        $dataKg_2 = array(
            'name' => 'kg_2',
            'id' => 'kg_2',
            'size' => '100',
            'value' => set_value('kg_2') === null ? set_value('kg_2') : isset($this->session->userdata['step_2']['kg_2']) ? $this->session->userdata['step_2']['kg_2'] : set_value('kg_2'),
            'class' => 'form-control',
            'placeholder' => 'pvz: 7.137'
        );
        echo form_input($dataKg_2);
        echo br(1);
        
        /* Per men 2*/
        echo form_label('Per mėnesius:', 'perkiekmen_2');
        $dataPerMen = array(
            'name' => 'perkiekmen_2',
            'id' => 'perkiekmen_2',
            'size' => '100',
            'value' => set_value('perkiekmen_2') === null ? set_value('perkiekmen_2') : isset($this->session->userdata['step_2']['perkiekmen_2']) ? $this->session->userdata['step_2']['perkiekmen_2'] : set_value('perkiekmen_2'),
            'class' => 'form-control',
            'placeholder' => 'pvz: 3'
        );
        echo form_input($dataPerMen);
        echo br(2);
        
        echo anchor("general/aboutPatients/createNewAboutPatient", "Grįžti atgal",
                array(  'name' => 'backCreateNewAboutPatient2',
                        'id' => 'backCreateNewAboutPatient2',
			'class'=> 'btn btn-default'));
        echo nbs(3);
        
        $buttonSubmit2 = array(
            'name' => 'mysubmit2',
            'id' => 'submit2',
            'value' => 'Testi',
            'class'=> 'btn btn btn-primary'
        );
        echo form_submit($buttonSubmit2);
        ?>
         
           </br> 
        <?php
//        var_dump($this->session->all_userdata());
//        var_dump($this->session->userdata['step_2']); 
//        echo "</br>";
//        var_dump($this->session->userdata['step_2']);
//        echo '</br>';
//        var_dump($this->session->userdata['step_2']);
        //var_dump($dataSvorKit);
        ?>
            
           
        <?php
        echo form_close();
        ?>

    </div>
                        <!-- /.col-lg-12 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
