<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo lang('create_miestas'); ?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
        <?php
        echo validation_errors();
        /* Tautybė */
        echo form_fieldset('Gyvenvietė (miestas ar kaimas)');
        echo form_open('/general/residences/createResidence');
        
        /* Gyvenviete */
        $dataResidence = array(
            'name' => 'residence',
            'id' => 'residence',
            'size' => '100',
            'value' => set_value('residence'),
            'class' => 'form-control'
        );
        echo form_input($dataResidence);
        echo br();
        
        /* Savivaldybe */
        echo form_label('Savivaldybė', 'savivaldybe');
        $dataSavivaldybe = [ '0' => '-'];
        foreach($resultsResidenceSavivaldybe as $row){
            $dataSavivaldybe[$row->id] = $row->gyvsavivaldybe . ', ' . $row->gyvapskritis;
        }
        echo form_dropdown('savivaldybe',$dataSavivaldybe, '', 'id="dropDownSearchResidence"', 'class="form-control"');
        echo br(2);
        
        
        /* Miestas ar kaimas  */
//        echo form_label('Gyvenamoji vieta(miestas/kaimas)', 'miestasarkaimas');
//        $dataGMiestas = array(
//            '0' => '-'
//        );
//        foreach ($resultCity as $row) {
//            $dataGMiestas[$row->id] = $row->miestasarkaimas;
//        }
//        echo form_dropdown('miestasarkaimas', $dataGMiestas, isset($this->session->userdata['step_1']['miestasarkaimas']) ? $this->session->userdata['step_1']['miestasarkaimas'] : '', 'id="dropDownSearchCity"', 'class="form-control"');
//        echo br(2);
//        
        
        
        echo form_fieldset_close();
        $buttonSubmit = array(
            'name' => 'mysubmit',
            'id' => 'submit',
            'class'=> 'btn btn-success',
            'value' => 'Išsaugoti'
        );
        echo form_submit($buttonSubmit);
        ?>
            <?php echo anchor("general/residences/seeResidence", "Grįžti atgal",
                array(  'name' => 'backSeeResidence',
                        'id' => 'backSeeResidence',
			'class'=> 'btn btn-default'))
            ?>
        <?php
        echo form_close();
        ?>
    </div>
                        <!-- /.col-lg-12 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
