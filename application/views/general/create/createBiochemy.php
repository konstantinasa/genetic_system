<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo lang('create_bio'); ?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
        <?php
        echo validation_errors();
        /* Tautybė */
        echo form_fieldset('Rodiklis');
        echo form_open('/general/biochemys/createBiochemy');
        $dataBiochemy = array(
            'name' => 'biochemy',
            'id' => 'biochemy',
            'size' => '100',
            'value' => set_value('biochemy'),
            'class' => 'form-control'
        );
        echo form_input($dataBiochemy);
        echo br();
        echo form_fieldset_close();
        $buttonSubmit = array(
            'name' => 'mysubmit',
            'id' => 'submit',
            'class'=> 'btn btn-success',
            'value' => 'Išsaugoti'
        );
        echo form_submit($buttonSubmit);
        ?>
            <?php echo anchor("general/biochemys/seeBiochemy", "Grįžti atgal",
                array(  'name' => 'backSeeBiochemy',
                        'id' => 'backSeeBiochemy',
			'class'=> 'btn btn-default'))
            ?>
        <?php
        echo form_close();
        ?>
    </div>
                        <!-- /.col-lg-12 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
