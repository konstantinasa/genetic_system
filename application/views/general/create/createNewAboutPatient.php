<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo lang('create_patient_heading'); ?> 1 žingsnis</h1>
        </div><!-- /.col-lg-12 -->
    </div><!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            
    <?php
    // --- Progres bar -------
    $done = array(
        'first' => 'progtrckr-done',
        'second' => 'progtrckr-todo',
        'third' => 'progtrckr-todo',
        'fourth' => 'progtrckr-todo',
        'fifth' => 'progtrckr-todo'
    );
    $this->load->view('include/progress_bar', $done);
    echo br(1);
    
        echo validation_errors();
        echo form_open('/general/aboutPatients/createNewAboutPatient');
        
        echo anchor("general/aboutPatients/seeAboutPatient", "Grįžti atgal",
        array(  'name' => 'backSeeAboutPatient',
                'id' => 'backSeeAboutPatient',
                'class'=> 'btn btn-default'));
        echo nbs(3);

        $buttonSubmit = array(
        'name' => 'mysubmit',
        'id' => 'submit',
        'value' => 'Testi',
        'class'=> 'btn btn btn-primary'
         );
        echo form_submit($buttonSubmit);
        echo nbs(3);
        
        ?>
        <div class="pull-right">
            <?php
            echo anchor("general/aboutPatients/clear_form", "Išvalyti anketą",
                    array('id' => 'clear_form',
                        'name' => 'clear_form',
                        'type' => 'button',
                        'width' => '200',
                        'height' => '200',
                        'class'=> 'btn btn-danger btn-xs'));  
           ?>
        </div>
        <?php
        
        echo br(2);
        
        /* Patient */
        echo form_label('Paciento kodas', 'pacientas');
        $dataPatient = array(
            'name' => 'pacientas',
            'id' => 'pacientas',
            'size' => '100',
            'value' => set_value('pacientas') === null ? set_value('pacientas') : isset($this->session->userdata['step_1']['pacientas']) ? $this->session->userdata['step_1']['pacientas'] : set_value('pacientas'),
            'class' => 'form-control',
            'placeholder' => 'LTG_XXXX_X_XXXX_XXXX_XX_XXX'
        );
        echo form_input($dataPatient);
        echo br(1);
        ?>
         
        <!--====================================================================-->
                

        <!--====================================================================-->
        
         <?php
        /* Tyrimo data */
        echo form_label('Tyrimo data', 'tdata');
        ?>
        
        <div class="input-group date form_date col-md-5" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
            <input name="tdata" id="tdata" class="form-control" size="16" type="text" value="<?php echo set_value('tdata') === null ? set_value('tdata') : isset($this->session->userdata['step_1']['tdata']) ? $this->session->userdata['step_1']['tdata'] : set_value('tdata'); ?>" readonly>
            <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
        </div>
       <input type="hidden" id="dtp_input2" value="" />
            
        <?php
        echo br(1);
        
        /* Lytis */
        echo form_label('Lytis', 'lytis');
        $dataSex = array(
            2 => '-',
            4 => 'Moteris',
            3 => 'Vyras'
            
        );
        echo form_dropdown('lytis', $dataSex,  set_value('lytis') === null ? set_value('lytis') : isset($this->session->userdata['step_1']['lytis']) ? $this->session->userdata['step_1']['lytis'] : set_value('lytis') , 'id="dropDownSearchGender"','class="form-control"');
        echo br(2);
        
        /* Gimimo data */
        echo form_label('Gimimo data', 'gdata');
        ?>
        
        <div class="input-group date form_date col-md-5" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
            <input name="gdata" id="gdata" class="form-control" size="16" type="text" value="<?php echo set_value('gdata') === null ? set_value('gdata') : isset($this->session->userdata['step_1']['gdata']) ? $this->session->userdata['step_1']['gdata'] : set_value('gdata'); ?>" readonly>
            <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
        </div>
       <input type="hidden" id="dtp_input2" value="" />
            
        <?php
        echo br(1);
        
        /* Tautybė  */
        echo form_label('Tautybė', 'tautybe');
        $dataNationality = array(
            '0' => '-'
        );
        foreach ($resultNationality as $row) {
            $dataNationality[$row->id] = $row->tautybe;
        }
        echo form_dropdown('tautybe', $dataNationality, set_value('tautybe') === null ? set_value('tautybe') : isset($this->session->userdata['step_1']['tautybe']) ? $this->session->userdata['step_1']['tautybe'] :  set_value('tautybe'), 'id="dropDownSearchNationality"', 'class="form-control"');
        echo br(2);
        
        /* Išsilavinimas  */
        echo form_label('Išsilavinimas', 'issilavinimas');
        $dataEducation = array(
            '0' => '-'
        );
        foreach ($resultEducation as $row) {
            $dataEducation[$row->id] = $row->issilavinimas;
        }
        echo form_dropdown('issilavinimas', $dataEducation, set_value('issilavinimas') === null ? set_value('issilavinimas') : isset($this->session->userdata['step_1']['issilavinimas']) ? $this->session->userdata['step_1']['issilavinimas'] :  set_value('issilavinimas'), 'id="dropDownSearchEducation"', 'class="form-control"');
        echo br(2);
        
//        -------------------------------
        ?>
      
<!--        <input type="text" id="e7" style="width:300px" />
        </br>-->
        <?php
 //        -------------------------------       
        /* Miestas ar kaimas  */
        echo form_label('Gyvenamoji vieta(miestas/kaimas)', 'miestasarkaimas');
        $dataGMiestas = array(
            '0' => '-'
        );
        foreach ($resultCity as $row) {
            $dataGMiestas[$row->id] = $row->miestasarkaimas . ', ' . $row->gyvsavivaldybe . ', ' . $row->gyvapskritis;
        }
        echo form_dropdown('miestasarkaimas', $dataGMiestas, set_value('miestasarkaimas') === null ? set_value('miestasarkaimas') : isset($this->session->userdata['step_1']['miestasarkaimas']) ? $this->session->userdata['step_1']['miestasarkaimas'] :  set_value('miestasarkaimas'), 'id="dropDownSearchCity"', 'class="form-control"');
        echo br(2);
                             
        
        /* Užimtumas */
        echo form_label('Užimtumas', 'uzimtumas');
        $dataUzimtumas = array(
            '0' => '-'
        );
        foreach ($resultEmployment as $row) {
            $dataUzimtumas[$row->id] = $row->uzimtumas;
        }
        echo form_dropdown('uzimtumas', $dataUzimtumas, set_value('uzimtumas') === null ? set_value('uzimtumas') : isset($this->session->userdata['step_1']['uzimtumas']) ? $this->session->userdata['step_1']['uzimtumas'] :  set_value('uzimtumas'), 'id="dropDownSearchEmployment"', 'class="form-control"');
        
        
        echo br(3);
        
        echo anchor("general/aboutPatients/seeAboutPatient", "Grįžti atgal",
                array(  'name' => 'backSeeAboutPatient',
                        'id' => 'backSeeAboutPatient',
			'class'=> 'btn btn-default'));
        echo nbs(3);
            
            $buttonSubmit = array(
            'name' => 'mysubmit',
            'id' => 'submit',
            'value' => 'Testi',
            'class'=> 'btn btn btn-primary'
             );
            echo form_submit($buttonSubmit);
            
        echo form_close();
        ?>

    </div>
                        <!-- /.col-lg-12 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->


