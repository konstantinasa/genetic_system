<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo lang('create_patient_heading'); ?> 4 žingsnis</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">

        <?php
    // --- Progres bar -------
    $done = array(
        'first' => 'progtrckr-done',
        'second' => 'progtrckr-done',
        'third' => 'progtrckr-done',
        'fourth' => 'progtrckr-done',
        'fifth' => 'progtrckr-todo'
    );
    $this->load->view('include/progress_bar', $done);
    echo br(1);
    
        echo validation_errors();
        $this->load->helper('html');
        echo form_open('/general/aboutPatients/createNewAboutPatient4');
        
        echo anchor("general/aboutPatients/createNewAboutPatient3", "Grįžti atgal",
        array(  'name' => 'backSeeAboutPatient',
                'id' => 'backSeeAboutPatient',
                'class'=> 'btn btn-default'));
        echo nbs(3);

        $buttonSubmit = array(
        'name' => 'mysubmit',
        'id' => 'submit',
        'value' => 'Testi',
        'class'=> 'btn btn btn-primary',
        'formaction' => base_url('general/aboutPatients/createNewAboutPatient4'),
         );
        echo form_submit($buttonSubmit);
        echo nbs(3);
        
//        $buttonSubmit_2 = array(
//        'name' => 'mysubmit_2',
//        'id' => 'submit_2',
//        'value' => 'Baigti',
//        'class'=> 'btn btn btn-success',
//        'formaction' => base_url('general/aboutPatients/createNewAboutPatient4_finish'),
//         );
//        echo form_submit($buttonSubmit_2);
//        echo nbs(3);
        
        ?>
        <div class="pull-right">
            <?php
            echo anchor("general/aboutPatients/clear_form", "Išvalyti anketą",
                    array('id' => 'clear_form',
                        'name' => 'clear_form',
                        'type' => 'button',
                        'width' => '200',
                        'height' => '200',
                        'class'=> 'btn btn-danger btn-xs'));  
           ?>
        </div>
        <?php  
       
        echo br(2);
        
        /* Ar nustatytas padid Chol kiekis */
        echo form_label('Ar buvo kada nors nustatytas padidintas cholesterolio kiekis?', 'arbuvocholes');
        $dataReiksme = array();
        foreach ($resultReiksme as $row) {
            $dataReiksme[$row->id] = $row->reiksme;
        }
        $attr = 'id="arbuvocholes" class="form-control" onchange="checkBoxDis();"';
        echo form_dropdown('arbuvocholes', $dataReiksme, isset($this->session->userdata['step_4']['arbuvocholes']) ? $this->session->userdata['step_4']['arbuvocholes'] : '', $attr);
        echo br(1);
        
        /* Chol kiekis */
        echo form_label('Jei taip pažymėkite:', 'chol');
        $pasirinkimai = isset($this->session->userdata['step_4']['svarbucholinfoid']) ? $this->session->userdata['step_4']['svarbucholinfoid'] : FALSE;
        foreach ($resultSvrbChol as $row) {
            $check = FALSE;
            if($pasirinkimai){
                foreach ($pasirinkimai as $row_2){
                    if($row->id == $row_2){
                       $check = TRUE; 
                    }
                }
            }
            ?>
            <div class="checkbox">
                <label>
                <?php 
                $dataChol = array(
                    'name'        => 'chol[]',
                    'id'          => 'chol[]',
                    'class'       => 'chol',
                    'value'       => $row->id,
                    'checked'     => $check,
                    );
                echo form_checkbox($dataChol);    
                echo $row->rodiklis; ?>
                </label>
            </div>
        <?php
        }
        echo br(1);
        
        /* Chol Korekcija */
        echo form_label('Ar cholesterolis buvo koreguojamas kuriuo nors iš šių metodų?', 'chol_kor');
        $pasirinkimai = isset($this->session->userdata['step_4']['metodonr']) ? $this->session->userdata['step_4']['metodonr'] : FALSE;
        foreach ($resultCholKor as $row) {
            $check = FALSE;
            if($pasirinkimai){
                foreach ($pasirinkimai as $row_2){
                    if($row->id == $row_2){
                       $check = TRUE; 
                    }
                } 
            }
            ?>
            <div class="checkbox">
                <label>
                    <?php
                    echo form_checkbox('chol_kor[]', $row->id, $check); 
                    echo $row->metodas; ?>
                </label>
            </div>
        <?php
        }
        echo br(1);
        
         /* Fizinis aktyvumas*/
        echo form_label('Ar esate pakankamai fiziškai aktyvus?', 'fizinisaktyvum');
        $dataFizAkt = array(
            '0' => '-'
        );
        foreach ($resultFizAkt as $row) {
            $dataFizAkt[$row->id] = $row->aprasas;
        }
        echo form_dropdown('fizinisaktyvum', $dataFizAkt, isset($this->session->userdata['step_4']['fizinisaktyvum']) ? $this->session->userdata['step_4']['fizinisaktyvum'] : '', 'class="form-control"');
        echo br(1);
        
        /* Kraujospudis */
        /* Sistolinis  */
        echo "<h4>Koks yra jusu kraujospudis?</h4>";
        echo form_label('Sistolinis ≥ 130 mmHg', 'akssistnenorm');
        $dataSis = array(
            '-1' => '-'
        );
        foreach ($resultReiksme as $row) {
            $dataSis[$row->id] = $row->reiksme;
        }
        echo form_dropdown('akssistnenorm', $dataSis, isset($this->session->userdata['step_4']['akssistnenorm']) ? $this->session->userdata['step_4']['akssistnenorm'] : '', 'class="form-control"');
        echo br(1);
        
        /* Diastolinis    */
        echo form_label('Diastolinis ≥ 85 mmHg', 'aksdiastnenm');
        $dataDia = array(
            '-1' => '-'
        );
        foreach ($resultReiksme as $row) {
            $dataDia[$row->id] = $row->reiksme;
        }
        echo form_dropdown('aksdiastnenm', $dataDia, isset($this->session->userdata['step_4']['aksdiastnenm']) ? $this->session->userdata['step_4']['aksdiastnenm'] : '', 'class="form-control"');
        echo br(1);
        
        /* Kvep Tak Lig Daz   */
        echo form_label('Kaip dažnai sergate kvėpavimo takų ligomis?', 'kveptkligdaz');
        $dataKvpTakLig = array(
            '0' => '-'
        );
        foreach ($resultKvpTakLig as $row) {
            $dataKvpTakLig[$row->id] = $row->aprasas;
        }
        echo form_dropdown('kveptkligdaz', $dataKvpTakLig, isset($this->session->userdata['step_4']['kveptkligdaz']) ? $this->session->userdata['step_4']['kveptkligdaz'] : 0, 'class="form-control"');
        echo br(1);
        
        
        
        echo anchor("general/aboutPatients/createNewAboutPatient3", "Grįžti atgal",
                array(  'name' => 'backCreateNewAboutPatien4',
                        'id' => 'backCreateNewAboutPatient4',
			'class'=> 'btn btn btn-default'));
        
        echo nbs(3); 
        $buttonSubmit2 = array(
            'name' => 'mysubmit2',
            'id' => 'submit2',
            'class' => 'btn btn btn-primary',
            'value' => 'Testi'
        );
        echo form_submit($buttonSubmit2);
        echo nbs(3);
        
//        $buttonSubmit_2 = array(
//        'name' => 'mysubmit_2',
//        'id' => 'submit_2',
//        'value' => 'Baigti',
//        'class'=> 'btn btn btn-success',
//        'formaction' => base_url('general/aboutPatients/createNewAboutPatient4_finish'),
//         );
//        echo form_submit($buttonSubmit_2);
//        echo nbs(3);
        
        
        
//        var_dump($this->session->userdata['step_3']); 
//        var_dump($this->session->userdata['dataBirthInformation']); 
        
        
        echo form_close();
        ?>

    </div>
                        <!-- /.col-lg-12 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
