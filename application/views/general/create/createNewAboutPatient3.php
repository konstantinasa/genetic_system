<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo lang('create_patient_heading'); ?> 3 žingsnis</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">

        <?php
    // --- Progres bar -------
    $done = array(
        'first' => 'progtrckr-done',
        'second' => 'progtrckr-done',
        'third' => 'progtrckr-done',
        'fourth' => 'progtrckr-todo',
        'fifth' => 'progtrckr-todo'
    );
    $this->load->view('include/progress_bar', $done);
    echo br(1);
    
        echo validation_errors();
        $form_attr = ['onsubmit' => 'send_arr()'];
        echo form_open('/general/aboutPatients/createNewAboutPatient3', $form_attr);
        
        echo anchor("general/aboutPatients/createNewAboutPatient2", "Grįžti atgal",
        array(  'name' => 'backSeeAboutPatient',
                'id' => 'backSeeAboutPatient',
                'class'=> 'btn btn-default'));
        echo nbs(3);

        $buttonSubmit = array(
        'name' => 'mysubmit',
        'id' => 'submit',
        'value' => 'Testi',
        'class'=> 'btn btn btn-primary'
         );
        echo form_submit($buttonSubmit);
        echo nbs(3);

        ?>
        <div class="pull-right">
            <?php
            echo anchor("general/aboutPatients/clear_form", "Išvalyti anketą",
                    array('id' => 'clear_form',
                        'name' => 'clear_form',
                        'type' => 'button',
                        'width' => '200',
                        'height' => '200',
                        'class'=> 'btn btn-danger btn-xs'));  
           ?>
        </div>
        <?php  
       
        echo br(4);
        
        /* Maisto Pasirinkimai */
        echo form_label('Pagrindinis kriterijus, pagal kurį Jūs renkatės maisto produktus:', 'food');
        echo br(1);
        ?>
        <!--<div class="span2 col-md-7">-->
        <div class="food_dropdown_checkbox"></div>
        <input type="hidden" name="food" id="food" value="">
        
        <?php
        $pasirinkimai = isset($this->session->userdata['step_3']['pasirinkimasid']) ? $this->session->userdata['step_3']['pasirinkimasid'] : FALSE;
        foreach ($resultFood as $row) {
            $check = FALSE;
            if($pasirinkimai){
                foreach ($pasirinkimai as $row_2){
                    if($row->id == $row_2){
                       $check = TRUE; 
                    }
                } 
            }
            $row->isChecked = $check;
            }
            echo br(3);
          /* END maisto pasirinkimai */
         
          /* Ligos */
//        echo form_label('Šiuo metu sergate kokiomis nors ligomis?', 'srg');
//        echo br(1);
//        ?>
        <div class="srg_dropdown_checkbox"></div>
        <input type="hidden" name="srg" id="srg" value="">
        
        <?php
//        $pasirinkimai = isset($this->session->userdata['step_3']['srginfoid']) ? $this->session->userdata['step_3']['srginfoid'] : FALSE;
//        foreach ($resultSrg as $row) {
//            $check = FALSE;
//            if($pasirinkimai){
//                foreach ($pasirinkimai as $row_2){
//                    if($row->id == $row_2){
//                       $check = TRUE; 
//                    }
//                }
//            }
//            $row->isChecked = $check;
//            }
//            echo br(3);
          /* END Ligos */
            
            
            
            /* Ligos 2K */
        echo form_label('Šiuo metu sergate kokiomis nors ligomis?', 'ligos');
        echo br(1);
        ?>
        <div class="ligos_dropdown_checkbox"></div>
        <input type="hidden" name="ligos" id="ligos" value="">
        
        <?php
        $pasirinkimai = isset($this->session->userdata['step_3']['ligosid']) ? $this->session->userdata['step_3']['ligosid'] : FALSE;
        foreach ($resultLigos as $row) {
            $check = FALSE;
            if($pasirinkimai){
                foreach ($pasirinkimai as $row_2){
                    if($row->id == $row_2){
                       $check = TRUE; 
                    }
                }
            }
            $row->isChecked = $check;
            }
            echo br(3);
          /* END Ligos 2K */

            
            /* Alergenai */
        echo form_label('Kam esate alergiskas?', 'alerg');
        echo br(1);
        ?>
        <div class="alerg_dropdown_checkbox"></div>
        <input type="hidden" name="alerg" id="alerg" value="">
        <?php
        $pasirinkimai = isset($this->session->userdata['step_3']['alergiskaskamid']) ? $this->session->userdata['step_3']['alergiskaskamid'] : FALSE;
        foreach ($resultAllergy as $row) {
            $check = FALSE;
            if($pasirinkimai){
                foreach ($pasirinkimai as $row_2){
                    if($row->id == $row_2){
                       $check = TRUE; 
                    }
                }
            }
            $row->isChecked = $check;
            }
            echo br(3);
          /* END  Alergenai */ 
            
        
             /* Onkologines */
        echo form_label('Ar sergate vėžiu?', 'srg_vez');
        echo br(1);
        ?>
        <div class="srg_vez_dropdown_checkbox"></div>
        <input type="hidden" name="srg_vez" id="srg_vez" value="">
<!--        </div>-->
        <?php
        $pasirinkimai = isset($this->session->userdata['step_3']['srgveziukokiuid']) ? $this->session->userdata['step_3']['srgveziukokiuid'] : FALSE;
        foreach ($resultSrgVez as $row) {
            $check = FALSE;
            if($pasirinkimai){
                foreach ($pasirinkimai as $row_2){
                    if($row->id == $row_2){
                       $check = TRUE; 
                    }
                }
            }
            $row->isChecked = $check;
            }
            echo br(5);
          /* END  Onkologines */
            
        
        echo anchor("general/aboutPatients/createNewAboutPatient2", "Grįžti atgal",
                array(  'name' => 'backCreateNewAboutPatient3',
                        'id' =>   'backCreateNewAboutPatient3',
			'class'=> 'btn btn-default'));
        
        echo nbs(3); 
        $buttonSubmit2 = array(
            'name' => 'mysubmit2',
            'id' => 'submit2',
            'class' => 'btn btn btn-primary',
            'value' => 'Testi',
            'onclick' => 'send_arr()'
        );
        echo form_submit($buttonSubmit2);
           
        echo br(1);
        
//        var_dump($this->session->userdata('step_3'));
       
      
        echo form_close();
        ?>

    </div>
                        <!-- /.col-lg-12 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->


