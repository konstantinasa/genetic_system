<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"> Apie pacientą</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
<!--            <div class="panel panel-default">
                <div class="panel-body">-->
                    <div class="row">
                        <div class="col-lg-12">
        
        <div class="panel panel-default">
                        <div class="panel-heading">
                            Visa informaciją apie pacientą
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                <div class="contentCreate">
                        <?php echo anchor("general/aboutPatients/createNewAboutPatient", "Sukurti naują pacientą",
                                array(
                                        'id' => 'createNewAboutPatient',
                                        'name' => 'createNewAboutPatient',
                                        'class' => 'btn btn-success',
                                        'type' => 'button'
                                    ));
                        echo br(2);
                        ?>
        </div><!--contentCreate-->
                                
 <p><center><?php echo $links; ?></center></p>                                
									
<div id="exTab2" class="container">

<!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
      <li class="active">
          <a href="#info_1" role="tab" data-toggle="tab">
               Bendra 1 <icon class="fa fa-user"></icon>
          </a>
      </li>
      <li><a href="#info_2" role="tab" data-toggle="tab">
           Bendra 2 <i class="fa fa-home"></i>
          </a>
      </li>
      <li>
          <a href="#info_3" role="tab" data-toggle="tab">
               Mitybos įpročiai
          </a>
      </li>
      <li>
          <a href="#info_4" role="tab" data-toggle="tab">
               Fizinė veikla
          </a>
      </li>
      <li>
          <a href="#info_5" role="tab" data-toggle="tab">
               Viršsvoris
          </a>
      </li>
      <li>
          <a href="#info_6" role="tab" data-toggle="tab">
               Tiriamojo sveikata
          </a>
      </li>
      <li>
          <a href="#info_7" role="tab" data-toggle="tab">
               Tiriamojo sveikata 2
          </a>
      </li>
      <li>
          <a href="#info_8" role="tab" data-toggle="tab">
               Tiriamojo sveikata 3
          </a>
      </li>
    </ul>

<!-- Tab panes -->
    <div class="tab-content">
      <div class="tab-pane fade active in" id="info_1">
                    <div class="dataTable_wrapper">
			<table class="table table-striped table-bordered table-hover" id="dataTables-example"  style="max-width: 90%;">
				<thead>
					<tr>
                                                <td class="tableBoldGreen">ID</td>
						<td class="tableBoldGreen">Paciento kodas</td>
                                                <td class="tableBoldGreen">Tyrimo Data</td>	
                                                <td class="tableBoldGreen">Lytis</td>
                                                <td class="tableBoldGreen">Gimimo Data</td>
						<td class="tableBoldGreen">Ugis</td>
						<td class="tableBoldGreen">Svoris</td>
                                                <td class="tableBoldGreen">KMI</td>
                                                <td class="tableBoldGreen">Tautybė</td>
					</tr>
				</thead>
				<tbody>
				<?php
				if ($resultAllPatient) {
					foreach ($resultAllPatient as $row) {
						echo "<tr>";
						echo "<td>" . $row->id . "</td>";
						echo "<td>" . $row->pacientas . "</td>";
                                                echo "<td>" . $row->tdata . "</td>";
                                                
                                                if ($row->lytis == '4') {
							echo "<td>" . " Moteris " . "</td>";
						} elseif ($row->lytis == '3') {
							echo "<td>" . " Vyras " . "</td>";
						} else {
                                                        echo "<td>" . " - " . "</td>";
                                                }
						echo "<td>" . $row->gdata . "</td>";
						echo "<td>" . $row->ugis . "</td>";
						echo "<td>" . $row->svoris . "</td>";
                                                if(($row->ugis != "") AND ($row->svoris != "")){
                                                    $varSvoris = $row->svoris;
                                                    $varUgis = $row->ugis;
                                                     echo "<td>" . round(($varSvoris / (($varUgis / 100) * ($varUgis / 100))), 3) . "</td>";
                                                }else{
                                                    echo "<td>" . ' - ' . "</td>";
                                                }
                                                echo "<td>" . $row->tautybe . "</td>";
						echo "</tr>";
					}
				} else {
					?>
					<div class="infoMessage">
						<?php
						echo "Informacijos nėra";
						?>
					</div>
					<!--infoMessage-->
					<?php
				}
				?>
				</tbody>
			</table>
                    </div>
		</div>
        
		 <div class="tab-pane fade" id="info_2">
			<table class="table table-striped table-bordered table-hover" id="dataTables-example"  style="max-width: 90%;">
                            <div class="dataTable_wrapper">
                                <thead>
					<tr>
						
                                            <td class="tableBoldGreen">Paciento kodas</td>
                                            <td class="tableBoldGreen">Išsilavinimas</td>
                                            <td class="tableBoldGreen">Gyvenviete</td>
                                            <td class="tableBoldGreen">Savivaldybe</td>
                                            <td class="tableBoldGreen">Apskritis</td>
                                            <td class="tableBoldGreen">Užimtumas</td>
                                            <td class="tableBoldGreen">Gimimo Svoris</td>
                                            <td class="tableBoldGreen">Gimimo Ugis</td>		
					</tr>
				</thead>
				<tbody>
				<?php
				if ($resultAllPatient) {
					foreach ($resultAllPatient as $row) {
						echo "<tr>";
						echo "<td>" . $row->pacientas . "</td>";
                                                echo "<td>" . $row->issilavinimas . "</td>";
						echo "<td>" . $row->miestasarkaimas . "</td>";
						echo "<td>" . $row->gyvsavivaldybe . "</td>";
						echo "<td>" . $row->gyvapskritis . "</td>";
						echo "<td>" . $row->uzimtumas . "</td>";
						echo "<td>" . $row->gimimosvoris . "</td>";
						echo "<td>" . $row->gimimougis . "</td>";
						echo "</tr>";
					}
				} else {
					?>
					<div class="infoMessage">
						<?php
						echo "Informacijos nėra";
						?>
					</div>
					<!--infoMessage-->
					<?php
				}
				?>
				</tbody>
                           </div>
			</table>
		</div>
		 <div class="tab-pane fade" id="info_3">
			<table class="table table-striped table-bordered table-hover" id="dataTables-example"  style="max-width: 90%;">
                            <div class="dataTable_wrapper">
                                <thead>
					<tr>
                                                <td class="tableBoldGreen">Paciento kodas</td>
                                                <td class="tableBoldGreen">Maista renkasi pagal šiuos kriterijus</td>    
					</tr>
				</thead>
				<tbody>
				<?php
				if ($resultAllPatient) {
					foreach ($resultAllPatient as $row) {
						echo "<tr>";
						echo "<td>" . $row->pacientas . "</td>";
                                                echo "<td>" . '<li>' . str_replace(',', ",</br><li>", str_replace('"', "", trim($row->maisto_pasirinkimai, '{}",'))) . "</td>";
						echo "</tr>";
					}
				} else {
					?>
					<div class="infoMessage">
						<?php
						echo "Informacijos nėra";
						?>
					</div>
					<!--infoMessage-->
					<?php
				}
				?>
				</tbody>
                            </div>
			</table>
		</div>
        
		<div class="tab-pane fade" id="info_4">
			<table class="table table-striped table-bordered table-hover" id="dataTables-example"  style="max-width: 90%;">
                            <div class="dataTable_wrapper">
                                <thead>
					<tr>
						<td class="tableBoldGreen">Paciento kodas</td>
                                                <td class="col-md-2">Fizinis aktyvumas</td>	
					</tr>
				</thead>
				<tbody>
				<?php
				if ($resultAllPatient) {
					foreach ($resultAllPatient as $row) {
						echo "<tr>";
						echo "<td>" . $row->pacientas . "</td>";
                                                echo "<td>" . $row->fiz_akt . "</td>";
                                                echo "</tr>";
					}
				} else {
					?>
					<div class="infoMessage">
						<?php
						echo "Informacijos nėra";
						?>
					</div>
					<!--infoMessage-->
					<?php
				}
				?>
				</tbody>
                            </div>
			</table>
		</div>
                
                <div class="tab-pane fade" id="info_5">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example"  style="max-width: 90%;">
                            <div class="dataTable_wrapper">
                                <thead>
					<tr>
						<td class="tableBoldGreen">Paciento kodas</td>
                                                <td class="tableBoldGreen">Liemens apimtis</td>
                                                <td class="tableBoldGreen">Svorio kitimas</td>
						<td class="tableBoldGreen">KG</td>
						<td class="tableBoldGreen">Per mėn</td>
						<td class="tableBoldGreen">Svorio kitimas</td>
						<td class="tableBoldGreen">KG</td>
						<td class="tableBoldGreen">Per mėn</td>        
					</tr>
				</thead>
				<tbody>
				<?php
				if ($resultAllPatient) {
					foreach ($resultAllPatient as $row) {
					echo "<tr>";
					echo "<td>" . $row->pacientas . "</td>";
                                        echo "<td>" . $row->tiriamliemapim . "</td>";
                                        echo "<td>" . $row->svor_kit . "</td>";
                                        echo "<td>" . $row->svor_kg . "</td>";
                                        echo "<td>" . $row->svor_men . "</td>";
                                        echo "<td>" . $row->svor_kit_2 . "</td>";
                                        echo "<td>" . $row->svor_kg_2 . "</td>";
                                        echo "<td>" . $row->svor_men_2 . "</td>";
					echo "</tr>";
					}
				} else {
					?>
					<div class="infoMessage">
						<?php
						echo "Informacijos nėra";
						?>
					</div>
					<!--infoMessage-->
					<?php
				}
				?>
				</tbody>
                            </div>
			</table> 
                </div>
        
        
		<div class="tab-pane fade" id="info_6">
			<table class="table table-striped table-bordered table-hover" id="dataTables-example"  style="max-width: 90%;">
                            <div class="dataTable_wrapper">
                                <thead>
					<tr> 
                                            <td class="tableBoldGreen">Paciento kodas</td>
                                            <td class="col-md-3">Ligos</td>
                                            <td class="col-md-3">Kokybė</td>
                                            <td class="col-md-3">Cholisterolio kiekis</td>
                                            <td class="col-md-3">Cholisterolio kiekio koregavimo metodai</td>
					</tr>
				</thead>
				<tbody>
				<?php
				if ($resultAllPatient) {
					foreach ($resultAllPatient as $row) {
					echo "<tr>";
					echo "<td>" . $row->pacientas . "</td>";
					echo "<td>" . '<li>' . str_replace(',', ",</br><li>", str_replace('"', "", trim($row->ligos, '{}",'))) . "</td>";
                                        echo "<td>" . '<li>' . str_replace(',', ",</br><li>", str_replace('"', "", trim($row->kokybe, '{}",'))) . "</td>";
                                        echo "<td>" . '<li>' . str_replace('ne','<strong>NE</strong>',str_replace('taip','<strong>TAIP</strong>',str_replace('","', ",</br><li>", trim($row->padid_chol_kiekis, '{}",')))) . "</td>";
					echo "<td>" . '<li>' . str_replace(',', ",</br><li>", str_replace('"', "", trim($row->chol_korekcijos_metodas, '{}",'))) . "</td>";
					echo "</tr>";
					}
				} else {
					?>
					<div class="infoMessage">
						<?php
						echo "Informacijos nėra";
						?>
					</div>
					<!--infoMessage-->
					<?php
				}
				?>
				</tbody>
                            </div>
			</table>
		</div>
        
                <div class="tab-pane fade" id="info_7">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example"  style="max-width: 90%;">
                                    <div class="dataTable_wrapper">
                                        <thead>
                                                <tr>   
                                                    <td class="tableBoldGreen">Paciento kodas</td>
                                                    <td class="col-md-2">Kraujo spaudimas</td>
                                                    <td class="tableBoldGreen">Alergiškumas</td>
                                                    <td class="tableBoldGreen">Onkologinės ligos</td> 
                                                    <td class="col-md-1">Kvepavimo Takų Ligų Dažnumas</td>   
                                                </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        if ($resultAllPatient) {
                                                foreach ($resultAllPatient as $row) {
                                                echo "<tr>";
                                                echo "<td>" . $row->pacientas . "</td>";
						echo "<td>" . 'Sistolinis: ' . '<strong>' . $row->aks_sist_ne_norm . '</strong>' . '</br>Diastolinis: ' . '<strong>' . $row->aks_diast_nenm . '</strong>' . "</td>";
                                                echo "<td>" . '<li>' . str_replace(',', ",</br><li>", str_replace('"', "", trim($row->alergiskas, '{}",'))) . "</td>";
                                                echo "<td>" . '<li>' . str_replace(',', ",</br><li>", str_replace('"', "", trim($row->srgvez, '{}",'))) . "</td>";
                                                echo "<td>" . $row->kvp_tk_lig_daz . "</td>";
                                                echo "</tr>";
                                                }
                                        } else {
                                                ?>
                                                <div class="infoMessage">
                                                        <?php
                                                        echo "Informacijos nėra";
                                                        ?>
                                                </div>
                                                <!--infoMessage-->
                                                <?php
                                        }
                                        ?>
                                        </tbody>
                                    </div>
                                </table>
                 </div>
        
        
                 <div class="tab-pane fade" id="info_8">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example"  style="max-width: 90%;">
                                    <div class="dataTable_wrapper">
                                        <thead>
                                                <tr>   
                                                    <td class="tableBoldGreen">Paciento kodas</td>
                                                    <td class="tableBoldGreen">Biocheminiai rodikliai</td>
                                                    <!--<td class="tableBoldGreen">Veiksmai</td>-->   
                                                </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        if ($resultAllPatient) {
                                                foreach ($resultAllPatient as $row) {
                                                echo "<tr>";
                                                echo "<td>" . $row->pacientas . "</td>";
                                                echo "<td>" . str_replace(',', "</br>", str_replace('"', "", trim($row->sveikatos_rodikliai, '{}",'))) . "</td>";
//                                                echo "<td>" . anchor("addbiorod/add_bio_rod/$row->id", "Pridėti</br>biocheminius</br>rodiklius", array('class' => "btn btn-outline btn-success")) . "</td>";
                                                echo "</tr>";
                                                }
                                        } else {
                                                ?>
                                                <div class="infoMessage">
                                                        <?php
                                                        echo "Informacijos nėra";
                                                        ?>
                                                </div>
                                                <!--infoMessage-->
                                                <?php
                                        }
                                        ?>
                                        </tbody>
                                    </div>
                                </table>
                 </div>
	</div>
</div>
        <p><center><?php echo $links; ?></center></p>
        
        
<!--        <p><?php // echo var_dump($this->session->all_userdata()); ?></p>
         <p><?php // echo var_dump($this->session->userdata('username')); ?></p>-->
         <!--<p><?php // echo var_dump($resultAllPatient); ?></p>-->
         <!--<p><?php // echo var_dump($resultAllPatient['0']->padid_chol_kiekis); ?></p>-->
         <!------------------------------------------------------>
         


         
         <!------- 2nd------------------------------------->
         
         
         
         
         <!--------------------------------------------------->
   </div>
</div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                        <!-- /.col-lg-12 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
