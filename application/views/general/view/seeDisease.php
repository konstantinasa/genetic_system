<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"> Ligų sąrašas</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
<!--            <div class="panel panel-default">
                <div class="panel-body">-->
                    <div class="row">
                        <div class="col-lg-12">
        <div class="contentCreate">
                <?php echo anchor("general/diseases/createDisease", "Sukurti naują",
                    array('id' => 'createDisease',
                        'name' => 'createDisease',
                        'type' => 'button',
                        'width' => '200',
                        'height' => '200',
			'class'=> 'btn btn-success'));
                echo br(1);
                ?>
            </br>
        </div><!--contentCreate-->
        <div class="panel panel-default">
                        <div class="panel-heading">
                            <?php echo lang('list_diseases'); ?>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                <td class="tableBoldGreen"> ID</td>
                <td class="tableBoldGreen"> Ligos pavadinimas</td>
            </tr>
            </thead>
            <?php
            if ($resultDisease) {
                foreach ($resultDisease as $row) {
                    echo "<tr>";
                    echo "<td>" . $row->id . "</td>";
                    echo "<td>" . $row->pavadinimas . "</td>";
                    echo "<td>" . anchor("general/diseases/edit_disease/$row->id", "Redaguoti", array('class' => 'btn btn-outline btn-success','onclick' => "return confirm('Ar tikrai norite redaguoti?')")) . "</td>";
                    echo "<td>" . anchor("general/diseases/delete_disease/$row->id", "Trinti", array('class' => 'btn btn-outline btn-danger','onclick' => "return confirm('Ar tikrai norite ištrinti?')")) . "</td>";
					echo "</tr>";
                }
            } else {
                ?>
                <div class="infoMessage">
                    <?php
                    echo "Informacijos nėra";
                    ?>
                </div>
                <!--infoMessage-->
            <?php
            }
            ?>
        </table>
        <p><?php echo $links; ?></p>
   </div>
</div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                        <!-- /.col-lg-12 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
