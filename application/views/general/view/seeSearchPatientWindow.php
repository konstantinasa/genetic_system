﻿<div id="page-wrapper">
	<div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Paieškos puslapis</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!--contentTop-->
    <?php 
		$options=array(
		'id'=>'searchform'
		);
		echo form_open("general/search/searchPlaceholder",$options); 
	?>			
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
	
	<script>
		var Typerefrence=[];
		var Relationrefrence=[];
		$(document).ready(function () {
			var uerel='<?php echo base_url()."general/search/retrieveTags/";?>';
			$.ajax(
				{
					type:'POST',
					url:uerel+1,
					success: function(data){
						//construct reference sheet
						var arr=data.split(";");
						for (i=0;i<arr.length;i++) 
						{
							var q=arr[i].split(":");
							Typerefrence[q[0]]=q[1];
						}
					}
				}
			);
			$.ajax(
				{
					type:'POST',
					url:uerel+2,
					success: function(data){
						//construct reference sheet
						var arr=data.split(";");
						for (i=0;i<arr.length;i++) 
						{
							var q=arr[i].split(":");
							Relationrefrence[q[0]]=q[1];
						}
					}
				}
			);
		});
		
		function clickening(aa) {
			var uerel='<?php echo base_url()."general/search/retrieveTags/";?>';
			uerel=uerel+1;
			var val=document.getElementById(aa).value;
			var ktype=Typerefrence[val];
			var m2n=Relationrefrence[val];
			//alert("type:"+ktype+" relation "+m2n);
			var idtc="reiksme"+aa.substring(aa.length-1,aa.length); //id we change, if it was search0 element then it'll be reiksme0
			//get father
			father=document.getElementById(idtc).parentNode
			//child 1
			label=document.createElement("label");
			label.innerHTML="Reikšmė";
			label.for=idtc;
			
			father.innerHTML='';
			
			if ((ktype!=0) && (m2n=="")) 
			{
				//child 2
				aa=document.createElement("input");
				aa.id=idtc;
				aa.name=idtc;
				aa.className ="form-control";
				
				father.appendChild(label);
				father.appendChild(aa);
			} else if (ktype==0) {
				aa=document.createElement("div");
				aa.id=idtc;
				aa.name=idtc;
				father.appendChild(aa);
			} else if ((ktype!=0) && (m2n!="")) {
				uerel='<?php echo base_url()."general/search/createDropDown/";?>';
				$.ajax(
					{
						type:'POST',
						url:uerel+m2n+"/"+idtc,
						success: function(data){
							aa=document.createElement("div");
							father.innerHTML=data;
							$("#"+idtc).select2({width: '100%'});
						}							
					}
				);
			} 
			
			
			
		}
		
		function loadXMLDoc()
		{
			var xmlhttp;
			var itteration=0;
			
			itteration=document.getElementById("itterator").innerHTML;
			itteration=parseInt(itteration)+1;
			document.getElementById("itterator").innerHTML=itteration;
			document.getElementById('rowcount').value=itteration;
			
			defstate="http://networkinterstateco.com/wp-content/uploads/plus.png";
			lodstate="http://thinkfuture.com/wp-content/uploads/2013/10/loading_spinner.gif";
			document.getElementById("addmore").src=lodstate;
			
			uerel='<?php echo base_url()."general/search/appendSearchbar/";?>';
			$.ajax(
				{
					type:'POST',
					url:uerel+itteration,
					success: function(data){
						//save search field values
						var searches=[];
						var values=[];
						for(i=0; i<itteration; i+=1) {
							idd="search"+i;
							searches[i]=document.getElementById(idd).value;
							idd="reiksme"+i;
							values[i]=document.getElementById(idd).value;
						}
						
						//change number of fiels
						//inscript="#reiksme"+itteration;
						//script="$(document).ready(function () {$("inscript").select2({width: '100%'});});"
						
						var div=document.createElement("div");
						div.className="row";
						div.innerHTML=data;
						
						document.getElementById("searchelements").appendChild(div);
						
						document.getElementById("addmore").src=defstate;
						//change ids and names
						newsearch=document.getElementById("search");
						newsearch.id="search"+itteration;
						newsearch.name="search"+itteration;
						$("#"+newsearch.id).select2({width: '100%'});
						
						newvalue=document.getElementById("reiksme");
						newvalue.id="reiksme"+itteration;
						newvalue.name="reiksme"+itteration;
						$("#"+newvalue.id).select2({width: '100%'});
						
						//reasign search field values
						for(i=0; i<itteration; i+=1) {
							idd="search"+i;
							document.getElementById(idd).value=searches[i];
							idd="reiksme"+i;
							document.getElementById(idd).value=values[i];
						}
					}
				}
			);
		}
	</script>
		
		<div id="itterator" hidden>0</div>
		
		<div id="searchelements">
			<input hidden name="rowcount" id="rowcount" value="0" />
			<div class="row">
				<div class="form-group col-md-6">
					<?php
					echo form_label('Paieška:', 'search0');
					$TagsQuestions=array('0' => '');
					foreach ($qtags as $tag) {
						$TagsQuestions[$tag->id]=$tag->shown;
					}
					$options='id="search0" class="form-control" onchange="clickening(id)"';
					echo form_dropdown('search0', $TagsQuestions,0, $options);
					?>
					<script>
					$(document).ready(function () {
						$("#search0").select2({width: '100%'});
					});
					</script>
				</div>
				
				<div style="margin-top:-5px;" class="col-md-4">
					<?php
					$dataNationality = array(
						'0' => '-'
					);
					
					echo form_label('Reikšmė:', 'reiksme0');
					$options='id="reiksme0" class="form-control"';
					echo form_dropdown('reiksme0', $dataNationality,0, $options);
					?>
					
					<script>
					$(document).ready(function () {
						$("#reiksme0").select2({width: '100%'});
					});
					</script>
					
				</div>
				<div>
					<button style="margin-left:5px; margin-top:-10px;" type="submit" class="btn btn-primary btn-sm">Ieškoti</button>
					<input for="" type="image" onclick="loadXMLDoc(); return false;" style="width:25px; height:25px; margin-top:-15px;" id="addmore" src="http://networkinterstateco.com/wp-content/uploads/plus.png" />
				</div>
			</div>
			<div id="further"></div>
		</div>
	<?php 
	echo form_close(); 
	$resultAllPatient=$patients;
	if ($infoquery) 
	{
		$infoquery=" pagal užklaus: ".$infoquery;
	}
	?>
		 
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">		
							<div class="panel panel-default">
								<div class="panel-heading">
									<?php 
										echo "<p id='myp'>Paieskos rezultatai".$infoquery."</p>";
									?>
								</div>
											<!-- /.panel-heading -->
								<div class="panel-body">
									<div class="dataTable_wrapper">								
										<div id="exTab2" class="container">	
											<ul class="nav nav-tabs">
												<li class="active">
													<a  href="#1" data-toggle="tab">Pagrindinis</a>
												</li>
												<li>
													<a  href="#2" data-toggle="tab">Lokaciniai</a>
												</li>
												<li>
													<a  href="#3" data-toggle="tab">Ligos</a>
												</li>
												<li>
													<a  href="#4" data-toggle="tab">Alergiskumas</a>
												</li>
												<li>
													<a  href="#5" data-toggle="tab">Svorio pokyciai</a>
												</li>
											</ul>
											<div class="tab-content ">
													<div class="tab-pane active" id="1">
														<table class="table table-striped table-bordered table-hover" id="dataTables-example"  style="max-width: 90%;">
															<thead>
																<tr>
																	<td class="tableBoldGreen">PATIENT_ID</td>
																	<td class="tableBoldGreen">PATIENT</td>
																	<td class="tableBoldGreen">Tyrimo Data</td>
																	<td class="tableBoldGreen">Ugis</td>
																	<td class="tableBoldGreen">Svoris</td>
																	<td class="tableBoldGreen">KMI</td>
																	<td class="tableBoldGreen">Tiriamojo Liemens<br> Apimtis</td>
																	<td class="tableBoldGreen">Lytis</td>
																</tr>
															</thead>
															<tbody>
															<?php
															if ($resultAllPatient) {
																foreach ($resultAllPatient as $row) {
																	echo "<tr>";
																	echo "<td>" . $row->id . "</td>";
																	echo "<td>" . $row->pacientas . "</td>";
																	echo "<td>" . $row->tdata . "</td>";
																	echo "<td>" . $row->ugis . "</td>";
																	echo "<td>" . $row->svoris . "</td>";
																							if(($row->ugis != "") AND ($row->svoris != "")){
																								$varSvoris = $row->svoris;
																								$varUgis = $row->ugis;
																								 echo "<td>" . ($varSvoris / (($varUgis / 100) * ($varUgis / 100))) . "</td>";
																							}else{
																								echo "<td>" . ' ' . "</td>";
																							}
																	echo "<td>" . $row->tiriamliemapim . "</td>";
																	if ($row->lytis == '4') {
																		echo "<td>" . " Moteris " . "</td>";
																	} else {
																		echo "<td>" . " Vyras " . "</td>";
																	}
																	echo "</tr>";
																}
															} else {
																?>
																<div class="infoMessage">
																	<?php
																	echo "Informacijos nėra";
																	?>
																</div>
																<!--infoMessage-->
																<?php
															}
															?>
															</tbody>
														</table>

													</div>
													<div class="tab-pane" id="2">
														<table class="table table-striped table-bordered table-hover" id="dataTables-example"  style="max-width: 90%;">
															<thead>
																<tr>
																	<td class="tableBoldGreen">PATIENT_ID</td>
																	<td class="tableBoldGreen">Gimimo Data</td>
																	<td class="tableBoldGreen">Gimimo Svoris</td>
																	<td class="tableBoldGreen">Gymimo Ugis</td>
																	<td class="tableBoldGreen">Gyvenviete</td>
																	<td class="tableBoldGreen">Savivaldybe</td>
																	<td class="tableBoldGreen">Apskritis</td>
																	<td class="tableBoldGreen">Užimtumas</td>
																	<td class="tableBoldGreen">Išsilavinimas</td>
																	<td class="tableBoldGreen">Tautybė</td>
																	<td class="tableBoldGreen">Anketos versija</td>
																</tr>
															</thead>
															<tbody>
															<?php
															if ($resultAllPatient) {
																foreach ($resultAllPatient as $row) {
																	echo "<tr>";
																	echo "<td>" . $row->id . "</td>";
																	echo "<td>" . $row->gdata . "</td>";
																	echo "<td>" . $row->gimimosvoris . "</td>";
																	echo "<td>" . $row->gimimougis . "</td>";
																	echo "<td>" . $row->miestasarkaimas . "</td>";
																	echo "<td>" . $row->gyvsavivaldybe . "</td>";
																	echo "<td>" . $row->gyvapskritis . "</td>";
																	echo "<td>" . $row->uzimtumas . "</td>";
																	echo "<td>" . $row->issilavinimas . "</td>";
																	echo "<td>" . $row->tautybe . "</td>";
																	echo "<td>" . $row->anketosversija . "</td>";
																	echo "</tr>";
																}
															} else {
																?>
																<div class="infoMessage">
																	<?php
																	echo "Informacijos nėra";
																	?>
																</div>
																<!--infoMessage-->
																<?php
															}
															?>
															</tbody>
														</table>
													</div>
													<div class="tab-pane" id="3">
														<table class="table table-striped table-bordered table-hover" id="dataTables-example"  style="max-width: 90%;">
															<thead>
																<tr>
																	<td class="tableBoldGreen">PATIENT_ID</td>
																	<td class="tableBoldGreen">Projektas</td>
																	<td class="tableBoldGreen">Fizinis aktyvumas</td>
																	<td class="tableBoldGreen">Aks_sist_ne_norm</td>
																	<td class="tableBoldGreen">aks_diast_nenm</td>
																	<td class="tableBoldGreen">Kvepavimo Takų Ligų Dažnumas</td>
																	<td class="tableBoldGreen">Ar buvo padidintas cholisterolio kiekis</td>
																	<td class="tableBoldGreen">Cholisterolio kiekio koregavimo metodai</td>
																	<td class="tableBoldGreen">Ligos</td>
																</tr>
															</thead>
															<tbody>
															<?php
															if ($resultAllPatient) {
																foreach ($resultAllPatient as $row) {
																	echo "<tr>";
																	echo "<td>" . $row->id . "</td>";
																	echo "<td>" . $row->projektas . "</td>";
																	echo "<td>" . $row->fiz_akt . "</td>";
																	echo "<td>" . $row->aks_sist_ne_norm . "</td>";
																	echo "<td>" . $row->aks_diast_nenm . "</td>";
																	echo "<td>" . $row->kvp_tk_lig_daz . "</td>";
																	echo "<td>" . $row->padid_chol_kiekis . "</td>";
																	echo "<td>" . $row->chol_korekcijos_metodas . "</td>";
																	echo "<td>" . $row->liga_ar_pagr_kat . "</td>";
																	echo "</tr>";
																}
															} else {
																?>
																<div class="infoMessage">
																	<?php
																	echo "Informacijos nėra";
																	?>
																</div>
																<!--infoMessage-->
																<?php
															}
															?>
															</tbody>
														</table>
													</div>
													<div class="tab-pane" id="4">
														<table class="table table-striped table-bordered table-hover" id="dataTables-example"  style="max-width: 90%;">
															<thead>
																<tr>
																	<td class="tableBoldGreen">PATIENT_ID</td>
																	<td class="tableBoldGreen">Alergiškumas</td>
																	<td class="tableBoldGreen">Kokybė</td>
																	<td class="tableBoldGreen">Maista renkasi pagal</td>
																	<td class="tableBoldGreen">Vėžys</td>
																	<td class="tableBoldGreen">Sveikatos rodikliai</td>
																</tr>
															</thead>
															<tbody>
															<?php
															if ($resultAllPatient) {
																foreach ($resultAllPatient as $row) {
																	echo "<tr>";
																	echo "<td>" . $row->id . "</td>";
																	echo "<td>" . $row->alergiskas . "</td>";
																	echo "<td>" . $row->kokybe . "</td>";
																	echo "<td>" . $row->maisto_pasirinkimai . "</td>";
																	echo "<td>" . $row->srgvez . "</td>";
																	echo "<td>" . $row->sveikatos_rodikliai . "</td>";
																	echo "</tr>";
																}
															} else {
																?>
																<div class="infoMessage">
																	<?php
																	echo "Informacijos nėra";
																	?>
																</div>
																<!--infoMessage-->
																<?php
															}
															?>
															</tbody>
														</table>
													</div>
													<div class="tab-pane" id="5">
														<table class="table table-striped table-bordered table-hover" id="dataTables-example"  style="max-width: 90%;">
															<thead>
																<tr>
																	<td class="tableBoldGreen">PATIENT_ID</td>
																	<td class="tableBoldGreen">Svorio kitimas</td>
																	<td class="tableBoldGreen">KG</td>
																	<td class="tableBoldGreen">Per mėn</td>
																	<td class="tableBoldGreen">Svorio kitimas</td>
																	<td class="tableBoldGreen">KG</td>
																	<td class="tableBoldGreen">Per mėn</td>
																</tr>
															</thead>
															<tbody>
															<?php
															if ($resultAllPatient) {
																foreach ($resultAllPatient as $row) {
																echo "<tr>";
																echo "<td>" . $row->id . "</td>";
																echo "<td>" . $row->svor_kit . "</td>";
																echo "<td>" . $row->svor_kg . "</td>";
																echo "<td>" . $row->svor_men . "</td>";
																echo "<td>" . $row->svor_kit_2 . "</td>";
																echo "<td>" . $row->svor_kg_2 . "</td>";
																echo "<td>" . $row->svor_men_2 . "</td>";
																echo "</tr>";
																}
															} else {
																?>
																<div class="infoMessage">
																	<?php
																	echo "Informacijos nėra";
																	?>
																</div>
																<!--infoMessage-->
																<?php
															}
															?>
															</tbody>
														</table>
													</div>
											</div>
										</div>
									</div>
								</div>
								<!-- /.panel-body -->
							</div>
						</div>
                        <!-- /.col-lg-12 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
</div><!--Content-->
