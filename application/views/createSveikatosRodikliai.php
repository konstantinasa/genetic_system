
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Biocheminių tyrimų rodikliai</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">

        <?php
    
        echo validation_errors();
        
        $att = array(
            'name' => "my_form",
            'onsubmit' => "return InsertDefaultValues()",
        );
       
        echo form_open('/addbiorod/add_bio_rod', $att);
        
        echo anchor("general/aboutPatients/seeAboutPatient", "Grįžti atgal",
        array(  'name' => 'backSeeAboutPatient',
                'id' => 'backSeeAboutPatient',
                'class'=> 'btn btn-default'));
        echo nbs(3);

        $buttonSubmit = array(
        'name' => 'mysubmit',
        'id' => 'submit',
        'value' => 'Baigti!',
        'class'=> 'btn btn btn-success'
         );
        echo form_submit($buttonSubmit);
       
        echo br(2);
        
        /* SVeikatos rodikliai*/
        $ii = 0;
        $pasirinkimai = isset($this->session->userdata['step_5']['sveikatosrodikliaiinfoid']) ? $this->session->userdata['step_5']['sveikatosrodikliaiinfoid'] : FALSE;
        foreach ($resultSvaikatosRod as $row) {
            echo form_label(" $row->pavadinimas ");
            $dataSvkRod = array(
            'name' => 'sveikat_rod[]',
            'id' => $row->pavadinimas,
            'size' => '10',
            'class' => 'form-control',
            'value' => ($pasirinkimai[$ii] == 'NULL') ? '' : $pasirinkimai[$ii],
            'placeholder' => 'pvz: 1.33'
                );
            $ii++;
        echo form_input($dataSvkRod);
        echo br(2);
        }
        
        echo anchor("general/aboutPatients/createNewAboutPatient4", "Grįžti atgal",
                array(  'name' => 'backCreateNewAboutPatien5',
                        'id' => 'backCreateNewAboutPatient5',
			'class'=> 'btn btn-default'));
        
        echo nbs(3); 
        $buttonSubmit2 = array(
            'name' => 'mysubmit2',
            'id' => 'submit2',
            'class' => 'btn btn btn-success',
            'value' => 'Baigti!',
            );
        echo form_submit($buttonSubmit2);   
        
//            var_dump($this->session->userdata('step_5'));
//            echo br(1);

        echo form_close();
        ?>

    </div>
                        <!-- /.col-lg-12 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->


<script type="text/javascript">
function InsertDefaultValues()
{
   // Leave this line as is. Customization follows.
   var FieldsAndDefault = new Array();
   // Customization:
   // For each field that will have custom information is 
   //   submitted blank, use this format:
   //     FieldsAndDefault.push("FieldIDvalue Default value");
   
   FieldsAndDefault.push("GLIU -1");
   FieldsAndDefault.push("CHOL -1");
   FieldsAndDefault.push("TG -1");
   FieldsAndDefault.push("DTL -1");
   FieldsAndDefault.push("MTL -1");
   FieldsAndDefault.push("CRB -1");
   FieldsAndDefault.push("APO_AI -1");
   FieldsAndDefault.push("APO_B -1");
   FieldsAndDefault.push("APOA_APOB -1");
   FieldsAndDefault.push("LP_A_ -1");

//   FieldsAndDefault.push("gliu 1");
//   FieldsAndDefault.push("siteURL 789");


   // End of customization.
   ///////////////////////////////////////
   for( var i=0; i<FieldsAndDefault.length; i++ )
   {
      FieldsAndDefault[i] = FieldsAndDefault[i].replace(/^\s*/,"");
      var pieces = FieldsAndDefault[i].split(" ");
      var field = pieces.shift();
      var f = document.getElementById(field);
      if( f.value.length < 1 ) { f.value = pieces.join(" "); }
   }
   return true;
}
</script>