<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo lang('script_heading'); ?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <?php echo form_open("script/scripts_copyFile"); ?>
                            <div class="form-group">
                                <label class="help-block"> <?php echo lang('script_copyFile_information'); ?></label>
                                <button type="submit" class="btn btn-primary btn-xs">
                                    <?php echo lang('script_copyFile_button'); ?>
                                </button>
                                <?php echo form_close(); ?>
                                <hr>
                            </div>
                            <?php echo validation_errors(); ?>
                            <?php echo form_open("script/script_quantiSnp"); ?>
                            <div class="form-group">
                                <label class="help-block"><?php echo lang('script_quantiSnp_information'); ?></label>
                                <div class="select2-bootstrap-prepend">
                                    <label><?php echo lang('script_file', 'file'); ?></label>
                                    <?php
                                    echo '<select id="dropDownSearchs1" name="file">';
                                    echo '<option value=""></option>';
                                    foreach ($resultsFile as $value) {
                                        $mystring = $value->failopavadinimas;
                                        $findme = 'QuantiSNP';
                                        $pos = strpos($mystring, $findme);
                                        if ($pos === false) {
                                        } else {
                                            echo '<option value="' . $value->failopavadinimas . '">' . $value->failopavadinimas . " - (" . $value->trumpinys . ")" . '</option>';
                                        }
                                    }
                                    echo '</select>';
                                    ?>
                                    <br>
                                    <br>
                                    <label><?php echo lang('script_sampleId', 'sampleId'); ?></label>
                                    <?php
                                    echo '<select id="dropDownSearchs2" name="sampleId">';
                                    echo '<option value=""></option>';
                                    foreach ($resultsSampleId as $value) {
                                        echo '<option value="' . $value->id . '">' . $value->trumpinys . '</option>';
                                    }
                                    echo '</select>';
                                    ?>
                                </div>
                                <br>
                                <button type="submit" class="btn btn-primary btn-xs">
                                    <?php echo lang('script_run'); ?>
                                </button>
                                <?php echo form_close(); ?>
                                <hr>
                            </div>
                            <?php echo validation_errors(); ?>
                            <?php echo form_open("script/script_pennCnv"); ?>
                            <div class="form-group">
                                <label class="help-block"><?php echo lang('script_pennCnv_information'); ?></label>

                                <div class="select2-bootstrap-prepend">
                                    <label><?php echo lang('script_file', 'file'); ?></label>
                                    <?php
                                    echo '<select id="dropDownSearchs3" name="file">';
                                    echo '<option value=""></option>';
                                    foreach ($resultsFile as $value) {
                                        $mystring = $value->failopavadinimas;
                                        $findme = 'PennCNV';
                                        $pos = strpos($mystring, $findme);
                                        if ($pos === false) {
                                        } else {
                                            echo '<option value="' . $value->failopavadinimas . '">' . $value->failopavadinimas . " - (" . $value->trumpinys . ")" . '</option>';
                                        }
                                    }
                                    echo '</select>';
                                    ?>
                                    <br>
                                    <br>
                                    <label><?php echo lang('script_sampleId', 'sampleId'); ?></label>
                                    <?php
                                    echo '<select id="dropDownSearchs4" name="sampleId">';
                                    echo '<option value=""></option>';
                                    foreach ($resultsSampleId as $value) {
                                        echo '<option value="' . $value->id . '">' . $value->trumpinys . '</option>';
                                    }
                                    echo '</select>';
                                    ?>
                                </div>
                                <br>
                                <button type="submit" class="btn btn-primary btn-xs">
                                    <?php echo lang('script_run'); ?>
                                </button>
                                <?php echo form_close(); ?>
                                <hr>
                            </div>
                            <?php echo validation_errors(); ?>
                            <?php echo form_open("script/script_cnvPart"); ?>
                            <div class="form-group">
                                <label class="help-block"><?php echo lang('script_cnvPart_information'); ?></label>

                                <div class="select2-bootstrap-prepend">
                                    <label><?php echo lang('script_file', 'file'); ?></label>
                                    <?php
                                    echo '<select id="dropDownSearchs5" name="file">';
                                    echo '<option value=""></option>';
                                    foreach ($resultsFile as $value) {
                                        $mystring = $value->failopavadinimas;
                                        $findme = 'CNVpart';
                                        $pos = strpos($mystring, $findme);
                                        if ($pos === false) {
                                        } else {
                                            echo '<option value="' . $value->failopavadinimas . '">' . $value->failopavadinimas . " - (" . $value->trumpinys . ")" . '</option>';
                                        }
                                    }
                                    echo '</select>';
                                    ?>
                                    <br>
                                    <br>
                                    <label><?php echo lang('script_sampleId', 'sampleId'); ?></label>
                                    <?php
                                    echo '<select id="dropDownSearchs6" name="sampleId">';
                                    echo '<option value=""></option>';
                                    foreach ($resultsSampleId as $value) {
                                        echo '<option value="' . $value->id . '">' . $value->trumpinys . '</option>';
                                    }
                                    echo '</select>';
                                    ?>
                                </div>
                                <br>
                                <button type="submit" class="btn btn-primary btn-xs">
                                    <?php echo lang('script_run'); ?>
                                </button>
                                <?php echo form_close(); ?>
                                <hr>
                            </div>
                            <?php echo validation_errors(); ?>
                            <?php echo form_open("script/script_finalReport"); ?>
                            <div class="form-group">
                                <label class="help-block"><?php echo lang('script_finalReport_information'); ?></label>

                                <div class="select2-bootstrap-prepend">
                                    <label><?php echo lang('script_file', 'file'); ?></label>
                                    <?php
                                    echo '<select id="dropDownSearchs7" name="file">';
                                    echo '<option value=""></option>';
                                    foreach ($resultsFile as $value) {
                                        $mystring = $value->failopavadinimas;
                                        $findme = 'FinalReport';
                                        $pos = strpos($mystring, $findme);
                                        if ($pos === false) {
                                        } else {
                                            echo '<option value="' . $value->failopavadinimas . '">' . $value->failopavadinimas . " - (" . $value->trumpinys . ")" . '</option>';
                                        }
                                    }
                                    echo '</select>';
                                    ?>
                                    <br>
                                    <br>
                                    <label><?php echo lang('script_sampleId', 'sampleId'); ?></label>
                                    <?php
                                    echo '<select id="dropDownSearchs8" name="sampleId">';
                                    echo '<option value=""></option>';
                                    foreach ($resultsSampleId as $value) {
                                        echo '<option value="' . $value->id . '">' . $value->trumpinys . '</option>';
                                    }
                                    echo '</select>';
                                    ?>
                                </div>
                                <br>
                                <button type="submit" class="btn btn-primary btn-xs">
                                    <?php echo lang('script_run'); ?>
                                </button>
                                <?php echo form_close(); ?>
                                <hr>
                            </div>
                            <?php echo validation_errors(); ?>
                            <?php echo form_open("script/script_indel"); ?>
                            <div class="form-group">
                                <label class="help-block"><?php echo lang('script_indel_information'); ?></label>

                                <div class="select2-bootstrap-prepend">
                                    <label><?php echo lang('script_file', 'file'); ?></label>
                                    <?php
                                    echo '<select id="dropDownSearchs9" name="file">';
                                    echo '<option value=""></option>';
                                    foreach ($resultsFile as $value) {
                                        $mystring = $value->failopavadinimas;
                                        $findme = 'INDEL';
                                        $pos = strpos($mystring, $findme);
                                        if ($pos === false) {
                                        } else {
                                            echo '<option value="' . $value->failopavadinimas . '">' . $value->failopavadinimas . " - (" . $value->trumpinys . ")" . '</option>';
                                        }
                                    }
                                    echo '</select>';
                                    ?>
                                    <br>
                                    <br>
                                    <label><?php echo lang('script_sampleId', 'sampleId'); ?></label>
                                    <?php
                                    echo '<select id="dropDownSearchs10" name="sampleId">';
                                    echo '<option value=""></option>';
                                    foreach ($resultsSampleId as $value) {
                                        echo '<option value="' . $value->id . '">' . $value->trumpinys . '</option>';
                                    }
                                    echo '</select>';
                                    ?>
                                </div>
                                <br>
                                <button type="submit" class="btn btn-primary btn-xs">
                                    <?php echo lang('script_run'); ?>
                                </button>
                                <?php echo form_close(); ?>
                                <hr>
                            </div>
                            <?php echo validation_errors(); ?>
                            <?php echo form_open("script/script_snp"); ?>
                            <div class="form-group">
                                <label class="help-block"><?php echo lang('script_snp_information'); ?></label>

                                <div class="select2-bootstrap-prepend">
                                    <label><?php echo lang('script_file', 'file'); ?></label>
                                    <?php
                                    echo '<select id="dropDownSearchs11" name="file">';
                                    echo '<option value=""></option>';
                                    foreach ($resultsFile as $value) {
                                        $mystring = $value->failopavadinimas;
                                        $findme = 'SNP';
                                        $pos = strpos($mystring, $findme);
                                        if ($pos === false) {
                                        } else {
                                            echo '<option value="' . $value->failopavadinimas . '">' . $value->failopavadinimas . " - (" . $value->trumpinys . ")" . '</option>';
                                        }
                                    }
                                    echo '</select>';
                                    ?>
                                    <br>
                                    <br>
                                    <label><?php echo lang('script_sampleId', 'sampleId'); ?></label>
                                    <?php
                                    echo '<select id="dropDownSearchs12" name="sampleId">';
                                    echo '<option value=""></option>';
                                    foreach ($resultsSampleId as $value) {
                                        echo '<option value="' . $value->id . '">' . $value->trumpinys . '</option>';
                                    }
                                    echo '</select>';
                                    ?>
                                </div>
                                <br>
                                <button type="submit" class="btn btn-primary btn-xs">
                                    <?php echo lang('script_run'); ?>
                                </button>
                                <?php echo form_close(); ?>
                                <hr>
                            </div>
                            <?php echo form_open("script/scripts_rmFile"); ?>
                            <div class="form-group">
                                <label class="help-block"> <?php echo lang('script_rmFile_information'); ?></label>
                                <button type="submit" class="btn btn-primary btn-xs">
                                    <?php echo lang('script_rmFile_button'); ?>
                                </button>
                                <?php echo form_close(); ?>
                                <hr>
                            </div>
                        </div>
                        <!-- /.col-lg-12 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->