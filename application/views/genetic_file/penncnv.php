<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo lang('pennCnv_heading'); ?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <!-- /.col-lg-12-->
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-lg-12">
                    <?php echo form_open("penncnv/search"); ?>
                    <div class="form-group">
                        <div class="select2-bootstrap-prepend">
                            <div>
                                <?php echo form_label(lang('genetic_sampleId', 'sampleId')); ?>
                                <?php echo form_dropdown('sampleId', $sampleid_options,
                                    set_value('sampleId'), 'id="dropDownSearch" name="sampleId"'); ?>
                            </div>
                        </div>
                    </div>
                    <button type="submit" formaction="<?php echo base_url('penncnv/search')?>" class="btn btn-primary"><i
                            class="fa fa-search"></i> <?php echo lang('genetic_search'); ?></button>
                             <button type="submit" formaction="<?php echo base_url('penncnv/pennCnvCsv')?>" class="btn btn-primary"><?php echo lang('genetic_csv'); ?></button>
                    <br>
                    <br>
                    <?php echo form_close(); ?>
                </div>
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover">
                            <p><?php echo lang('allRecords'), $num_results; ?></p>
                            <thead>
                            <?php foreach ($fields as $field_name => $field_display): ?>
                                <th <?php if ($sort_by == $field_name) echo "class=\"sort_$sort_order\"" ?>>
                                    <?php echo anchor("penncnv/display/$query_id/$field_name/" .
                                        (($sort_order == 'asc' && $sort_by == $field_name) ? 'desc' : 'asc'),
                                        $field_display); ?>
                                </th>
                            <?php endforeach; ?>
                            </thead>

                            <tbody>
                            <?php foreach ($pennCnvs as $pennCnv): ?>
                                <tr>
                                    <?php foreach ($fields as $field_name => $field_display): ?>
                                        <td>
                                            <?php echo $pennCnv->$field_name; ?>
                                        </td>
                                    <?php endforeach; ?>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>

                        </table>

                        <?php if (strlen($pagination)): ?>
                            <div>
                                <center><?php echo $pagination; ?> </center>
                            </div>
                        <?php endif; ?>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-6 -->
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->