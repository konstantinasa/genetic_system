<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo lang('fileDownload_heading'); ?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <!-- /.col-lg-12-->
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th><?php echo lang('file_download_name'); ?></th>
                                <th><?php echo lang('file_download_download'); ?></th>
                                <th><?php echo lang('file_download_delete'); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if ($resultFileDownload) {
                                foreach ($resultFileDownload as $f) {
                                    echo "<tr>";
                                    echo "<th>" . $f . "</th>";
                                    echo "<th><a class=\"btn btn-primary\" href=" . base_url('downloadExport/'.$f) . ">Atsisiųsti</a></th>";
                                    echo "<th><a class=\"btn btn-primary\" href=" . base_url('file/fileDownloadRm/'.$f) . ">Trinti</a></th>";
                                    //echo "<th><button formaction=" . base_url('file/fileDownloadRm/'.$f) . " class=\"btn btn-primary\">Trinti</button></th>";
                                   // echo "<th><a class=\"btn btn-default\" href=" . base_url("$f") . ">Trinti</a></th> ";
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-6 -->
    <!-- /.row -->
</div>
<!-- /.col-lg-6 -->
<!-- /.row -->
</div>
<!-- /#page-wrapper -->