<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo lang('fileUpload_heading'); ?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <p class="help-block"><?php echo lang('fileUpload_subheading'); ?></p>
                            <?php echo validation_errors(); ?>
                            <?php echo form_open_multipart("file/fileUpload"); ?>
                            <div class="form-group">
                                <div class="select2-bootstrap-prepend">
                                    <label><?php echo lang('fileUpload_sampleId', 'sampleId'); ?></label>
                                    <?php
                                    echo '<select id="dropDownSearch" name="sampleId">';
                                    echo '<option value=""></option>';
                                    foreach ($resultsSampleId as $value) {
                                        echo '<option value="' . $value->id . '">' . $value->trumpinys . '</option>';
                                    }
                                    echo '</select>';
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="file" id="uploadFile" name="uploadFile[]" multiple ="multiple" class="filestyle"
                                       data-buttonBefore="true">
                            </div>
                            <button type="submit" class="btn btn-primary">
                                <?php echo lang('fileUpload_submit_btn'); ?>
                            </button>
                            <?php echo form_close(); ?>
                        </div>
                        <!-- /.col-lg-12 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->