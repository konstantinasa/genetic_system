<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo lang('main_heading'); ?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <a class="btn btn-default" href="<?php echo base_url('main/create_main'); ?>"><i
                    class="fa fa-pencil fa-fw"></i><?php echo lang('main_create') ?></a>
            </br>
            </br>
        </div>
        <!-- /.col-lg-12-->
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover">
                            <p><?php echo lang('main_allRecords'), $total_rows; ?></p>
                           <thead>
                            <tr>
                                <th><?php echo lang('main_insertDate'); ?></th>
                                <th><?php echo lang('main_username'); ?></th>
                                <th><?php echo lang('main_sampleId'); ?></th>
                                <th><?php echo lang('main_patientId'); ?></th>
                                <th><?php echo lang('main_action'); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if ($resultMainTable) { ?>
                            <?php foreach ($resultMainTable as $mainTable): ?>
                            <tr>
                                <td>
                                    <?php echo($mainTable->ikelimodata); ?><br/>
                                </td>
                                <td>
                                    <?php echo($mainTable->username); ?><br/>
                                </td>
                                <td>
                                    <?php echo($mainTable->meginioid); ?><br/>
                                </td>
                                <td>
                                    <?php echo($mainTable->pacientas); ?><br/>
                                </td>
                                <td><?php echo anchor("main/delete_main/" . $mainTable->id, lang('main_delete')); ?></td>
                            </tr>
                            <?php endforeach; } ?>
                            </tbody>
                        </table>
                        <center><?php echo $links; ?> </center>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-6 -->
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->