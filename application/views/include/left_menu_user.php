<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            
            <li><a href="<?php echo base_url(); ?>general/main/home"><i class="fa fa-dashboard fa-fw"></i> Pradinis puslapis</a></li>
            
          
            
            <li> 
                <a href="<?php echo base_url(); ?>general/search/showSearchPage""><i class="fa fa-search"></i> Paieška</a>
            </li>
            
            <li>
               <a href="#"><i class="fa fa-table fa-fw"></i> Informacija apie pacientą<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="<?php echo base_url(); ?>general/aboutPatients/seeAboutPatient"><i class="fa fa-pencil fa-fw"></i>Apie pacientą</a></li>
                    <!--<li><a href="#"><i class="fa fa-pencil fa-fw"></i>sss</a></li>-->
                </ul> 
            </li>
            
            
            
           
<!--            <li>
                <a href="blank.html">Blank Page</a>
            </li>-->
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->
</nav>