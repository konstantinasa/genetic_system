<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            
            <li><a href="<?php echo base_url(); ?>general/main/home"><i class="fa fa-dashboard fa-fw"></i> Pradinis puslapis</a></li>
            
            <li>
                <a href="#"><i class="fa fa-wrench fa-fw"></i> <?php echo lang("include_create_user_group") ?><span
                        class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="<?php echo base_url(); ?>auth/index"> <?php echo lang('include_user_list') ?></a>
                    </li>
                    
                    <li>
                        <a href="<?php echo base_url(); ?>auth/group"><?php echo lang('include_group_list') ?></a>
                    </li>
                </ul><!-- /.nav-second-level -->
            </li>
            
            <li> 
                <a href="<?php echo base_url(); ?>general/search/showSearchPage""><i class="fa fa-search"></i> Paieška</a>
            </li>
            
            <li>
               <a href="#"><i class="fa fa-table fa-fw"></i> Informacija apie pacientą<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="<?php echo base_url(); ?>general/aboutPatients/createNewAboutPatient"><i class="fa fa-stack-overflow"></i> Sukurti naują pacientą</a></li>
                    <li><a href="<?php echo base_url(); ?>general/aboutPatients/seeAboutPatient"><i class="fa fa-pencil fa-fw"></i>Apie pacientą</a></li>
                    <!--<li><a href="#"><i class="fa fa-pencil fa-fw"></i>sss</a></li>-->
                </ul> 
            </li>
            
            <li>
               <a href="#"><i class="fa fa-edit fa-fw"></i> Papildyti informacija<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                   <li><a href="<?php echo base_url(); ?>general/nationalitys/seeNationality"><i class="fa fa-pencil fa-fw"></i>Tautybės</a></li>
                   <li><a href="<?php echo base_url(); ?>general/educations/seeEducation"><i class="fa fa-pencil fa-fw"></i>Išsilavinimas</a></li>
                   <li><a href="<?php echo base_url(); ?>general/residences/seeResidence"><i class="fa fa-pencil fa-fw"></i>Miestai</a></li>
                   <li><a href="<?php echo base_url(); ?>general/employments/seeEmployment"><i class="fa fa-pencil fa-fw"></i>Užimtumas</a></li>
                   <li><a href="<?php echo base_url(); ?>general/foods/seeFood"><i class="fa fa-pencil fa-fw"></i>Maisto pasirinkimai</a></li>
                   <li><a href="<?php echo base_url(); ?>general/diseases/seeDisease"><i class="fa fa-pencil fa-fw"></i>Ligos</a></li>
                   <li><a href="<?php echo base_url(); ?>general/oncologys/seeOncology"><i class="fa fa-pencil fa-fw"></i>Onkologinės ligos</a></li>
                   <li><a href="<?php echo base_url(); ?>general/allergys/seeAllergy"><i class="fa fa-pencil fa-fw"></i>Alergenai</a></li>
                   <li><a href="<?php echo base_url(); ?>general/biochemys/seeBiochemy"><i class="fa fa-pencil fa-fw"></i>Biocheminių tyrimų rodikliai</a></li>

                    <!--<li><a href="<?php echo base_url(); ?>#"><i class="fa fa-pencil fa-fw"></i>sss</a></li>-->
                </ul> 
            </li>
            
            
        </ul>
             
            
           
<!--            <li>
                <a href="blank.html">Blank Page</a>
            </li>-->
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->
</nav>