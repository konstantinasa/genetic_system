<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            
            <li><a href="<?php echo base_url(); ?>general/main/home"><i class="fa fa-dashboard fa-fw"></i> Pradinis puslapis</a></li>
            
          
            
            <li> 
                <a href="<?php echo base_url(); ?>general/search/showSearchPage""><i class="fa fa-search"></i> Paieška</a>
            </li>
            
            <li>
               <a href="#"><i class="fa fa-table fa-fw"></i> Informacija apie pacientą<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="<?php echo base_url(); ?>general/aboutPatients/createNewAboutPatient"><i class="fa fa-stack-overflow"></i> Sukurti naują pacientą</a></li>
                    <li><a href="<?php echo base_url(); ?>general/aboutPatients/seeAboutPatient"><i class="fa fa-pencil fa-fw"></i>Apie pacientą</a></li>
                    <!--<li><a href="#"><i class="fa fa-pencil fa-fw"></i>sss</a></li>-->
                </ul> 
            </li>
            
            
            <li>
                <a href="<?php echo base_url('main/mains'); ?>"><?php echo lang('include_mainTable') ?></a>
            </li>
            <li>
                <a href="<?php echo base_url('file/display'); ?>"><?php echo lang('include_file') ?></a>
            </li>
            <li>
                <a href="<?php echo base_url('file/fileDownload'); ?>"><?php echo lang('include_file_download') ?></a>
            </li>
            <li>
                <a href="<?php echo base_url('script/scripts'); ?>"><?php echo lang('include_script') ?></a>
            </li>
            <li>
                <a href="#"><?php echo lang("include_genetic_file") ?><span
                        class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="<?php echo base_url('quantisnp/display'); ?>"> <?php echo lang('include_quantiSnp') ?></a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('penncnv/display'); ?>"> <?php echo lang('include_pennCnv') ?></a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('cnvpart/display'); ?>"> <?php echo lang('include_cnvpart') ?></a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('finalreport/display'); ?>"> <?php echo lang('include_finalReport') ?></a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('snp/display'); ?>"> <?php echo lang('include_snp') ?></a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('indel/display'); ?>"> <?php echo lang('include_indel') ?></a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->
</nav>