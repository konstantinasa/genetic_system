<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php echo lang('reset_password_heading'); ?></h3>
                </div>
                <div class="panel-body">
                    <?php echo validation_errors(); ?>
                    <?php echo form_open('auth/reset_password/' . $code); ?>
                    <fieldset>
                        <div class="form-group">
                            <label><?php echo sprintf(lang('reset_password_new_password_label', 'new'), $min_password_length); ?></label>
                            <?php echo form_input($new_password); ?>
                        </div>
                        <div class="form-group">
                            <label><?php echo lang('reset_password_new_password_confirm_label', 'new_confirm'); ?></label>
                            <?php echo form_input($new_password_confirm); ?>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-lg btn-success btn-block" type="submit"
                                    name="sumbit"><?php echo lang('reset_password_submit_btn'); ?></button>
                                 <span class="pull-right">
                        </div>
                        <?php echo form_input($user_id); ?>
                        <?php echo form_hidden($csrf); ?>
                    </fieldset>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>