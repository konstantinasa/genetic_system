<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo lang('deactivate_heading'); ?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <?php echo validation_errors(); ?>
                            <?php echo form_open("auth/deactivate/" . $user->id); ?>
                            <div class="form-group">
                                <p><?php echo sprintf(lang('deactivate_subheading'), $user->username); ?></p>
                            </div>
                            <div class="form-group">
                                <?php echo lang('deactivate_confirm_y_label', 'confirm'); ?>
                                <input type="radio" name="confirm" value="yes" checked="checked">
                            </div>
                            <div class="form-group">
                                <?php echo lang('deactivate_confirm_n_label', 'confirm'); ?>
                                <input type="radio" name="confirm" value="no">
                            </div>
                            <?php echo form_hidden($csrf); ?>
                            <?php echo form_hidden(array('id' => $user->id)); ?>
                            <button type="submit" class="btn btn-primary">
                                <?php echo lang('deactivate_submit_btn'); ?>
                            </button>
                            <?php echo form_close(); ?>
                        </div>
                        <!-- /.col-lg-12 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
