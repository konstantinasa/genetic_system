<?php Class Script_model extends CI_Model {
    function __construct()
    {
        parent::__construct();
    }
    protected $table_main = 'pagrindinelentele';
    protected $table_sample = 'meginys';
    protected $table_file = 'failas';
    protected $var_destination = '/var/www/html/upload_file';
    protected $var_destination_sql = '/var/www/html/result_insert/';
    public function sampleId_options()
    {
        $query = $this->db->query("SELECT tm.id,ts.trumpinys
                                    FROM $this->table_main tm, $this->table_sample ts
                                    WHERE tm.pacientasid=ts.pacientasid;");
        return $query->result();
    }
    public function file_options()
    {
        $query = $this->db->query("SELECT tf.failopavadinimas,ts.trumpinys
                                    FROM $this->table_file tf,$this->table_main tm, 
$this->table_sample ts
                                    WHERE tf.pagrindineslentelesid=tm.id AND 
tm.pacientasid=ts.pacientasid;");
        return $query->result();
    }
    public function copyFile()
    {
        $old_path = getcwd();
        chdir('bashScript');
        shell_exec('./copy.sh');
        chdir($old_path);
    }
    function quantiSnp($fileName, $sampleId)
    {
        $old_path = getcwd();
        chdir('bashScript');
        $fileWithDir = ($this->var_destination . "/" . $fileName);
        $a = escapeshellarg(trim($fileWithDir));
        $b = escapeshellarg(trim($sampleId));
        $output = shell_exec("./quantiSNP.sh $a $b");
        chdir($old_path);
        return $output;
    }
    public function insertQuantiSnp($fileName, $sampleId)
    {
        $fileNameBash = $this->quantiSnp($fileName, $sampleId);
        $files = trim("$this->var_destination_sql$fileNameBash");
		$this->insertToDatabase($files);
        unlink($files);
    }
    public function exportQuantiSnp($sampleName)
    {
        $old_path = getcwd();
        chdir('bashScript');
        $a = escapeshellarg(trim($sampleName));
        $output = shell_exec("./quantiSnpExport.sh $a");
        chdir($old_path);
    }
    function pennCnv($fileName, $sampleId)
    {
        $old_path = getcwd();
        chdir('bashScript');
        $fileWithDir = ($this->var_destination . "/" . $fileName);
        $a = escapeshellarg(trim($fileWithDir));
        $b = escapeshellarg(trim($sampleId));
        $output = shell_exec("./pennCNV.sh $a $b");
        chdir($old_path);
        return $output;
    }
    public function insertPennCnv($fileName, $sampleId)
    {
        $fileNameBash = $this->pennCnv($fileName, $sampleId);
        $files = trim("$this->var_destination_sql$fileNameBash");
        $this->insertToDatabase($files);
        unlink($files);
    }
     public function exportPennCnv($sampleName)
    {
        $old_path = getcwd();
        chdir('bashScript');
        $a = escapeshellarg(trim($sampleName));
        $output = shell_exec("./pennCnvExport.sh $a");
        chdir($old_path);
    }
    function cnvPart($fileName, $sampleId)
    {
        $old_path = getcwd();
        chdir('bashScript');
        $fileWithDir = ($this->var_destination . "/" . $fileName);
        $a = escapeshellarg(trim($fileWithDir));
        $b = escapeshellarg(trim($sampleId));
        $output = shell_exec("./cnvPart.sh $a $b");
        chdir($old_path);
        return $output;
    }
    public function insertCnvPart($fileName, $sampleId)
    {
        $fileNameBash = $this->cnvPart($fileName, $sampleId);
        $files = trim("$this->var_destination_sql$fileNameBash");
        $this->insertToDatabase($files);
        unlink($files);
    }
    public function exportCnvPart($sampleName)
    {
        $old_path = getcwd();
        chdir('bashScript');
        $a = escapeshellarg(trim($sampleName));
        $output = shell_exec("./cnvPartExport.sh $a");
        chdir($old_path);
    }
    function finalReport($fileName, $sampleId)
    {
        $old_path = getcwd();
        chdir('bashScript');
        $fileWithDir = ($this->var_destination . "/" . $fileName);
        $a = escapeshellarg(trim($fileWithDir));
        $b = escapeshellarg(trim($sampleId));
        $output = shell_exec("./finalReport.sh $a $b");
        chdir($old_path);
        return $output;
    }
    public function insertFinalReport($fileName, $sampleId)
    {
        $fileNameBash = $this->finalReport($fileName, $sampleId);
        $files = trim("$this->var_destination_sql$fileNameBash");
        $this->insertToDatabase($files);
        unlink($files);
    }
    public function exportFinalReport($sampleName)
    {
        $old_path = getcwd();
        chdir('bashScript');
        $a = escapeshellarg(trim($sampleName));
        $output = shell_exec("./finalReportExport.sh $a");
        chdir($old_path);
    }
    function indel($fileName, $sampleId)
    {
        $old_path = getcwd();
        chdir('bashScript');
        $fileWithDir = ($this->var_destination . "/" . $fileName);
        $a = escapeshellarg(trim($fileWithDir));
        $b = escapeshellarg(trim($sampleId));
        $output = shell_exec("./indel.sh $a $b");
        chdir($old_path);
        return $output;
    }
    public function insertIndel($fileName, $sampleId)
    {
        $fileNameBash = $this->indel($fileName, $sampleId);
        $files = trim("$this->var_destination_sql$fileNameBash");
        $this->insertToDatabase($files);
        unlink($files);
    }
      public function exportIndel($sampleName)
    {
        $old_path = getcwd();
        chdir('bashScript');
        $a = escapeshellarg(trim($sampleName));
        $output = shell_exec("./indelExport.sh $a");
        chdir($old_path);
    }
    public function snp($fileName, $sampleId)
    {
        $old_path = getcwd();
        chdir('bashScript');
        $fileWithDir = ($this->var_destination . "/" . $fileName);
        $a = escapeshellarg(trim($fileWithDir));
        $b = escapeshellarg(trim($sampleId));
        $output = shell_exec("./snp.sh $a $b");
        chdir($old_path);
        return $output;
    }
    public function insertSnp($fileName, $sampleId)
    {
        $fileNameBash = $this->snp($fileName, $sampleId);
        $files = trim("$this->var_destination_sql$fileNameBash");
        $this->insertToDatabase($files);
        unlink($files);
    }
    public function exportSnp($sampleName)
    {
        $old_path = getcwd();
        chdir('bashScript');
        $a = escapeshellarg(trim($sampleName));
        $output = shell_exec("./snpExport.sh $a");
        chdir($old_path);
    }
    function insertToDatabase($files){
        $old_path = getcwd();
        chdir('bashScript');
        $a = escapeshellarg(trim($files));
        $command="./database.sh $a";
        $output=shell_exec($command." 2>&1");
        chdir($old_path);
    }
    public function rmFile()
    {
        $old_path = getcwd();
        chdir('bashScript');
        shell_exec('./removeFile.sh');
        chdir($old_path);
    }
}
?>
