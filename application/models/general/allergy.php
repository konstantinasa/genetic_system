<?php

Class Allergy extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    protected $table_alergiskaskam = 'alergiskaskam';

    public function record_count()
    {
        return $this->db->count_all($this->table_alergiskaskam);
    }

    public function fetch_allergy($limit, $offset)
    {
        $query = $this->db->query("
                                    SELECT n.id, n.alergiskaskam
                                    FROM $this->table_alergiskaskam n
                                    ORDER BY n.id ASC 
                                    LIMIT $limit 
                                    OFFSET $offset
                                    ");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    function insertAllergy($dataIssilavinimas)
    {
        $this->db->insert($this->table_alergiskaskam, $dataIssilavinimas);
    }

    function editAllergy($id)
    {
        $query = $this->db->query(" SELECT n.id, n.alergiskaskam
                                    FROM $this->table_alergiskaskam n
                                    WHERE  n.id='$id'");
        return $query->result();
    }

    function updateAllergy($id, $dataIssilavinimas)
    {
        $this->db->update($this->table_alergiskaskam, $dataIssilavinimas, array('id' => $id));
    }

    function deleteAllergy($id)
    {
        $this->db->delete($this->table_alergiskaskam, array('id' => $id));
    }
}

?>
