<?php

Class Oncology extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    protected $table_srgveziukokiu = 'srgveziukokiu';

    public function record_count()
    {
        return $this->db->count_all($this->table_srgveziukokiu);
    }

    public function fetch_oncology($limit, $offset)
    {
        $query = $this->db->query("
                                    SELECT n.id, n.aprasas
                                    FROM $this->table_srgveziukokiu n
                                    ORDER BY n.id ASC 
                                    LIMIT $limit 
                                    OFFSET $offset
                                    ");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    function insertOncology($dataIssilavinimas)
    {
        $this->db->insert($this->table_srgveziukokiu, $dataIssilavinimas);
    }

    function editOncology($id)
    {
        $query = $this->db->query(" SELECT n.id, n.aprasas
                                    FROM $this->table_srgveziukokiu n
                                    WHERE  n.id='$id'");
        return $query->result();
    }

    function updateOncology($id, $dataIssilavinimas)
    {
        $this->db->update($this->table_srgveziukokiu, $dataIssilavinimas, array('id' => $id));
    }

    function deleteOncology($id)
    {
        $this->db->delete($this->table_srgveziukokiu, array('id' => $id));
    }
}

?>
