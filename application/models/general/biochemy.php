<?php

Class Biochemy extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    protected $sveikatosrodikliaiinfo = 'sveikatosrodikliaiinfo';

    public function record_count()
    {
        return $this->db->count_all($this->sveikatosrodikliaiinfo);
    }

    public function fetch_biochemy($limit, $offset)
    {
        $query = $this->db->query("
                                    SELECT n.id, n.pavadinimas
                                    FROM $this->sveikatosrodikliaiinfo n
                                    ORDER BY n.id ASC 
                                    LIMIT $limit 
                                    OFFSET $offset
                                    ");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    function insertBiochemy($dataBiochemy)
    {
        $this->db->insert($this->sveikatosrodikliaiinfo, $dataBiochemy);
    }

    function editBiochemy($id)
    {
        $query = $this->db->query(" SELECT n.id, n.pavadinimas
                                    FROM $this->sveikatosrodikliaiinfo n
                                    WHERE  n.id='$id'");
        return $query->result();
    }

    function updateBiochemy($id, $dataBiochemy)
    {
        $this->db->update($this->sveikatosrodikliaiinfo, $dataBiochemy, array('id' => $id));
    }

    function deleteBiochemy($id)
    {
        $this->db->delete($this->sveikatosrodikliaiinfo, array('id' => $id));
    }
}

?>
