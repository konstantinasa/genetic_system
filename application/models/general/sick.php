<?php

Class Sick extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    protected $table_srginfo = 'srginfo';

    public function record_count()
    {
        return $this->db->count_all($this->table_srginfo);
    }

    public function fetch_sick($limit, $offset)
    {
        $query = $this->db->query("
                                    SELECT n.id, n.srginfo, r.reiksme
                                    FROM $this->table_srginfo n
                                    LEFT JOIN reiksme AS r ON n.arpagrkategorija = r.id
                                    ORDER BY n.id ASC 
                                    LIMIT $limit 
                                    OFFSET $offset
                                    ");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    function insertSick($dataIssilavinimas)
    {
        $this->db->insert($this->table_srginfo, $dataIssilavinimas);
    }

    function editSick($id)
    {
        $query = $this->db->query(" SELECT n.id, n.srginfo, n.arpagrkategorija
                                    FROM $this->table_srginfo n
                                    WHERE  n.id='$id'");
        return $query->result();
    }
    
    function fetch_reiksme()
    {
        $query = $this->db->query(" SELECT n.id, n.reiksme
                                    FROM reiksme n
                                   ");
        return $query->result();
    }

    function updateSick($id, $dataIssilavinimas)
    {
        $this->db->update($this->table_srginfo, $dataIssilavinimas, array('id' => $id));
    }

    function deleteSick($id)
    {
        $this->db->delete($this->table_srginfo, array('id' => $id));
    }
}

?>
