<?php

Class Food extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    protected $table_maistopasirinkimas = 'maistopasirinkimas';

    public function record_count()
    {
        return $this->db->count_all($this->table_maistopasirinkimas);
    }

    public function fetch_food($limit, $offset)
    {
        $query = $this->db->query("
                                    SELECT n.id, n.pasirinkimas
                                    FROM $this->table_maistopasirinkimas n
                                    ORDER BY n.id ASC 
                                    LIMIT $limit 
                                    OFFSET $offset
                                    ");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    function insertFood($dataIssilavinimas)
    {
        $this->db->insert($this->table_maistopasirinkimas, $dataIssilavinimas);
    }

    function editFood($id)
    {
        $query = $this->db->query(" SELECT n.id, n.pasirinkimas
                                    FROM $this->table_maistopasirinkimas n
                                    WHERE  n.id='$id'");
        return $query->result();
    }

    function updateFood($id, $dataIssilavinimas)
    {
        $this->db->update($this->table_maistopasirinkimas, $dataIssilavinimas, array('id' => $id));
    }

    function deleteFood($id)
    {
        $this->db->delete($this->table_maistopasirinkimas, array('id' => $id));
    }
}

?>
