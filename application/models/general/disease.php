<?php

Class Disease extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    protected $table_pavadinimas = 'ligos';

    public function record_count()
    {
        return $this->db->count_all($this->table_pavadinimas);
    }

    public function fetch_disease($limit, $offset)
    {
        $query = $this->db->query("
                                    SELECT n.id, n.pavadinimas
                                    FROM $this->table_pavadinimas n
                                    ORDER BY n.id ASC 
                                    LIMIT $limit 
                                    OFFSET $offset
                                    ");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    function insertDisease($dataIssilavinimas)
    {
        $this->db->insert($this->table_pavadinimas, $dataIssilavinimas);
    }

    function editDisease($id)
    {
        $query = $this->db->query(" SELECT n.id, n.pavadinimas
                                    FROM $this->table_pavadinimas n
                                    WHERE  n.id='$id'");
        return $query->result();
    }

    function updateDisease($id, $dataIssilavinimas)
    {
        $this->db->update($this->table_pavadinimas, $dataIssilavinimas, array('id' => $id));
    }

    function deleteDisease($id)
    {
        $this->db->delete($this->table_pavadinimas, array('id' => $id));
    }
}

?>
