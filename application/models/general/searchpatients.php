<?php

Class SearchPatients extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    protected $table_about_patient = 'about_patient';
    protected $table_patient = 'patient';
    protected $table_residence = 'residence';
    protected $table_nationality = 'nationality';
    protected $table_municipality_of_residence = 'municipality_of_residence';
    protected $table_country_of_residence = 'country_of_residence';
    protected $table_education = 'education';
    protected $table_employment = 'employment';
    protected $table_city = 'city';
    protected $table_food = 'food';
    protected $table_food_info = 'food_info';
    protected $table_sick = 'sick';
    protected $table_sick_info = 'sick_info';
    protected $table_allergenicity = 'allergenicity';
    protected $table_allergenicity_info = 'allergenicity_info';
    protected $table_birth_information = 'birth_information';
    protected $table_srg = 'srg';
    protected $table_srg_info = 'srg_info';
    protected $table_weight = 'weight';
    protected $table_patientDetails = 'patientDetails';
    protected $table_choles = 'choles';
    protected $table_serumo_tag = 'serumo_tag';
    protected $table_serumo_dtl = 'serumo_dtl';
    protected $table_chol_korekcija = 'chol_korekcija';
    protected $table_fizinis_aktyvum = 'fizinis_aktyvum';
    protected $table_aks_sist_ne_norm = 'aks_sist_ne_norm';
    protected $table_aks_diast_ne_nm = 'aks_diast_ne_nm';
    protected $table_kvep_tk_lig_daz = 'kvep_tk_lig_daz';
    protected $table_healthIndicators = 'healthIndicators';
	
	function getptr($other) 
	{
		echo "other:$other";
		$query="select qtags.id,lenteles_vardas,kintamasis ,ktipas,m2n from qtags,lenlistas where lentele=lenlistas.id and qtags.id='".$other."'";
		$query=$this->db->query($query);
		return $query->result();
	}
	
	function gettagval($tag) 
	{
		#create our temporary tag table
		#$creation="CREATE OR REPLACE FUNCTION create_mytable () RETURNS void AS \$_$ BEGIN IF EXISTS ( SELECT * FROM   pg_catalog.pg_tables WHERE  tablename  = 'att' ) THEN RAISE NOTICE 'Table \"att\" already exists.'; ELSE CREATE TEMP TABLE att AS (select * from (select * from (select tagas,(lenteles_vardas || '.' || kintamasis ) as pointer, vartipai.tipas from tags,vartipai,lenlistas where tags.lentele=lenlistas.id and tags.Ktipas=vartipai.id) as aa union (select tagas,kintamasis as pointer, '0' as tipas from tags where lentele=0)) as qqq); END IF; END; \$_$ LANGUAGE plpgsql; ";
		#$b=$this->db->query($creation);
		#echo "<br>B is:";
		#print_r($b->result());
		#$a=$this->db->query("SELECT create_mytable();");
		#echo "<br>A is:";
		#var_dump($a->result());
		
		$stdque="select * from (select * from (select * from (select tagas,(lenteles_vardas || '.' || kintamasis ) as pointer, vartipai.tipas from tags,vartipai,lenlistas where tags.lentele=lenlistas.id and tags.Ktipas=vartipai.id) as aa union (select tagas,kintamasis as pointer, '0' as tipas from tags where lentele=0)) as qq) as qqq where tagas like '".$tag."';";

		echo "stdque=".$stdque."<br>";
		$ans=$this->db->query($stdque);
		
		$ans=$ans->result();
		echo "<br> result <br>";
		var_dump($ans);
		
		return $ans;
	}
	
	function fetchModified($construct=" ") 
	{
		$query ="
         SELECT 
            ap.id,
            ap.tdata,
            ap.ugis,
            ap.svoris,
            ap.tiriamliemapim,
            pac.pacientas,
            pac.lytis,
            pac.gdata,
            pac.gimimosvoris,
            pac.gimimougis,
            miest.miestasarkaimas,
            uz.uzimtumas,
            iss.issilavinimas,
            aps.gyvapskritis,
            sav.gyvsavivaldybe,
            taut.tautybe,
            ap.anketosversija,
            prj.projektas,
            fizinisaktyvuminfo.aprasas fiz_akt,
            r1.reiksme aks_sist_ne_norm,
            r2.reiksme aks_diast_nenm,
            kld.aprasas kvp_tk_lig_daz,
            padid_chol_kiekis,
            chol_korekcijos_metodas,
            liga_ar_pagr_kat,
            alergiskas,
            kokybe,
            maisto_pasirinkimai,
            srgvez,
            sveikatos_rodikliai,
            svoris.svor_kit[1] svor_kit,
            svoris.kg[1] svor_kg,
            per_men[1] svor_men,
            svor_kit[2] svor_kit_2,
            svoris.kg[2] svor_kg_2,
            per_men[2] svor_men_2

            FROM
                (SELECT 
			ap.id ap_id,
			ARRAY_AGG (svinf.pavadinimas || ' - ' || sv.reiksme order by svinf.id) sveikatos_rodikliai
		FROM 
			apiepacienta AS ap
			LEFT JOIN sveikatosrodikliai AS sv ON sv.apiepacientaid = ap. ID
			LEFT JOIN sveikatosrodikliaiinfo AS svinf ON svinf. ID = sv.sveikatosrodikliaiinfoid
		GROUP BY
		ap. ID) AS sveik_rod,

                (SELECT 
				ap.id ap_id,
			ARRAY_AGG (svoriokitimas.kitimas order by svoriokitimas.id) svor_kit,
			ARRAY_AGG (svoriokit.kg order by svoriokit.svoriokitimasid) kg	,
			ARRAY_AGG(svoriokit.perkiekmen order by svoriokit.svoriokitimasid) per_men
		FROM 
			apiepacienta AS ap
			LEFT JOIN svoriokit AS svoriokit ON svoriokit.apiepacientaid = ap.id
			LEFT JOIN svoriokitimas AS svoriokitimas ON svoriokitimas.id = svoriokit.svoriokitimasid
		GROUP BY
			ap.id
		) as svoris,

                (SELECT
                    ap.id ap_id,
                    ARRAY_AGG(svcholinf.rodiklis || ' - ' || r3.reiksme order by svcholinf.id) padid_chol_kiekis
                    FROM
                    apiepacienta as ap
                    LEFT JOIN pacientoinfo as pacinf ON pacinf.apiepacientaid = ap.id
                    LEFT JOIN svarbuchol as svchol ON svchol.pacientoinfoid = pacinf.id
                    LEFT JOIN reiksme as r3 ON svchol.arnustatyta = r3.id
                    LEFT JOIN svarbucholinfo as svcholinf ON svcholinf.id = svchol.svarbucholinfoid
                    GROUP BY
                    ap.id
                    ) as svarbu_chol_info,

                    (SELECT
                            ap.id ap_id,
                            ARRAY_AGG(cholkorinf.metodas order by cholkorinf.id) chol_korekcijos_metodas
                    FROM
                            apiepacienta as ap
                            LEFT JOIN pacientoinfo as pacinf ON pacinf.apiepacientaid = ap.id
                            LEFT JOIN cholkorekcija as cholkor ON cholkor.pacientoinfoid = pacinf.id
                            LEFT JOIN cholkorekcijainfo as cholkorinf ON cholkorinf.id = cholkor.metodonr
                    GROUP BY 
                            ap_id
                    ORDER BY 
                            ap_id
                    ) as chol_korekcija,

                    (SELECT
                    ap.id ap_id,
                    ARRAY_AGG(srginfo.srginfo order by srginfo.id) liga_ar_pagr_kat
                    FROM
                    apiepacienta as ap
                    LEFT JOIN srg ON srg.apiepacientaid = ap.id
                    LEFT JOIN srginfo ON srginfo.id = srg.srginfoid 
                    GROUP BY
                    ap_id
                    order by ap_id) as srg,

                    (SELECT
                    ap.id ap_id,
                    ARRAY_AGG(alerkam.alergiskaskam order by alerkam.id) alergiskas 
                    FROM
                    apiepacienta as ap
                    LEFT JOIN alergiskas aler ON aler.apiepacientaid = ap.id
                    LEFT JOIN alergiskaskam alerkam ON alerkam.id = aler.alergiskaskamid
                    group BY
                    ap_id
                    order by 
                    ap_id) as alergiskas_kam,

                    (SELECT 
                    ap.id ap_id,
                    ARRAY_AGG(kokinf.info || ' - ' || r5.reiksme order by kokinf.id) as kokybe
                    FROM 
                     apiepacienta ap
                    LEFT JOIN kokybe as kok ON kok.apiepacientaid = ap.id
                    LEFT JOIN kokybeinfo as kokinf ON kokinf.id = kok.kokybeinfoid
                    LEFT JOIN reiksme as r5 ON r5.id = kok.reiksme
                    group BY
                    ap_id
                    order by 
                    ap_id) as kokyb,

                    (SELECT
                    ap.id ap_id,
                    ARRAY_AGG(maistpas.pasirinkimas order by maistpas.id) as maisto_pasirinkimai
                    FROM
                    apiepacienta ap
                    LEFT JOIN maistas as maist ON maist.apiepacientaid = ap.id
                    LEFT JOIN maistopasirinkimas as maistpas ON maistpas.id = maist.pasirinkimasid
                    group by ap_id
                    order by ap_id) as maistas,

                    (SELECT 
                    ap.id ap_id,
                    ARRAY_AGG(srgvezkok.aprasas order by srgvezkok.id) as srgvez
                    FROM
                    apiepacienta as ap
                    LEFT JOIN srgveziu as srgvez ON srgvez.apiepacientaid = ap.id
                    LEFT JOIN srgveziukokiu as srgvezkok ON srgvezkok.id = srgvez.srgveziukokiuid
                    group by ap_id
                    order by ap_id) as srgveziu,

                    apiepacienta as ap

                    LEFT JOIN issilavinimas as iss ON ap.issilavinimas = iss.id
                    LEFT JOIN miestasarkaimas as miest ON ap.miestasarkaimas = miest.id
                    LEFT JOIN uzimtumas as uz ON ap.uzimtumas = uz.id
                    LEFT JOIN savivaldybe as sav ON miest.savivaldybeid = sav.id
                    LEFT JOIN apskritis as aps ON sav.apskritisid = aps.id
                    LEFT JOIN pacientas as pac ON ap.pacientasid = pac.id
                    LEFT JOIN tautybe as taut ON pac.tautybe = taut.id
                    LEFT JOIN anketosversija av ON av.id = ap.anketosversija
                    LEFT JOIN projektas as prj ON prj.id = av.projektasid
                    LEFT JOIN pacientoinfo as pacinf ON pacinf.apiepacientaid = ap.id
                    LEFT JOIN fizinisaktyvuminfo ON fizinisaktyvuminfo.id = pacinf.fizinisaktyvum
                    LEFT JOIN reiksme as r1 ON r1.id = pacinf.akssistnenorm
                    LEFT JOIN reiksme as r2 ON  r2.id = pacinf.aksdiastnenm
                    LEFT JOIN kveptkligdaz as kld ON kld.id = pacinf.kveptkligdaz

                WHERE
                    sveik_rod.ap_id = ap.id AND
                    svoris.ap_id = ap.id AND
                    svarbu_chol_info.ap_id = ap.id AND
                    chol_korekcija.ap_id = ap.id AND
                    srg.ap_id = ap.id AND
                    alergiskas_kam.ap_id = ap.id AND
                    kokyb.ap_id = ap.id AND
                    maistas.ap_id = ap.id AND
                    srgveziu.ap_id = ap.id ".$construct."

                GROUP BY
                    ap.id,
                    pac.pacientas,
                    pac.lytis,
                    pac.lytis,
                    pac.gdata,
                    pac.gimimosvoris,
                    pac.gimimougis,
                    miest.miestasarkaimas,
                    uz.uzimtumas,
                    iss.issilavinimas,
                    aps.gyvapskritis,
                    sav.gyvsavivaldybe,
                    taut.tautybe,
                    prj.projektas,
                    fiz_akt,
                    aks_sist_ne_norm,
                    aks_diast_nenm,
                    kvp_tk_lig_daz,
                    padid_chol_kiekis,
                    chol_korekcijos_metodas,
                    liga_ar_pagr_kat,
                    alergiskas,
                    kokybe,
                    maisto_pasirinkimai,
                    srgvez,
                    sveik_rod.sveikatos_rodikliai,
                    svoris.svor_kit,
                    svoris.kg,
                    svoris.per_men

                    ORDER BY ap.id
                    ;
                    ";
		$query=$this->db->query($query);
		if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
	}
    
	function getAllTags() 
	{
		$query = $this->db->query("select id,(nr || '. ' || kl) as shown,ktipas,m2n from qtags;");
        return $query->result();
	}
	
	function retrieveTable($table) 
	{
		if ($table=="srgvezkok") $query = $this->db->query("select id,(aprasas) as shown from $table;");
		if ($table=="lytis")
		{
			$li=array('3'=>'Vyras','4'=>'Moteris');
			$dtt=array('0'=>'1', $li);
			return $dtt;
		}
		if ($table=="tautybe") $query = $this->db->query("select id,(tautybe) as shown from $table;");
		if ($table=="issilavinimas") $query = $this->db->query("select id,(issilavinimas) as shown from $table;");
		if ($table=="apskritis") $query = $this->db->query("select id,(gyvapskritis) as shown from $table;");
		if ($table=="savivaldybe") $query = $this->db->query("select id,(gyvsavivaldybe) as shown from $table;");
		if ($table=="ligos") $query = $this->db->query("select id,(pavadinimas) as shown from $table;");
		if ($table=="miestasarkaimas") $query = $this->db->query("select id,(miestasarkaimas) as shown from $table;");
		if ($table=="uzimtumas") $query = $this->db->query("select id,(uzimtumas) as shown from $table;");
		if ($table=="alergiskaskam") $query = $this->db->query("select id,(alergiskaskam) as shown from $table;");
		if ($table=="srginfo") $query = $this->db->query("select id,(srginfo) as shown from $table;");
		if ($table=="svoriokitimas") $query = $this->db->query("select id,(kitimas) as shown from $table;");
		if ($table=="cholkorekcijainfo") $query = $this->db->query("select id,(metodas) as shown from $table;");
		if ($table=="fizinisaktyvuminfo") $query = $this->db->query("select id,(aprasas) as shown from $table;");
		$dtt=array('0'=>'0', '1'=>$query->result());
		return $dtt;
	}
	
	function getAllPatient()
    {
        $query = $this->db->query("SELECT p.sampleId,p.patient FROM $this->table_patient p");
        return $query->result();
    }

    function getAllEducation()
    {
        $query = $this->db->query("SELECT e.education FROM $this->table_education e");
        return $query->result();
    }

    function getAllEmployment()
    {
        $query = $this->db->query("SELECT e.employment FROM $this->table_employment e");
        return $query->result();
    }

    function getAllSick()
    {
        $query = $this->db->query("SELECT si.id,si.description,si.boolean FROM $this->table_sick_info si");
        return $query->result();
    }

    function getAllSrg()
    {
        $query = $this->db->query("SELECT si.id,si.description,si.boolean FROM $this->table_srg_info si");
        return $query->result();
    }

    function getAllFood()
    {
        $query = $this->db->query("SELECT fi.id,fi.description,fi.boolean FROM $this->table_food_info fi");
        return $query->result();
    }

    function getAllAllergenicity()
    {
        $query = $this->db->query("SELECT id,ai.description FROM $this->table_allergenicity_info ai");
        return $query->result();
    }


    function searchPatientsDetail($searchSampleId, $searchPatient, $searchEducation, $searchEmployment, $searchSick, $searchSrg, $searchFood, $searchAllergenicity)
    {

        $query = "SELECT p.sampleId,p.patient,p.lytis,p.g_data,r.residence,n.nationality,mor.municipality_of_residence,cor.country_of_residence,e.education,em.employment,c.city,ap.t_data,ap.ugis,ap.svoris,ft.foodInfoDescription,st.sickInfoDescription,st.serga_text,ab.allergenicityDescription,ab.alergiskas_kam,bi.gimimo_svoris,bi.gimimo_ugis,srgt.srgInfoDescription,srgt.srg_veziu_kokiu,srgt.srg_kitom_text,srgt.neserga,w.tiriam_liem_apim,w.ar_priaugta_sv,w.priaugta_kg,w.priaugta_kg_per,w.numesta_kg,w.numesti_kg_per,pdt.cholesDescription,pdt.serumoTagDescription,pdt.serumoDtlDescription,pdt.cholKorekcijaDescription,pdt.fizinisAktyvumDescription,pdt.aksSistNeNormDescription,pdt.aksDiastNeNmDescription,pdt.kvepTkLigDazDescription,hit.gliu,hit.chol,hit.tg,hit.dtl,hit.mtl,hit.crb,hit.apo_ai,hit.apo_b,hit.apoa_apob,hit.lp_a_
					FROM $this->table_patient p,$this->table_residence r,$this->table_nationality n,$this->table_about_patient ap,$this->table_municipality_of_residence mor,$this->table_country_of_residence cor,$this->table_education e,$this->table_employment em,$this->table_city c,";

        if (!empty($searchSick)) {
            $query = $query . "(SELECT GROUP_CONCAT(DISTINCT concat(si.description, ' - ',  si.boolean )) AS sickInfoDescription, s.serga_text,s.aboutPatientId FROM $this->table_sick s, $this->table_sick_info si WHERE s.sick_info_id = si.id AND s.sick_info_id LIKE '$searchSick' GROUP BY s.aboutPatientId) AS st,";
        } else {
            $query = $query . "(SELECT GROUP_CONCAT(DISTINCT concat(si.description, ' - ',  si.boolean )) AS sickInfoDescription, s.serga_text,s.aboutPatientId FROM $this->table_sick s, $this->table_sick_info si WHERE s.sick_info_id = si.id GROUP BY s.aboutPatientId) AS st,";
        }

        if (!empty($searchSrg)) {
            $query = $query . "(SELECT GROUP_CONCAT(DISTINCT concat(sri.description,' - ',  sri.boolean )) AS srgInfoDescription, sr.srg_veziu_kokiu,   sr.srg_kitom_text, sr.neserga,  sr.aboutPatientId FROM $this->table_srg sr, $this->table_srg_info sri WHERE sr.srg_info_id = sri.id AND sr.srg_info_id LIKE '$searchSrg' GROUP BY sr.aboutPatientId) AS srgt,";
        } else {
            $query = $query . "(SELECT GROUP_CONCAT(DISTINCT concat(sri.description,' - ',  sri.boolean )) AS srgInfoDescription, sr.srg_veziu_kokiu,   sr.srg_kitom_text, sr.neserga,  sr.aboutPatientId FROM $this->table_srg sr, $this->table_srg_info sri WHERE sr.srg_info_id = sri.id GROUP BY sr.aboutPatientId) AS srgt,";
        }

        if (!empty($searchFood)) {
            $query = $query . "(SELECT f.aboutPatientId,GROUP_CONCAT(DISTINCT concat(fi.description, ' - ', fi.boolean)) AS foodInfoDescription FROM $this->table_food f, $this->table_food_info fi WHERE f.food_info_id = fi.id  AND f.food_info_id LIKE '$searchFood'  GROUP BY f.aboutPatientId ) AS ft,";
        } else {
            $query = $query . "(SELECT f.aboutPatientId,GROUP_CONCAT(DISTINCT concat(fi.description, ' - ', fi.boolean)) AS foodInfoDescription FROM $this->table_food f, $this->table_food_info fi WHERE f.food_info_id = fi.id GROUP BY f.aboutPatientId ) AS ft,";
        }

        if (!empty($searchAllergenicity)) {
            $query = $query . "(SELECT  GROUP_CONCAT(DISTINCT ai.description) AS allergenicityDescription, a.alergiskas_kam, a.aboutPatientId FROM $this->table_allergenicity a, $this->table_allergenicity_info ai WHERE a.alergiskumas = ai.id AND ai.description LIKE '$searchAllergenicity' GROUP BY a.aboutPatientId) AS ab,";
        } else {
            $query = $query . "(SELECT  GROUP_CONCAT(DISTINCT ai.description) AS allergenicityDescription, a.alergiskas_kam, a.aboutPatientId FROM $this->table_allergenicity a, $this->table_allergenicity_info ai WHERE a.alergiskumas = ai.id GROUP BY a.aboutPatientId) AS ab,";
        }

        $query = $query . "birth_information bi, weight w, (SELECT  ch.description AS cholesDescription,st.description AS serumoTagDescription,sd.description AS serumoDtlDescription,ck.description AS cholKorekcijaDescription,fa.description AS fizinisAktyvumDescription,asnn.description AS aksSistNeNormDescription,adnn.description AS aksDiastNeNmDescription,ktld.description AS kvepTkLigDazDescription,pd.aboutPatientId FROM $this->table_patientDetails pd,    $this->table_choles ch, $this->table_serumo_tag st, $this->table_serumo_dtl sd, $this->table_chol_korekcija ck, $this->table_fizinis_aktyvum fa, $this->table_aks_sist_ne_norm asnn,    $this->table_aks_diast_ne_nm adnn, $this->table_kvep_tk_lig_daz ktld WHERE pd.ar_buvo_choles = ch.id AND pd.serumo_tag = st.id AND pd.serumo_dtl = sd.id AND pd.chol_korekcija = ck.id AND pd.fizinis_aktyvum = fa.id AND pd.aks_sist_ne_norm = asnn.id AND pd.aks_diast_ne_nm = adnn.id AND pd.kvep_tk_lig_daz = ktld.id) AS pdt, (SELECT hi.gliu,hi.chol,hi.tg,hi.dtl,hi.mtl,hi.crb,hi.apo_ai,hi.apo_b,hi.apoa_apob,hi.lp_a_,hi.aboutPatientId FROM $this->table_healthIndicators hi ) AS hit
					WHERE   p.g_vieta = r.id AND p.tautybe = n.id AND p.id = ap.patientId AND ap.gyv_savivaldybe = mor.id AND ap.gyv_apskritis = cor.id AND ap.issilavinimas = e.id AND ap.uzimtumas = em.id AND ap.gyv_miestas = c.id AND ft.aboutPatientId = ap.id AND st.aboutPatientId = ap.id AND ab.aboutPatientId = ap.id AND ap.id = bi.aboutPatientId AND srgt.aboutPatientId = ap.id AND ap.id = w.aboutPatientId AND pdt.aboutPatientId = ap.id AND hit.aboutPatientId = ap.id";

        if (!empty($searchSampleId)) {
            $query = $query . " AND p.sampleid LIKE '$searchSampleId'";
        }

        if (!empty($searchPatient)) {
            $query = $query . " AND p.patient LIKE '$searchPatient'";
        }

        if (!empty($searchEducation)) {
            $query = $query . " AND e.education LIKE '$searchEducation'";
        }

        if (!empty($searchEmployment)) {
            $query = $query . " AND em.employment LIKE '$searchEmployment'";
        }

        $querys = $this->db->query($query);
        return $querys->result();
    }

}

?>
