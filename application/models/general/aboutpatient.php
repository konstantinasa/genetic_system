<?php

Class AboutPatient extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    protected $table_patient = 'pacientas';
    protected $table_about_patient = 'apiepacienta';
    protected $table_svorio_kit = 'svoriokit';
    protected $table_maistas = 'maistas'; 
    protected $table_srg = 'srg';
    protected $table_alergiskas = 'alergiskas';
    protected $table_srgveziu = 'srgveziu';
    protected $table_svarbu_chol = 'svarbuchol';
    protected $table_chol_kor = 'cholkorekcija';
    protected $table_pac_inf = 'pacientoinfo';
    protected $table_kokybe = 'kokybe';
    protected $table_sveikat_rod = 'sveikatosrodikliai';
    protected $table_sergamumas = 'sergamumas';
//    protected $ss = 'ss';

    
    


    public function record_count()
    {
        return $this->db->count_all($this->table_about_patient);
    }

    public function fetch_Patient($limit, $offset){
        
        $query = $this->db->query("
        SELECT 
            ap.id,
            ap.tdata,
            ap.ugis,
            ap.svoris,
            ap.tiriamliemapim,
            pac.pacientas,
            pac.lytis,
            pac.gdata,
            pac.gimimosvoris,
            pac.gimimougis,
            miest.miestasarkaimas,
            uz.uzimtumas,
            iss.issilavinimas,
            aps.gyvapskritis,
            sav.gyvsavivaldybe,
            taut.tautybe,
            ap.anketosversija,
            prj.projektas,
            fizinisaktyvuminfo.aprasas fiz_akt,
            r1.reiksme aks_sist_ne_norm,
            r2.reiksme aks_diast_nenm,
            kld.aprasas kvp_tk_lig_daz,
            padid_chol_kiekis,
            chol_korekcijos_metodas,
            alergiskas,
            kokybe,
            maisto_pasirinkimai,
            srgvez,
            sveikatos_rodikliai,
            svoris.svor_kit[1] svor_kit,
            svoris.kg[1] svor_kg,
            per_men[1] svor_men,
            svor_kit[2] svor_kit_2,
            svoris.kg[2] svor_kg_2,
            per_men[2] svor_men_2,
						ligos_2.ligos

            FROM
                (SELECT 
			ap.id ap_id,
			ARRAY_AGG (svinf.pavadinimas || ' - ' || sv.reiksme order by svinf.id) sveikatos_rodikliai
		FROM 
			apiepacienta AS ap
			LEFT JOIN sveikatosrodikliai AS sv ON sv.apiepacientaid = ap. ID
			LEFT JOIN sveikatosrodikliaiinfo AS svinf ON svinf. ID = sv.sveikatosrodikliaiinfoid
		GROUP BY
		ap. ID) AS sveik_rod,

                (SELECT 
				ap.id ap_id,
			ARRAY_AGG (svoriokitimas.kitimas order by svoriokitimas.id) svor_kit,
			ARRAY_AGG (svoriokit.kg order by svoriokit.svoriokitimasid) kg	,
			ARRAY_AGG(svoriokit.perkiekmen order by svoriokit.svoriokitimasid) per_men
		FROM 
			apiepacienta AS ap
			LEFT JOIN svoriokit AS svoriokit ON svoriokit.apiepacientaid = ap.id
			LEFT JOIN svoriokitimas AS svoriokitimas ON svoriokitimas.id = svoriokit.svoriokitimasid
		GROUP BY
			ap.id
		) as svoris,

                (SELECT
                    ap.id ap_id,
                    ARRAY_AGG(svcholinf.rodiklis || ' - ' || r3.reiksme order by svcholinf.id) padid_chol_kiekis
                    FROM
                    apiepacienta as ap
                    LEFT JOIN pacientoinfo as pacinf ON pacinf.apiepacientaid = ap.id
                    LEFT JOIN svarbuchol as svchol ON svchol.pacientoinfoid = pacinf.id
                    LEFT JOIN reiksme as r3 ON svchol.arnustatyta = r3.id
                    LEFT JOIN svarbucholinfo as svcholinf ON svcholinf.id = svchol.svarbucholinfoid
                    GROUP BY
                    ap.id
                    ) as svarbu_chol_info,

                    (SELECT
                            ap.id ap_id,
                            ARRAY_AGG(cholkorinf.metodas order by cholkorinf.id) chol_korekcijos_metodas
                    FROM
                            apiepacienta as ap
                            LEFT JOIN pacientoinfo as pacinf ON pacinf.apiepacientaid = ap.id
                            LEFT JOIN cholkorekcija as cholkor ON cholkor.pacientoinfoid = pacinf.id
                            LEFT JOIN cholkorekcijainfo as cholkorinf ON cholkorinf.id = cholkor.metodonr
                    GROUP BY 
                            ap_id
                    ORDER BY 
                            ap_id
                    ) as chol_korekcija,

                    (SELECT
                    ap.id ap_id,
                    ARRAY_AGG(alerkam.alergiskaskam order by alerkam.id) alergiskas 
                    FROM
                    apiepacienta as ap
                    LEFT JOIN alergiskas aler ON aler.apiepacientaid = ap.id
                    LEFT JOIN alergiskaskam alerkam ON alerkam.id = aler.alergiskaskamid
                    group BY
                    ap_id
                    order by 
                    ap_id) as alergiskas_kam,

                    (SELECT 
                    ap.id ap_id,
                    ARRAY_AGG(kokinf.info || ' - ' || r5.reiksme order by kokinf.id) as kokybe
                    FROM 
                     apiepacienta ap
                    LEFT JOIN kokybe as kok ON kok.apiepacientaid = ap.id
                    LEFT JOIN kokybeinfo as kokinf ON kokinf.id = kok.kokybeinfoid
                    LEFT JOIN reiksme as r5 ON r5.id = kok.reiksme
                    group BY
                    ap_id
                    order by 
                    ap_id) as kokyb,

                    (SELECT
                    ap.id ap_id,
                    ARRAY_AGG(maistpas.pasirinkimas order by maistpas.id) as maisto_pasirinkimai
                    FROM
                    apiepacienta ap
                    LEFT JOIN maistas as maist ON maist.apiepacientaid = ap.id
                    LEFT JOIN maistopasirinkimas as maistpas ON maistpas.id = maist.pasirinkimasid
                    group by ap_id
                    order by ap_id) as maistas,

                    (SELECT 
                    ap.id ap_id,
                    ARRAY_AGG(srgvezkok.aprasas order by srgvezkok.id) as srgvez
                    FROM
                    apiepacienta as ap
                    LEFT JOIN srgveziu as srgvez ON srgvez.apiepacientaid = ap.id
                    LEFT JOIN srgveziukokiu as srgvezkok ON srgvezkok.id = srgvez.srgveziukokiuid
                    group by ap_id
                    order by ap_id) as srgveziu,
						
					(SELECT
						 ap.id ap_id,
						 pac.pacientas,
						 ARRAY_AGG(lig.pavadinimas) ligos
						FROM
						 apiepacienta ap
						LEFT JOIN pacientas AS pac ON  ap.pacientasid = pac.id
						LEFT JOIN sergamumas AS serg ON ap.id = serg.apiepacientaid
						LEFT JOIN ligos as lig ON lig.id = serg.ligosid
						GROUP BY
						 ap.id,
						 pac.pacientas
						) as ligos_2,

                    apiepacienta as ap

                    LEFT JOIN issilavinimas as iss ON ap.issilavinimas = iss.id
                    LEFT JOIN miestasarkaimas as miest ON ap.miestasarkaimas = miest.id
                    LEFT JOIN uzimtumas as uz ON ap.uzimtumas = uz.id
                    LEFT JOIN savivaldybe as sav ON miest.savivaldybeid = sav.id
                    LEFT JOIN apskritis as aps ON sav.apskritisid = aps.id
                    LEFT JOIN pacientas as pac ON ap.pacientasid = pac.id
                    LEFT JOIN tautybe as taut ON pac.tautybe = taut.id
                    LEFT JOIN anketosversija av ON av.id = ap.anketosversija
                    LEFT JOIN projektas as prj ON prj.id = av.projektasid
                    LEFT JOIN pacientoinfo as pacinf ON pacinf.apiepacientaid = ap.id
                    LEFT JOIN fizinisaktyvuminfo ON fizinisaktyvuminfo.id = pacinf.fizinisaktyvum
                    LEFT JOIN reiksme as r1 ON r1.id = pacinf.akssistnenorm
                    LEFT JOIN reiksme as r2 ON  r2.id = pacinf.aksdiastnenm
                    LEFT JOIN kveptkligdaz as kld ON kld.id = pacinf.kveptkligdaz

                WHERE
                    sveik_rod.ap_id = ap.id AND
                    svoris.ap_id = ap.id AND
                    svarbu_chol_info.ap_id = ap.id AND
                    chol_korekcija.ap_id = ap.id AND
                    alergiskas_kam.ap_id = ap.id AND
                    kokyb.ap_id = ap.id AND
                    maistas.ap_id = ap.id AND
                    srgveziu.ap_id = ap.id AND
		ligos_2.ap_id = ap.id

                GROUP BY
                    ap.id,
                    pac.pacientas,
                    pac.lytis,
                    pac.lytis,
                    pac.gdata,
                    pac.gimimosvoris,
                    pac.gimimougis,
                    miest.miestasarkaimas,
                    uz.uzimtumas,
                    iss.issilavinimas,
                    aps.gyvapskritis,
                    sav.gyvsavivaldybe,
                    taut.tautybe,
                    prj.projektas,
                    fiz_akt,
                    aks_sist_ne_norm,
                    aks_diast_nenm,
                    kvp_tk_lig_daz,
                    padid_chol_kiekis,
                    chol_korekcijos_metodas,
                    alergiskas,
                    kokybe,
                    maisto_pasirinkimai,
                    srgvez,
                    sveik_rod.sveikatos_rodikliai,
                    svoris.svor_kit,
                    svoris.kg,
                    svoris.per_men,
		ligos_2.ligos

                    ORDER BY ap.id
                    
                    LIMIT  $limit 
                    OFFSET $offset
                    ;
                    ");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
    
    
    function insertPatient_test($dataAboutPatient, $dataPatient, $dataSvorKit, $step_3, $dataPacientoInfo, $kok_svchol_chol, $step_5){
        
        $this->db->trans_start();
        
        if($dataPatient['gdata'] !== 'NULL'){
            $this->db->query("INSERT INTO $this->table_patient (pacientas, lytis, gdata, tautybe, gimimosvoris, gimimougis) VALUES ('" . $dataPatient['pacientas'] . "', " . $dataPatient['lytis'] . ", '" . $dataPatient['gdata'] . "', " . $dataPatient['tautybe'] . ", " . $dataPatient['gimimosvoris'] . ", " . $dataPatient['gimimougis'] . ");");
            $pac_id = $this->db->insert_id();
        } else {
            $this->db->query("INSERT INTO $this->table_patient (pacientas, lytis, gdata, tautybe, gimimosvoris, gimimougis) VALUES ('" . $dataPatient['pacientas'] . "', " . $dataPatient['lytis'] . ", " . $dataPatient['gdata'] . ", " . $dataPatient['tautybe'] . ", " . $dataPatient['gimimosvoris'] . ", " . $dataPatient['gimimougis'] . ");");
            $pac_id = $this->db->insert_id();
        }
        
        if($dataAboutPatient['tdata'] !== 'NULL'){
            $this->db->query("INSERT INTO $this->table_about_patient (tdata, issilavinimas, miestasarkaimas, uzimtumas, ugis, svoris, tiriamliemapim, pacientasid) VALUES ('" . $dataAboutPatient['tdata'] . "', " . $dataAboutPatient['issilavinimas'] . ", " . $dataAboutPatient['miestasarkaimas'] . ", " . $dataAboutPatient['uzimtumas'] . ", " . $dataAboutPatient['ugis'] . "," . $dataAboutPatient['svoris'] . "," . $dataAboutPatient['tiriamliemapim'] . ",'" . $pac_id . "') ");
            $apie_pac_id = $this->db->insert_id();
        } else {
            $this->db->query("INSERT INTO $this->table_about_patient (tdata, issilavinimas, miestasarkaimas, uzimtumas, ugis, svoris, tiriamliemapim, pacientasid) VALUES (" . $dataAboutPatient['tdata'] . ", " . $dataAboutPatient['issilavinimas'] . ", " . $dataAboutPatient['miestasarkaimas'] . ", " . $dataAboutPatient['uzimtumas'] . ", " . $dataAboutPatient['ugis'] . "," . $dataAboutPatient['svoris'] . "," . $dataAboutPatient['tiriamliemapim'] . ",'" . $pac_id . "') ");
            $apie_pac_id = $this->db->insert_id();
        }
        
        
        if(!empty($dataSvorKit['kg']) AND !empty($dataSvorKit['perkiekmen'])){
            $this->db->query("INSERT INTO $this->table_svorio_kit (apiepacientaid, svoriokitimasid, kg, perkiekmen) VALUES ('" . $apie_pac_id . "', '1' ," . $dataSvorKit['kg'] . ", " . $dataSvorKit['perkiekmen'] . ") ");
        }
        if(!empty($dataSvorKit['kg_2']) AND !empty($dataSvorKit['perkiekmen_2'])){
            $this->db->query("INSERT INTO $this->table_svorio_kit (apiepacientaid, svoriokitimasid, kg, perkiekmen) VALUES ('" . $apie_pac_id . "', '2' ," . $dataSvorKit['kg_2'] . ", " . $dataSvorKit['perkiekmen_2'] . ") ");
        }
        
        /* Maistas inseerts*/
        if($step_3['pasirinkimasid']['0'] !== 'NULL'){
        $queryFood = "INSERT INTO $this->table_maistas(pasirinkimasid, apiepacientaid) VALUES ";
        for ($i = 0; $i < sizeof($step_3['pasirinkimasid']); $i++) {
            $queryFood .= "(" . $step_3['pasirinkimasid'][$i] . ", '" . $apie_pac_id . "'),";
        }
        $querySqlFood = rtrim($queryFood, ',');
        $this->db->query($querySqlFood);
        }
        
        /* Sergamumas inserts*/
        if($step_3['ligosid']['0'] !== 'NULL'){
        $querySrg = "INSERT INTO $this->table_sergamumas(apiepacientaid, ligosid) VALUES ";
        for ($i = 0; $i < sizeof($step_3['ligosid']); $i++) {
            $querySrg .= "('" . $apie_pac_id . "', " . $step_3['ligosid'][$i] . "),";
        }
        $querySqlSrg = rtrim($querySrg, ',');
        $this->db->query($querySqlSrg);
        }
        
        /* Alerg inserts*/
        if($step_3['alergiskaskamid']['0'] !== 'NULL'){
        $queryAlerg = "INSERT INTO $this->table_alergiskas(alergiskaskamid, apiepacientaid) VALUES ";
        for ($i = 0; $i < sizeof($step_3['alergiskaskamid']); $i++) {
            $queryAlerg .= "(" . $step_3['alergiskaskamid'][$i] . ", '" . $apie_pac_id . "'),";
        }
        $querySqlAlerg = rtrim($queryAlerg, ',');
        $this->db->query($querySqlAlerg);
        }
        
        /* SrgVez inserts*/
        if($step_3['srgveziukokiuid']['0'] !== 'NULL'){
        $querySrgVez = "INSERT INTO $this->table_srgveziu(srgveziukokiuid, apiepacientaid) VALUES ";
        for ($i = 0; $i < sizeof($step_3['srgveziukokiuid']); $i++) {
            $querySrgVez .= "(" . $step_3['srgveziukokiuid'][$i] . ", '" . $apie_pac_id . "'),";
        }
        $querySqlSrgVez = rtrim($querySrgVez, ',');
        $this->db->query($querySqlSrgVez);
        }
        
         $this->db->query("INSERT INTO $this->table_pac_inf (fizinisaktyvum, akssistnenorm, aksdiastnenm, kveptkligdaz, apiepacientaid) VALUES (" . $dataPacientoInfo['fizinisaktyvum'] . ", " . $dataPacientoInfo['akssistnenorm'] . ", " . $dataPacientoInfo['aksdiastnenm'] . ", " . $dataPacientoInfo['kveptkligdaz'] . ", '" . $apie_pac_id . "') ");
         $pac_info_id = $this->db->insert_id();
         $this->db->query("INSERT INTO $this->table_kokybe(apiepacientaid, kokybeinfoid, reiksme) VALUES ('" . $apie_pac_id . "', '5' , " . $kok_svchol_chol['arbuvocholes'] . ")");
         
         /* SvarbuChol inserts*/
        if($kok_svchol_chol['svarbucholinfoid']['0'] !== 'NULL'){
        $querySvarbuChol = "INSERT INTO $this->table_svarbu_chol(pacientoinfoid, svarbucholinfoid, arnustatyta) VALUES ";
        for ($i = 0; $i < sizeof($kok_svchol_chol['svarbucholinfoid']); $i++) {
            $querySvarbuChol .= "('" . $pac_info_id . "', " . $kok_svchol_chol['svarbucholinfoid'][$i] . " , '1'),";
        }
        $querySqlSvarbuChol = rtrim($querySvarbuChol, ',');
        $this->db->query($querySqlSvarbuChol);
        }
        
        /* Chol Korekcija inserts*/
        if($kok_svchol_chol['metodonr']['0'] !== 'NULL'){
        $queryCholKorekcija = "INSERT INTO $this->table_chol_kor(metodonr, pacientoinfoid) VALUES ";
        for ($i = 0; $i < sizeof($kok_svchol_chol['metodonr']); $i++) {
            $queryCholKorekcija .= "(" . $kok_svchol_chol['metodonr'][$i] . " , " . $pac_info_id . "),";
        }
        $querySqlCholKorekcija = rtrim($queryCholKorekcija, ',');
        $this->db->query($querySqlCholKorekcija);
        }
        
        for ($i = 0; $i < count($step_5['sveikatosrodikliaiinfoid']); $i++){
            $this->db->query("INSERT INTO $this->table_sveikat_rod(apiepacientaid, sveikatosrodikliaiinfoid, reiksme) VALUES ('" . $apie_pac_id . "', '" . ($i + 1) . "' , " . $step_5['sveikatosrodikliaiinfoid'][$i] . ")");
        }
         

//        if($kok_svchol_chol['svarbucholinfoid']['0'] !== 'NULL'){
//        $querySvarbuChol = "INSERT INTO $this->table_svarbu_chol(pacientoinfoid, svarbucholinfoid, arnustatyta) VALUES ";
//        for ($i = 0; $i < sizeof($kok_svchol_chol['svarbucholinfoid']); $i++) {
//            $querySvarbuChol .= "('" . $pac_info_id . "', " . $kok_svchol_chol['svarbucholinfoid'][$i] . " , '1'),";
//        }
//        $querySqlSvarbuChol = rtrim($querySvarbuChol, ',');
//        $this->db->query($querySqlSvarbuChol);
//        }
// 
//        $this->db->query("INSERT INTO $this->table_template(pacientoinfoid, svarbucholinfoid, reiksme) VALUES ('" . $var . "', " . $arr['smth'] . " , '1')");

        
        $this->db->trans_complete();
        
    
    }

    
    function insertPatient_4_step_finish($dataAboutPatient, $dataPatient, $dataSvorKit, $step_3, $dataPacientoInfo, $kok_svchol_chol){
        
        $this->db->trans_start();
        
        if($dataPatient['gdata'] !== 'NULL'){
            $this->db->query("INSERT INTO $this->table_patient (pacientas, lytis, gdata, tautybe, gimimosvoris, gimimougis) VALUES ('" . $dataPatient['pacientas'] . "', " . $dataPatient['lytis'] . ", '" . $dataPatient['gdata'] . "', " . $dataPatient['tautybe'] . ", " . $dataPatient['gimimosvoris'] . ", " . $dataPatient['gimimougis'] . ");");
            $pac_id = $this->db->insert_id();
        } else {
            $this->db->query("INSERT INTO $this->table_patient (pacientas, lytis, gdata, tautybe, gimimosvoris, gimimougis) VALUES ('" . $dataPatient['pacientas'] . "', " . $dataPatient['lytis'] . ", " . $dataPatient['gdata'] . ", " . $dataPatient['tautybe'] . ", " . $dataPatient['gimimosvoris'] . ", " . $dataPatient['gimimougis'] . ");");
            $pac_id = $this->db->insert_id();
        }
        
        if($dataAboutPatient['tdata'] !== 'NULL'){
            $this->db->query("INSERT INTO $this->table_about_patient (tdata, issilavinimas, miestasarkaimas, uzimtumas, ugis, svoris, tiriamliemapim, pacientasid) VALUES ('" . $dataAboutPatient['tdata'] . "', " . $dataAboutPatient['issilavinimas'] . ", " . $dataAboutPatient['miestasarkaimas'] . ", " . $dataAboutPatient['uzimtumas'] . ", " . $dataAboutPatient['ugis'] . "," . $dataAboutPatient['svoris'] . "," . $dataAboutPatient['tiriamliemapim'] . ",'" . $pac_id . "') ");
            $apie_pac_id = $this->db->insert_id();
        } else {
            $this->db->query("INSERT INTO $this->table_about_patient (tdata, issilavinimas, miestasarkaimas, uzimtumas, ugis, svoris, tiriamliemapim, pacientasid) VALUES (" . $dataAboutPatient['tdata'] . ", " . $dataAboutPatient['issilavinimas'] . ", " . $dataAboutPatient['miestasarkaimas'] . ", " . $dataAboutPatient['uzimtumas'] . ", " . $dataAboutPatient['ugis'] . "," . $dataAboutPatient['svoris'] . "," . $dataAboutPatient['tiriamliemapim'] . ",'" . $pac_id . "') ");
            $apie_pac_id = $this->db->insert_id();
        }
        
        
        if(!empty($dataSvorKit['kg']) AND !empty($dataSvorKit['perkiekmen'])){
            $this->db->query("INSERT INTO $this->table_svorio_kit (apiepacientaid, svoriokitimasid, kg, perkiekmen) VALUES ('" . $apie_pac_id . "', '1' ," . $dataSvorKit['kg'] . ", " . $dataSvorKit['perkiekmen'] . ") ");
        }
        if(!empty($dataSvorKit['kg_2']) AND !empty($dataSvorKit['perkiekmen_2'])){
            $this->db->query("INSERT INTO $this->table_svorio_kit (apiepacientaid, svoriokitimasid, kg, perkiekmen) VALUES ('" . $apie_pac_id . "', '2' ," . $dataSvorKit['kg_2'] . ", " . $dataSvorKit['perkiekmen_2'] . ") ");
        }
        
        /* Maistas inseerts*/
        if($step_3['pasirinkimasid']['0'] !== 'NULL'){
        $queryFood = "INSERT INTO $this->table_maistas(pasirinkimasid, apiepacientaid) VALUES ";
        for ($i = 0; $i < sizeof($step_3['pasirinkimasid']); $i++) {
            $queryFood .= "(" . $step_3['pasirinkimasid'][$i] . ", '" . $apie_pac_id . "'),";
        }
        $querySqlFood = rtrim($queryFood, ',');
        $this->db->query($querySqlFood);
        }
        
        /* Sergamumas inserts*/
        if($step_3['ligosid']['0'] !== 'NULL'){
        $querySrg = "INSERT INTO $this->table_sergamumas(apiepacientaid, ligosid) VALUES ";
        for ($i = 0; $i < sizeof($step_3['ligosid']); $i++) {
            $querySrg .= "('" . $apie_pac_id . "', " . $step_3['ligosid'][$i] . "),";
        }
        $querySqlSrg = rtrim($querySrg, ',');
        $this->db->query($querySqlSrg);
        }
        
        /* Alerg inserts*/
        if($step_3['alergiskaskamid']['0'] !== 'NULL'){
        $queryAlerg = "INSERT INTO $this->table_alergiskas(alergiskaskamid, apiepacientaid) VALUES ";
        for ($i = 0; $i < sizeof($step_3['alergiskaskamid']); $i++) {
            $queryAlerg .= "(" . $step_3['alergiskaskamid'][$i] . ", '" . $apie_pac_id . "'),";
        }
        $querySqlAlerg = rtrim($queryAlerg, ',');
        $this->db->query($querySqlAlerg);
        }
        
        /* SrgVez inserts*/
        if($step_3['srgveziukokiuid']['0'] !== 'NULL'){
        $querySrgVez = "INSERT INTO $this->table_srgveziu(srgveziukokiuid, apiepacientaid) VALUES ";
        for ($i = 0; $i < sizeof($step_3['srgveziukokiuid']); $i++) {
            $querySrgVez .= "(" . $step_3['srgveziukokiuid'][$i] . ", '" . $apie_pac_id . "'),";
        }
        $querySqlSrgVez = rtrim($querySrgVez, ',');
        $this->db->query($querySqlSrgVez);
        }
        
         $this->db->query("INSERT INTO $this->table_pac_inf (fizinisaktyvum, akssistnenorm, aksdiastnenm, kveptkligdaz, apiepacientaid) VALUES (" . $dataPacientoInfo['fizinisaktyvum'] . ", " . $dataPacientoInfo['akssistnenorm'] . ", " . $dataPacientoInfo['aksdiastnenm'] . ", " . $dataPacientoInfo['kveptkligdaz'] . ", '" . $apie_pac_id . "') ");
         $pac_info_id = $this->db->insert_id();
         $this->db->query("INSERT INTO $this->table_kokybe(apiepacientaid, kokybeinfoid, reiksme) VALUES ('" . $apie_pac_id . "', '5' , " . $kok_svchol_chol['arbuvocholes'] . ")");
         
         /* SvarbuChol inserts*/
        if($kok_svchol_chol['svarbucholinfoid']['0'] !== 'NULL'){
        $querySvarbuChol = "INSERT INTO $this->table_svarbu_chol(pacientoinfoid, svarbucholinfoid, arnustatyta) VALUES ";
        for ($i = 0; $i < sizeof($kok_svchol_chol['svarbucholinfoid']); $i++) {
            $querySvarbuChol .= "('" . $pac_info_id . "', " . $kok_svchol_chol['svarbucholinfoid'][$i] . " , '1'),";
        }
        $querySqlSvarbuChol = rtrim($querySvarbuChol, ',');
        $this->db->query($querySqlSvarbuChol);
        }
        
        /* Chol Korekcija inserts*/
        if($kok_svchol_chol['metodonr']['0'] !== 'NULL'){
        $queryCholKorekcija = "INSERT INTO $this->table_chol_kor(metodonr, pacientoinfoid) VALUES ";
        for ($i = 0; $i < sizeof($kok_svchol_chol['metodonr']); $i++) {
            $queryCholKorekcija .= "(" . $kok_svchol_chol['metodonr'][$i] . " , " . $pac_info_id . "),";
        }
        $querySqlCholKorekcija = rtrim($queryCholKorekcija, ',');
        $this->db->query($querySqlCholKorekcija);
        }
        
        $this->db->trans_complete();
    }
    
    public function insert_sveikatos_rodikliai($apie_pac_id, $sveiaktos_rod){
        $this->db->trans_start();
        
         for ($i = 0; $i < count($sveiaktos_rod['sveikatosrodikliaiinfoid']); $i++){
            $this->db->query("INSERT INTO $this->table_sveikat_rod(apiepacientaid, sveikatosrodikliaiinfoid, reiksme) VALUES ('" . $apie_pac_id . "', '" . ($i + 1) . "' , " . $sveiaktos_rod['sveikatosrodikliaiinfoid'][$i] . ")");
        }
        $this->db->trans_complete();
    }
    
    

}

?>
