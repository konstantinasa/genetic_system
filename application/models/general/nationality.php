<?php

Class Nationality extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    protected $table_tautybe = 'tautybe';

    public function record_count()
    {
        return $this->db->count_all($this->table_tautybe);
    }

    public function fetch_tautybe($limit, $offset)
    {
        $query = $this->db->query("
                                    SELECT n.id, n.tautybe
                                    FROM $this->table_tautybe n
                                    ORDER BY n.id ASC 
                                    LIMIT $limit 
                                    OFFSET $offset
                                    ");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    function insertNationality($dataNationality)
    {
        $this->db->insert($this->table_tautybe, $dataNationality);
    }

    function editNationality($id)
    {
        $query = $this->db->query(" SELECT n.id, n.tautybe
                                    FROM $this->table_tautybe n
                                    WHERE  n.id='$id'");
        return $query->result();
    }

    function updateNationality($id, $dataNationality)
    {
        $this->db->update($this->table_tautybe, $dataNationality, array('id' => $id));
    }

    function deleteNationality($id)
    {
        $this->db->delete($this->table_tautybe, array('id' => $id));
    }
}

?>
