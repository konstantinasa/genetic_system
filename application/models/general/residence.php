<?php

Class Residence extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    protected $table_miestasarkaimas = 'miestasarkaimas';
    protected $table_savivaldybe = 'savivaldybe';
    protected $table_apskritis = 'apskritis';

    public function record_count()
    {
        return $this->db->count_all($this->table_miestasarkaimas);
    }

    public function fetch_residence($limit, $offset)
    {
        $query = $this->db->query("
                                    SELECT n.id, n.miestasarkaimas, sav.gyvsavivaldybe, aps.gyvapskritis
                                    FROM $this->table_miestasarkaimas n
                                    LEFT JOIN savivaldybe AS sav ON n.savivaldybeid = sav.id
                                    LEFT JOIN apskritis AS aps ON sav.apskritisid = aps.id
                                    ORDER BY n.id ASC 
                                    LIMIT $limit 
                                    OFFSET $offset
                                    ");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    function insertResidence($dataResidence)
    {
        $this->db->insert($this->table_miestasarkaimas, $dataResidence);
    }
    
    function insertResidenceSavivaldybe(){
        $query = $this->db->query(" 
                                    SELECT s.id, s.gyvsavivaldybe, aps.gyvapskritis
                                    FROM $this->table_savivaldybe s
                                    LEFT JOIN apskritis AS aps ON s.apskritisid = aps.id
                                    ");
        return $query->result();  
    }

    function editResidence($id)
    {
        $query = $this->db->query(" SELECT n.id, n.miestasarkaimas, n.savivaldybeid
                                    FROM $this->table_miestasarkaimas n
                                    WHERE  n.id='$id'");
        return $query->result();
    }
    
    
   

    function updateResidence($id, $dataResidence)
    {
        $this->db->update($this->table_miestasarkaimas, $dataResidence, array('id' => $id));
    }

    function deleteResidence($id)
    {
        $this->db->delete($this->table_miestasarkaimas, array('id' => $id));
    }
}

?>
