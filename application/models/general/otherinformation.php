<?php

Class OtherInformation extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    protected $table_nationality = 'tautybe';
    protected $table_municipality_of_residence = 'municipality_of_residence';
    protected $table_country_of_residence = 'country_of_residence';
    protected $table_education = 'issilavinimas';
    protected $table_employment = 'uzimtumas';
    protected $table_city = 'miestasarkaimas';
    protected $table_food_info = 'maistopasirinkimas';
    protected $table_srg_info = 'srginfo';
    protected $table_alergiskas_kam = 'alergiskaskam';
    protected $table_srg_vez = 'srgveziukokiu';
    protected $table_svarbuCholInfo = 'svarbucholinfo';
    protected $table_cholkorekcijaInfo = 'cholkorekcijainfo';
    protected $table_fizinisAkt = 'fizinisaktyvuminfo';
    protected $table_kvep_tak_lig = 'kveptkligdaz';
    protected $table_reiksme = 'reiksme';
    protected $table_sveikatosRod = 'sveikatosrodikliaiinfo';
    protected $table_ligos = 'ligos';
    
//    protected $ss = 'ss';

    

    function allNationality()
    {
        $query = $this->db->query("SELECT n.id,n.tautybe FROM $this->table_nationality n");
        return $query->result();
    }
    
     function allEducation()
    {
        $query = $this->db->query("SELECT e.id,e.issilavinimas FROM $this->table_education e");
        return $query->result();
    }
    
    function allCity()
    {
        $query = $this->db->query("
                SELECT c.id, c.miestasarkaimas, sav.gyvsavivaldybe, aps.gyvapskritis 
                FROM $this->table_city c
                LEFT JOIN savivaldybe AS sav ON c.savivaldybeid = sav.id
                LEFT JOIN apskritis AS aps ON sav.apskritisid = aps.id
                ");
        return $query->result();
    }
    
    function allEmployment()
    {
        $query = $this->db->query("SELECT e.id,e.uzimtumas FROM $this->table_employment e");
        return $query->result();
    }
    
    function allFoodInfo(){
        $query = $this->db->query("SELECT f.id, f.pasirinkimas FROM $this->table_food_info AS f ORDER BY id ASC;");
        return $query->result();
    }
    
    function allSrgInfo(){
        $query = $this->db->query("SELECT s.id, s.srginfo FROM $this->table_srg_info AS s ORDER BY s.id ASC");
        return $query->result();
    }
    
    function allAllergy(){
        $query = $this->db->query("SELECT a.id, a.alergiskaskam FROM $this->table_alergiskas_kam AS a ORDER BY a.id ASC");
        return $query->result();
    }
    
    function allSrgVez(){
        $query = $this->db->query("SELECT sv.id, sv.aprasas FROM $this->table_srg_vez AS sv ORDER BY sv.id ASC");
        return $query->result();
    }
    
    function allSvrbChol(){
        $query = $this->db->query("SELECT sc.id, sc.rodiklis FROM $this->table_svarbuCholInfo AS sc");
        return $query->result();
    }
    
    function allCholKor(){
        $query = $this->db->query("SELECT ck.id, ck.metodas FROM $this->table_cholkorekcijaInfo AS ck");
        return $query->result();
    }
    
    function allFizAkt(){
            $query = $this->db->query("SELECT fiz.id, fiz.aprasas FROM $this->table_fizinisAkt AS fiz");
            return $query->result();
        }
    
    function allKvepTakLig(){
            $query = $this->db->query("SELECT kvp.id, kvp.aprasas FROM $this->table_kvep_tak_lig AS kvp");
            return $query->result();
        }
        
    function allReiksme(){
            $query = $this->db->query("SELECT r.id, r.reiksme FROM $this->table_reiksme AS r");
            return $query->result();
        }
        
        function allSveikatosRod(){
            $query = $this->db->query("SELECT sr.id, sr.pavadinimas FROM $this->table_sveikatosRod AS sr");
            return $query->result();
        }
        
        function allLigos(){
            $query = $this->db->query("SELECT lig.id, lig.pavadinimas FROM $this->table_ligos AS lig");
            return $query->result();
        }
        
    
   //----------------------------------------------------žžžžžžžžžžžžžžžžžžž------- 

    
//    function allTempl(){
//            $query = $this->db->query("SELECT a.id, a.aaa FROM $this->table_smth AS a");
//            return $query->result();
//        }

}

?>
