<?php

Class Education extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    protected $table_issilavinimas = 'issilavinimas';

    public function record_count()
    {
        return $this->db->count_all($this->table_issilavinimas);
    }

    public function fetch_education($limit, $offset)
    {
        $query = $this->db->query("
                                    SELECT n.id, n.issilavinimas
                                    FROM $this->table_issilavinimas n
                                    ORDER BY n.id ASC 
                                    LIMIT $limit 
                                    OFFSET $offset
                                    ");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    function insertEducation($dataIssilavinimas)
    {
        $this->db->insert($this->table_issilavinimas, $dataIssilavinimas);
    }

    function editEducation($id)
    {
        $query = $this->db->query(" SELECT n.id, n.issilavinimas
                                    FROM $this->table_issilavinimas n
                                    WHERE  n.id='$id'");
        return $query->result();
    }

    function updateEducation($id, $dataIssilavinimas)
    {
        $this->db->update($this->table_issilavinimas, $dataIssilavinimas, array('id' => $id));
    }

    function deleteEducation($id)
    {
        $this->db->delete($this->table_issilavinimas, array('id' => $id));
    }
}

?>
