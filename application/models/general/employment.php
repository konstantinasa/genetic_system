<?php

Class Employment extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    protected $table_uzimtumas = 'uzimtumas';

    public function record_count()
    {
        return $this->db->count_all($this->table_uzimtumas);
    }

    public function fetch_employment($limit, $offset)
    {
        $query = $this->db->query("
                                    SELECT n.id, n.uzimtumas
                                    FROM $this->table_uzimtumas n
                                    ORDER BY n.id ASC 
                                    LIMIT $limit 
                                    OFFSET $offset
                                    ");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    function insertEmployment($dataEmployment)
    {
        $this->db->insert($this->table_uzimtumas, $dataEmployment);
    }

    function editEmployment($id)
    {
        $query = $this->db->query(" SELECT n.id, n.uzimtumas
                                    FROM $this->table_uzimtumas n
                                    WHERE  n.id='$id'");
        return $query->result();
    }

    function updateEmployment($id, $dataEmployment)
    {
        $this->db->update($this->table_uzimtumas, $dataEmployment, array('id' => $id));
    }

    function deleteEmployment($id)
    {
        $this->db->delete($this->table_uzimtumas, array('id' => $id));
    }
}

?>
