<?php

Class Quantisnp_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    protected $table_mainTable = 'pagrindinelentele';
    protected $table_quantiSnp = 'quantisnp';
    protected $table_sample = 'meginys';

    function search($query_array, $limit, $offset, $sort_by, $sort_order)
    {

        $sort_order = ($sort_order == 'desc') ? 'desc' : 'asc';
        $sort_columns = array('pacientasid', 'trumpinys', 'chromosome', 'startposition', 'endposition', 'startprobe', 'endprobe', 'length', 'noprobes', 'copynumber', 'maxlogbf');
        $sort_by = (in_array($sort_by, $sort_columns)) ? $sort_by : 'trumpinys';

        // results query
        $q = $this->db->select('trumpinys, chromosome, startposition, endposition, startprobe, endprobe,length,noprobes,copynumber,maxlogbf')
            ->from($this->table_quantiSnp)
            ->join($this->table_mainTable, 'quantisnp.pagrindineslentelesid=pagrindinelentele.id')
            ->join($this->table_sample, 'pagrindinelentele.pacientasid=meginys.pacientasid')
            ->limit($limit, $offset)
            ->order_by($sort_by, $sort_order);


        if (strlen($query_array['trumpinys'])) {
            $q->where('trumpinys', $query_array['trumpinys']);
        }

        $ret['rows'] = $q->get()->result();

        // count query
        $q = $this->db->select('COUNT(*) as count', FALSE)
            ->from($this->table_quantiSnp)
            ->join($this->table_mainTable, 'quantisnp.pagrindineslentelesid=pagrindinelentele.id')
            ->join($this->table_sample, 'pagrindinelentele.pacientasid=meginys.pacientasid');

        if (strlen($query_array['trumpinys'])) {
            $q->where('trumpinys', $query_array['trumpinys']);
        }

        $tmp = $q->get()->result();

        $ret['num_rows'] = $tmp[0]->count;

        return $ret;
    }

}

?>
