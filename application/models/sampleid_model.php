<?php

Class Sampleid_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    protected $table_sample = 'meginys';
    protected $table_mainTable = 'pagrindinelentele';

    public function sampleid_options()
    {
        $rows = $this->db->select('trumpinys')
            ->from($this->table_sample)
            ->join($this->table_mainTable, 'pagrindinelentele.pacientasid = meginys.pacientasid')
            ->get()->result();

        $sampleid_options = array('' => '');
        foreach ($rows as $row) {
            $sampleid_options[$row->trumpinys] = $row->trumpinys;
        }

        return $sampleid_options;
    }

    function save_query($query_array)
    {

        $CI =& get_instance();

        $CI->db->insert('ci_query', array('query_string' => http_build_query($query_array)));

        return $CI->db->insert_id();
    }

    function load_query($query_id)
    {

        $CI =& get_instance();

        $rows = $CI->db->get_where('ci_query', array('id' => $query_id))->result();
        if (isset($rows[0])) {
            parse_str($rows[0]->query_string, $_GET);
        }

    }
}

?>
