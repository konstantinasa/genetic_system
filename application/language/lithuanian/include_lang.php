<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['include_create_user_group'] = 'Vartotojo ir grupės nustatymai';
$lang['include_user_list'] = 'Vartotojų sarašas';
$lang['include_group_list'] = 'Grupių sarašas';
$lang['include_user_information'] = 'Vartotojo informacija';
$lang['include_change_password'] = 'Pasikeisti slaptažodį';
$lang['include_welcome']='Sveiki prisijungę, %s';

/* Create */
$lang['create_patient_heading']='Naujas pacientas';
$lang['create_tautybe']='Nauja tautybė';
$lang['create_masito_pasisrink']='Naujas maisto pasirinkimas';
$lang['create_alerg']='Naujas alergenas';
$lang['create_vez']='Naujas ankologinė liga';
$lang['create_biochem']='Naujas biocheminių tyrimų rodiklis';
$lang['create_issilavinimas']='Naujas išsilavinimas';
$lang['create_uzimtumas']='Naujas užimtumas';
$lang['create_miestas']='Nauja gyvenvietė';
$lang['create_masistas']='Naujas maisto pasirinkimas';
$lang['create_srginfo']='Pridėti ligą';
$lang['create_alergiskaskam']='Pridėti alergeną';
$lang['create_oncol']='Pridėti onkologine ligą';
$lang['create_bio']='Pridėti biocheminį rodiklį';



/* Edit */
$lang['edit_item']='Redaguoti įrašą';


/* List */
$lang['list_employment']='Užimtumų sąrašas';
$lang['list_educations']='Išsilavinimų sąrašas';
$lang['list_maistas']='Maisto pasirinkimų sąrašas';
$lang['list_sicks']='Ligų sąrašas';
$lang['list_alergiskaskam']='Alergenų sąrašas';
$lang['list_oncol']='Onkologinių ligų sąrašas';
$lang['list_bio']='Biocheminių rodiklių sąrašas';

// genetic
$lang['include_mainTable']='Pagrindinė lentelė';
$lang['include_genetic_file']='Genetiniai failai';
$lang['include_quantiSnp']='QuantiSNP';
$lang['include_pennCnv']='PennCNV';
$lang['include_cnvpart']='CNVpart';
$lang['include_finalReport']='FinalReport';
$lang['include_snp'] = 'SNP';
$lang['include_indel'] = 'INDEL';
$lang['include_file']='Failai';
$lang['include_file_download']='Atsisiųsti CSV';
$lang['include_file_upload'] = 'Įkelti naują failą';
$lang['include_script'] = 'Skriptai';
$lang['include_search'] = 'Genetinių failų paieška';


//$lang['create_tautybe']='Nauja tautybė';
//$lang['create_tautybe']='Nauja tautybė';
//$lang['create_tautybe']='Nauja tautybė';



