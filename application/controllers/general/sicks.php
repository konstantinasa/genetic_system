<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sicks extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->helper("url");
        $this->load->model('general/sick');
        $this->load->library("pagination");
    }

    public function seeSick()
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {             
            redirect('auth', 'refresh');
        }
        
            $session_data = $this->session->userdata('logged_in');

            $config = array();
            $config["base_url"] = base_url() . "general/sicks/seeSick";
            $config["total_rows"] = $this->sick->record_count();
            $config["per_page"] = 25;
            $config["uri_segment"] = 4;

            $data = array(
                'title' => "Platforma vietinių gyventojų genomo duomenų bazei",
                'firstFolder' => "general/view",
                'main_content' => "seeSick",
                'username' => $this->session->userdata('username')
            );
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $data["resultSick"] = $this->sick->fetch_sick($config["per_page"], $page);
            $data["links"] = $this->pagination->create_links();
            
                if ($this->ion_auth->is_admin())
		{
		     $this->load->view('template/template_admin', $data);
		} 
		elseif ($this->ion_auth->in_group('superUser')) 
		{
		     $this->load->view('template/template_superUser', $data);
		}
		elseif ($this->ion_auth->in_group('user'))
		{
		     $this->load->view('template/template_user', $data);
		}
        
    }

    public function createSick()
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {            
            redirect('auth', 'refresh');
        }
            $session_data = $this->session->userdata('logged_in');
            
            
            $data = array(
                'title' => "Platforma vietinių gyventojų genomo duomenų bazei",
                'firstFolder' => "general/create",
                'main_content' => "createSick",
                'username' => $this->session->userdata('username')
            );
            $this->form_validation->set_rules('sick', 'Liga', 'required');
            $this->form_validation->set_rules('kategorija', 'Ar pagrindinė kategorija', 'required');
            
            $data['resultReiksme'] = $this->sick->fetch_reiksme();
            
            if ($this->form_validation->run() == FALSE) {
                if ($this->ion_auth->is_admin())
		{
		     $this->load->view('template/template_admin', $data);
		} 
		elseif ($this->ion_auth->in_group('superUser')) 
		{
		     $this->load->view('template/template_superUser', $data);
		}
		elseif ($this->ion_auth->in_group('user'))
		{
		     $this->load->view('template/template_user', $data);
		}
            } else {
                $dataSick = array(
                    'srginfo' => $this->input->post('sick'),
                    'arpagrkategorija' => $this->input->post('kategorija'),
                );
                
                $this->sick->insertSick($dataSick);
                
                redirect('/general/sicks/seeSick', 'refresh');
            }
        
    }

    function edit_sick($id)
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {            
            redirect('auth', 'refresh');
        }
            $session_data = $this->session->userdata('logged_in');
            
            $data = array(
                'title' => "Platforma vietinių gyventojų genomo duomenų bazei",
                'firstFolder' => "general/edit",
                'main_content' => "editSick",
                'username' => $this->session->userdata('username')
            );
            
            $data['resultsEditSick'] = $this->sick->editSick($id);
            $data['resultReiksme'] = $this->sick->fetch_reiksme();
            
            $this->form_validation->set_rules('sick', 'Liga', 'required');
            $this->form_validation->set_rules('kategorija', 'Ar pagrindinė kategorija', 'required');
            
            if ($this->form_validation->run() == FALSE) {
                if ($this->ion_auth->is_admin())
		{
		     $this->load->view('template/template_admin', $data);
		} 
		elseif ($this->ion_auth->in_group('superUser')) 
		{
		     $this->load->view('template/template_superUser', $data);
		}
		elseif ($this->ion_auth->in_group('user'))
		{
		     $this->load->view('template/template_user', $data);
		}
            } else {
                $dataSick = array(
                    'srginfo' => $this->input->post('sick'),
                    'arpagrkategorija' => $this->input->post('kategorija'),
                );
                $this->sick->updateSick($id, $dataSick);
                redirect('/general/sicks/seeSick', 'refresh');
            }
        
    }

    function delete_sick($id)
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {           
            redirect('auth', 'refresh');
        }
            $this->sick->deleteSick($id);
            redirect('/general/sicks/seeSick', 'refresh');
    }
    

}


