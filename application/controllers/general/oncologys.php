<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Oncologys extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->helper("url");
        $this->load->model('general/oncology');
        $this->load->library("pagination");
    }

    public function seeOncology()
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {             
            redirect('auth', 'refresh');
        }
        
            $session_data = $this->session->userdata('logged_in');

            $config = array();
            $config["base_url"] = base_url() . "general/oncologys/seeOncology";
            $config["total_rows"] = $this->oncology->record_count();
            $config["per_page"] = 25;
            $config["uri_segment"] = 4;

            $data = array(
                'title' => "Platforma vietinių gyventojų genomo duomenų bazei",
                'firstFolder' => "general/view",
                'main_content' => "seeOncology",
                'username' => $this->session->userdata('username')
            );
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $data["resultOncology"] = $this->oncology->fetch_oncology($config["per_page"], $page);
            $data["links"] = $this->pagination->create_links();
            
                if ($this->ion_auth->is_admin())
		{
		     $this->load->view('template/template_admin', $data);
		} 
		elseif ($this->ion_auth->in_group('superUser')) 
		{
		     $this->load->view('template/template_superUser', $data);
		}
		elseif ($this->ion_auth->in_group('user'))
		{
		     $this->load->view('template/template_user', $data);
		}
        
    }

    public function createOncology()
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {            
            redirect('auth', 'refresh');
        }
            $session_data = $this->session->userdata('logged_in');
            
            
            $data = array(
                'title' => "Platforma vietinių gyventojų genomo duomenų bazei",
                'firstFolder' => "general/create",
                'main_content' => "createOncology",
                'username' => $this->session->userdata('username')
            );
            $this->form_validation->set_rules('oncology', 'Onkologinė liga', 'required');
            
            if ($this->form_validation->run() == FALSE) {
                if ($this->ion_auth->is_admin())
		{
		     $this->load->view('template/template_admin', $data);
		} 
		elseif ($this->ion_auth->in_group('superUser')) 
		{
		     $this->load->view('template/template_superUser', $data);
		}
		elseif ($this->ion_auth->in_group('user'))
		{
		     $this->load->view('template/template_user', $data);
		}
            } else {
                $dataOncology = array(
                    'aprasas' => $this->input->post('oncology')
                );
                
                $this->oncology->insertOncology($dataOncology);
                
                redirect('/general/oncologys/seeOncology', 'refresh');
            }
        
    }

    function edit_oncology($id)
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {            
            redirect('auth', 'refresh');
        }
            $session_data = $this->session->userdata('logged_in');
            
            $data = array(
                'title' => "Platforma vietinių gyventojų genomo duomenų bazei",
                'firstFolder' => "general/edit",
                'main_content' => "editOncology",
                'username' => $this->session->userdata('username')
            );
            
            $data['resultsEditOncology'] = $this->oncology->editOncology($id);
            
            $this->form_validation->set_rules('oncology', 'Onkologinė liga', 'required');
            
            if ($this->form_validation->run() == FALSE) {
                if ($this->ion_auth->is_admin())
		{
		     $this->load->view('template/template_admin', $data);
		} 
		elseif ($this->ion_auth->in_group('superUser')) 
		{
		     $this->load->view('template/template_superUser', $data);
		}
		elseif ($this->ion_auth->in_group('user'))
		{
		     $this->load->view('template/template_user', $data);
		}
            } else {
                $dataOncology = array(
                    'aprasas' => $this->input->post('oncology')
                );
                $this->oncology->updateOncology($id, $dataOncology);
                redirect('/general/oncologys/seeOncology', 'refresh');
            }
        
    }

    function delete_oncology($id)
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {           
            redirect('auth', 'refresh');
        }
            $this->oncology->deleteOncology($id);
            redirect('/general/oncologys/seeOncology', 'refresh');
    }
    

}


