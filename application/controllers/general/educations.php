<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Educations extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->helper("url");
        $this->load->model('general/education');
        $this->load->library("pagination");
    }

    public function seeEducation()
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {             
            redirect('auth', 'refresh');
        }
        
            $session_data = $this->session->userdata('logged_in');

            $config = array();
            $config["base_url"] = base_url() . "general/educations/seeEducation";
            $config["total_rows"] = $this->education->record_count();
            $config["per_page"] = 25;
            $config["uri_segment"] = 4;

            $data = array(
                'title' => "Platforma vietinių gyventojų genomo duomenų bazei",
                'firstFolder' => "general/view",
                'main_content' => "seeEducation",
                'username' => $this->session->userdata('username')
            );
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $data["resultEducation"] = $this->education->fetch_education($config["per_page"], $page);
            $data["links"] = $this->pagination->create_links();
            
                if ($this->ion_auth->is_admin())
		{
		     $this->load->view('template/template_admin', $data);
		} 
		elseif ($this->ion_auth->in_group('superUser')) 
		{
		     $this->load->view('template/template_superUser', $data);
		}
		elseif ($this->ion_auth->in_group('user'))
		{
		     $this->load->view('template/template_user', $data);
		}
        
    }

    public function createEducation()
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {            
            redirect('auth', 'refresh');
        }
            $session_data = $this->session->userdata('logged_in');
            
            
            $data = array(
                'title' => "Platforma vietinių gyventojų genomo duomenų bazei",
                'firstFolder' => "general/create",
                'main_content' => "createEducation",
                'username' => $this->session->userdata('username')
            );
            $this->form_validation->set_rules('education', 'Išsilavinimas', 'required');
            
            if ($this->form_validation->run() == FALSE) {
                if ($this->ion_auth->is_admin())
		{
		     $this->load->view('template/template_admin', $data);
		} 
		elseif ($this->ion_auth->in_group('superUser')) 
		{
		     $this->load->view('template/template_superUser', $data);
		}
		elseif ($this->ion_auth->in_group('user'))
		{
		     $this->load->view('template/template_user', $data);
		}
            } else {
                $dataEducation = array(
                    'issilavinimas' => $this->input->post('education')
                );
                
                $this->education->insertEducation($dataEducation);
                
                redirect('/general/educations/seeEducation', 'refresh');
            }
        
    }

    function edit_education($id)
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {            
            redirect('auth', 'refresh');
        }
            $session_data = $this->session->userdata('logged_in');
            
            $data = array(
                'title' => "Platforma vietinių gyventojų genomo duomenų bazei",
                'firstFolder' => "general/edit",
                'main_content' => "editEducation",
                'username' => $this->session->userdata('username')
            );
            
            $data['resultsEditEducation'] = $this->education->editEducation($id);
            
            $this->form_validation->set_rules('education', 'Išsilavinimas', 'required');
            
            if ($this->form_validation->run() == FALSE) {
                if ($this->ion_auth->is_admin())
		{
		     $this->load->view('template/template_admin', $data);
		} 
		elseif ($this->ion_auth->in_group('superUser')) 
		{
		     $this->load->view('template/template_superUser', $data);
		}
		elseif ($this->ion_auth->in_group('user'))
		{
		     $this->load->view('template/template_user', $data);
		}
            } else {
                $dataEducation = array(
                    'issilavinimas' => $this->input->post('education')
                );
                $this->education->updateEducation($id, $dataEducation);
                redirect('/general/educations/seeEducation', 'refresh');
            }
        
    }

    function delete_education($id)
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {           
            redirect('auth', 'refresh');
        }
            $this->education->deleteEducation($id);
            redirect('/general/educations/seeEducation', 'refresh');
    }
    

}


