<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Employments extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->helper("url");
        $this->load->model('general/employment');
        $this->load->library("pagination");
    }

    public function seeEmployment()
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {             
            redirect('auth', 'refresh');
        }
        
            $session_data = $this->session->userdata('logged_in');

            $config = array();
            $config["base_url"] = base_url() . "general/employments/seeEmployment";
            $config["total_rows"] = $this->employment->record_count();
            $config["per_page"] = 25;
            $config["uri_segment"] = 4;

            $data = array(
                'title' => "Platforma vietinių gyventojų genomo duomenų bazei",
                'firstFolder' => "general/view",
                'main_content' => "seeEmployment",
                'username' => $this->session->userdata('username')
            );
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $data["resultEmployment"] = $this->employment->fetch_employment($config["per_page"], $page);
            $data["links"] = $this->pagination->create_links();
            
                if ($this->ion_auth->is_admin())
		{
		     $this->load->view('template/template_admin', $data);
		} 
		elseif ($this->ion_auth->in_group('superUser')) 
		{
		     $this->load->view('template/template_superUser', $data);
		}
		elseif ($this->ion_auth->in_group('user'))
		{
		     $this->load->view('template/template_user', $data);
		}
        
    }

    public function createEmployment()
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {            
            redirect('auth', 'refresh');
        }
            $session_data = $this->session->userdata('logged_in');
            
            
            $data = array(
                'title' => "Platforma vietinių gyventojų genomo duomenų bazei",
                'firstFolder' => "general/create",
                'main_content' => "createEmployment",
                'username' => $this->session->userdata('username')
            );
            $this->form_validation->set_rules('employment', 'Užimtumas', 'required');
            
            if ($this->form_validation->run() == FALSE) {
                if ($this->ion_auth->is_admin())
		{
		     $this->load->view('template/template_admin', $data);
		} 
		elseif ($this->ion_auth->in_group('superUser')) 
		{
		     $this->load->view('template/template_superUser', $data);
		}
		elseif ($this->ion_auth->in_group('user'))
		{
		     $this->load->view('template/template_user', $data);
		}
            } else {
                $dataEmployment = array(
                    'uzimtumas' => $this->input->post('employment')
                );
                
                $this->employment->insertEmployment($dataEmployment);
                
                redirect('/general/employments/seeEmployment', 'refresh');
            }
        
    }

    function edit_employment($id)
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {            
            redirect('auth', 'refresh');
        }
            $session_data = $this->session->userdata('logged_in');
            
            $data = array(
                'title' => "Platforma vietinių gyventojų genomo duomenų bazei",
                'firstFolder' => "general/edit",
                'main_content' => "editEmployment",
                'username' => $this->session->userdata('username')
            );
            
            $data['resultsEditEmployment'] = $this->employment->editEmployment($id);
            
            $this->form_validation->set_rules('employment', 'Užimtumas', 'required');
            
            if ($this->form_validation->run() == FALSE) {
                if ($this->ion_auth->is_admin())
		{
		     $this->load->view('template/template_admin', $data);
		} 
		elseif ($this->ion_auth->in_group('superUser')) 
		{
		     $this->load->view('template/template_superUser', $data);
		}
		elseif ($this->ion_auth->in_group('user'))
		{
		     $this->load->view('template/template_user', $data);
		}
            } else {
                $dataEmployment = array(
                    'uzimtumas' => $this->input->post('employment')
                );
                $this->employment->updateEmployment($id, $dataEmployment);
                redirect('/general/employments/seeEmployment', 'refresh');
            }
        
    }

    function delete_employment($id)
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {           
            redirect('auth', 'refresh');
        }
            $this->employment->deleteEmployment($id);
            redirect('/general/employments/seeEmployment', 'refresh');
    }
    

}


