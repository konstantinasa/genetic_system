<?php

//if (!defined('BASEPATH'))
//    exit('No direct script access allowed');

class AboutPatients extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->helper("url");
        $this->load->model('general/aboutPatient');
        $this->load->model('general/otherInformation');
        $this->load->library("pagination");
        
        $this->load->database();
        $this->load->library(array('ion_auth','form_validation'));
        $this->load->helper(array('url','language', 'form'));

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

    }

    public function seeAboutPatient($page = 0){
        
        if (!$this->ion_auth->logged_in()) {             
                redirect('auth', 'refresh');
                }
            $session_data = $this->session->userdata('logged_in');
            
            $data['username'] = $this->session->userdata('username');
            $config = array();
            $config["base_url"] = base_url() . "general/aboutPatients/seeAboutPatient";
            $config["total_rows"] = $this->aboutPatient->record_count();
            $config["per_page"] = 10;
            $config["uri_segment"] = 4;
            $data = array(
                'title' => "Apie pacientą",
                'firstFolder' => "general/view",
                'main_content' => "seeAboutPatient",
                'username' => $this->session->userdata('username')
            );
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            
            $data["resultAllPatient"] = $this->aboutPatient->fetch_Patient($config["per_page"], $page);
            
//            $tmp_val = $data["resultAllPatient"]['0']->padid_chol_kiekis;
//            $tmp_val = "<li>" . str_replace('","', ",</br><li>", trim($tmp_val, '{}",'));
//            $data['resultAllPatient']['0']->padid_chol_kiekis = $tmp_val;
            
            
            
            
            
            $data["links"] = $this->pagination->create_links();
            
                if ($this->ion_auth->is_admin())
		{
		     $this->load->view('template/template_admin', $data);
		} 
		elseif ($this->ion_auth->in_group('superUser')) 
		{
		     $this->load->view('template/template_superUser', $data);
		}
		elseif ($this->ion_auth->in_group('user'))
		{
		     $this->load->view('template/template_user', $data);
		}
        
    }

    public function createNewAboutPatient(){
        
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group('superUser'))) {             
            redirect('auth', 'refresh');
        }
                
            $session_data = $this->session->userdata('logged_in');
            
            $data['username'] = $this->session->userdata('username');

            $data = array(
                'title' => "Platforma vietiniЕі gyventojЕі genomo duomenЕі bazei",
                'firstFolder' => "general/create",
                'main_content' => "createNewAboutPatient",
                'username' => $this->session->userdata('username')
            );
            
            $this->form_validation->set_rules('pacientas', 'Paciento Kodas', 'required|is_unique[pacientas.pacientas]');
            $this->form_validation->set_rules('tdata', 'Tyrimo data');
            $this->form_validation->set_rules('lytis', 'Lytis', 'required');
            $this->form_validation->set_rules('gdata', 'Gimimo data');
            $this->form_validation->set_rules('tautybe', 'Tautybė', 'required');
            $this->form_validation->set_rules('issilavinimas', 'Išsivalinimas', 'required');
            $this->form_validation->set_rules('miestasarkaimas', 'Gyvenamoji vieta', 'required');
            $this->form_validation->set_rules('uzimtumas', 'Užѕimtumas', 'required');
            

            $data["resultNationality"] = $this->otherInformation->allNationality();
            $data["resultEducation"] = $this->otherInformation->allEducation();
            $data["resultCity"] = $this->otherInformation->allCity();
            $data["resultEmployment"] = $this->otherInformation->allEmployment();
            

            if ($this->form_validation->run() == FALSE) {
                
                if ($this->ion_auth->is_admin())
		{
		     $this->load->view('template/template_admin', $data);
		} 
		elseif ($this->ion_auth->in_group('superUser')) 
		{
		     $this->load->view('template/template_superUser', $data);
		}
		elseif ($this->ion_auth->in_group('user'))
		{
		     $this->load->view('template/template_user', $data);
		}
            } else {
                
                $step_1 = array(
                    'pacientas' => $this->input->post('pacientas'),
                    'lytis' => $this->input->post('lytis'),
                    'gdata'=> $this->input->post('gdata'),
                    'tautybe'=> $this->input->post('tautybe'),
                    'tdata'=> $this->input->post('tdata'),
                    'issilavinimas'=> $this->input->post('issilavinimas'),
                    'miestasarkaimas'=> $this->input->post('miestasarkaimas'),
                    'uzimtumas'=> $this->input->post('uzimtumas')
                );
             
                $this->session->set_userdata('step_1', $step_1);
                
                redirect('/general/aboutPatients/createNewAboutPatient2', 'refresh');
            }
        
    }

    public function createNewAboutPatient2()
    {
        
         if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group('superUser'))) {             
            redirect('auth', 'refresh');
        }
               $session_data = $this->session->userdata('logged_in');
               
               $data['username'] = $this->session->userdata('username');

               $data = array(
                   'title' => "Platforma vietiniЕі gyventojЕі genomo duomenЕі bazei",
                   'firstFolder' => "general/create",
                   'main_content' => "createNewAboutPatient2",
                   'username' => $this->session->userdata('username')
               );
               
        $this->form_validation->set_rules('ugis', 'Ūgis', 'greater_than[1]|less_than[300]');
        $this->form_validation->set_rules('svoris', 'Svoris', 'greater_than[0]|less_than[300]');
        $this->form_validation->set_rules('tiriamliemapim', 'Liemens apimtis', 'greater_than[0]|less_than[999]');
        $this->form_validation->set_rules('gimimosvoris', 'Gimimo svoris', 'greater_than[0]|less_than[99]');
        $this->form_validation->set_rules('gimimougis', 'Gimimo ūgis', 'is_natural|greater_than[0]|less_than[100]');
        $this->form_validation->set_rules('kg', 'Kilogramai', 'greater_than[0]|less_than[200]');
        $this->form_validation->set_rules('perkiekmen', 'Per kiem mėnesių', 'is_natural');
        $this->form_validation->set_rules('kg_2', 'Kilogramai', 'greater_than[0]|less_than[200]');
        $this->form_validation->set_rules('perkiekmen_2', 'Per kiem mėnesių', 'is_natural');
               
        
        if ($this->form_validation->run() == FALSE) {
            
            if ($this->ion_auth->is_admin())
		{
		     $this->load->view('template/template_admin', $data);
		} 
		elseif ($this->ion_auth->in_group('superUser')) 
		{
		     $this->load->view('template/template_superUser', $data);
		}
		elseif ($this->ion_auth->in_group('user'))
		{
		     $this->load->view('template/template_user', $data);
		}
            } else {
                
                $step_2 = array(
                    'ugis' => $this->input->post('ugis'),
                    'svoris' => $this->input->post('svoris'),
                    'tiriamliemapim' => $this->input->post('tiriamliemapim'),
                    'gimimosvoris' => $this->input->post('gimimosvoris'),
                    'gimimougis' => $this->input->post('gimimougis'),
                    'kg' => $this->input->post('kg'),
                    'perkiekmen' => $this->input->post('perkiekmen'),
                    'kg_2' => $this->input->post('kg_2'),
                    'perkiekmen_2' => $this->input->post('perkiekmen_2'),
                 );
                
               $this->session->set_userdata('step_2', $step_2);
               
               redirect('/general/aboutPatients/createNewAboutPatient3', 'refresh');
            } 
        
    }
    
    
    public function createNewAboutPatient3(){
        
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group('superUser'))) {             
            redirect('auth', 'refresh');
        }
            $session_data = $this->session->userdata('logged_in');
            
            $data['username'] = $this->session->userdata('username');

            $data = array(
                'title' => "Platforma vietiniЕі gyventojЕі genomo duomenЕі bazei",
                'firstFolder' => "general/create",
                'main_content' => "createNewAboutPatient3",
                'username' => $this->session->userdata('username')
            );
        
        $this->form_validation->set_rules('food[]', 'Maistas', '');
        $this->form_validation->set_rules('srg[]', 'Ligos', '');
        $this->form_validation->set_rules('alerg[]', 'Alergija', '');
        $this->form_validation->set_rules('srg_vez[]', 'Onkologines ligos', '');
        $this->form_validation->set_rules('ligos', 'Ligos 2k', '');
        
        $data["resultFood"] = $this->otherInformation->allFoodInfo();
//        $data["resultSrg"] = $this->otherInformation->allSrgInfo();
        $data["resultAllergy"] = $this->otherInformation->allAllergy();
        $data["resultSrgVez"] = $this->otherInformation->allSrgVez();
        $data["resultLigos"] = $this->otherInformation->allLigos();
        
        if ($this->form_validation->run() == FALSE) {
            if ($this->ion_auth->is_admin())
		{
		     $this->load->view('template/template_admin', $data);
		} 
		elseif ($this->ion_auth->in_group('superUser')) 
		{
		     $this->load->view('template/template_superUser', $data);
		}
		elseif ($this->ion_auth->in_group('user'))
		{
		     $this->load->view('template/template_user', $data);
		}
        } else {
            
            $step_3 = array(
                'pasirinkimasid' => (rtrim($this->input->post('food'), ',') !== "") ? explode(',', rtrim($this->input->post('food'), ',')) : array('NULL'),  //($this->input->post('food') !== FALSE) ? $this->input->post('food') : array('NULL')
//                'srginfoid' =>  (rtrim($this->input->post('srg'), ',') !== "") ? explode(',', rtrim($this->input->post('srg'), ',')) : array('NULL'),
                'alergiskaskamid' => (rtrim($this->input->post('alerg'), ',') !== "") ? explode(',', rtrim($this->input->post('alerg'), ',')) : array('NULL'),
                'srgveziukokiuid' => (rtrim($this->input->post('srg_vez'), ',') !== "") ? explode(',', rtrim($this->input->post('srg_vez'), ',')) : array('NULL'),
                'ligosid' => (rtrim($this->input->post('ligos'), ',') !== "") ? explode(',', rtrim($this->input->post('ligos'), ',')) : array('NULL'),
                );
            
         
            $this->session->set_userdata('step_3', $step_3);
            
            redirect('/general/aboutPatients/createNewAboutPatient4', 'refresh'); 
        }
    }
    
    public function createNewAboutPatient4(){
       if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group('superUser'))){             
           redirect('auth', 'refresh');
       }
            $session_data = $this->session->userdata('logged_in');
            
            $data['username'] = $this->session->userdata('username');

            $data = array(
                'title' => "Platforma vietiniЕі gyventojЕі genomo duomenЕі bazei",
                'firstFolder' => "general/create",
                'main_content' => "createNewAboutPatient4",
                'username' => $this->session->userdata('username')
            );
            
            $this->form_validation->set_rules('arbuvocholes', 'Ar nustatytas padidintas chol. kiekis', '');
            $this->form_validation->set_rules('chol[]', 'Cholisterolio kiekis', '');
            $this->form_validation->set_rules('chol_kor[]', 'Cholisterolio korekcija', '');
            $this->form_validation->set_rules('fizinisaktyvum', 'Fizinis aktyvumas', '');
            $this->form_validation->set_rules('akssistnenorm', 'Kraujospudis sistolinis', '');
            $this->form_validation->set_rules('aksdiastnenm', 'Kraujospudis diastolinis', '');
            $this->form_validation->set_rules('kveptkligdaz', 'Kvepavimo taku ligos', '');
            
            $data["resultSvrbChol"] = $this->otherInformation->allSvrbChol();
            $data["resultCholKor"] = $this->otherInformation->allCholKor();
            $data["resultFizAkt"] = $this->otherInformation->allFizAkt();
            $data["resultKvpTakLig"] = $this->otherInformation->allKvepTakLig();
            $data["resultReiksme"] = $this->otherInformation->allReiksme();
            
            if ($this->form_validation->run() == FALSE) {
                if ($this->ion_auth->is_admin())
		{
		     $this->load->view('template/template_admin', $data);
		} 
		elseif ($this->ion_auth->in_group('superUser')) 
		{
		     $this->load->view('template/template_superUser', $data);
		}
		elseif ($this->ion_auth->in_group('user'))
		{
		     $this->load->view('template/template_user', $data);
		}
            } else {
                 $step_4 = array(
                    'arbuvocholes' => $this->input->post('arbuvocholes'),
                    'svarbucholinfoid' => ($this->input->post('chol') !== FALSE) ? $this->input->post('chol') : array('NULL'),
                    'metodonr' => ($this->input->post('chol_kor') !== FALSE) ? $this->input->post('chol_kor') : array('NULL'),
                    'fizinisaktyvum' => $this->input->post('fizinisaktyvum'),
                    'akssistnenorm' => $this->input->post('akssistnenorm'),
                    'aksdiastnenm' => $this->input->post('aksdiastnenm'),
                    'kveptkligdaz' => $this->input->post('kveptkligdaz'), 
                );
                
            $this->session->set_userdata('step_4', $step_4);
            
            redirect('/general/aboutPatients/createNewAboutPatient5', 'refresh');     
                
            } 
    }
    
    
    public function createNewAboutPatient4_finish(){
       if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group('superUser'))){             
           redirect('auth', 'refresh');
       }
            $session_data = $this->session->userdata('logged_in');
            
            $data['username'] = $this->session->userdata('username');

            $data = array(
                'title' => "Platforma vietiniЕі gyventojЕі genomo duomenЕі bazei",
                'firstFolder' => "general/create",
                'main_content' => "createNewAboutPatient4",
                'username' => $this->session->userdata('username')
            );
            
            $this->form_validation->set_rules('arbuvocholes', 'Ar nustatytas padidintas chol. kiekis', '');
            $this->form_validation->set_rules('chol[]', 'Cholisterolio kiekis', '');
            $this->form_validation->set_rules('chol_kor[]', 'Cholisterolio korekcija', '');
            $this->form_validation->set_rules('fizinisaktyvum', 'Fizinis aktyvumas', '');
            $this->form_validation->set_rules('akssistnenorm', 'Kraujospudis sistolinis', '');
            $this->form_validation->set_rules('aksdiastnenm', 'Kraujospudis diastolinis', '');
            $this->form_validation->set_rules('kveptkligdaz', 'Kvepavimo taku ligos', '');
            
            $data["resultSvrbChol"] = $this->otherInformation->allSvrbChol();
            $data["resultCholKor"] = $this->otherInformation->allCholKor();
            $data["resultFizAkt"] = $this->otherInformation->allFizAkt();
            $data["resultKvpTakLig"] = $this->otherInformation->allKvepTakLig();
            $data["resultReiksme"] = $this->otherInformation->allReiksme();
            
            if ($this->form_validation->run() == FALSE) {
                if ($this->ion_auth->is_admin())
		{
		     $this->load->view('template/template_admin', $data);
		} 
		elseif ($this->ion_auth->in_group('superUser')) 
		{
		     $this->load->view('template/template_superUser', $data);
		}
		elseif ($this->ion_auth->in_group('user'))
		{
		     $this->load->view('template/template_user', $data);
		}
            } else {
                 $step_4 = array(
                    'arbuvocholes' => $this->input->post('arbuvocholes'),
                    'svarbucholinfoid' => ($this->input->post('chol') !== FALSE) ? $this->input->post('chol') : array('NULL'),
                    'metodonr' => ($this->input->post('chol_kor') !== FALSE) ? $this->input->post('chol_kor') : array('NULL'),
                    'fizinisaktyvum' => $this->input->post('fizinisaktyvum'),
                    'akssistnenorm' => $this->input->post('akssistnenorm'),
                    'aksdiastnenm' => $this->input->post('aksdiastnenm'),
                    'kveptkligdaz' => $this->input->post('kveptkligdaz'), 
                );
                
                $this->session->set_userdata('step_4', $step_4);

                $step_1 = $this->session->userdata['step_1'];
                $step_2 = $this->session->userdata['step_2'];
                $step_3 = $this->session->userdata['step_3'];
                $step_4 = $this->session->userdata['step_4'];
               
               
               $dataAboutPatient = [
                   'tdata' =>           !empty($step_1['tdata']) ? $step_1['tdata'] : 'NULL',
                   'issilavinimas' =>   !empty($step_1['issilavinimas']) ? $step_1['issilavinimas'] : 'NULL',
                   'miestasarkaimas' => !empty($step_1['miestasarkaimas']) ? $step_1['miestasarkaimas'] : 'NULL',
                   'uzimtumas' =>       !empty($step_1['uzimtumas']) ? $step_1['uzimtumas'] : 'NULL',
                   'ugis' =>            !empty($step_2['ugis']) ? $step_2['ugis'] : 'NULL',
                   'svoris' =>          !empty($step_2['svoris']) ? $step_2['svoris'] : 'NULL',
                   'tiriamliemapim' =>  !empty($step_2['tiriamliemapim']) ? $step_2['tiriamliemapim'] : 'NULL',
                   ];
               
               $dataPatient = array(
                   'pacientas' =>       $step_1['pacientas'],
                   'lytis' =>           !empty($step_1['lytis']) ?  $step_1['lytis'] : 'NULL',
                   'gdata' =>           !empty($step_1['gdata']) ? $step_1['gdata'] : 'NULL',
                   'tautybe' =>         !empty($step_1['tautybe']) ? $step_1['tautybe'] : 'NULL',
                   'gimimosvoris' =>    !empty($step_2['gimimosvoris']) ? $step_2['gimimosvoris'] : 'NULL',
                   'gimimougis' =>      !empty($step_2['gimimougis']) ? $step_2['gimimougis'] :'NULL',
                   
               );
               
               $dataSvorKit = array(
                   'kg' =>              (!empty($step_2['kg'])) ? ($step_2['kg']) : 'NULL',
                   'perkiekmen' =>      (!empty($step_2['perkiekmen'])) ? ($step_2['perkiekmen']) : 'NULL',
                   'kg_2' =>            (!empty($step_2['kg_2']))  ? ($step_2['kg_2']) : 'NULL',
                   'perkiekmen_2' =>    (!empty($step_2['perkiekmen_2']))  ? ($step_2['perkiekmen_2']) : 'NULL',
               );
               
               $dataPacientoInfo = array(
                   'fizinisaktyvum' => $step_4['fizinisaktyvum'] != '0' ? $step_4['fizinisaktyvum']: 'NULL',
                   'akssistnenorm' => $step_4['akssistnenorm'] != '-1' ? $step_4['akssistnenorm'] : 'NULL' ,
                   'aksdiastnenm' => $step_4['aksdiastnenm'] != '-1' ? $step_4['aksdiastnenm'] : 'NULL' ,
                   'kveptkligdaz' => $step_4['kveptkligdaz'] != '0' ? $step_4['kveptkligdaz']: 'NULL',   
               );
               
               $kok_svchol_chol = array(
                   'arbuvocholes' => $step_4['arbuvocholes'],
                   'svarbucholinfoid' => $step_4['svarbucholinfoid'],
                   'metodonr' => $step_4['metodonr'],
               );
               
               $this->aboutPatient->insertPatient_4_step_finish($dataAboutPatient, $dataPatient, $dataSvorKit, $step_3, $dataPacientoInfo, $kok_svchol_chol);
               
               
               $this->session->unset_userdata('step_1');
               $this->session->unset_userdata('step_2');
               $this->session->unset_userdata('step_3');
               $this->session->unset_userdata('step_4');

               redirect('/general/aboutPatients/seeAboutPatient', 'refresh');    
                
            } 
    }
    
    
     public function createNewAboutPatient5(){
         
 if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group('superUser'))) {             
            redirect('auth', 'refresh');
        }            $session_data = $this->session->userdata('logged_in');
            
            $data['username'] = $this->session->userdata('username');

            $data = array(
                'title' => "Platforma vietiniЕі gyventojЕі genomo duomenЕі bazei",
                'firstFolder' => "general/create",
                'main_content' => "createNewAboutPatient5",
                'username' => $this->session->userdata('username')
            ); 
            
            $this->form_validation->set_rules('sveikat_rod[]', 'Sveikatos rodikliai', 'required|numeric');
         
            $data["resultSvaikatosRod"] = $this->otherInformation->allSveikatosRod();
            
            
             if ($this->form_validation->run() == FALSE) {
                if ($this->ion_auth->is_admin())
		{
		     $this->load->view('template/template_admin', $data);
		} 
		elseif ($this->ion_auth->in_group('superUser')) 
		{
		     $this->load->view('template/template_superUser', $data);
		}
		elseif ($this->ion_auth->in_group('user'))
		{
		     $this->load->view('template/template_user', $data);
		}
            } else {
                
                 $step_5 = array(
                    'sveikatosrodikliaiinfoid' => $this->input->post('sveikat_rod')
                );
                 
                 for ($i = 0; $i < count($step_5['sveikatosrodikliaiinfoid']); $i++){
                     if($step_5['sveikatosrodikliaiinfoid'][$i] == -1){
                         $step_5['sveikatosrodikliaiinfoid'][$i] = 'NULL';
                     }   
                 }
                
               $this->session->set_userdata('step_5', $step_5);
              
               $step_1 = $this->session->userdata['step_1'];
               $step_2 = $this->session->userdata['step_2'];
               $step_3 = $this->session->userdata['step_3'];
               $step_4 = $this->session->userdata['step_4'];
               $step_5 = $this->session->userdata['step_5'];
               
               
               $dataAboutPatient = [
                   'tdata' =>           !empty($step_1['tdata']) ? $step_1['tdata'] : 'NULL',
                   'issilavinimas' =>   !empty($step_1['issilavinimas']) ? $step_1['issilavinimas'] : 'NULL',
                   'miestasarkaimas' => !empty($step_1['miestasarkaimas']) ? $step_1['miestasarkaimas'] : 'NULL',
                   'uzimtumas' =>       !empty($step_1['uzimtumas']) ? $step_1['uzimtumas'] : 'NULL',
                   'ugis' =>            !empty($step_2['ugis']) ? $step_2['ugis'] : 'NULL',
                   'svoris' =>          !empty($step_2['svoris']) ? $step_2['svoris'] : 'NULL',
                   'tiriamliemapim' =>  !empty($step_2['tiriamliemapim']) ? $step_2['tiriamliemapim'] : 'NULL',
                   ];
               
               $dataPatient = array(
                   'pacientas' =>       $step_1['pacientas'],
                   'lytis' =>           !empty($step_1['lytis']) ?  $step_1['lytis'] : 'NULL',
                   'gdata' =>           !empty($step_1['gdata']) ? $step_1['gdata'] : 'NULL',
                   'tautybe' =>         !empty($step_1['tautybe']) ? $step_1['tautybe'] : 'NULL',
                   'gimimosvoris' =>    !empty($step_2['gimimosvoris']) ? $step_2['gimimosvoris'] : 'NULL',
                   'gimimougis' =>      !empty($step_2['gimimougis']) ? $step_2['gimimougis'] :'NULL',
                   
               );
               
               $dataSvorKit = array(
                   'kg' =>              (!empty($step_2['kg'])) ? ($step_2['kg']) : 'NULL',
                   'perkiekmen' =>      (!empty($step_2['perkiekmen'])) ? ($step_2['perkiekmen']) : 'NULL',
                   'kg_2' =>            (!empty($step_2['kg_2']))  ? ($step_2['kg_2']) : 'NULL',
                   'perkiekmen_2' =>    (!empty($step_2['perkiekmen_2']))  ? ($step_2['perkiekmen_2']) : 'NULL',
               );
               
               $dataPacientoInfo = array(
                   'fizinisaktyvum' => $step_4['fizinisaktyvum'] != '0' ? $step_4['fizinisaktyvum']: 'NULL',
                   'akssistnenorm' => $step_4['akssistnenorm'] != '-1' ? $step_4['akssistnenorm'] : 'NULL' ,
                   'aksdiastnenm' => $step_4['aksdiastnenm'] != '-1' ? $step_4['aksdiastnenm'] : 'NULL' ,
                   'kveptkligdaz' => $step_4['kveptkligdaz'] != '0' ? $step_4['kveptkligdaz']: 'NULL',   
               );
               
               $kok_svchol_chol = array(
                   'arbuvocholes' => $step_4['arbuvocholes'],
                   'svarbucholinfoid' => $step_4['svarbucholinfoid'],
                   'metodonr' => $step_4['metodonr'],
               );
               
               $this->aboutPatient->insertPatient_test($dataAboutPatient, $dataPatient, $dataSvorKit, $step_3, $dataPacientoInfo, $kok_svchol_chol, $step_5);
               
//               $this->load->view('template_super_admin', $data);
               
               $this->session->unset_userdata('step_1');
               $this->session->unset_userdata('step_2');
               $this->session->unset_userdata('step_3');
               $this->session->unset_userdata('step_4');
               $this->session->unset_userdata('step_5');

               redirect('/general/aboutPatients/seeAboutPatient', 'refresh');
                
            } 
     }
     
    
    
    function clear_form(){
        
        $this->session->unset_userdata('step_1');
        $this->session->unset_userdata('step_2');
        $this->session->unset_userdata('step_3');
        $this->session->unset_userdata('step_4');
        $this->session->unset_userdata('step_5');

         redirect('general/aboutPatients/createNewAboutPatient', 'refresh');
    }
    
    
}


