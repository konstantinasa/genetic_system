<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Biochemys extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->helper("url");
        $this->load->model('general/biochemy');
        $this->load->library("pagination");
    }

    public function seeBiochemy()
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {             
            redirect('auth', 'refresh');
        }
        
            $session_data = $this->session->userdata('logged_in');

            $config = array();
            $config["base_url"] = base_url() . "general/biochemys/seeBiochemy";
            $config["total_rows"] = $this->biochemy->record_count();
            $config["per_page"] = 25;
            $config["uri_segment"] = 4;

            $data = array(
                'title' => "Platforma vietinių gyventojų genomo duomenų bazei",
                'firstFolder' => "general/view",
                'main_content' => "seeBiochemy",
                'username' => $this->session->userdata('username')
            );
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $data["resultBiochemy"] = $this->biochemy->fetch_biochemy($config["per_page"], $page);
            $data["links"] = $this->pagination->create_links();
            
                if ($this->ion_auth->is_admin())
		{
		     $this->load->view('template/template_admin', $data);
		} 
		elseif ($this->ion_auth->in_group('superUser')) 
		{
		     $this->load->view('template/template_superUser', $data);
		}
		elseif ($this->ion_auth->in_group('user'))
		{
		     $this->load->view('template/template_user', $data);
		}
        
    }

    public function createBiochemy()
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {            
            redirect('auth', 'refresh');
        }
            $session_data = $this->session->userdata('logged_in');
            
            
            $data = array(
                'title' => "Platforma vietinių gyventojų genomo duomenų bazei",
                'firstFolder' => "general/create",
                'main_content' => "createBiochemy",
                'username' => $this->session->userdata('username')
            );
            $this->form_validation->set_rules('biochemy', 'Rodiklis', 'required');
            
            if ($this->form_validation->run() == FALSE) {
                if ($this->ion_auth->is_admin())
		{
		     $this->load->view('template/template_admin', $data);
		} 
		elseif ($this->ion_auth->in_group('superUser')) 
		{
		     $this->load->view('template/template_superUser', $data);
		}
		elseif ($this->ion_auth->in_group('user'))
		{
		     $this->load->view('template/template_user', $data);
		}
            } else {
                $dataBiochemy = array(
                    'pavadinimas' => $this->input->post('biochemy')
                );
                
                $this->biochemy->insertBiochemy($dataBiochemy);
                
                redirect('/general/biochemys/seeBiochemy', 'refresh');
            }
        
    }

    function edit_biochemy($id)
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {            
            redirect('auth', 'refresh');
        }
            $session_data = $this->session->userdata('logged_in');
            
            $data = array(
                'title' => "Platforma vietinių gyventojų genomo duomenų bazei",
                'firstFolder' => "general/edit",
                'main_content' => "editBiochemy",
                'username' => $this->session->userdata('username')
            );
            
            $data['resultsEditBiochemy'] = $this->biochemy->editBiochemy($id);
            
            $this->form_validation->set_rules('biochemy', 'Rodiklis', 'required');
            
            if ($this->form_validation->run() == FALSE) {
                if ($this->ion_auth->is_admin())
		{
		     $this->load->view('template/template_admin', $data);
		} 
		elseif ($this->ion_auth->in_group('superUser')) 
		{
		     $this->load->view('template/template_superUser', $data);
		}
		elseif ($this->ion_auth->in_group('user'))
		{
		     $this->load->view('template/template_user', $data);
		}
            } else {
                $dataBiochemy = array(
                    'pavadinimas' => $this->input->post('biochemy')
                );
                $this->biochemy->updateBiochemy($id, $dataBiochemy);
                redirect('/general/biochemys/seeBiochemy', 'refresh');
            }
        
    }

    function delete_biochemy($id)
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {           
            redirect('auth', 'refresh');
        }
            $this->biochemy->deleteBiochemy($id);
            redirect('/general/biochemys/seeBiochemy', 'refresh');
    }
    

}


