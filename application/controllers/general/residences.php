<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Residences extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->helper("url");
        $this->load->model('general/residence');
        $this->load->library("pagination");
    }

    public function seeResidence()
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {             
            redirect('auth', 'refresh');
        }
        
            $session_data = $this->session->userdata('logged_in');

            $config = array();
            $config["base_url"] = base_url() . "general/residences/seeResidence";
            $config["total_rows"] = $this->residence->record_count();
            $config["per_page"] = 25;
            $config["uri_segment"] = 4;

            $data = array(
                'title' => "Platforma vietinių gyventojų genomo duomenų bazei",
                'firstFolder' => "general/view",
                'main_content' => "seeResidence",
                'username' => $this->session->userdata('username')
            );
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $data["resultResidence"] = $this->residence->fetch_residence($config["per_page"], $page);
            $data["links"] = $this->pagination->create_links();
            
                if ($this->ion_auth->is_admin())
		{
		     $this->load->view('template/template_admin', $data);
		} 
		elseif ($this->ion_auth->in_group('superUser')) 
		{
		     $this->load->view('template/template_superUser', $data);
		}
		elseif ($this->ion_auth->in_group('user'))
		{
		     $this->load->view('template/template_user', $data);
		}
        
    }

    public function createResidence()
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {            
            redirect('auth', 'refresh');
        }
            $session_data = $this->session->userdata('logged_in');
            
            
            $data = array(
                'title' => "Platforma vietinių gyventojų genomo duomenų bazei",
                'firstFolder' => "general/create",
                'main_content' => "createResidence",
                'username' => $this->session->userdata('username')
            );
            $this->form_validation->set_rules('residence', 'Gyvenamoji vieta', 'required');
            $this->form_validation->set_rules('savivaldybe', 'Savivaldybe', 'required');
            
             $data['resultsResidenceSavivaldybe'] = $this->residence->insertResidenceSavivaldybe();
//             $data['resultsResidenceApskritis'] = $this->residence->insertResidenceApskritis();
            
            if ($this->form_validation->run() == FALSE) {
                if ($this->ion_auth->is_admin())
		{
		     $this->load->view('template/template_admin', $data);
		} 
		elseif ($this->ion_auth->in_group('superUser')) 
		{
		     $this->load->view('template/template_superUser', $data);
		}
		elseif ($this->ion_auth->in_group('user'))
		{
		     $this->load->view('template/template_user', $data);
		}
            } else {
                $dataResidence = array(
                    'miestasarkaimas' => $this->input->post('residence'),
                    'savivaldybeid' => $this->input->post('savivaldybe'),
                );
                
                $this->residence->insertResidence($dataResidence);
                
                redirect('/general/residences/seeResidence', 'refresh');
            }
        
    }

    function edit_residence($id)
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {            
            redirect('auth', 'refresh');
        }
            $session_data = $this->session->userdata('logged_in');
            
            $data = array(
                'title' => "Platforma vietinių gyventojų genomo duomenų bazei",
                'firstFolder' => "general/edit",
                'main_content' => "editResidence",
                'username' => $this->session->userdata('username')
            );
            
            $data['resultsEditResidence'] = $this->residence->editResidence($id);
            $data['resultsSavivaldybe'] = $this->residence->insertResidenceSavivaldybe();
            
            $this->form_validation->set_rules('residence', 'Gyvenamoji vieta', 'required');
            $this->form_validation->set_rules('savivaldybe', 'Savivaldybė', 'required');
            
            if ($this->form_validation->run() == FALSE) {
                if ($this->ion_auth->is_admin())
		{
		     $this->load->view('template/template_admin', $data);
		} 
		elseif ($this->ion_auth->in_group('superUser')) 
		{
		     $this->load->view('template/template_superUser', $data);
		}
		elseif ($this->ion_auth->in_group('user'))
		{
		     $this->load->view('template/template_user', $data);
		}
            } else {
                $dataResidence = array(
                    'miestasarkaimas' => $this->input->post('residence'),
                    'savivaldybeid' => $this->input->post('savivaldybe'),
                );
                $this->residence->updateResidence($id, $dataResidence);
                redirect('/general/residences/seeResidence', 'refresh');
            }
        
    }

    function delete_residence($id)
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {           
            redirect('auth', 'refresh');
        }
            $this->residence->deleteResidence($id);
            redirect('/general/residences/seeResidence', 'refresh');
    }
    

}


