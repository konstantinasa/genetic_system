<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Search extends CI_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->model('general/searchPatients');
		$this->load->library("pagination");
        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
    }
	public function showSearchPage($patients=array(),$infoquery="") {
		$session_data = $this->session->userdata;
		$roles = $session_data['identity'];
		$data['username'] = $session_data['username'];
                
		if (!$this->ion_auth->logged_in()) {
		redirect('auth', 'refresh');
		}
                
		$qtags=$this->retrieveTags();
		$data = array(
                'title' => "Platforma vietinių gyventojų genomo duomenų bazei",
                'firstFolder' => "general/view",
                'main_content' => "seeSearchPatientWindow",
                'username' => $session_data['username'],
				'patients' => $patients,
				'infoquery' => $infoquery,
				'qtags' =>$qtags
            );
		if ($this->ion_auth->is_admin())
		{
		     $this->load->view('template/template_admin', $data);
		} 
		elseif ($this->ion_auth->in_group('superUser')) 
		{
		     $this->load->view('template/template_superUser', $data);
		}
		elseif ($this->ion_auth->in_group('user'))
		{
		     $this->load->view('template/template_user', $data);
		}
	}
	
	public function createDropDown($table,$info) 
	{
		echo form_label('Reikšmė',$info);
		$answer=$this->searchPatients->retrieveTable($table);
		if ($answer[0]==0)
		{
			$answer=$answer[1];
			$result=array('0' => '');
			foreach ($answer as $row) {
				$result[$row->id]=$row->shown;
			}
			$options="id=$info class='form-control'";
			echo form_dropdown($info, $result,0, $options);
		} 
		else {
			$options="id=$info class='form-control'";
			echo form_dropdown($info, $answer[1],0, $options);
		}
	}
	
	public function retrieveTags($JOP=0) {
		$answer=$this->searchPatients->getAllTags();
		if ($JOP==1) {
			foreach($answer as $row)
			{
				echo $row->id.":";
				echo $row->ktipas.";";
			}
		} 
		else if ($JOP==2) {
			foreach($answer as $row)
				{
					echo $row->id.":";
					echo $row->m2n.";";
				}
		}
		else {
		return $answer;
		}
	}
	
	public function searchPlaceholder() 
	{
		function multineedle_stripos($haystack, $needles, $offset=0) 
		{
			foreach($needles as $needle) {
				$found[$needle] = stripos($haystack, $needle, $offset);
			}
			return $found;
		}
		
		
		$upto=$this->input->post('rowcount');
		$searchterms=array();
		for ($i=0;$i<=$upto;$i++) {
			$item="search".$i;
			$tmp=array( 'search' => $this->input->post($item));
			$item="reiksme".$i;
			$tmp['reiksme']=$this->input->post($item);
			$searchterms[$i]=$tmp;
		}
		echo "searchterms:";
		var_dump($searchterms);
		echo "<br>";
		
		$genstr="";
		foreach ($searchterms as $term) {
			$paieska=$term['search'];
			if ($paieska!=0) {
				$genstr=$genstr." and ";
				$answer=$this->searchPatients->getptr($paieska);
				
				$zenklas="";
				$reiksme=$term['reiksme'];
				$tipas=$answer[0]->ktipas;
				
				$needles=array('=','<','>');
				$poss=multineedle_stripos($reiksme,$needles);
				
				$reiksme=trim($reiksme,'<=>');
				
				if ($poss['<']===0) $zenklas=$zenklas."<";
				if ($poss['>']===0) $zenklas=$zenklas.">";
				if ($poss['=']===0) $zenklas=$zenklas."=";
				echo "<br>zenklas po poss: $zenklas";
				
				echo "<br>answer ";
				var_dump($answer);
				$pointer=$answer[0]->lenteles_vardas.".".$answer[0]->kintamasis;
				echo "pointer: ".$pointer;
				if ($answer[0]->kintamasis=="pacientas")
					{
						echo "<br>paciento subelementai:<br>";
						$subelements=explode(':',$reiksme);
						var_dump($subelements);
						if (sizeof($subelements) > 1) {
							$subelements[1]=trim($subelements[1],"'");
							$pointer="substring( ".$pointer." from ".$subelements[0]." for ".strlen($subelements[1])." )";
							$reiksme=$subelements[1];
							echo "<br>po viso ko pointer: $pointer";
							echo "<br>po viso ko reiksme: $reiksme";
						}
					}
				echo "<br>";
				if ($tipas=="2") {
					echo "tipas:1<br>";
					$zenklas=" like ";
					
				} else if ($tipas==="0") {
					echo "tipas:2<br>";
					$zenklas=""; 
				} else {
					echo "tipas:3<br>";
					if (strlen($zenklas)==0) $zenklas="=";
				}
				
				
				echo "<br>genstr".$genstr;
				echo "<br>pointer ".$pointer;
				echo "<br>zenklas ".$zenklas;
				echo "<br>reiksme: ".$reiksme;
				$tmp=substr($reiksme,0,1);
				if ((strlen($reiksme)>0) and ($tmp!="'")) $reiksme="'".$reiksme."'";
				
				$genstr=$genstr.$pointer.$zenklas.$reiksme;
				echo "<br>str: ".$genstr;
			}
		} 
		$aa=$this->searchPatients->fetchModified($genstr);
		$this->showSearchPage($aa);
	}
	
	public function searchGQuery() 
	{
		function multineedle_stripos($haystack, $needles, $offset=0) 
		{
			foreach($needles as $needle) {
				$found[$needle] = stripos($haystack, $needle, $offset);
			}
			return $found;
		}

		$search = $this->input->post('search'); #search term
		$ignore=$this->input->post('ignore');	#toggle weather to ignore unknown tags
		
		$STokens=explode(" ",$search);
		#Kiekvienas $STokens elementas susidarys is tago kuri reikia pakeist, <>= zenklu, vertes
		$sel=" and ";
		var_dump($STokens);
		$elie=array();
		foreach($STokens as $token) 
		{
			$parser="";
			echo "<br>===================================<br>token: ".$token."<br>";
			
			$needles=array('=','<','>');
			$poss=multineedle_stripos($token,$needles);
			if ($poss['<']) $parser=$parser."<";
			if ($poss['>']) $parser=$parser.">";
			if ($poss['=']) $parser=$parser."=";
			
			if ($parser=="") $parser=" ";
			
			$elements=explode($parser,$token);
			echo "parser: ".$parser."<br>"."elements:";
			print_r($elements);
			#$elements[0]=	tag
			#$parser 		the parser
			#$elements[1]=	value


			$tagdata=$this->searchPatients->gettagval($elements[0]);
			if (!empty($tagdata)) {
				$tagdata=$tagdata[0]; #questionable
				$pointer=$tagdata->pointer;
				echo "<br>pointer:".$pointer."<br>";
				echo "type:".$tagdata->tipas."<br>";
				
				if ($elements[0]=='pacientas')
				{
					echo "<br>paciento subelementai:<br>";
					$subelements=explode(':',$elements[1]);
					var_dump($subelements);
					if (sizeof($subelements) > 1) {
						$subelements[1]=trim($subelements[1],"'");
						$pointer="substring( ".$pointer." from ".$subelements[0]." for ".strlen($subelements[1])." )";
						$elements[1]=$subelements[1];
					}
				}
			
				
				$sep="";
				if (!isset($elements[1])) $elements[1]="''";
				if ($tagdata->tipas=="string") {
					echo "tipas:1<br>";
					$sep=" like ";
					if (!(substr($elements[1],0,1)=="'") )
					{
						$elements[1]="'".$elements[1]."'";
					}
				} else if ($tagdata->tipas==="0") {
					echo "tipas:2<br>";
					$sep=""; 
				} else {
					echo "tipas:3<br>";
					$sep=" ".$parser." ";
				}
				array_push($elie,$pointer.$sep.$elements[1]);
			}
			else if ($ignore)
			{
				echo "<br>Was Empty<br>";
				echo $token."<br>";
				var_dump($tagdata);
			}
			else 
			{
				if (!isset($elements[1])) $elements[1]="";
				array_push($elie,$elements[0].$parser.$elements[1]);
			}
		} 
		
		echo "elie:";
		var_dump($elie);
		echo "<br>";
		$el=array_shift($elie);
		$comp=0;
		while ($el)
		{
			if (!(($el=="and") or ($el=="or"))) #if current element isn't and/or
			{
				if ($comp===1) #if the last element also wasn't and/or add an and
				{ 
					$sel=$sel." and ";
				} 
				else  #last element was and/or, change comp to 1 so that we'd know that this element isn't and/or
				{
					$comp=1;
				}
			}
			else 
			{
				$comp=0;
			}
			$sel=$sel." ".$el." ";
			$el=array_shift($elie);
		}
		echo "<br>select * from * where * ".$sel."<br>";
		
		$data = array(
			'title' => "Platforma vietiniЕі gyventojЕі genomo duomenЕі bazei",
			'firstFolder' => "general",
			'secondFolder' => "view",
			'main_content' => "seeSearchPatientWindow"
		);
		$aa=$this->searchPatients->fetchModified($sel);
		echo "<br>output:<br>";
		$info="";
		foreach($STokens as $token) {
			$info=$info." ".$token;
		}
		$this->showSearchPage($aa,$info);
	}
	function appendSearchbar($itt) {
	
		echo '<div class="form-group col-md-6">';
		echo form_label('Paieška:', 'search'.$itt);
		$qtags=$this->retrieveTags();
		$TagsQuestions=array('0' => '');
		foreach ($qtags as $tag) {
			$TagsQuestions[$tag->id]=$tag->shown;
		}
		$options='id="search".$itt class="form-control" onchange="clickening(id)"';
		echo form_dropdown('search'.$itt, $TagsQuestions,0, $options);
		echo '</div>';
		
		echo '<div style="margin-top:-5px;" class="col-md-4">';
		echo form_label('Reikšmė:', 'tautybe');
		$dataNationality = array(
			'0' => '-'
		);
		echo form_dropdown('tautybe', $dataNationality, set_value('tautybe') === null ? set_value('tautybe') : isset($this->session->userdata['step_1']['tautybe']) ? $this->session->userdata['step_1']['tautybe'] :  set_value('tautybe'), 'id="reiksme"', 'class="form-control"');
		echo '</div>';
		echo '	<input for="" type="image" onclick="loadXMLDoc(); return false;" style="width:25px; height:25px; margin-top:-15px;" id="myimage" src="http://networkinterstateco.com/wp-content/uploads/plus.png" />';
		echo '</div>';
		#$inscript='{$("#reiksme'.$itt.'")';
		#echo "<script>";
		#echo "$(document).ready(function ()";
		#echo $inscript.".select2({width: '30%'});});";
		#echo "</script>";
	}

	
}


