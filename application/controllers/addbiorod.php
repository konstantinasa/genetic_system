<?php

class Addbiorod extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->helper(array('url','language', 'form'));
        $this->load->model('general/aboutPatient');
        $this->load->model('general/otherInformation');
        $this->load->library("pagination");
        
        $this->load->database();
        $this->load->library(array('ion_auth','form_validation'));

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

    }





function add_bio_rod($id){    
         if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {             
            redirect('auth', 'refresh');
        }
            $data = array(
                'title' => "Platforma vietiniЕі gyventojЕі genomo duomenЕі bazei",
                'firstFolder' => "",
                'main_content' => "createSveikatosRodikliai",
                'username' => $this->session->userdata('username')
            );
            $this->form_validation->set_rules('sveikat_rod[]', 'Sveikatos rodikliai', 'required|numeric');
            $data["resultSvaikatosRod"] = $this->otherInformation->allSveikatosRod();
//            $apie_pac_id = $this->id;
             if ($this->form_validation->run() == FALSE) {
                if ($this->ion_auth->is_admin())
		{
		     $this->load->view('template/template_admin', $data);
		} 
		elseif ($this->ion_auth->in_group('superUser')) 
		{
		     $this->load->view('template/template_superUser', $data);
		}
		elseif ($this->ion_auth->in_group('user'))
		{
		     $this->load->view('template/template_user', $data);
		}
            } else {
                 $step_5 = array(
                    'sveikatosrodikliaiinfoid' => $this->input->post('sveikat_rod')
                );
                 for ($i = 0; $i < count($step_5['sveikatosrodikliaiinfoid']); $i++){
                     if($step_5['sveikatosrodikliaiinfoid'][$i] == -1){
                         $step_5['sveikatosrodikliaiinfoid'][$i] = 'NULL';
                     }   
                 }
                  $this->session->set_userdata('step_5', $step_5);
               $this->aboutPatient->insert_sveikatos_rodikliai($id, $step_5);
               redirect('/general/aboutPatients/seeAboutPatient', 'refresh');
            } 
     }
     
}    
 ?>