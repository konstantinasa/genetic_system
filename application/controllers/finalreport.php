<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Finalreport extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->database();
        $this->load->helper(array('url', 'language'));
        $this->load->library("pagination");
        $this->load->model('finalreport_model');
        $this->load->model('sampleid_model');
        $this->load->model('script_model');
        $this->lang->load('include');
        $this->lang->load('genetic_file');
        $this->lang->load('auth');
    }

    protected $groupSU = 'superUser';

    function display($query_id = 0, $sort_by = 'sampleId', $sort_order = 'asc', $offset = 0)
    {
        $templateDataSU = 'template/template_superUser';
        $data = array('firstFolder' => "genetic_file",
            'main_content' => "finalreport");
        if (!$this->ion_auth->logged_in() || $this->ion_auth->in_group($this->groupSU)) {
            $data['username'] = $this->session->userdata('username');
            $limit = 500;
            $data['fields'] = array(
                'trumpinys' => $this->lang->line('finalReport_sampleId'),
                'chr' => $this->lang->line('finalReport_chromosome'),
                'position' => $this->lang->line('finalReport_position'),
                'snpname' => $this->lang->line('finalReport_snpName'),
                'allele1design' => $this->lang->line('finalReport_allele1design'),
                'allele2design' => $this->lang->line('finalReport_allele2design'),
                'genes' => $this->lang->line('finalReport_genes'),
                'inexon' => $this->lang->line('finalReport_inExon'),
                'mutations' => $this->lang->line('finalReport_mutations')
            );

            $this->sampleid_model->load_query($query_id);

            $query_array = array(
                'trumpinys' => $this->input->get('trumpinys')
            );

            $data['query_id'] = $query_id;

            $results = $this->finalreport_model->search($query_array, $limit, $offset, $sort_by, $sort_order);

            $data['finalreports'] = $results['rows'];
            $data['num_results'] = $results['num_rows'];

            $config = array();
            $config['base_url'] = site_url("finalreport/display/$query_id/$sort_by/$sort_order");
            $config['total_rows'] = $data['num_results'];
            $config['per_page'] = $limit;
            $config['uri_segment'] = 6;
            $this->pagination->initialize($config);
            $data['pagination'] = $this->pagination->create_links();

            $data['sort_by'] = $sort_by;
            $data['sort_order'] = $sort_order;

            $data['sampleid_options'] = $this->sampleid_model->sampleid_options();

            $this->_render_page($templateDataSU, $data);
        } else {
            redirect('auth', 'refresh');
        }
    }

    function search()
    {
        if (!$this->ion_auth->logged_in() || $this->ion_auth->in_group($this->groupSU)) {

            $query_array = array(
                'trumpinys' => $this->input->post('sampleId'),
            );
            $query_id = $this->sampleid_model->save_query($query_array);

            redirect("finalreport/display/$query_id");
        } else {
            redirect('auth', 'refresh');
        }
    }

    function finalReportCsv()
    {
        if (!$this->ion_auth->logged_in() || $this->ion_auth->in_group($this->groupSU)) {
            $sampleName = $this->input->post("sampleId");
            $this->script_model->exportFinalReport($sampleName);
            redirect('finalreport/display', 'refresh');
        } else {
            redirect('auth', 'refresh');
        }
    }


    function _render_page($templateData, $data = null, $render = false)
    {

        $this->viewdata = (empty($data)) ? $this->data : $data;

        $view_html = $this->load->view($templateData, $this->viewdata, $render);

        if (!$render) return $view_html;
    }


}