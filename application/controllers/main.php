<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->database();
        $this->load->helper(array('url', 'language'));
        $this->load->library("pagination");
        $this->load->model('main_model');
        $this->lang->load('include');
        $this->lang->load('main');
        $this->lang->load('auth');
    }

    protected $groupSU = 'superUser';

    function mains()
    {
        $templateDataSU = 'template/template_superUser';
        $data = array('firstFolder' => "genetic_file",
            'main_content' => "main");
        $config = array();
        $config["base_url"] = base_url() . "/main/mains";
        $config["total_rows"] = $this->main_model->record_count();
        $config["per_page"] = 50;
        $config["uri_segment"] = 3;
        
        $data['username'] = $this->session->userdata('username');
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data["resultMainTable"] = $this->main_model->fetch_main($config["per_page"], $page);
            $data["links"] = $this->pagination->create_links();
            $data["total_rows"] = $this->main_model->record_count();
        
        
        if ($this->ion_auth->is_admin())
		{
		     $this->load->view('template/template_admin', $data);
		} 
		elseif ($this->ion_auth->in_group('superUser')) 
		{
		     $this->load->view('template/template_superUser', $data);
		}
		elseif ($this->ion_auth->in_group('user'))
		{
		     $this->load->view('template/template_user', $data);
		}
            
            
         else {
            redirect('auth', 'refresh');
        }
    }

    function create_main()
    {
        $templateDataSU = 'template/template_superUser';
        $data = array('firstFolder' => "genetic_file",
            'main_content' => "create_main");
        if (!$this->ion_auth->logged_in() || $this->ion_auth->in_group($this->groupSU)) {
            $data['username'] = $this->session->userdata('username');
            $dataUserId = $this->session->userdata('user_id');
            $data['resultsAboutPatient'] = $this->main_model->getAllPatient();
            $this->form_validation->set_rules('patient', 'Paciento Id', 'required|numeric');
            if ($this->form_validation->run() == TRUE) {
                $dataMainTable = array(
                    'vartotojoid' => $dataUserId,
                    'pacientasid' => $this->input->post('patient')
                );
                $this->main_model->create_mainTable($dataMainTable);
                redirect("main/mains", 'refresh');
            } else {
                $this->_render_page($templateDataSU, $data);
            }
        } else {
            redirect('auth', 'refresh');
        }
    }

    function delete_main($id)
    {
        if (!$this->ion_auth->logged_in() || $this->ion_auth->in_group($this->groupSU)) {
            $this->main_model->delete_main($id);

            redirect('main/mains', 'refresh');
        } else {
            redirect('auth', 'refresh');
        }

    }

    function _render_page($templateData, $data = null, $render = false)
    {

        $this->viewdata = (empty($data)) ? $this->data : $data;

        $view_html = $this->load->view($templateData, $this->viewdata, $render);

        if (!$render) return $view_html;
    }


}