<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Script extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->database();
        $this->load->helper(array('url', 'language'));
        $this->load->model('script_model');
        $this->lang->load('include');
        $this->lang->load('script');
        $this->lang->load('auth');
    }

    protected $groupSU = 'superUser';

    function Scripts()
    {
        $templateDataSU = 'template/template_superUser';
        $data = array('firstFolder' => "genetic_file",
            'main_content' => "script");
        if (!$this->ion_auth->logged_in() || $this->ion_auth->in_group($this->groupSU)) {
            $data['username'] = $this->session->userdata('username');
            $data['resultsSampleId'] = $this->script_model->sampleId_options();
            $data['resultsFile'] = $this->script_model->file_options();
            $this->_render_page($templateDataSU, $data);
        } else {
            redirect('auth', 'refresh');
        }
    }

    function scripts_copyFile()
    {
        if (!$this->ion_auth->logged_in() || $this->ion_auth->in_group($this->groupSU)) {
            $this->script_model->copyFile();
            redirect('script/Scripts', 'refresh');
        } else {
            redirect('auth', 'refresh');
        }
    }

    function script_quantiSnp()
    {
        if (!$this->ion_auth->logged_in() || $this->ion_auth->in_group($this->groupSU)) {
            $this->form_validation->set_rules('file', 'Failas', 'required');
            $this->form_validation->set_rules('sampleId', 'Sample ID', 'required');
            if ($this->form_validation->run() == FALSE) {
                    redirect('script/Scripts', 'refresh');
            } else {
                $fileName = $this->input->post('file');
                $sampleId = $this->input->post('sampleId');
                $this->script_model->insertQuantiSnp($fileName, $sampleId);
                redirect('script/Scripts', 'refresh');
            }
        } else {
            redirect('auth', 'refresh');
        }
    }

    function script_pennCnv()
    {
        if (!$this->ion_auth->logged_in() || $this->ion_auth->in_group($this->groupSU)) {
            $this->form_validation->set_rules('file', 'Failas', 'required');
            $this->form_validation->set_rules('sampleId', 'Sample ID', 'required');
            if ($this->form_validation->run() == FALSE) {
                redirect('script/Scripts', 'refresh');
            } else {
                $fileName = $this->input->post('file');
                $sampleId = $this->input->post('sampleId');
                $this->script_model->insertPennCnv($fileName, $sampleId);
                redirect('script/Scripts', 'refresh');
            }
        } else {
            redirect('auth', 'refresh');
        }
    }

    function script_cnvPart()
    {
        if (!$this->ion_auth->logged_in() || $this->ion_auth->in_group($this->groupSU)) {
            $this->form_validation->set_rules('file', 'Failas', 'required');
            $this->form_validation->set_rules('sampleId', 'Sample ID', 'required');
            if ($this->form_validation->run() == FALSE) {
                redirect('script/Scripts', 'refresh');
            } else {
                $fileName = $this->input->post('file');
                $sampleId = $this->input->post('sampleId');
                $this->script_model->insertCnvPart($fileName, $sampleId);
                redirect('script/Scripts', 'refresh');
            }
        } else {
            redirect('auth', 'refresh');
        }
    }

    function script_finalReport()
    {
        if (!$this->ion_auth->logged_in() || $this->ion_auth->in_group($this->groupSU)) {
            $this->form_validation->set_rules('file', 'Failas', 'required');
            $this->form_validation->set_rules('sampleId', 'Sample ID', 'required');
            if ($this->form_validation->run() == FALSE) {
                redirect('script/Scripts', 'refresh');
            } else {
                $fileName = $this->input->post('file');
                $sampleId = $this->input->post('sampleId');
                $this->script_model->insertFinalReport($fileName, $sampleId);
                redirect('script/Scripts', 'refresh');
            }
        } else {
            redirect('auth', 'refresh');
        }
    }

    function script_indel()
    {
        if (!$this->ion_auth->logged_in() || $this->ion_auth->in_group($this->groupSU)) {
            $this->form_validation->set_rules('file', 'Failas', 'required');
            $this->form_validation->set_rules('sampleId', 'Sample ID', 'required');
            if ($this->form_validation->run() == FALSE) {
                redirect('script/Scripts', 'refresh');
            } else {
                $fileName = $this->input->post('file');
                $sampleId = $this->input->post('sampleId');
                $this->script_model->insertIndel($fileName, $sampleId);
                redirect('script/Scripts', 'refresh');
            }
        } else {
            redirect('auth', 'refresh');
        }
    }

    function script_snp()
    {
        if (!$this->ion_auth->logged_in() || $this->ion_auth->in_group($this->groupSU)) {
            $this->form_validation->set_rules('file', 'Failas', 'required');
            $this->form_validation->set_rules('sampleId', 'Sample ID', 'required');
            if ($this->form_validation->run() == FALSE) {
                redirect('script/Scripts', 'refresh');
            } else {
                $fileName = $this->input->post('file');
                $sampleId = $this->input->post('sampleId');
                $this->script_model->insertSnp($fileName, $sampleId);
                redirect('script/Scripts', 'refresh');
            }
        } else {
            redirect('auth', 'refresh');
        }
    }

    function scripts_RmFile()
    {
        if (!$this->ion_auth->logged_in() || $this->ion_auth->in_group($this->groupSU)) {
            $this->script_model->rmFile();
            redirect('script/Scripts', 'refresh');
        } else {
            redirect('auth', 'refresh');
        }
    }

    function _render_page($templateData, $data = null, $render = false)
    {

        $this->viewdata = (empty($data)) ? $this->data : $data;

        $view_html = $this->load->view($templateData, $this->viewdata, $render);

        if (!$render)
            return $view_html;
    }

}
