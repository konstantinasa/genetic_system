<?php defined('BASEPATH') OR exit('No direct script access allowed');

class File extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->database();
        $this->load->helper(array('url', 'language'));
        $this->load->library("pagination");
        $this->load->model('file_model');
        $this->load->model('sampleid_model');
        $this->lang->load('include');
        $this->lang->load('file');
        $this->lang->load('auth');
    }

    protected $groupSU = 'superUser';
    protected $uploadDir = '/var/www/html/upload_file';

    function display($query_id = 0, $sort_by = 'sampleId', $sort_order = 'asc', $offset = 0)
    {
        $templateDataSU = 'template/template_superUser';
        $data = array('firstFolder' => "genetic_file",
            'main_content' => "file");
        if (!$this->ion_auth->logged_in() || $this->ion_auth->in_group($this->groupSU)) {
            $data['username'] = $this->session->userdata('username');
            $limit = 120;
            $data['fields'] = array(
                'trumpinys' => $this->lang->line('file_sampleId'),
                'failopavadinimas' => $this->lang->line('file_fileName'),
                'ikelimodata' => $this->lang->line('file_insertTime')
            );

            $this->sampleid_model->load_query($query_id);

            $query_array = array(
                'trumpinys' => $this->input->get('trumpinys')
            );

            $data['query_id'] = $query_id;

            $results = $this->file_model->search($query_array, $limit, $offset, $sort_by, $sort_order);

            $data['files'] = $results['rows'];
            $data['num_results'] = $results['num_rows'];

            $config = array();
            $config['base_url'] = site_url("file/display/$query_id/$sort_by/$sort_order");
            $config['total_rows'] = $data['num_results'];
            $config['per_page'] = $limit;
            $config['uri_segment'] = 6;
            $this->pagination->initialize($config);
            $data['pagination'] = $this->pagination->create_links();

            $data['sort_by'] = $sort_by;
            $data['sort_order'] = $sort_order;

            $data['sampleid_options'] = $this->sampleid_model->sampleid_options();

            $this->_render_page($templateDataSU, $data);
        } else {
            redirect('auth', 'refresh');
        }
    }

    function search()
    {
        if (!$this->ion_auth->logged_in() || $this->ion_auth->in_group($this->groupSU)) {

            $query_array = array(
                'trumpinys' => $this->input->post('sampleId'),
            );
            $query_id = $this->sampleid_model->save_query($query_array);

            redirect("file/display/$query_id");
        } else {
            redirect('auth', 'refresh');
        }
    }

    function fileUpload()
    {
        $templateDataSU = 'template/template_superUser';
        $data = array('firstFolder' => "genetic_file",
            'main_content' => "file_upload");
        if (!$this->ion_auth->logged_in() || $this->ion_auth->in_group($this->groupSU)) {
            $data['username'] = $this->session->userdata('username');
            $data['resultsSampleId'] = $this->file_model->getAllSample();
            $config['upload_path'] = $this->uploadDir;
            $config['overwrite'] = FALSE;
            $config['allowed_types'] = 'txt|csv';
            $this->load->library('upload', $config);
            $this->form_validation->set_rules('sampleId', 'Sample ID', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->_render_page($templateDataSU, $data);
                return false;
            }
            if (($data_files_count = $this->upload->do_multi_upload("uploadFile"))) {
                $data_files = "";
                for ($i = 0; $i < count($data_files_count); $i++) {
                    if ($data_files_count[$i]['file_name'] != "") {
                        $data_files .= $data_files_count[$i]['file_name'] . "\n";
                    }
                }
                $dataFile['failopavadinimas'] = $data_files;
                $dataFile['pagrindineslentelesid'] = $this->input->post('sampleId');
                $this->file_model->insertFile($dataFile);
                redirect('/file/display', 'refresh');
            } else {
                echo $this->upload->display_errors();
            }
        } else {
            redirect('auth', 'refresh');
        }
    }

    function fileDownload()
    {
        $templateDataSU = 'template/template_superUser';
        $data = array('firstFolder' => "genetic_file",
            'main_content' => "file_download");
        if (!$this->ion_auth->logged_in() || $this->ion_auth->in_group($this->groupSU)) {
            $data['username'] = $this->session->userdata('username');
            $data['resultFileDownload'] = $this->file_model->downloadFile();
            $this->_render_page($templateDataSU, $data);
        } else {
            redirect('auth', 'refresh');
        }
    }

   public function fileDownloadRm($fileName)
    {
        if (!$this->ion_auth->logged_in() || $this->ion_auth->in_group($this->groupSU)) {
            unlink('downloadExport/'.$fileName);
            redirect('/file/fileDownload', 'refresh');
        } else {
            redirect('auth', 'refresh');
        }
    }

    function _render_page($templateData, $data = null, $render = false)
    {

        $this->viewdata = (empty($data)) ? $this->data : $data;

        $view_html = $this->load->view($templateData, $this->viewdata, $render);

        if (!$render) return $view_html;
    }

}