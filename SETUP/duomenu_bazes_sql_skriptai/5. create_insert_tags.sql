create table ktipas (
	id serial primary key,
	tipas varchar(50) unique not null
);
insert into ktipas values (1, 'integer');
insert into ktipas values (2, 'string');
insert into ktipas values (3, 'date');

create table lenlistas (
	id serial primary key,
	lenteles_vardas varchar(50) unique not null
);
insert into lenlistas values (1,'meginys');
insert into lenlistas values (2,'pac');
insert into lenlistas values (3,'ap');
insert into lenlistas values (4,'kmi');
insert into lenlistas values (5,'taut');
insert into lenlistas values (6,'iss');
insert into lenlistas values (7,'aps');--

insert into lenlistas values (8,'sav');--
insert into lenlistas values (9,'miest');--
insert into lenlistas values (10,'uz');
insert into lenlistas values (11,'maistas');
insert into lenlistas values (12,'srg');
insert into lenlistas values (13,'kokyb');
insert into lenlistas values (14,'srginfo');
insert into lenlistas values (15,'aler');
insert into lenlistas values (16,'srgveziu');
insert into lenlistas values (17,'svoriokit');
insert into lenlistas values (18,'svarbu_chol_info');

insert into lenlistas values (19,'chol_korekcija');
insert into lenlistas values (20,'pacinf');
insert into lenlistas values (21,'lytis');

create table qtags (
	id serial primary key,
	nr int unique,
	lentele int not null,
	kintamasis varchar(50),
	ktipas int not null,
	m2n varchar(50) default null,
	kl varchar(150) unique
);


insert into qtags (nr,lentele,kintamasis,ktipas,kl) values (1,1,'trumpinys',2,'SampleID');
insert into qtags (nr,lentele,kintamasis,ktipas,kl) values (2,2,'pacientas',2,'Patient');
insert into qtags (nr,lentele,kintamasis,ktipas,kl) values (3,3,'tdata',3,'Tyrimo data');
insert into qtags (nr,lentele,kintamasis,ktipas,m2n,kl) values (4,2,'lytis',1,'lytis','Lytis');
insert into qtags (nr,lentele,kintamasis,ktipas,kl) values (5,2,'gdata',3,'Gimimo data (metai, menuo, diena)');
insert into qtags (nr,lentele,kintamasis,ktipas,kl) values (6,3,'ugis',1,'ugis	(cm)');
insert into qtags (nr,lentele,kintamasis,ktipas,kl) values (7,3,'svoris',1,'Svoris	(kg)');
insert into qtags (nr,lentele,kintamasis,ktipas,kl) values (8,4,'kmi',1,'KMI');
insert into qtags (nr,lentele,kintamasis,ktipas,m2n,kl) values (9,2,'tautybe',1,'tautybe','Tautybe');
insert into qtags (nr,lentele,kintamasis,ktipas,m2n,kl) values (10,3,'issilavinimas',1,'issilavinimas','Issilavinimas');
--11 nera kl gyvenamoji vieta
insert into qtags (nr,lentele,kintamasis,ktipas,m2n,kl) values (12,8,'apskritisid',1,'apskritis','Apskritis');--ar gerai miestai?
insert into qtags (nr,lentele,kintamasis,ktipas,m2n,kl) values (13,9,'savivaldybeid',1,'savivaldybe','Savivaldybe');
insert into qtags (nr,lentele,kintamasis,ktipas,m2n,kl) values (14,3,'miestasarkaimas',1,'miestasarkaimas','Miestas');
insert into qtags (nr,lentele,kintamasis,ktipas,m2n,kl) values (15,3,'uzimtumas',1,'uzimtumas','Uzimtumas');
insert into qtags (nr,lentele,kintamasis,ktipas,m2n,kl) values (16,11,'pasirinkimasid=1',0,'maistopasirinkimas','Pagrindinis kriterijus, pagal kuri Jus renkates maisto produktus yra sveikatos gerinimas (ligu profilaktika)');--sablonas --16-20 - maistas
insert into qtags (nr,lentele,kintamasis,ktipas,m2n,kl) values (17,11,'pasirinkimasid=2',0,'maistopasirinkimas','Pagrindinis kriterijus, pagal kuri Jus renkates maisto produktus yra specialios dietos butinumas');
insert into qtags (nr,lentele,kintamasis,ktipas,m2n,kl) values (18,11,'pasirinkimasid=3',0,'maistopasirinkimas','Pagrindinis kriterijus, pagal kuri Jus renkates maisto produktus yra kaina');
insert into qtags (nr,lentele,kintamasis,ktipas,m2n,kl) values (19,11,'pasirinkimasid=4',0,'maistopasirinkimas','Pagrindinis kriterijus, pagal kuri Jus renkates maisto produktus yra skonines savybes');
insert into qtags (nr,lentele,kintamasis,ktipas,m2n,kl) values (20,11,'pasirinkimasid=5',0,'maistopasirinkimas','Pagrindinis kriterijus, pagal kuri Jus renkates maisto produktus yra seimos nariu itaka');
--21-24 srg duplikatai
insert into qtags (nr,lentele,kintamasis,ktipas,m2n,kl) values (25,12,'srginfoid=5',0,'srginfo','Osteoporoze ');
--26-27 srg duplikatai
insert into qtags (nr,lentele,kintamasis,ktipas,m2n,kl) values (28,13,'kokybeinfoid=1',0,'kokybeinfo','Ar esate alergiskas tam tikriems maisto produktams?');
insert into qtags (nr,lentele,kintamasis,ktipas,m2n,kl) values (29,15,'alergiskaskamid',1,'alergiskaskam','Kam esate alergiskas?');
insert into qtags (nr,lentele,kintamasis,ktipas,kl) values (30,2,'gimimosvoris',1,'Gimimo svoris ');
insert into qtags (nr,lentele,kintamasis,ktipas,kl) values (31,2,'gimimougis',1,'Gimimo ugis ');
insert into qtags (nr,lentele,kintamasis,ktipas,m2n,kl) values (32,12,'srginfoid=1',0,'srginfo','Ar siuo metu sergate arba sirgote sirdies kraujagysliu ligomis (miokardo infarktu, sirdies ritmo sutrikimu, stenokardija.)');
insert into qtags (nr,lentele,kintamasis,ktipas,m2n,kl) values (33,12,'srginfoid=2',0,'srginfo','Ar siuo metu sergate arba sirgote padidejusiu kraujospudziu (hipertonija)');
insert into qtags (nr,lentele,kintamasis,ktipas,m2n,kl) values (34,12,'srginfoid=3',0,'srginfo','Ar siuo metu sergate arba sirgote uminiais galvos smegenu kraujotakos sutrikimais (insultu)');
insert into qtags (nr,lentele,kintamasis,ktipas,m2n,kl) values (35,12,'srginfoid=4',0,'srginfo','Ar siuo metu sergate arba sirgote cukralige (diabetu)');
insert into qtags (nr,lentele,kintamasis,ktipas,m2n,kl) values (36,12,'srginfoid=6',0,'srginfo','Ar siuo metu sergate arba sirgote nutukimu');
insert into qtags (nr,lentele,kintamasis,ktipas,m2n,kl) values (37,12,'srginfoid=7',0,'srginfo','Ar siuo metu sergate arba sirgote trumparegyste');
insert into qtags (nr,lentele,kintamasis,ktipas,m2n,kl) values (38,13,'kokybeinfoid=4',0,'kokybeinfo','Ar siuo metu sergate arba sirgote veziu ');
insert into qtags (nr,lentele,kintamasis,ktipas,m2n,kl) values (39,16,'srgveziukokiuid',1,'srgveziukokiu','Kokio tipo veziu sirgote?');-- ar gerai sitaas???
insert into qtags (nr,lentele,kintamasis,ktipas,m2n,kl) values (40,12,'srginfoid=8',0,'srginfo','Ar siuo metu sergate arba sirgote letinemis plauciu ligomis (astma, letine obstrukcine plauciu liga)');
insert into qtags (nr,lentele,kintamasis,ktipas,m2n,kl) values (41,12,'srginfoid=9',0,'srginfo','Ar siuo metu sergate arba sirgote depresija, polinkiu i depresiskuma');
insert into qtags (nr,lentele,kintamasis,ktipas,m2n,kl) values (42,12,'srginfoid=10',0,'srginfo','Ar siuo metu sergate arba sirgote alkoholizmu');
insert into qtags (nr,lentele,kintamasis,ktipas,m2n,kl) values (43,12,'srginfoid=11',0,'srginfo','Ar siuo metu sergate arba sirgote odos ligomis  (zvyneline, dermatitai)');
insert into qtags (nr,lentele,kintamasis,ktipas,m2n,kl) values (44,13,'kokybeinfoid=3',0,'kokybeinfo','Ar sergate kitomis ligomis?');
insert into qtags (nr,lentele,kintamasis,ktipas,m2n,kl) values (45,12,'srginfoid',1,'srginfo','Kokiomis?');--buvo blogai 100%. Dbr ieskos is visu. Didelio skirtumo ner.
--plius jie turi patys susivesti duomenis, jei nori ligu naujos sistemos ir turi surasti atitikmenis jau dabar esanciom ligom.
insert into qtags (nr,lentele,kintamasis,ktipas,m2n,kl) values (46,13,'kokybeinfoid=2',0,'kokybeinfo','Arserga');
insert into qtags (nr,lentele,kintamasis,ktipas,kl) values (47,3,'tiriamliemapim',1,'Jusu liemens apimtis (cm)');
insert into qtags (nr,lentele,kintamasis,ktipas,m2n,kl) values (48,13,'kokybeinfoid=6',0,'kokybeinfo','Ar buvo svorio augimas/kritimas per pastaruosius metus?');
insert into qtags (nr,lentele,kintamasis,ktipas,m2n,kl) values (49,17,'svoriokitimasid=1 and kg',0,'svoriokitimas','Priaugau kg');--gali buti blogai
insert into qtags (nr,lentele,kintamasis,ktipas,kl) values (50,17,'perkiekmen',1,'Per menesius');
insert into qtags (nr,lentele,kintamasis,ktipas,m2n,kl) values (51,17,'svoriokitimasid=2 and kg',0,'svoriokitimas','Numeciau kg ');--gali buti blogai
insert into qtags (nr,lentele,kintamasis,ktipas,kl) values (52,17,'perkiekmen',1,'Per_menesius');
insert into qtags (nr,lentele,kintamasis,ktipas,m2n,kl) values (53,13,'kokybeinfoid=5',0,'kokybeinfo','Ar buvo kada nors nustatytas padidintas cholesterolio kiekis?');
insert into qtags (nr,lentele,kintamasis,ktipas,m2n,kl) values (54,18,'svarbucholinfoid=1 and arnustatyta',0,'svarbuchol','Ar buvo kada nors nustatyti serumo trigliceridai daugiau kaip 1,7 mmol/l  ');--gali buti blogai
insert into qtags (nr,lentele,kintamasis,ktipas,m2n,kl) values (55,18,'svarbucholinfoid=2 and arnustatyta',0,'svarbuchol','Ar buvo kada nors nustatyti serumo DTLT (didelio tankio lipoproteinai) daugiau kaip 1,03 mmol/l vyrams  daugiau kaip 1,3 mmol/l moterims');--gali buti blogai
insert into qtags (nr,lentele,kintamasis,ktipas,m2n,kl) values (56,19,'metodonr',1,'cholkorekcijainfo','Ar cholesterolis buvo koreguojamas kuriuo nors is siu metodu?');
insert into qtags (nr,lentele,kintamasis,ktipas,m2n,kl) values (57,20,'fizinisaktyvum',1,'fizinisaktyvuminfo','Ar esate pakankamai fiziskai aktyvus? ');
insert into qtags (nr,lentele,kintamasis,ktipas,kl) values (58,20,'aksistnenorm',1,'Ar Jusu kraujospudis sistolinis (daugiau kaip  130 mmHg) ');
insert into qtags (nr,lentele,kintamasis,ktipas,kl) values (59,20,'aksdiastnenm',1,'Ar Jusu kraujospudis diastolinis (maziau kaip 85 mmHg)');
insert into qtags (nr,lentele,kintamasis,ktipas,m2n,kl) values (60,20,'kveptkligdaz',1,'kveptkligdaz','Kaip daznai sergate kvepavimo taku ligomis');


