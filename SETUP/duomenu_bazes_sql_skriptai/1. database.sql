create table pacientas (
	id serial primary key,
/*	meginioid varchar(8) unique not null,  sukuriau viewa: meginys  */
	pacientas char(27) unique not null,
	lytis int check (lytis in (2, 3, 4)),  /* 3 - vyras, 4 - moteris   */
	tautybe int,
	gdata date,
	gimimosvoris numeric(5,3) check ((gimimosvoris > 0) and (gimimosvoris < 99)),
	gimimougis int check ((gimimougis > 0) and (gimimougis < 100))
);

create table apiepacienta (
	id serial primary key,
	anketosversija int,
	tdata date, 
	issilavinimas int,
	miestasarkaimas int,
	uzimtumas int,
	ugis int check ((ugis >= 1) and (ugis <= 300)),
	svoris numeric(6,3) check ((svoris > 0) and (svoris < 999)),
	tiriamliemapim int check ((tiriamliemapim > 0) and (tiriamliemapim < 999)),
	pacientasid int not null
);

create table anketosversija (
	id serial primary key,
	projektasid int not null,
	versija int not null default 1,
	unique (projektasid, versija)
);

create table projektas (
	id serial primary key,
	projektas varchar(30) not null unique
);

/* old version 04_14
create table kokybe (
	id serial primary key,
	aralergiskas int check (aralergiskas in(0,1,null)),
	arsrg int check (arsrg in(0,1,null)),
	arsrgkitom int check (arsrgkitom in(0,1,null)),
	arsrgveziu int check (arsrgveziu in(0,1,null)),
	arbuvocholes int check (arbuvocholes in (0,1,null)),
	arkitosvoris int check (arkitosvoris in (0,1,null)),
	gvietaarmiestas int check (gvietaarmiestas in (0,1,null)),
	apiepacientaid int not null
);
*/

create table kokybe (
	id serial primary key,
	apiepacientaid int not null,
	kokybeinfoid int not null,
	reiksme int check (reiksme in(0,1,null)),
	unique (apiepacientaid,kokybeinfoid)
);

create table kokybeinfo (
	id serial primary key,
	info varchar(30) unique not null
);

create table uzimtumas (
	id serial primary key,
	uzimtumas varchar(30) unique not null
);

create table issilavinimas (
	id serial primary key,
	issilavinimas varchar(30) unique not null
);

create table maistas (
	id serial primary key,
	pasirinkimasid int not null,
	apiepacientaid int not null,
	unique (pasirinkimasid,apiepacientaid)
);

create table maistopasirinkimas (
	id serial primary key,
	pasirinkimas varchar(40) unique not null
);

create table srg (
	id serial primary key,
	srginfoid int not null,
	apiepacientaid int,
	unique (srginfoid,apiepacientaid)
);

create table srginfo (
	id serial primary key,
	arpagrkategorija int not null check (arpagrkategorija in (0,1)),
	srginfo varchar(100) unique not null
);


create table alergiskas (
	id serial primary key,
	alergiskaskamid int not null,
	apiepacientaid int not null,
	unique (alergiskaskamid,apiepacientaid)
);

create table alergiskaskam (
	id serial primary key,
	alergiskaskam varchar(60) unique not null
);

create table srgveziu (
	id serial primary key,
	srgveziukokiuid int not null,
	apiepacientaid int not null,
	unique (srgveziukokiuid,apiepacientaid)
);

create table srgveziukokiu (
	id serial primary key,
	aprasas varchar(40) unique not null
);

create table svoriokit (
	id serial primary key,
	apiepacientaid int not null,
	svoriokitimasid int not null,
	kg int check ((kg >0) and (kg < 200)),
	perkiekmen int check (perkiekmen > 0),
	unique (apiepacientaid,svoriokitimasid)
);

create table svoriokitimas (
	id serial primary key,
	kitimas varchar(20) not null unique
);


create table sveikatosrodikliai (
	id serial primary key,
	apiepacientaid int not null,
	sveikatosrodikliaiinfoid int not null,
	reiksme numeric(10,5),
	unique (apiepacientaid,sveikatosrodikliaiinfoid)
);

create table sveikatosrodikliaiinfo (
	id serial primary key,
	pavadinimas varchar(30) unique not null
);

create table apskritis (
	id serial primary key,
	gyvapskritis varchar(40) unique not null
);

create table savivaldybe (
	id serial primary key,
	gyvsavivaldybe varchar(40) unique not null,
	apskritisid int not null
);

create table miestasarkaimas (
	id serial primary key,
	miestasarkaimas varchar(150) not null,
	savivaldybeid int not null
);

create table tautybe (
	id serial primary key,
	tautybe varchar(20) unique not null
);

create table pacientoinfo (
	id serial primary key,
	fizinisaktyvum int,
	akssistnenorm int,
	aksdiastnenm int,
	kveptkligdaz int,
	apiepacientaid int not null unique
);

create table svarbuchol (
	id serial primary key,
	pacientoinfoid int not null,
	svarbucholinfoid int not null,
	arnustatyta int not null check (arnustatyta in (0,1)),
	unique (pacientoinfoid,svarbucholinfoid)
);

create table svarbucholinfo (
	id serial primary key,
	rodiklis varchar(50) not null unique /* serumotag, serumodtl */
);

create table cholkorekcija (
	id serial primary key,
	metodonr int not null,
	pacientoinfoid int not null,
	unique (metodonr,pacientoinfoid)
);

create table cholkorekcijainfo (
	id serial primary key,
	metodas varchar(60) unique not null
);

create table fizinisaktyvuminfo (
	id serial primary key,
	aprasas varchar(60) unique not null
);

create table kveptkligdaz (
	id serial primary key,
	aprasas varchar(60) unique not null
);

create table reiksme (
	id int primary key,
	reiksme varchar(20) unique 
);

create view kmi as 
select id apiepacientaid, svoris, ugis, (svoris/((ugis::numeric/100)^2)) kmi
from apiepacienta;

create view meginys as 
select id::int pacientasid, (substring(pacientas from 1 for 4) || substring(pacientas from 5 for 4)::numeric(4))::varchar(8) trumpinys
from pacientas;

create table grupiuListas (
id varchar(4) primary key,
pavadinimas varchar (200) not null unique
);


create table ligos (
id serial primary key,
raide char(1),
skaicius char(2),
grupiuListasid varchar(4),
pavadinimas varchar(250)
--unique(raide,skaicius) neveikia siatas
);
create table lytis (
	id serial primary key,
	lyt varchar(10) unique not null
);

insert into lytis values (2,'nežinoma');
insert into lytis values (3,'vyras');
insert into lytis values (4,'moteris');

create table sergamumas (
id serial primary key,
apiepacientaid int not null,
ligosid int not null,
unique(apiepacientaid,ligosid)
);

alter table sergamumas add foreign key (apiepacientaid) references apiepacienta(id);
alter table sergamumas add foreign key (ligosid) references ligos(id);

alter table pacientas add foreign key (lytis) references lytis(id);

/*alter table ligos add foreign key (grupiuListasid) references grupiuListas(id);*/

alter table apiepacienta add foreign key (uzimtumas) references uzimtumas(id);

alter table maistas add foreign key (apiepacientaid) references apiepacienta(id);

alter table maistas add foreign key (pasirinkimasid) references maistopasirinkimas(id);

alter table srg add foreign key (apiepacientaid) references apiepacienta(id);

alter table srg add foreign key (srginfoid) references srginfo(id);

alter table svoriokit add foreign key (apiepacientaid) references apiepacienta(id);
alter table svoriokit add foreign key (svoriokitimasid) references svoriokitimas(id);

alter table sveikatosrodikliai add foreign key (apiepacientaid) references apiepacienta(id);
alter table sveikatosrodikliai add foreign key (sveikatosrodikliaiinfoid) references sveikatosrodikliaiinfo(id);

alter table pacientas add foreign key (tautybe) references tautybe(id);

alter table apiepacienta add foreign key (issilavinimas) references issilavinimas(id);

alter table pacientoinfo add foreign key (apiepacientaid) references apiepacienta(id);
alter table pacientoinfo add foreign key (kveptkligdaz) references kveptkligdaz(id);
alter table pacientoinfo add foreign key (fizinisaktyvum) references fizinisaktyvuminfo(id);

alter table pacientoinfo add foreign key (akssistnenorm) references reiksme(id);
alter table pacientoinfo add foreign key (aksdiastnenm) references reiksme(id);

/* bus kaip insertai i kokybesinfo
alter table kokybe add foreign key (aralergiskas) references reiksme(id);
alter table kokybe add foreign key (arsrgveziu) references reiksme(id);
alter table kokybe add foreign key (arsrgkitom) references reiksme(id);
alter table kokybe add foreign key (arsrg) references reiksme(id);
alter table kokybe add foreign key (arbuvocholes) references reiksme(id);
alter table kokybe add foreign key (arkitosvoris) references reiksme(id);
alter table kokybe add foreign key (gvietaarmiestas) references reiksme(id);
*/

alter table kokybe add foreign key (apiepacientaid) references apiepacienta(id);
alter table kokybe add foreign key (kokybeinfoid) references kokybeinfo(id);

alter table apiepacienta add foreign key (miestasarkaimas) references miestasarkaimas(id);


alter table svarbuchol add foreign key (arnustatyta) references reiksme(id);
alter table srginfo add foreign key (arpagrkategorija) references reiksme(id);

alter table alergiskas add foreign key (apiepacientaid) references apiepacienta(id);
alter table alergiskas add foreign key (alergiskaskamid) references alergiskaskam(id);

alter table srgveziu add foreign key (apiepacientaid) references apiepacienta(id);
alter table srgveziu add foreign key (srgveziukokiuid) references srgveziukokiu(id);

alter table savivaldybe add foreign key (apskritisid) references apskritis(id);
alter table miestasarkaimas add foreign key (savivaldybeid) references savivaldybe(id);


alter table cholkorekcija add foreign key (pacientoinfoid) references pacientoinfo(id);
alter table cholkorekcija add foreign key (metodonr) references cholkorekcijainfo(id);

alter table apiepacienta add foreign key (pacientasid) references pacientas(id);

alter table apiepacienta add foreign key (anketosversija) references anketosversija(id);
alter table anketosversija add foreign key (projektasid) references projektas(id);

alter table svarbuchol add foreign key (pacientoinfoid) references pacientoinfo(id);
alter table svarbuchol add foreign key (svarbucholinfoid) references svarbucholinfo(id);


/* ################################ genotipiniai failai, loginas, maintable. ################################################ */

CREATE TABLE "users" (
    "id" SERIAL NOT NULL,
    "ip_address" varchar(15),
    "username" varchar(100) NOT NULL,
    "password" varchar(255) NOT NULL,
    "salt" varchar(255),
    "email" varchar(100) NOT NULL,
    "activation_code" varchar(40),
    "forgotten_password_code" varchar(40),
    "forgotten_password_time" int,
    "remember_code" varchar(40),
    "created_on" int NOT NULL,
    "last_login" int,
    "active" int4,
    "first_name" varchar(50),
    "last_name" varchar(50),
  PRIMARY KEY("id"),
  CONSTRAINT "check_id" CHECK(id >= 0),
  CONSTRAINT "check_active" CHECK(active >= 0)
);

CREATE TABLE "users_groups" (
    "id" SERIAL NOT NULL,
    "user_id" integer NOT NULL,
    "group_id" integer NOT NULL,
  PRIMARY KEY("id"),
  CONSTRAINT "uc_users_groups" UNIQUE (user_id, group_id),
  CONSTRAINT "users_groups_check_id" CHECK(id >= 0),
  CONSTRAINT "users_groups_check_user_id" CHECK(user_id >= 0),
  CONSTRAINT "users_groups_check_group_id" CHECK(group_id >= 0)
);

CREATE TABLE "groups" (
    "id" SERIAL NOT NULL,
    "name" varchar(20) NOT NULL,
    "description" varchar(100) NOT NULL,
  PRIMARY KEY("id"),
  CONSTRAINT "check_id" CHECK(id >= 0)
);

CREATE TABLE pagrindineLentele(
  id SERIAL NOT NULL,
  ikelimoData TIMESTAMP NOT NULL DEFAULT NOW(),
  vartotojoId INTEGER NOT NULL,
  meginioId VARCHAR(20) NOT NULL,
  pacientasid INTEGER NOT NULL,
  PRIMARY KEY (id)
);



CREATE TABLE cnvPart (
	id SERIAL NOT NULL,
	chromosome VARCHAR (2),
	startPosition NUMERIC,
	endPosition NUMERIC,
	copyNumber NUMERIC,
	confidence NUMERIC,
	pagrindinesLentelesId INTEGER NOT NULL,
	PRIMARY KEY (id,pagrindinesLentelesId)
);

CREATE TABLE genotipavimo (
	id SERIAL NOT NULL,
	chr VARCHAR(2),
	position NUMERIC(16),
	snpName VARCHAR(16),
	allele1Design VARCHAR (1),
	allele2Design VARCHAR (1),
	bAlleleFreq NUMERIC,
	logRRatio NUMERIC,
	pagrindinesLentelesId INTEGER NOT NULL,
	PRIMARY KEY (id,pagrindinesLentelesId)
);






INSERT INTO groups (id, name, description) VALUES
    (1,'user','Vartotojas'),
    (2,'superUser','Pagerintas vartotojas'),
    (3,'admin','Administartorius');

INSERT INTO users (ip_address, username, password, salt, email, activation_code, forgotten_password_code, created_on, last_login, active, first_name, last_name) VALUES
    ('127.0.0.1','administrator','$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36','','admin@admin.com','',NULL,'1268889823','1268889823','1','Admin','istrator'),
    ('127.0.0.1','superUser','$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36','','superUser@admin.com','',NULL,'1268889823','1268889823','1','super','user'),
    ('127.0.0.1','user','$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36','','user@admin.com','',NULL,'1268889823','1268889823','1','user','user');;

INSERT INTO users_groups (user_id, group_id) VALUES
    (1,3),
    (2,2),
    (3,1);

CREATE TABLE "login_attempts" (
    "id" SERIAL NOT NULL,
    "ip_address" varchar(15),
    "login" varchar(100) NOT NULL,
    "time" int,
  PRIMARY KEY("id"),
  CONSTRAINT "check_id" CHECK(id >= 0)
);

CREATE TABLE pennCnv (
	id SERIAL NOT NULL,
	chromosome VARCHAR (2),
	startPosition NUMERIC,
	endPosition NUMERIC,
	noProbes NUMERIC,		
	length NUMERIC,
	state NUMERIC,
	copyNumber NUMERIC,
	startProbeId VARCHAR (50),		
	endProbeId VARCHAR (50),		
	confidence NUMERIC,
	pagrindinesLentelesId INTEGER,
	PRIMARY KEY (id,pagrindinesLentelesId)
);



CREATE TABLE indel (
	id SERIAL NOT NULL,
	chr VARCHAR(2),
	start NUMERIC,
	ends NUMERIC,
	ref VARCHAR(100),
	alt VARCHAR(100),
	cytoBand VARCHAR(20),
	funcrefGene VARCHAR(30),
	generefGene VARCHAR(30),
	geneDetailrefGene VARCHAR(100),
	exonicFuncrefGene VARCHAR(50),
	aAChangeRefGene VARCHAR(300),
	funcEnsGene VARCHAR(30),
	geneEnsGene VARCHAR(100),
	geneDetailEnsGene VARCHAR(100),
	exonicFuncEnsGene VARCHAR(100),
	aAChangeEnsGene VARCHAR(300),
	snp VARCHAR(30),
	snpNonFlagged VARCHAR(30),
	octAll NUMERIC,
	octEur NUMERIC,
	octAmr NUMERIC,
	octAfr NUMERIC,
	octEas NUMERIC,
	octSas NUMERIC,
	exACFreq NUMERIC,
	exACAFR NUMERIC,
	exACAMR NUMERIC,
	exACEAS NUMERIC,
	exACFIN NUMERIC,
	exACNFE NUMERIC,
	exACOTH NUMERIC,
	exACSAS NUMERIC,
	clinvar VARCHAR(200),
	cosmic VARCHAR(200),
	phastConsElementsWay VARCHAR(200),
	genomicSuperDups VARCHAR(200),
	Homozigosity VARCHAR(10),
	DP NUMERIC,
	refDP NUMERIC,
	altDP NUMERIC,
	pagrindinesLentelesId INTEGER,
	PRIMARY KEY (id,pagrindinesLentelesId)
);





CREATE TABLE quantiSnp (
	id SERIAL NOT NULL,
	chromosome VARCHAR (2),
	startPosition NUMERIC,
	endPosition NUMERIC,
	startProbe VARCHAR (20),
	endProbe VARCHAR (20),
	length NUMERIC,
	noProbes NUMERIC,
	copyNumber NUMERIC,
	maxLogBf NUMERIC,
	pagrindinesLentelesId INTEGER NOT NULL,
	PRIMARY KEY (id,pagrindinesLentelesId)
);



CREATE TABLE snp (
	id SERIAL NOT NULL,
	chr VARCHAR(2) NOT NULL,
	start NUMERIC NOT NULL,
	ends NUMERIC NOT NULL,
	ref VARCHAR(1) NOT NULL,
	alt VARCHAR(1) NOT NULL,
	cytoBand VARCHAR(20) NOT NULL,
	genefunc VARCHAR(20) NOT NULL,
	gene VARCHAR(50) NOT NULL,
	geneDetails VARCHAR(50),
	exonicFunc VARCHAR(50),
	aAChange VARCHAR(100),
	funcEnsGene VARCHAR(30) NOT NULL,
	ensembl VARCHAR(100) NOT NULL,
	geneDetailEnsGene VARCHAR(100),
	exonicFuncEnsGene VARCHAR(50),
	aAChangeEnsGene VARCHAR(150),
	snp VARCHAR(20) NOT NULL,
	snpNonFlagged VARCHAR(20) NOT NULL,
	genomeAll NUMERIC,
	genomeEur NUMERIC,
	genomeAmr NUMERIC,
	genomeAfr NUMERIC,
	genomeEas NUMERIC,
	genomeSas NUMERIC,
	ESPFreq NUMERIC,
	ESPAFR NUMERIC,
	ESPAMR NUMERIC,
	ESPEAS NUMERIC,
	ESPFIN NUMERIC,
	ESPNFE NUMERIC,
	ESPOTH NUMERIC,
	ESPSAS NUMERIC,
	clinVar VARCHAR(200),
	cosmic VARCHAR(100),
	SIFTScore NUMERIC,
	SIFTPred VARCHAR(1),
	polyphenHDIVScore NUMERIC,
	polyphenHDIVPred VARCHAR(1),
	polyphenHVARScore NUMERIC,
	polyphenHVARPred VARCHAR(1),
	LRTScore NUMERIC,
	LRTPred VARCHAR(1),
	mutationTasterScore NUMERIC,
	mutationTasterPred VARCHAR(1),
	mutationAssessorScore NUMERIC,
	mutationAssessorPred VARCHAR(1),
	FATHMMScore NUMERIC,
	FATHMMPred VARCHAR(1),
	radialSVMScore NUMERIC,
	radialSVMPred VARCHAR(1),
	LRScore NUMERIC,
	LRPred VARCHAR(1),
	VESTScore NUMERIC,
	CADDRaws NUMERIC,
	CADDPhred NUMERIC,
	GERP NUMERIC,
	phyloPWayPlacental NUMERIC,
	phyloPWayVertebrate NUMERIC,
	SiPhy NUMERIC,
	CADDRaw NUMERIC,
	CADD NUMERIC,
	phastConsElementsWay VARCHAR(100),
	genomicSuperDups VARCHAR(100),
	HomozygCoverage VARCHAR(20),
	pagrindinesLentelesId INTEGER,
	PRIMARY KEY (id,pagrindinesLentelesId)
);


ALTER TABLE cnvPart ADD CONSTRAINT cnvPart_ibfk_1 FOREIGN KEY (pagrindinesLentelesId) REFERENCES pagrindineLentele (id);
ALTER TABLE genotipavimo ADD CONSTRAINT genotipavimo_ibfk_1 FOREIGN KEY (pagrindinesLentelesId) REFERENCES pagrindineLentele (id);
ALTER TABLE users_groups ADD CONSTRAINT users_groups_ibfk_1 FOREIGN KEY (user_id) REFERENCES users(id);
ALTER TABLE users_groups ADD CONSTRAINT users_groups_ibfk_2 FOREIGN KEY (group_id) REFERENCES groups(id);
ALTER TABLE pennCnv ADD CONSTRAINT pennCnv_ibfk_1 FOREIGN KEY (pagrindinesLentelesId) REFERENCES pagrindineLentele (id);
ALTER TABLE pagrindineLentele ADD	CONSTRAINT pagrindineLentele_ibfk1 FOREIGN KEY (vartotojoId) REFERENCES users (id);
ALTER TABLE pagrindineLentele ADD	CONSTRAINT pagrindineLentele_ibfk2 FOREIGN KEY (pacientasid) REFERENCES pacientas(id);
ALTER TABLE indel ADD CONSTRAINT indel_ibfk_1 FOREIGN KEY (pagrindinesLentelesId) REFERENCES pagrindineLentele (id);
ALTER TABLE quantiSnp ADD CONSTRAINT quantiSnp_ibfk_1 FOREIGN KEY (pagrindinesLentelesId) REFERENCES pagrindineLentele (id);
ALTER TABLE snp ADD CONSTRAINT snp_ibfk_1 FOREIGN KEY (pagrindinesLentelesId) REFERENCES pagrindineLentele (id); 



