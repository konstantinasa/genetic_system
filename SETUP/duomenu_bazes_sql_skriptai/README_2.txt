Duomenu bazes iterpimas i PostgreSQL serveri:

Visi failai yra sunumeruoti, juos reikia terpti eiles tvarka.

1. database.sql - duomenu bazes modelio skriptas.
2. insertai_pacientai.sql - duomenys uzpildantys duomenu bazes modeli kuris yra pirmam zingsnyje, taip pat testiniai duomenys apie penkis pacientus.
3. insertai_miestai.sql - visu Lietuvos miestu ir kaimu sarasas (apie 21 tukst.)
4. insertai_ligos.sql - ligu sarasas (apie 2 tukst.)
5. create_insert_tags.sql - paieskai reikalinga duomenu bazes modelio dalis ir duomenys.
6. Final_report.sql - dalis duomenu bazes modelio skirto genetiniams duomenims talpinti

