
CREATE TABLE finalReport (
	id SERIAL,
	chr VARCHAR(2),
	position NUMERIC(50),
	snpName VARCHAR(50),
	allele1Design VARCHAR (1),
	allele2Design VARCHAR (1),
	genesInExonMutations VARCHAR(500),
	pagrindinesLentelesId INTEGER,
	PRIMARY KEY (id,pagrindinesLentelesId)
);


ALTER TABLE finalReport ADD CONSTRAINT genotipavimo_ibfk_1 FOREIGN KEY (pagrindinesLentelesId) REFERENCES pagrindineLentele (id);
