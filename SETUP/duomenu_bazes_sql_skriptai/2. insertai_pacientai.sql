
insert into reiksme values (0,'ne');
insert into reiksme values (1,'taip');

insert into tautybe (tautybe) values ('lietuvis');
insert into tautybe (tautybe) values ('rusas');
insert into tautybe (tautybe) values ('lenkas');
insert into tautybe (tautybe) values ('latvis');
insert into tautybe (tautybe) values ('estas');
insert into tautybe (tautybe) values ('britas');

insert into svarbucholinfo (rodiklis) values ('serumo trigliceridai');
insert into svarbucholinfo (rodiklis) values ('serumo DTLT (didelio tankio lipoproteinai)');

insert into cholkorekcijainfo (metodas) values ('laikiausi dietos');
insert into cholkorekcijainfo (metodas) values ('pakankamai fizi�kai buvau aktyvus (3k/sav. po 45 min)');
insert into cholkorekcijainfo (metodas) values ('vartojau vaistus cholesterio kiekiui suma�inti');

insert into kveptkligdaz (aprasas) values ('Karta per metus');
insert into kveptkligdaz (aprasas) values ('2-3 kartus metuose');
insert into kveptkligdaz (aprasas) values ('4 ir daugiau');
insert into kveptkligdaz (aprasas) values ('nesergu');

insert into fizinisaktyvuminfo (aprasas) values ('Taip (sportuoju daugiau kaip 3k/sav po 45 min )');
insert into fizinisaktyvuminfo (aprasas) values ('Ne (sportuoju ma�iau kaip 3k/sav po 45 min )');
insert into fizinisaktyvuminfo (aprasas) values ('Kita (darbas sedimas, prakt��kai nesportuoju)');

insert into srginfo (arpagrkategorija,srginfo) values (1,'�irdies kraujagysliu ligomis (miokardo infarktu ir kt.)');
insert into srginfo (arpagrkategorija,srginfo) values (1,'Padidejusiu kraujospud�iu (hipertonija) ');
insert into srginfo (arpagrkategorija,srginfo) values (1,'uminiais galvos smegenu kraujotakos sutrikimais (insultu)');
insert into srginfo (arpagrkategorija,srginfo) values (1,'Cukralige (diabetu)');
insert into srginfo (arpagrkategorija,srginfo) values (1,'Osteoporoze');
insert into srginfo (arpagrkategorija,srginfo) values (1,'Nutukimu');
insert into srginfo (arpagrkategorija,srginfo) values (1,'Trumparegyste');
insert into srginfo (arpagrkategorija,srginfo) values (1,'Letinemis plauciu ligomis (astma, letine obstrukcine plauciu liga)');
insert into srginfo (arpagrkategorija,srginfo) values (1,'Depresija, polinkiu i depresi�kuma');
insert into srginfo (arpagrkategorija,srginfo) values (1,'Alkoholizmu');
insert into srginfo (arpagrkategorija,srginfo) values (1,'Odos ligomis  (�vyneline, dermatitai)');
insert into srginfo (arpagrkategorija,srginfo) values (0,'inkstu akmenlige');
insert into srginfo (arpagrkategorija,srginfo) values (0,'epilepsija');
insert into srginfo (arpagrkategorija,srginfo) values (0,'GeRL');
insert into srginfo (arpagrkategorija,srginfo) values (0,'sarkoidoze');
insert into srginfo (arpagrkategorija,srginfo) values (0,'Skydliaukes liga');
insert into srginfo (arpagrkategorija,srginfo) values (0,'Bronchine astma');
insert into srginfo (arpagrkategorija,srginfo) values (0,'Glaukoma');
insert into srginfo (arpagrkategorija,srginfo) values (0,'hepatitas');
insert into srginfo (arpagrkategorija,srginfo) values (0,'daznos pneumonijos');

insert into alergiskaskam (alergiskaskam) values ('morkos');
insert into alergiskaskam (alergiskaskam) values ('cesnakai');
insert into alergiskaskam (alergiskaskam) values ('grudiniai produktai');
insert into alergiskaskam (alergiskaskam) values ('kiaulpieniu ziedai');
insert into alergiskaskam (alergiskaskam) values ('�okoladas');
insert into alergiskaskam (alergiskaskam) values ('kakava');
insert into alergiskaskam (alergiskaskam) values ('citrusiniai vaisiai');
insert into alergiskaskam (alergiskaskam) values ('nenaturalios sultys');
insert into alergiskaskam (alergiskaskam) values ('brak�es');
insert into alergiskaskam (alergiskaskam) values ('mandarinai');
insert into alergiskaskam (alergiskaskam) values ('kiausiniai');
insert into alergiskaskam (alergiskaskam) values ('juodi pipirai');
insert into alergiskaskam (alergiskaskam) values ('cukrus');

insert into kokybeinfo (info) values ('aralergiskas');
insert into kokybeinfo (info) values ('arsrg');
insert into kokybeinfo (info) values ('arsrgkitom');
insert into kokybeinfo (info) values ('arsrgveziu');
insert into kokybeinfo (info) values ('arbuvocholes');
insert into kokybeinfo (info) values ('arkitosvoris');
insert into kokybeinfo (info) values ('gvietaarmiestas');

insert into maistopasirinkimas (pasirinkimas) values ('sveikatos gerinimas (ligu profilaktika)');
insert into maistopasirinkimas (pasirinkimas) values ('specialios dietos butinumas');
insert into maistopasirinkimas (pasirinkimas) values ('Kaina');
insert into maistopasirinkimas (pasirinkimas) values ('Skonines savybes');
insert into maistopasirinkimas (pasirinkimas) values ('�eimos nariu itaka ');

insert into srgveziukokiu (aprasas) values ('kruties');
insert into srgveziukokiu (aprasas) values ('zarnyno');
insert into srgveziukokiu (aprasas) values ('skydliaukes');
insert into srgveziukokiu (aprasas) values ('prostatos');
insert into srgveziukokiu (aprasas) values ('skrandzio');

insert into issilavinimas (issilavinimas) values ('pradinis');
insert into issilavinimas (issilavinimas) values ('vidurinis');
insert into issilavinimas (issilavinimas) values ('specialusis vidurinis');
insert into issilavinimas (issilavinimas) values ('auk�tasis');

insert into uzimtumas (uzimtumas) values ('darbininkas,-e');
insert into uzimtumas (uzimtumas) values ('tarnautojas,-e');
insert into uzimtumas (uzimtumas) values ('pensininkas,-e');
insert into uzimtumas (uzimtumas) values ('namu �eimininke');
insert into uzimtumas (uzimtumas) values ('moksleivis, -e,');
insert into uzimtumas (uzimtumas) values ('studentas, -e');
insert into uzimtumas (uzimtumas) values ('bedarbis, -e');

insert into projektas (projektas) values ('anketa_1');
insert into projektas (projektas) values ('anketa_2');
insert into projektas (projektas) values ('anketa_3');
insert into projektas (projektas) values ('anketa_4');
insert into projektas (projektas) values ('anketa_5');

insert into svoriokitimas (kitimas) values ('priaugta');
insert into svoriokitimas (kitimas) values ('numesta');

insert into sveikatosrodikliaiinfo (pavadinimas) values ('GLiu');
insert into sveikatosrodikliaiinfo (pavadinimas) values ('CHOL');
insert into sveikatosrodikliaiinfo (pavadinimas) values ('TG');
insert into sveikatosrodikliaiinfo (pavadinimas) values ('DTL');
insert into sveikatosrodikliaiinfo (pavadinimas) values ('MTL');
insert into sveikatosrodikliaiinfo (pavadinimas) values ('CRB');
insert into sveikatosrodikliaiinfo (pavadinimas) values ('APO_Ai');
insert into sveikatosrodikliaiinfo (pavadinimas) values ('APO_B');
insert into sveikatosrodikliaiinfo (pavadinimas) values ('APOA_APOB');
insert into sveikatosrodikliaiinfo (pavadinimas) values ('LP_A_');

/* Kosto miestai */

iNSeRT iNTO "apskritis" VALueS (1, 'Alytaus apskritis');
iNSeRT iNTO "apskritis" VALueS (2, 'Kauno apskritis');
iNSeRT iNTO "apskritis" VALueS (3, 'Klaip�dos apskritis');
iNSeRT iNTO "apskritis" VALueS (4, 'Marijampoles apskritis');
iNSeRT iNTO "apskritis" VALueS (5, 'Paneve�io apskritis');
iNSeRT iNTO "apskritis" VALueS (6, '�iauliu apskritis');
iNSeRT iNTO "apskritis" VALueS (7, 'Taurag�s apskritis');
iNSeRT iNTO "apskritis" VALueS (8, 'Telsiu apskritis');
iNSeRT iNTO "apskritis" VALueS (9, 'utenos apskritis');
iNSeRT iNTO "apskritis" VALueS (10, 'Vilniaus apskritis');

iNSeRT iNTO "savivaldybe" VALueS (1, 'Akmen�s rajonas', 6);
iNSeRT iNTO "savivaldybe" VALueS (2, 'Alytaus miestas', 1);
iNSeRT iNTO "savivaldybe" VALueS (3, 'Alytaus rajonas', 1);
iNSeRT iNTO "savivaldybe" VALueS (4, 'Anyksciu rajonas', 9);
iNSeRT iNTO "savivaldybe" VALueS (5, 'Birstono savivaldybe', 2);
iNSeRT iNTO "savivaldybe" VALueS (6, 'Birzu rajonas', 5);
iNSeRT iNTO "savivaldybe" VALueS (7, 'Druskininku savivaldybe', 1);
iNSeRT iNTO "savivaldybe" VALueS (8, 'elektrenu savivaldybe', 10);
iNSeRT iNTO "savivaldybe" VALueS (9, 'ignalinos rajonas', 9);
iNSeRT iNTO "savivaldybe" VALueS (10, 'Jonavos rajonas', 2);
iNSeRT iNTO "savivaldybe" VALueS (11, 'Joniskio rajonas', 6);
iNSeRT iNTO "savivaldybe" VALueS (12, 'Jurbarko rajonas', 7);
iNSeRT iNTO "savivaldybe" VALueS (13, 'Kaisiadoriu rajonas', 2);
iNSeRT iNTO "savivaldybe" VALueS (14, 'Kalvarijos savivaldybe', 4);
iNSeRT iNTO "savivaldybe" VALueS (15, 'Kauno miestas', 2);
iNSeRT iNTO "savivaldybe" VALueS (16, 'Kauno rajonas', 2);
iNSeRT iNTO "savivaldybe" VALueS (17, 'Kazlu Rudos savivaldybe', 4);
iNSeRT iNTO "savivaldybe" VALueS (18, 'Kedainiu rajonas', 2);
iNSeRT iNTO "savivaldybe" VALueS (19, 'Kelmes rajonas', 6);
iNSeRT iNTO "savivaldybe" VALueS (20, 'Klaipedos miestas', 3);
iNSeRT iNTO "savivaldybe" VALueS (21, 'Klaipedos rajonas', 3);
iNSeRT iNTO "savivaldybe" VALueS (22, 'Kretingos rajonas', 3);
iNSeRT iNTO "savivaldybe" VALueS (23, 'Kupiskio rajonas', 5);
iNSeRT iNTO "savivaldybe" VALueS (24, 'Lazdiju rajonas', 1);
iNSeRT iNTO "savivaldybe" VALueS (25, 'Marijampoles savivaldybe', 4);
iNSeRT iNTO "savivaldybe" VALueS (26, 'Mazeikiu rajonas', 8);
iNSeRT iNTO "savivaldybe" VALueS (27, 'Moletu rajonas', 9);
iNSeRT iNTO "savivaldybe" VALueS (28, 'Neringos savivaldybe', 3);
iNSeRT iNTO "savivaldybe" VALueS (29, 'Pagegiu savivaldybe', 7);
iNSeRT iNTO "savivaldybe" VALueS (30, 'Pakruojo rajonas', 6);
iNSeRT iNTO "savivaldybe" VALueS (31, 'Palangos miestas', 3);
iNSeRT iNTO "savivaldybe" VALueS (32, 'Panevezio miestas', 5);
iNSeRT iNTO "savivaldybe" VALueS (33, 'Panevezio rajonas', 5);
iNSeRT iNTO "savivaldybe" VALueS (34, 'Pasvalio rajonas', 5);
iNSeRT iNTO "savivaldybe" VALueS (35, 'Plunges rajonas', 8);
iNSeRT iNTO "savivaldybe" VALueS (36, 'Prienu rajonas', 2);
iNSeRT iNTO "savivaldybe" VALueS (37, 'Radviliskio rajonas', 6);
iNSeRT iNTO "savivaldybe" VALueS (38, 'Raseiniu rajonas', 2);
iNSeRT iNTO "savivaldybe" VALueS (39, 'Rietavo savivaldybe', 8);
iNSeRT iNTO "savivaldybe" VALueS (40, 'Rokiskio rajonas', 5);
iNSeRT iNTO "savivaldybe" VALueS (41, 'Skuodo rajonas', 3);
iNSeRT iNTO "savivaldybe" VALueS (42, 'Sakiu rajonas', 4);
iNSeRT iNTO "savivaldybe" VALueS (43, 'Salcininku rajonas', 10);
iNSeRT iNTO "savivaldybe" VALueS (44, 'Siauliu miestas', 6);
iNSeRT iNTO "savivaldybe" VALueS (45, 'Siauliu rajonas', 6);
iNSeRT iNTO "savivaldybe" VALueS (46, 'Silutes rajonas', 3);
iNSeRT iNTO "savivaldybe" VALueS (47, 'Silales rajonas', 7);
iNSeRT iNTO "savivaldybe" VALueS (48, 'Sirvintu rajonas', 10);
iNSeRT iNTO "savivaldybe" VALueS (49, 'Svencioniu rajonas', 10);
iNSeRT iNTO "savivaldybe" VALueS (50, 'Taurages rajonas', 7);
iNSeRT iNTO "savivaldybe" VALueS (51, 'Telsiu rajonas', 8);
iNSeRT iNTO "savivaldybe" VALueS (52, 'Traku rajonas', 10);
iNSeRT iNTO "savivaldybe" VALueS (53, 'ukmerges rajonas', 10);
iNSeRT iNTO "savivaldybe" VALueS (54, 'utenos rajonas', 9);
iNSeRT iNTO "savivaldybe" VALueS (55, 'Varenos rajonas', 1);
iNSeRT iNTO "savivaldybe" VALueS (56, 'Vilkaviskio rajonas', 4);
iNSeRT iNTO "savivaldybe" VALueS (57, 'Vilniaus miestas', 10);
iNSeRT iNTO "savivaldybe" VALueS (58, 'Vilniaus rajonas', 10);
iNSeRT iNTO "savivaldybe" VALueS (59, 'Visagino savivaldybe', 9);
iNSeRT iNTO "savivaldybe" VALueS (60, 'Zarasu rajonas', 9);

/* Kosto miestu pabaiga */

/* ##############			paciento dalis	pirmi 5 pacienta pagal fenoripini faila	 ############################## */


insert into pacientas (pacientas,lytis,tautybe,gdata,gimimosvoris,gimimougis) values ('LTG_0528_2_1100_0241_01_000',4,1,'1964.03.05',3.500,53);
insert into pacientas (pacientas,lytis,tautybe,gdata,gimimosvoris,gimimougis) values ('LTG_0529_1_1300_0241_01_000',3,1,'1965.06.04',3.500,53);
insert into pacientas (pacientas,lytis,tautybe,gdata,gimimosvoris,gimimougis) values ('LTG_0530_1_1300_0241_02_000',3,1,'1988.04.22',3.800,53);
insert into pacientas (pacientas,lytis,tautybe,gdata,gimimosvoris,gimimougis) values ('LTG_0531_2_1300_0242_01_000',4,1,'1958.02.26',null,null);
insert into pacientas (pacientas,lytis,tautybe,gdata,gimimosvoris,gimimougis) values ('LTG_0532_1_1300_0242_01_000',3,1,'1959.01.24',null,null);

/* miestas ar kaimas */

insert into miestasarkaimas (miestasarkaimas,savivaldybeid) values ('Abaku k.',22);

/* eND miestas ar kaimas */

insert into anketosversija (projektasid,versija) values (5,default);


insert into apiepacienta (anketosversija,tdata,issilavinimas,miestasarkaimas,uzimtumas,ugis,svoris,tiriamliemapim,pacientasid) values (1,'2011-10-27',4,1,2,164,65.0,73,1);
insert into apiepacienta (anketosversija,tdata,issilavinimas,miestasarkaimas,uzimtumas,ugis,svoris,tiriamliemapim,pacientasid) values (1,'2011-10-27',4,1,2,180,78.0,78,2);
insert into apiepacienta (anketosversija,tdata,issilavinimas,miestasarkaimas,uzimtumas,ugis,svoris,tiriamliemapim,pacientasid) values (1,'2011-10-27',4,1,6,182,86.0,80,3);
insert into apiepacienta (anketosversija,tdata,issilavinimas,miestasarkaimas,uzimtumas,ugis,svoris,tiriamliemapim,pacientasid) values (1,'2011-10-27',4,1,2,168,65.0,78,4);
insert into apiepacienta (anketosversija,tdata,issilavinimas,miestasarkaimas,uzimtumas,ugis,svoris,tiriamliemapim,pacientasid) values (1,'2011-10-27',4,1,1,182,110.0,null,5);

insert into pacientoinfo (fizinisaktyvum,akssistnenorm,aksdiastnenm,kveptkligdaz,apiepacientaid) values (3,1,null,1,1);
insert into pacientoinfo (fizinisaktyvum,akssistnenorm,aksdiastnenm,kveptkligdaz,apiepacientaid) values (3,0,0,1,2);
insert into pacientoinfo (fizinisaktyvum,akssistnenorm,aksdiastnenm,kveptkligdaz,apiepacientaid) values (3,1,0,2,3);
insert into pacientoinfo (fizinisaktyvum,akssistnenorm,aksdiastnenm,kveptkligdaz,apiepacientaid) values (2,0,1,1,4);
insert into pacientoinfo (fizinisaktyvum,akssistnenorm,aksdiastnenm,kveptkligdaz,apiepacientaid) values (2,1,1,1,5);

insert into svarbuchol (pacientoinfoid,svarbucholinfoid,arnustatyta) values (1,1,1);
insert into svarbuchol (pacientoinfoid,svarbucholinfoid,arnustatyta) values (1,2,1);

insert into cholkorekcija (metodonr,pacientoinfoid) values (1,3);
insert into cholkorekcija (metodonr,pacientoinfoid) values (1,4);

insert into srg (srginfoid,apiepacientaid) values (3,2);
insert into srg (srginfoid,apiepacientaid) values (11,2);
insert into srg (srginfoid,apiepacientaid) values (5,1);
insert into srg (srginfoid,apiepacientaid) values (16,4);

insert into alergiskas (alergiskaskamid,apiepacientaid) values (3,2);
insert into alergiskas (alergiskaskamid,apiepacientaid) values (5,2);
insert into alergiskas (alergiskaskamid,apiepacientaid) values (6,2);
insert into alergiskas (alergiskaskamid,apiepacientaid) values (5,4);
insert into alergiskas (alergiskaskamid,apiepacientaid) values (1,5);

insert into maistas (pasirinkimasid,apiepacientaid) values (1,1);
insert into maistas (pasirinkimasid,apiepacientaid) values (1,2);
insert into maistas (pasirinkimasid,apiepacientaid) values (1,3);
insert into maistas (pasirinkimasid,apiepacientaid) values (1,4);
insert into maistas (pasirinkimasid,apiepacientaid) values (2,1);
insert into maistas (pasirinkimasid,apiepacientaid) values (3,4);
insert into maistas (pasirinkimasid,apiepacientaid) values (4,3);
insert into maistas (pasirinkimasid,apiepacientaid) values (4,5);
insert into maistas (pasirinkimasid,apiepacientaid) values (5,2);
insert into maistas (pasirinkimasid,apiepacientaid) values (5,3);

insert into srgveziu (srgveziukokiuid,apiepacientaid) values (2,3);

insert into svoriokit (apiepacientaid,svoriokitimasid,kg,perkiekmen) values (2,2,8,10);
insert into svoriokit (apiepacientaid,svoriokitimasid,kg,perkiekmen) values (3,1,5,36);
insert into svoriokit (apiepacientaid,svoriokitimasid,kg,perkiekmen) values (3,2,4,1);

insert into sveikatosrodikliai (apiepacientaid,sveikatosrodikliaiinfoid,reiksme) values (1,1,5.25);
insert into sveikatosrodikliai (apiepacientaid,sveikatosrodikliaiinfoid,reiksme) values (1,2,7.15);
insert into sveikatosrodikliai (apiepacientaid,sveikatosrodikliaiinfoid,reiksme) values (1,3,1.02);
insert into sveikatosrodikliai (apiepacientaid,sveikatosrodikliaiinfoid,reiksme) values (1,4,1.81);
insert into sveikatosrodikliai (apiepacientaid,sveikatosrodikliaiinfoid,reiksme) values (1,5,4.87);
insert into sveikatosrodikliai (apiepacientaid,sveikatosrodikliaiinfoid,reiksme) values (1,6,1.5);
insert into sveikatosrodikliai (apiepacientaid,sveikatosrodikliaiinfoid,reiksme) values (1,7,1.81);
insert into sveikatosrodikliai (apiepacientaid,sveikatosrodikliaiinfoid,reiksme) values (1,8,1.37);
insert into sveikatosrodikliai (apiepacientaid,sveikatosrodikliaiinfoid,reiksme) values (1,10,0.85);

insert into sveikatosrodikliai (apiepacientaid,sveikatosrodikliaiinfoid,reiksme) values (2,1,5.81);
insert into sveikatosrodikliai (apiepacientaid,sveikatosrodikliaiinfoid,reiksme) values (2,2,6.23);
insert into sveikatosrodikliai (apiepacientaid,sveikatosrodikliaiinfoid,reiksme) values (2,3,1.21);
insert into sveikatosrodikliai (apiepacientaid,sveikatosrodikliaiinfoid,reiksme) values (2,4,1.42);
insert into sveikatosrodikliai (apiepacientaid,sveikatosrodikliaiinfoid,reiksme) values (2,5,4.25);
insert into sveikatosrodikliai (apiepacientaid,sveikatosrodikliaiinfoid,reiksme) values (2,6,0.8);
insert into sveikatosrodikliai (apiepacientaid,sveikatosrodikliaiinfoid,reiksme) values (2,7,1.74);
insert into sveikatosrodikliai (apiepacientaid,sveikatosrodikliaiinfoid,reiksme) values (2,8,1.15);
insert into sveikatosrodikliai (apiepacientaid,sveikatosrodikliaiinfoid,reiksme) values (2,10,0.02);

insert into sveikatosrodikliai (apiepacientaid,sveikatosrodikliaiinfoid,reiksme) values (3,1,1.2);
insert into sveikatosrodikliai (apiepacientaid,sveikatosrodikliaiinfoid,reiksme) values (3,2,1.2);
insert into sveikatosrodikliai (apiepacientaid,sveikatosrodikliaiinfoid,reiksme) values (3,3,1.2);
insert into sveikatosrodikliai (apiepacientaid,sveikatosrodikliaiinfoid,reiksme) values (3,4,1.2);
insert into sveikatosrodikliai (apiepacientaid,sveikatosrodikliaiinfoid,reiksme) values (3,5,1.2);
insert into sveikatosrodikliai (apiepacientaid,sveikatosrodikliaiinfoid,reiksme) values (3,6,1.2);
insert into sveikatosrodikliai (apiepacientaid,sveikatosrodikliaiinfoid,reiksme) values (3,7,1.2);
insert into sveikatosrodikliai (apiepacientaid,sveikatosrodikliaiinfoid,reiksme) values (3,8,1.2);
insert into sveikatosrodikliai (apiepacientaid,sveikatosrodikliaiinfoid,reiksme) values (3,10,1.2);

insert into sveikatosrodikliai (apiepacientaid,sveikatosrodikliaiinfoid,reiksme) values (4,1,1.2);
insert into sveikatosrodikliai (apiepacientaid,sveikatosrodikliaiinfoid,reiksme) values (4,2,1.2);
insert into sveikatosrodikliai (apiepacientaid,sveikatosrodikliaiinfoid,reiksme) values (4,3,1.2);
insert into sveikatosrodikliai (apiepacientaid,sveikatosrodikliaiinfoid,reiksme) values (4,4,1.2);
insert into sveikatosrodikliai (apiepacientaid,sveikatosrodikliaiinfoid,reiksme) values (4,5,1.2);
insert into sveikatosrodikliai (apiepacientaid,sveikatosrodikliaiinfoid,reiksme) values (4,6,1.2);
insert into sveikatosrodikliai (apiepacientaid,sveikatosrodikliaiinfoid,reiksme) values (4,7,1.2);
insert into sveikatosrodikliai (apiepacientaid,sveikatosrodikliaiinfoid,reiksme) values (4,8,1.2);

insert into sveikatosrodikliai (apiepacientaid,sveikatosrodikliaiinfoid,reiksme) values (5,1,1.2);
insert into sveikatosrodikliai (apiepacientaid,sveikatosrodikliaiinfoid,reiksme) values (5,2,1.2);
insert into sveikatosrodikliai (apiepacientaid,sveikatosrodikliaiinfoid,reiksme) values (5,3,1.2);
insert into sveikatosrodikliai (apiepacientaid,sveikatosrodikliaiinfoid,reiksme) values (5,4,1.2);
insert into sveikatosrodikliai (apiepacientaid,sveikatosrodikliaiinfoid,reiksme) values (5,5,1.2);
insert into sveikatosrodikliai (apiepacientaid,sveikatosrodikliaiinfoid,reiksme) values (5,6,1.2);
insert into sveikatosrodikliai (apiepacientaid,sveikatosrodikliaiinfoid,reiksme) values (5,7,1.2);
insert into sveikatosrodikliai (apiepacientaid,sveikatosrodikliaiinfoid,reiksme) values (5,8,1.2);


insert into kokybe (apiepacientaid,kokybeinfoid,reiksme) values (1,1,0);
insert into kokybe (apiepacientaid,kokybeinfoid,reiksme) values (1,2,1);
insert into kokybe (apiepacientaid,kokybeinfoid,reiksme) values (1,3,1);
insert into kokybe (apiepacientaid,kokybeinfoid,reiksme) values (1,4,0);
insert into kokybe (apiepacientaid,kokybeinfoid,reiksme) values (1,5,1);
insert into kokybe (apiepacientaid,kokybeinfoid,reiksme) values (1,6,0);
insert into kokybe (apiepacientaid,kokybeinfoid,reiksme) values (1,7,1);

insert into kokybe (apiepacientaid,kokybeinfoid,reiksme) values (2,1,1);
insert into kokybe (apiepacientaid,kokybeinfoid,reiksme) values (2,2,1);
insert into kokybe (apiepacientaid,kokybeinfoid,reiksme) values (2,3,1);
insert into kokybe (apiepacientaid,kokybeinfoid,reiksme) values (2,4,1);
insert into kokybe (apiepacientaid,kokybeinfoid,reiksme) values (2,5,1);
insert into kokybe (apiepacientaid,kokybeinfoid,reiksme) values (2,6,1);
insert into kokybe (apiepacientaid,kokybeinfoid,reiksme) values (2,7,1);

insert into kokybe (apiepacientaid,kokybeinfoid,reiksme) values (3,1,1);
insert into kokybe (apiepacientaid,kokybeinfoid,reiksme) values (3,2,1);
insert into kokybe (apiepacientaid,kokybeinfoid,reiksme) values (3,3,1);
insert into kokybe (apiepacientaid,kokybeinfoid,reiksme) values (3,4,1);
insert into kokybe (apiepacientaid,kokybeinfoid,reiksme) values (3,5,1);
insert into kokybe (apiepacientaid,kokybeinfoid,reiksme) values (3,6,1);
insert into kokybe (apiepacientaid,kokybeinfoid,reiksme) values (3,7,1);

insert into kokybe (apiepacientaid,kokybeinfoid,reiksme) values (4,1,1);
insert into kokybe (apiepacientaid,kokybeinfoid,reiksme) values (4,2,1);
insert into kokybe (apiepacientaid,kokybeinfoid,reiksme) values (4,3,1);
insert into kokybe (apiepacientaid,kokybeinfoid,reiksme) values (4,4,1);
insert into kokybe (apiepacientaid,kokybeinfoid,reiksme) values (4,5,1);
insert into kokybe (apiepacientaid,kokybeinfoid,reiksme) values (4,6,1);
insert into kokybe (apiepacientaid,kokybeinfoid,reiksme) values (4,7,1);

insert into kokybe (apiepacientaid,kokybeinfoid,reiksme) values (5,1,1);
insert into kokybe (apiepacientaid,kokybeinfoid,reiksme) values (5,2,1);
insert into kokybe (apiepacientaid,kokybeinfoid,reiksme) values (5,3,1);
insert into kokybe (apiepacientaid,kokybeinfoid,reiksme) values (5,4,1);
insert into kokybe (apiepacientaid,kokybeinfoid,reiksme) values (5,5,1);
insert into kokybe (apiepacientaid,kokybeinfoid,reiksme) values (5,6,1);
insert into kokybe (apiepacientaid,kokybeinfoid,reiksme) values (5,7,1);




