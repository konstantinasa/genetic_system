<?php

$lang['migration_none_found']			= "Migracijų nerasta.";
$lang['migration_not_found']			= "Nepavyko rassti migracijos.";
$lang['migration_multiple_version']		= "Šios yra kelios migracijos turinčios tą patį versijos numerį: %d.";
$lang['migration_class_doesnt_exist']	= "Migracijos klasė \"%s\" nerasta.";
$lang['migration_missing_up_method']	= "Migracijos klasė \"%s\" neturi 'up' metodo.";
$lang['migration_missing_down_method']	= "Migracijos klasė \"%s\" neturi 'down' metodo.";
$lang['migration_invalid_filename']		= "Migracijos klasė \"%s\" turi klaidingą bylos pavadinimą.";


/* End of file migration_lang.php */
/* Location: ./system/language/english/migration_lang.php */
