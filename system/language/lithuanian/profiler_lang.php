<?php

$lang['profiler_database']		= 'DUOMBAZĖ';
$lang['profiler_controller_info'] = 'KLASĖ/METODAS';
$lang['profiler_benchmarks']	= 'GAIRĖ';
$lang['profiler_queries']		= 'UŽKLAUSOS';
$lang['profiler_get_data']		= 'GAUTI DUOMENIS';
$lang['profiler_post_data']		= 'SIŲSTI DUOMENIS';
$lang['profiler_uri_string']	= 'URI SAKINYS';
$lang['profiler_memory_usage']	= 'ATMINTIES NAUDOJIMAS';
$lang['profiler_config']		= 'CONFIG KINTAMIEJI';
$lang['profiler_session_data']	= 'SESIJOS DUOMENYS';
$lang['profiler_headers']		= 'HTTP ANTRAŠTĖS';
$lang['profiler_no_db']			= 'Duomenų bazės tvarkyklė nėra užkrauta';
$lang['profiler_no_queries']	= 'Nebuvo paleistos jokios užklausos';
$lang['profiler_no_post']		= 'Jokie siuntimo duomenys neegzistuoja';
$lang['profiler_no_get']		= 'Jokie gavimo duomenys neegzistuoja';
$lang['profiler_no_uri']		= 'Jokie URI duomenys neegzistuoja';
$lang['profiler_no_memory']		= 'Atminties naudojimas neprieinamas';
$lang['profiler_no_profiles']	= 'Jokia profilio data - visos profilių sudarinėjimo sekcijos buvo išjungtos.';
$lang['profiler_section_hide']	= 'Slėpti';
$lang['profiler_section_show']	= 'Rodyti';

/* End of file profiler_lang.php */
/* Location: ./system/language/english/profiler_lang.php */
