<?php

$lang['required']			= "Laukelis %s yra privalomas.";
$lang['isset']				= "Laukelis %s turi turėti reikšmę.";
$lang['valid_email']		= "Laukelis %s turi turėti teisingą elektroninio pašto adresą.";
$lang['valid_emails']		= "Laukelis %s turi turėti tik teisingus elektroninio pašto adresus.";
$lang['valid_url']			= "Laukelis %s turi turėti teisingą URL.";
$lang['valid_ip']			= "Laukelis %s turi turėti teisingą IP.";
$lang['min_length']			= "Laukelis %s turi būti bent %s simbolių ilgio.";
$lang['max_length']			= "Laukelis %s negali viršyti %s simbolių ilgio.";
$lang['exact_length']		= "Laukelis %s turi turėti %s simbolius.";
$lang['alpha']				= "Laukelis %s gali būti sudarytas tik iš alfabetinių simbolių.";
$lang['alpha_numeric']		= "Laukelis %s gali būti sudarytas tik iš alfabetinių bei skaitinių simbolių.";
$lang['alpha_dash']			= "Laukelis %s gali būti sudarytas tik iš alfabetinių, skaitinių simbolių bei apatinių ir paprastų brūkšnių.";
$lang['numeric']			= "Laukelis %s gali būti sudarytas tik iš skaičių.";
$lang['is_numeric']			= "Laukelis %s gali būti sudarytas tik iš skaitinių simbolių.";
$lang['integer']			= "Laukelis %s privalo turėti sveikajį skaičių.";
$lang['regex_match']		= "Laukelis %s yra neteisingo formato.";
$lang['matches']			= "Laukelis %s neatitinka laukelio %s.";
$lang['is_unique'] 			= "Laukelis %s privalo turėti unikalią reikšmę.";
$lang['is_natural']			= "Laukelis %s privalo turėti naturalų skaičių.";
$lang['is_natural_no_zero']	= "Laukelis %s privalo turėti skaičių didesnį už nulį.";
$lang['decimal']			= "Laukelis %s privalo turėti dešimtainį skaičių.";
$lang['less_than']			= "Laukelis %s privalo turėti skaičių mažesnį už %s.";
$lang['greater_than']		= "Laukelis %s privalo turėti skaičių didesnį už %s.";


/* End of file form_validation_lang.php */
/* Location: ./system/language/english/form_validation_lang.php */
