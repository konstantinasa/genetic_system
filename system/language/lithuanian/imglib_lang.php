<?php

$lang['imglib_source_image_required'] = "Nustatymuose turite nurodyti šaltinio vaizdą.";
$lang['imglib_gd_required'] = "GD image biblioteka yra būtina šiai savybei.";
$lang['imglib_gd_required_for_props'] = "Jūsų serveris turi palaikyti GD image biblioteką norint nustatyti vaizdinės medžiagos savybes.";
$lang['imglib_unsupported_imagecreate'] = "Jūsų serveris nepalaiko GD funkcijos reikalingos apdoroti tokio tipo vaizdinę medžiagą.";
$lang['imglib_gif_not_supported'] = "GIF paveikslėliai yra dažnai nepalaikomi dėl licenzijavimo apribojimų. Jums gali tekti naudoti JPG arba PNG paveikslėlius.";
$lang['imglib_jpg_not_supported'] = "JPG formato paveikslėliai yra nepalaikomi.";
$lang['imglib_png_not_supported'] = "PNG formato paveikslėliai yra nepalaikomi.";
$lang['imglib_jpg_or_png_required'] = "paveikslėlio dydžio keitimo protokolas, kuris buvo nurodytas jūsų nustatymuose veikia tik su JPEG arba PNG tipo paveikslėliais.";
$lang['imglib_copy_error'] = "Įvyko klaida bandant pakeisti failą. Prašome įsitikinti kad į bylos direktoriją galima rašyti duomenis.";
$lang['imglib_rotate_unsupported'] = "Panašu, kad jūsų serveris nepalaiko paveikslėlio pasikumo funkcijos.";
$lang['imglib_libpath_invalid'] = "Kelias į jūsų paveikslėlių biblioteką yra neteisingas. Prašome nustatyti teisingą kelią paveikslėlių nustatymuose.";
$lang['imglib_image_process_failed'] = "Paveikslėlio apdorojimas nepavyko. Prašome įsitikinti are jūsų serveris palaiko pasirinktą protokolą, ir kad kelias į jūsų paveikslėlių biblioteką yra teisingas.";
$lang['imglib_rotation_angle_required'] = "Norint pasukti paveikslėlį reikia nurodyti posukio kampą .";
$lang['imglib_writing_failed_gif'] = "GIF paveikslėlis.";
$lang['imglib_invalid_path'] = "Kelias į paveikslėlį yra neteisingas.";
$lang['imglib_copy_failed'] = "Paveikslėlio kopijavimas nepavyko.";
$lang['imglib_missing_font'] = "Nepavyko rasti šrifto naudojimui.";
$lang['imglib_save_failed'] = "Nepavyko užsaugoti paveikslėlio. Prašome įsitikinti, kad paveikslėlis ir bylos direktorija yra rašoma.";


/* End of file imglib_lang.php */
/* Location: ./system/language/english/imglib_lang.php */
