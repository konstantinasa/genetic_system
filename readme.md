#Genetic data management system

Genetic data management system purpose is to manage genetic and phenotypic data about Lithuanian people. This system allows users to create, store and search through genetic and phenotypic data. 
System is created for Vilniaus university "Santaros" clinics personnel with purpose to replace their previosly used MS Excel. 

System created using PHP MVC CodeIgniter Framework.

For more information check ./SETUP folder.

