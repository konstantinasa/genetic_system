#!/bin/bash
source FileDir.sh

DATA_FILE="$1"
last_sampleId="$2"
RIGHT_NOW=$(date +"%F_%T")
RESULT_FILE="quantiSnp$RIGHT_NOW.txt"
INSERT_HEADER="INSERT INTO quantiSnp VALUES"

if [ -z "$DATA_FILE" ]; then
	echo "Usage: $0 DATA_FILE"
	exit 1
fi

trap "rm -f col1.tsv" EXIT

tail -n +1 "$DATA_FILE" | \
sed -e'1d' | \
sed 's/[^;]*;//' | \
awk 'BEGIN {FS=OFS=";"} {gsub("23","X",$1);print}' |\
sed 's/,/\./g' |\
cut -d';' -f 1-9 >col1.tsv

paste col1.tsv | \
	sed "s/^/$INSERT_HEADER (DEFAULT,'/" | \
	sed "s/;/','/g" | \
	sed "s/$/',$last_sampleId);/" | \
        sed '$ s/,$/;/' | \
	sed "$ s/,$/;\n$INSERT_HEADER\n/" >>"$RESULT_FILE"

	cp "$VAR"/bashScript/"$RESULT_FILE" "$VAR"/result_insert/

	rm "$RESULT_FILE"
echo $RESULT_FILE;
exit