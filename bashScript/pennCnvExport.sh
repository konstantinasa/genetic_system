#!/bin/bash
source FileDir.sh
sampleName="$1"
RIGHT_NOW=$(date +"%F_%T")
RESULT_FILE="PennCNV-$sampleName-$RIGHT_NOW.csv"
echo "kostas" | psql -h  localhost  -p 5432 -U kostas  -d db_merged  -F $';' --no-align -c "SELECT trumpinys, chromosome, startposition, endposition, noprobes, length, state, copynumber, startprobeid, endprobeid, confidence FROM penncnv JOIN pagrindinelentele on penncnv.pagrindineslentelesid=pagrindinelentele.id JOIN meginys on pagrindinelentele.pacientasid=meginys.pacientasid WHERE meginys.trumpinys='$sampleName'"> "$VAR"/downloadExport/"$RESULT_FILE"

failas="$VAR"/downloadExport/"$RESULT_FILE"
eilute="Sample ID;Chromosome;Start Position (bp);End Position (bp);No, Probes;Length (bp);State;Copy Number;Start Probe ID;End Probe ID;Confidence"

lines=$(($(cat $failas | wc -l)-1))
echo $eilute>tmp
tail -n $lines $failas | head -n $(($lines-1))>>tmp
rm $failas
mv tmp $failas