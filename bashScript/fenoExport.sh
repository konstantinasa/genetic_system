#!/bin/bash
source FileDir.sh
sampleName="$1"
selections="$2"
RIGHT_NOW=$(date +"%F_%T")
RESULT_FILE="Fenotipine-$sampleName-$RIGHT_NOW.csv"
echo "kostas" | psql -h  localhost  -p 5432 -U kostas  -d db_merged  -F $';' --no-align -c $selections > "$VAR"/downloadExport/"$RESULT_FILE"

failas="$VAR"/downloadExport/"$RESULT_FILE"
eilute="Sample ID;Chr ;Position ;SNP Name ;Allele1 - Design ;Allele2 - Design ;Gene(s);In-exon;Mutation(s)"

lines=$(($(cat $failas | wc -l)-1))
echo $eilute>tmp
tail -n $lines $failas | head -n $(($lines-1))>>tmp
rm $failas
mv tmp $failas