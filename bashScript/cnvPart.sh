#!/bin/bash
source FileDir.sh

VAR=/var/www/html
DATA_FILE="$1"
last_sampleId="$2"
RIGHT_NOW=$(date +"%F_%T")
RESULT_FILE="cnvPart$RIGHT_NOW.txt"
INSERT_HEADER="INSERT INTO cnvPart VALUES"

if [ -z "$DATA_FILE" ]; then
	echo "Usage: $0 DATA_FILE"
	exit 1
fi

trap "rm -f col1.tsv" EXIT

tail -n +9 "$DATA_FILE"|\
tr '\t' ';'|\
sed "s/,/./g" | \
cut -d';' -f 2-6 >col1.tsv

paste col1.tsv | \
	sed "s/^/$INSERT_HEADER (DEFAULT,'/" | \
	sed "s/;/','/g" | \
	sed "s/$/',$last_sampleId);/" | \
	sed '$ s/,$/;/' | \
	sed "$ s/,$/;\n$INSERT_HEADER\n/" >>"$RESULT_FILE"

	cp "$VAR"/bashScript/"$RESULT_FILE" "$VAR"/result_insert/

	rm "$RESULT_FILE"
echo $RESULT_FILE;
exit