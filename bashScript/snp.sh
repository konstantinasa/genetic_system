#!/bin/bash
source FileDir.sh

DATA_FILE="$1"
last_sampleId="$2"
RIGHT_NOW=$(date +"%F_%T")
RESULT_FILE="snp$RIGHT_NOW.txt"
INSERT_HEADER='INSERT INTO snp VALUES'

if [ -z "$DATA_FILE" ]; then
	echo "Usage: $0 DATA_FILE"
	exit 1
fi

trap "rm -f col1.tsv col2.tsv col3.tsv col4.tsv col5.tsv" EXIT

tail -n +2 "$DATA_FILE" | \
	cut -d ',' -f 1-5 | \
	sed 's/^chr//' | \
	tr ',' '\t' >col1.tsv

tail -n +2 "$DATA_FILE" | \
	cut -d ',' -f 6- | \
	tr '\t' ' ' | \
	sed -e 's/,NA,/,"",/g' -e 's/,NA,/,"",/g'  -e 's/,"NA",/,"",/g'| \
	sed -e 's/^"//' -e 's/"$//' | \
	sed 's/","/\t/g' | \
	sed 's/",/\t/g' | \
	sed 's/\\/\\\\/g' | \
	sed "s/'/\\\\'/g" >col2.tsv
	
cat col2.tsv | awk -F '\t' '{print $NF}' | awk -F, '{print $1"\t"$2"\t"$3"\t"$4}' >col3.tsv
cat col2.tsv | rev | cut -f 2- | rev > col4.tsv
paste col4.tsv col3.tsv > col5.tsv
	

paste col1.tsv col5.tsv | \
	sed "s/^/$INSERT_HEADER (DEFAULT,'/" | \
	sed "s/\t/','/g" | \
	sed "s/$/',$last_sampleId);/" | \
	sed '$ s/,$/;/' | \
	sed "$ s/,$/;\n$INSERT_HEADER_SQL\n/" >>"$RESULT_FILE"

    cp "$VAR"/bashScript/"$RESULT_FILE" "$VAR"/result_insert/

	rm "$RESULT_FILE"
    echo $RESULT_FILE;

exit