<?php

$lang['db_invalid_connection_str'] = 'Nepavyko nustatyti duomenų bazės nustatymų pagal jūsų pateiktą prisijungimo sakinį.';
$lang['db_unable_to_connect'] = 'Nepavyko prisijungti prie duomenų bazės serverio pagal jūsų pateiktus nustatymus.';
$lang['db_unable_to_select'] = 'Nepavyko pasirinkti nurodytos duomenų bazės: %s';
$lang['db_unable_to_create'] = 'Nepavyko sukurti nurodytos duomenų bazės: %s';
$lang['db_invalid_query'] = 'Užklausa kurią pateikėte yra neteisinga.';
$lang['db_must_set_table'] = 'Turite nustatyti duomenų bazės lentelę, kurią reikia naudoti su jūsų užklausa.';
$lang['db_must_use_set'] = 'Turite naudoti "set" metodą, kad atnaujintumėte įrašą.';
$lang['db_must_use_index'] = 'Turite patikslinti indeksą batch atnaujinimų suderinimui.';
$lang['db_batch_missing_index'] = 'Viena arba kelios juostos pateiktos batch atnaujinimui neturi norodyto indekso.';
$lang['db_must_use_where'] = 'Atnaujinimai nėra leidžiami nebent savyje turi "where" išlygą.';
$lang['db_del_must_use_where'] = 'Ištrynimai nėra leidžiami nebent savyje turi "where" arba "like" išlygas.';
$lang['db_field_param_missing'] = 'Norint atnešti laukus reikia pateikti lentelės pavadinimą kaip parametrą.';
$lang['db_unsupported_function'] = 'Ši savybė neprieinama jūsų duomenų bazei.';
$lang['db_transaction_failure'] = 'Transakcijos klaida: atliekamas sugrąžinimas.';
$lang['db_unable_to_drop'] = 'Nepavyko pašalinti nurodytos duomenų bazės.';
$lang['db_unsuported_feature'] = 'Ši savybė yra nepalaikoma jūsų duomenų bazės platformos.';
$lang['db_unsuported_compression'] = 'Dokumentų suspaudimo formatas nėra palaikomas jūsų serverio.';
$lang['db_filepath_error'] = 'Nepavyko surašyti duomenų į nurodytą dokumentų kelią.';
$lang['db_invalid_cache_path'] = 'Talpyklos kelias kurį nurodėte yra klaidingas arba į jį negalima rašyti.';
$lang['db_table_name_required'] = 'Lentelės pavadinimas yra privalomas operacijai vykdyti.';
$lang['db_column_name_required'] = 'Stulpelio pavadinimas yra privalomas operacijai vykdyti.';
$lang['db_column_definition_required'] = 'Stulpelio apibrėžimas yra privalomas operacijai vykdyti.';
$lang['db_unable_to_set_charset'] = 'Nepavyko nustatyti kliento simbolių rinkinio: %s';
$lang['db_error_heading'] = 'Įvyko duomenų bazės klaida';

/* End of file db_lang.php */
/* Location: ./system/language/english/db_lang.php */
