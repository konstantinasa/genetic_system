<?php

$lang['ut_test_name']		= 'Testinis vardas';
$lang['ut_test_datatype']	= 'Testinis duomenų tipas';
$lang['ut_res_datatype']	= 'Tikimasis duomenų tipas';
$lang['ut_result']			= 'Rezultatas';
$lang['ut_undefined']		= 'Nenustatytas testinis vardas';
$lang['ut_file']			= 'Bylos pavadinimas';
$lang['ut_line']			= 'Linijos numeris';
$lang['ut_passed']			= 'Pavyko';
$lang['ut_failed']			= 'Nepavyko';
$lang['ut_boolean']			= 'Būlio';
$lang['ut_integer']			= 'Sveikasis skaičius';
$lang['ut_float']			= 'Realusis skaičius';
$lang['ut_double']			= 'Realusis skaičius'; // can be the same as float
$lang['ut_string']			= 'Sakinys';
$lang['ut_array']			= 'Masyvas';
$lang['ut_object']			= 'Objektas';
$lang['ut_resource']		= 'Resursas';
$lang['ut_null']			= 'Null';
$lang['ut_notes']			= 'Užrašai';


/* End of file unit_test_lang.php */
/* Location: ./system/language/english/unit_test_lang.php */
