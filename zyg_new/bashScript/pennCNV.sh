#!/bin/bash
source FileDir.sh

DATA_FILE="$1"
last_sampleId="$2"
RIGHT_NOW=$(date +"%F_%T")
RESULT_FILE="pennCnv$RIGHT_NOW.txt"
INSERT_HEADER="INSERT INTO pennCnv VALUES"

if [ -z "$DATA_FILE" ]; then
	echo "Usage: $0 DATA_FILE"
	exit 1
fi

trap "rm -f col1.tsv" EXIT

tail -n +1 "$DATA_FILE"|\
sed -E "s/[[:space:]]+/ /g" |\
tr ':' ';'|\
tr ' ' ';'|\
cut -d';' -f 1-5,7-9|\
tr '-' ';'|\
sed 's/^chr//' |\
sed "s/numsnp=//g" |\
sed "s/length=//g" |\
sed "s/state//g" |\
sed "s/,cn=/;/g" |\
sed "s/startsnp=//g" |\
sed "s/endsnp=//g" |\
sed "s/conf=//g" |\
sed 's/,//g' |\
awk 'BEGIN {FS=OFS=";"} {gsub("23","X",$1);print}'>col1.tsv

paste col1.tsv | \
	sed "s/^/$INSERT_HEADER (DEFAULT,'/" | \
	sed "s/;/','/g" | \
	sed "s/$/',$last_sampleId);/" | \
        sed '$ s/,$/;/' | \
	sed "$ s/,$/;\n$    INSERT_HEADER\n/" >>"$RESULT_FILE"

	cp "$VAR"/bashScript/"$RESULT_FILE" "$VAR"/result_insert/

	rm "$RESULT_FILE"
echo $RESULT_FILE;
exit