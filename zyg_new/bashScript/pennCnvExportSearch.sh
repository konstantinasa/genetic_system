#!/bin/bash
source FileDir.sh
sql1="$1"
sql2="$2"
RIGHT_NOW=$(date +"%F_%T")
RESULT_FILE1="PennCnv-Export-Search-COUNT.csv"
RESULT_FILE2="PennCnv-Export-Search.csv"
RESULT_FILE3="PennCnv-Export-Search-$RIGHT_NOW.csv"
echo "root" |psql -h localhost -p 5432 -U root -d genetic -F $';' --no-align -c "$sql1" > "$VAR"/downloadExport/"$RESULT_FILE1"
echo "root" |psql -h localhost -p 5432 -U root -d genetic -F $';' --no-align -c "$sql2" > "$VAR"/downloadExport/"$RESULT_FILE2"

failas1="$VAR"/downloadExport/"$RESULT_FILE1"
failas2="$VAR"/downloadExport/"$RESULT_FILE2"
eilute2="Sample ID;Chromosome;Start Position (bp);End Position (bp);No, Probes;Length (bp);State;Copy Number;Start Probe ID;End Probe ID;Confidence"

lines=$(($(cat $failas1 | wc -l)-1))
tail -n $lines $failas1 | head -n $(($lines-1))>>tmp
rm $failas1
mv tmp $failas1

lines=$(($(cat $failas2 | wc -l)-1))
echo $eilute2>tmp
tail -n $lines $failas2 | head -n $(($lines-1))>>tmp
rm $failas2
mv tmp $failas2

cat $failas1 $failas2 > "$VAR"/downloadExport/"$RESULT_FILE3"

rm $failas1 $failas2

echo "$RESULT_FILE3";