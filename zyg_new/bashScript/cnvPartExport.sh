#!/bin/bash
source FileDir.sh

sampleName="$1"
RIGHT_NOW=$(date +"%F_%T")
RESULT_FILE="CnvPart-$sampleName-$RIGHT_NOW.csv"

echo "root" | psql -h  localhost  -p 5432 -U root  -d genetic  -F $';' --no-align -c "SELECT trumpinys,chromosome,startposition,endposition,copynumber,confidence FROM cnvpart JOIN pagrindinelentele on cnvpart.pagrindineslentelesid=pagrindinelentele.id JOIN meginys on pagrindinelentele.pacientasid=meginys.pacientasid WHERE meginys.trumpinys='$sampleName'" > "$VAR"/downloadExport/"$RESULT_FILE"

failas="$VAR"/downloadExport/"$RESULT_FILE"
eilute="Sample ID;Chromosome;Start Position (bp);End Position (bp);Copy Number;Confidence"

lines=$(($(cat $failas | wc -l)-1))
echo $eilute>tmp
tail -n $lines $failas | head -n $(($lines-1))>>tmp
rm $failas
mv tmp $failas