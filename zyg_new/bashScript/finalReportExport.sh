#!/bin/bash
source FileDir.sh
sampleName="$1"
RIGHT_NOW=$(date +"%F_%T")
RESULT_FILE="FinalReport-$sampleName-$RIGHT_NOW.csv"
echo "root" | psql -h  localhost  -p 5432 -U root  -d genetic  -F $';' --no-align -c "SELECT trumpinys,chr,position,snpname,allele1design,allele2design,genes,inexon,mutations FROM finalreport JOIN pagrindinelentele on finalreport.pagrindineslentelesid=pagrindinelentele.id JOIN meginys on pagrindinelentele.pacientasid=meginys.pacientasid WHERE meginys.trumpinys='$sampleName'" > "$VAR"/downloadExport/"$RESULT_FILE"

failas="$VAR"/downloadExport/"$RESULT_FILE"
eilute="Sample ID;Chr ;Position ;SNP Name ;Allele1 - Design ;Allele2 - Design ;Gene(s);In-exon;Mutation(s)"

lines=$(($(cat $failas | wc -l)-1))
echo $eilute>tmp
tail -n $lines $failas | head -n $(($lines-1))>>tmp
rm $failas
mv tmp $failas