#!/bin/bash
source FileDir.sh
RIGHT_NOW=$(date +"%F_%T")
RESULT_FILE="Export-Search-$RIGHT_NOW.csv"

cat $1 $2 $3 $4 $5 $6 > "$VAR"/downloadExport/"$RESULT_FILE"

rm "$1" "$2" "$3" "$4" "$5" "$6"

