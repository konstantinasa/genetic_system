#!/bin/bash
source FileDir.sh
sampleName="$1"
RIGHT_NOW=$(date +"%F_%T")
RESULT_FILE="Snp-$sampleName-$RIGHT_NOW.csv"
echo "root" | psql -h  localhost  -p 5432 -U root  -d genetic  -F $';' --no-align -c "SELECT trumpinys,chr,start,ends,ref,alt,cytoband,funcrefgene,generefgene,genedetailrefgene,exonicfuncrefgene,aachangerefgene,snp,homozigosity,dp,refdp,altdp FROM snp JOIN pagrindinelentele on snp.pagrindineslentelesid=pagrindinelentele.id JOIN meginys on pagrindinelentele.pacientasid=meginys.pacientasid WHERE meginys.trumpinys='$sampleName'"> "$VAR"/downloadExport/"$RESULT_FILE"

failas="$VAR"/downloadExport/"$RESULT_FILE"
eilute="Sample ID;Chr;Start;End;Ref;Alt;cytoBand;Func.refGene;Gene.refGene;GeneDetail.refGene;ExonicFunc.refGene;AAChange.refGene;snp142;Homozigosity;DP;Ref_DP;Alt_DP"

lines=$(($(cat $failas | wc -l)-1))
echo $eilute>tmp
tail -n $lines $failas | head -n $(($lines-1))>>tmp
rm $failas
mv tmp $failas