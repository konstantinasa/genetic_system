<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$lang['geneticSearch_heading'] = 'Genetinių failų paieška';
$lang['geneticSearch_search'] = 'Ieškoti';
$lang['geneticSearch_reset'] = 'Išvalyti paieškos laukus';
$lang['geneticSearch_csv'] = 'Eksprotuoti į CSV';
$lang['geneticSearch_csvMerger'] = 'Eksprotuoti į vieną CSV';
$lang['geneticSearch_sampleId'] = 'Sample ID';
$lang['geneticSearch_chromosome'] = 'Chromosoma';
$lang['geneticSearch_start_position'] = 'Start Position (bp)';
$lang['geneticSearch_end_position'] = 'End Position (bp)';
$lang['geneticSearch_location'] = 'Location';
$lang['geneticSearch_gene'] = 'Gene';
$lang['geneticSearch_exonicFunction'] = 'Exonic function';
$lang['geneticSearch_snpName'] = 'SNP name';
$lang['geneticSearch_length'] = 'Length (bp)';
$lang['geneticSearch_copyNumber'] = 'Copy Number';
$lang['geneticSearch_result'] = 'Rezultatai :';
$lang['geneticSearchCSV_result'] = 'Failą galite pamatyti Atsiųsti CSV';
$lang['geneticSearch_quantisnp'] = 'Quanti';
$lang['geneticSearch_penncnv'] = 'PennCNV';
$lang['geneticSearch_cnvpart'] = 'CNVpart';
$lang['geneticSearch_finalreport'] = 'FinalReport';
$lang['geneticSearch_indel'] = 'INDEL';
$lang['geneticSearch_snp'] = 'SNP';

