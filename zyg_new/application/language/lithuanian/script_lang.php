<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$lang['script_information']='Informacija : ';
$lang['script_heading'] = 'Skriptai';
$lang['script_copyFile_information']='Nukopijuoja esamus failus į kitą aplanką.';
$lang['script_copyFile_button']='Kopijuoti';
$lang['script_rmFile_information']='Ištrina failus, kad nesidubliuotų.';
$lang['script_rmFile_button']='Ištrinti';
$lang['script_run']='Paleisti';
$lang['script_sampleId']='Sample ID';
$lang['script_file']='Failas';