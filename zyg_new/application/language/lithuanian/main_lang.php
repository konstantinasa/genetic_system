<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['main_insertDate'] = 'Įkėlimo data';
$lang['main_username'] = 'Vartotojo vardas';
$lang['main_sampleId'] = 'Mėginio pavadinimas';
$lang['main_patientId']='Paciento ID';
$lang['main_allRecords'] = 'Iš viso įrašų - ';
$lang['main_heading'] = 'Pagrindinė lentelė';
$lang['main_create'] = 'Sukurti naują įrašą';
$lang['main_action'] = 'Veiksmas';
$lang['main_delete'] = 'Trinti';
$lang['create_main_heading'] = 'Naujas įrašas į pagrindinę lentelę';
$lang['create_main_subheading'] = 'Prašau suvesti informaciją.';
$lang['create_main_sampleId_label'] = 'Sample Id';
$lang['create_main_patient_label'] = 'Paciento Id';
$lang['create_main_submit_btn'] = 'Pateikti';



