<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Name:  Auth Lang - English
 *
 * Author: Ben Edmunds
 *          ben.edmunds@gmail.com
 * @benedmunds
 *
 * Author: Daniel Davis
 * @ourmaninjapan
 *
 * Location: http://github.com/benedmunds/ion_auth/
 *
 * Created:  03.09.2013
 *
 * Description:  English language file for Ion Auth example views
 *
 */

// Errors
$lang['error_csrf'] = 'This form post did not pass our security checks.';

// Login
$lang['login_heading'] = 'Prisijungimas';
$lang['login_subheading'] = 'Prašome prisijungti su savo vartotojo vardu ir slaptažodžiu.';
$lang['login_identity_label'] = 'Vartotojo vardas';
$lang['login_password_label'] = 'Slaptažodis';
$lang['login_remember_label'] = 'Prisiminti mane';
$lang['login_submit_btn'] = 'Prisijungti';
$lang['login_forgot_password'] = 'Pamiršote slaptažodį?';

// Index
$lang['index_heading'] = 'Vartotojai';
$lang['index_subheading'] = 'Žemiau yra pateiktas vartotojų sąrašas.';
$lang['index_fname_th'] = 'Vardas';
$lang['index_lname_th'] = 'Pavardė';
$lang['index_email_th'] = 'El. paštas';
$lang['index_username_th'] = 'Vartotojo vardas';
$lang['index_groups_th'] = 'Grupė';
$lang['index_status_th'] = 'Statusas';
$lang['index_action_th'] = 'Veiksmai';
$lang['index_active_link'] = 'Aktyvus';
$lang['index_inactive_link'] = 'Neaktyvus';
$lang['index_create_user_link'] = 'Sukurti naują vartotoją';
$lang['index_create_group_link'] = 'Sukurti naują grupę';
$lang['index_edit'] = 'Redaguoti';
$lang['index_delete'] = 'Ištrinti';
$lang['index_logout'] = 'Atsijungti';

// Deactivate User
$lang['deactivate_heading'] = 'Vartotojo neaktyvumas';
$lang['deactivate_subheading'] = 'Ar tikria norite išjungti vartotoją \'%s\'?';
$lang['deactivate_confirm_y_label'] = 'Taip:';
$lang['deactivate_confirm_n_label'] = 'Ne:';
$lang['deactivate_submit_btn'] = 'Pateikti';
$lang['deactivate_validation_confirm_label'] = 'patvirtinimas';
$lang['deactivate_validation_user_id_label'] = 'Vartotojo ID';

// Create User
$lang['create_user_heading'] = 'Naujas vartotojas';
$lang['create_user_subheading'] = 'Prašau suvesti informaciją apie vartotoją žemiau.';
$lang['create_user_fname_label'] = 'Vardas';
$lang['create_user_lname_label'] = 'Pavardė';
$lang['create_user_company_label'] = 'Kompanijos pavadinimas';
$lang['create_user_email_label'] = 'El. paštas';
$lang['create_user_phone_label'] = 'Telefonas';
$lang['create_user_password_label'] = 'Slaptažodis';
$lang['create_user_password_confirm_label'] = 'Pakartokite slaptažodį';
$lang['create_user_submit_btn'] = 'Pateikti naują varototoją';
$lang['create_user_validation_fname_label'] = 'Vardas';
$lang['create_user_validation_lname_label'] = 'Pavardė';
$lang['create_user_validation_email_label'] = 'El. pašto adresas';
$lang['create_user_validation_phone1_label'] = 'Telefonas';
$lang['create_user_validation_phone2_label'] = 'Telefonas';
$lang['create_user_validation_phone3_label'] = 'Telefonas';
$lang['create_user_validation_company_label'] = 'Kompanijos pavadinimas';
$lang['create_user_validation_password_label'] = 'Slaptažodis';
$lang['create_user_validation_password_confirm_label'] = 'Pakartokite slaptažodį';

// Edit User
$lang['edit_user_heading'] = 'Redaguoti vartotoją';
$lang['edit_user_subheading'] = 'Prašome pateikti informaciją apie varotoją žemiau.';
$lang['edit_user_fname_label'] = 'Vardas';
$lang['edit_user_lname_label'] = 'Pavardė';
$lang['edit_user_company_label'] = 'Kompanijos pavadinimas';
$lang['edit_user_email_label'] = 'El. paštas';
$lang['edit_user_phone_label'] = 'Telefonas';
$lang['edit_user_password_label'] = 'Slaptažodis (jeigu keičiate slaptažodį)';
$lang['edit_user_password_confirm_label'] = 'Pakartokite slaptažodį (jeigu keičiate slaptažodį)';
$lang['edit_user_groups_heading'] = 'Vartotojo grupės';
$lang['edit_user_submit_btn'] = 'Išsaugoti vartotojo pakeitimus';
$lang['edit_user_validation_fname_label'] = 'Vardas';
$lang['edit_user_validation_lname_label'] = 'Pavardė';
$lang['edit_user_validation_email_label'] = 'El. pašto adresas';
$lang['edit_user_validation_phone1_label'] = 'Telefonas';
$lang['edit_user_validation_phone2_label'] = 'Telefonas';
$lang['edit_user_validation_phone3_label'] = 'Telefonas';
$lang['edit_user_validation_company_label'] = 'Kompanijos pavadinimas';
$lang['edit_user_validation_groups_label'] = 'Grupė';
$lang['edit_user_validation_password_label'] = 'Slaptažodis';
$lang['edit_user_validation_password_confirm_label'] = 'Pakartokite slaptažodį';

// Create Group
$lang['create_group_title'] = 'Nauja grupė';
$lang['create_group_heading'] = 'Nauja grupė';
$lang['create_group_subheading'] = 'Prašome suvesti informaciją apie naują grupę žemiau.';
$lang['create_group_name_label'] = 'Grupės pavadinimas';
$lang['create_group_desc_label'] = 'Aprašymas';
$lang['create_group_submit_btn'] = 'Išsaugoti naują grupę';
$lang['create_group_validation_name_label'] = 'Grupės pavadinimas';
$lang['create_group_validation_desc_label'] = 'Aprašymas';

//See Group
$lang['see_group_heading'] = 'Grupės';
$lang['see_group_th'] = 'Grupė';
$lang['see_group_desc_th'] = 'Aprašymas';
$lang['see_group_action_th'] = 'Veiksmai';
$lang['see_group_edit'] = 'Redaguoti';
$lang['see_group_delete'] = 'Trinti';

// Edit Group
$lang['edit_group_title'] = 'Redaguoti grupę';
$lang['edit_group_saved'] = 'Išsaugota grupė';
$lang['edit_group_heading'] = 'Redaguoti grupę';
$lang['edit_group_subheading'] = 'Prašau suvesti informaciją apie grupę žemiau.';
$lang['edit_group_name_label'] = 'Grupės pavadinimas';
$lang['edit_group_desc_label'] = 'Aprašymas';
$lang['edit_group_submit_btn'] = 'Išsaugoti pakeitimus';
$lang['edit_group_validation_name_label'] = 'Grupės pavadinimas';
$lang['edit_group_validation_desc_label'] = 'Aprašymas';

// Change Password
$lang['change_password_heading'] = 'Pakeisti slaptažodį';
$lang['change_password_old_password_label'] = 'Senas slaptažodis:';
$lang['change_password_new_password_label'] = 'Naujas slaptažodis (ne mažiaus %s simbolių ilgio):';
$lang['change_password_new_password_confirm_label'] = 'Pakartokite naują slaptažodį:';
$lang['change_password_submit_btn'] = 'Išsaugoti pakeistą slaptažodį';
$lang['change_password_validation_old_password_label'] = 'Senas slaptažodis';
$lang['change_password_validation_new_password_label'] = 'Naujas slaptažodis';
$lang['change_password_validation_new_password_confirm_label'] = 'Pakartokite naują slaptažodį';

// Forgot Password
$lang['forgot_password_heading'] = 'Pamiršote slaptažodį?';
$lang['forgot_password_subheading'] = 'Prašome suvesti savo %s, tada mes galėsime išsiųsti Jums el. paštu naujus slaptažodžio nustatymus.';
$lang['forgot_password_email_label'] = '%s';
$lang['forgot_password_submit_btn'] = 'Pateikti';
$lang['forgot_password_validation_email_label'] = 'El. pašto adresas';
$lang['forgot_password_username_identity_label'] = 'Vartotojo vardas';
$lang['forgot_password_email_identity_label'] = 'El. paštas';
$lang['forgot_password_email_not_found'] = 'Nėra tokio el. pašto adreso įrašuose.';

// Reset Password
$lang['reset_password_heading'] = 'Pakeisti slaptažodį';
$lang['reset_password_new_password_label'] = 'Naujas slaptažodis (ne mažiaus %s simbolių ilgio)';
$lang['reset_password_new_password_confirm_label'] = 'Pakartokite naują slaptažodį';
$lang['reset_password_submit_btn'] = 'Pakeisti slaptažodį';
$lang['reset_password_validation_new_password_label'] = 'Naujas Slaptažodis';
$lang['reset_password_validation_new_password_confirm_label'] = 'Pakartokite naują slaptažodį';

// Activation Email
$lang['email_activate_heading'] = 'Aktyvuoti vartotoją %s';
$lang['email_activate_subheading'] = 'Prašome paspausti šią nuorodą %s.';
$lang['email_activate_link'] = 'Aktyvuoti Jūsų vartotoją';

// Forgot Password Email
$lang['email_forgot_password_heading'] = 'Atstatyti slaptažodį vartotojui %s';
$lang['email_forgot_password_subheading'] = 'Prašome paspausti šią nuorodą %s.';
$lang['email_forgot_password_link'] = 'Atstatyti Jūsų slaptažodį';

// New Password Email
$lang['email_new_password_heading'] = 'Naujas slaptažodis vartotojui %s';
$lang['email_new_password_subheading'] = 'Jūsų slaptažodis buvo atstatytas: %s';

