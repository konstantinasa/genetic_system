<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['include_create_user_group'] = 'Vartotojo ir grupės nustatymai';
$lang['include_user_list'] = 'Vartotojų sarašas';
$lang['include_group_list'] = 'Grupių sarašas';
$lang['include_user_information'] = 'Vartotojo informacija';
$lang['include_change_password'] = 'Pasikeisti slaptažodį';
$lang['include_welcome']='Sveiki prisijungę, %s';
$lang['include_mainTable']='Pagrindinė lentelė';
$lang['include_genetic_file']='Genetiniai failai';
$lang['include_quantiSnp']='QuantiSNP';
$lang['include_pennCnv']='PennCNV';
$lang['include_cnvpart']='CNVpart';
$lang['include_finalReport']='FinalReport';
$lang['include_snp'] = 'SNP';
$lang['include_indel'] = 'INDEL';
$lang['include_file']='Failai';
$lang['include_file_download']='Atsisiųsti CSV';
$lang['include_file_upload'] = 'Įkelti naują failą';
$lang['include_script'] = 'Skriptai';
$lang['include_search'] = 'Genetinių failų paieška';



