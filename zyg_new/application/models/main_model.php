<?php

Class Main_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    protected $table_mainTable = 'pagrindinelentele';
    protected $table_users = 'users';
    protected $table_patient = 'pacientas';
    protected $table_sample = 'meginys';

    public function record_count()
    {
        $query = $this->db->query("SELECT * FROM $this->table_mainTable");
        return $query->num_rows();
    }

    public function fetch_main($limit, $offset)
    {
        $query = $this->db->query("SELECT mt.id,mt.ikelimodata,u.username,ts.trumpinys as meginioid,mt.pacientasid,p.pacientas
                                   FROM $this->table_mainTable mt, $this->table_users u,$this->table_patient p,$this->table_sample ts
                                   WHERE mt.vartotojoid=u.id AND mt.pacientasid=p.id AND mt.pacientasid=ts.pacientasid
                                    LIMIT $limit OFFSET $offset");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getAllPatient()
    {
        $query = $this->db->query("SELECT p.id, p.pacientas FROM $this->table_patient p");
        return $query->result();
    }

    public function create_mainTable($data)
    {
        $this->db->query("INSERT INTO $this->table_mainTable VALUES (DEFAULT,'now()','" . $data['vartotojoid'] . "','" . $data['pacientasid'] . "')");
    }

    public function delete_main($id)
    {
        $this->db->delete($this->table_mainTable, array('id' => $id));
    }
}

?>
