<?php

Class File_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    protected $table_mainTable = 'pagrindinelentele';
    protected $table_file = 'failas';
    protected $table_sample = 'meginys';

    function search($query_array, $limit, $offset, $sort_by, $sort_order)
    {

        $sort_order = ($sort_order == 'desc') ? 'desc' : 'asc';
        $sort_columns = array('trumpinys', 'failopavadinimas', 'ikelimodata');
        $sort_by = (in_array($sort_by, $sort_columns)) ? $sort_by : 'trumpinys';

        // results query
        $q = $this->db->select('failas.id as failasid,trumpinys,failopavadinimas,failas.ikelimodata,pagrindinelentele.id')
            ->from($this->table_file)
            ->join($this->table_mainTable, 'failas.pagrindineslentelesid=pagrindinelentele.id')
            ->join($this->table_sample, 'pagrindinelentele.pacientasid=meginys.pacientasid')
            ->limit($limit, $offset)
            ->order_by($sort_by, $sort_order);

        if (strlen($query_array['trumpinys'])) {
            $q->where('trumpinys', $query_array['trumpinys']);
        }

        $ret['rows'] = $q->get()->result();

        // count query
        $q = $this->db->select('COUNT(*) as count', FALSE)
            ->from($this->table_file)
            ->join($this->table_mainTable, 'failas.pagrindineslentelesid=pagrindinelentele.id')
            ->join($this->table_sample, 'pagrindinelentele.pacientasid=meginys.pacientasid');

        if (strlen($query_array['trumpinys'])) {
            $q->where('trumpinys', $query_array['trumpinys']);
        }

        $tmp = $q->get()->result();

        $ret['num_rows'] = $tmp[0]->count;

        return $ret;
    }

    public function getAllSample()
    {
        $query = $this->db->query("SELECT mt.id,ts.trumpinys "
            . "FROM $this->table_mainTable mt,$this->table_sample ts "
            . "WHERE mt.pacientasid=ts.pacientasid; ");
        return $query->result();
    }


    public function insertFile($data)
    {
        $this->db->query("INSERT INTO $this->table_file VALUES (DEFAULT,'" . $data['failopavadinimas'] . "','now()','" . $data['pagrindineslentelesid'] . "')");
    }

    public function deleteFileFromDatabase($data)
    {
        $this->db->delete($this->table_file, array('id' => $data));
    }


    public function downloadFile()
    {

        $dir = 'downloadExport';
        $files = array();
        if (is_dir($dir)) {
            if ($dh = opendir($dir)) {
                while (($file = readdir($dh)) !== false) {
                    if ($file == '.' || $file == '..') {
                        continue;
                    }
                    $files[] = $file;
                }
                closedir($dh);
            }
            return $files;
        }
    }
}

?>
