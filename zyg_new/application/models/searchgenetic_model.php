<?php

Class Searchgenetic_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    protected $table_sample = 'meginys';
    protected $table_mainTable = 'pagrindinelentele';
    protected $table_snp = 'snp';
    protected $table_indel = 'indel';
    protected $table_finalReport = 'finalreport';
    protected $table_quantiSnp = 'quantisnp';
    protected $table_pennCnv = 'penncnv';
    protected $table_cnvPart = 'cnvpart';

    public function searchSnp($sampleId, $chromosome, $startPosition, $endPosition, $location, $gene, $exonicFunction, $snpName)
    {

        $query = "SELECT count(*)
                FROM $this->table_snp ts, $this->table_mainTable tm,$this->table_sample tse
                WHERE ts.pagrindineslentelesid=tm.id AND tm.pacientasid=tse.pacientasid";

        if (!empty($sampleId)) {
            $sampleIdArray = array();
            $sampleIdExplode = explode(',', $sampleId);
            foreach ($sampleIdExplode as $t) {
                $sampleIdArray[] = "'" . $t . "'";
            }
            $searchSampleId = implode(",", $sampleIdArray);
            $query = $query . " AND tse.trumpinys IN ($searchSampleId)";
        }

        if (!empty($chromosome)) {
            $chromosomeArray = array();
            $chromosomeExplode = explode(',', $chromosome);
            foreach ($chromosomeExplode as $t) {
                $chromosomeArray[] = "'" . $t . "'";
            }
            $searchChromosome = implode(",", $chromosomeArray);
            $query = $query . " AND ts.chr IN ($searchChromosome)";
        }

        if (!empty($startPosition)) {
            $startPositionArray = array();
            $startPositionExplode = explode(',', $startPosition);
            foreach ($startPositionExplode as $t) {
                $startPositionArray[] = "'" . $t . "'";
            }
            $searchStartPosition = implode(",", $startPositionArray);
            $query = $query . " AND ts.start IN ($searchStartPosition)";
        }

        if (!empty($endPosition)) {
            $endPositionArray = array();
            $endPositionExplode = explode(',', $endPosition);
            foreach ($endPositionExplode as $t) {
                $endPositionArray[] = "'" . $t . "'";
            }
            $searchEndPosition = implode(",", $endPositionArray);
            $query = $query . " AND ts.ends IN ($searchEndPosition)";
        }

        if (!empty($location)) {
            $locationArray = array();
            $locationExplode = explode(',', $location);
            foreach ($locationExplode as $t) {
                $locationArray[] = "'" . $t . "'";
            }
            $searchLocation = implode(",", $locationArray);
            $query = $query . " AND ts.funcrefgene IN ($searchLocation)";
        }

        if (!empty($gene)) {
            $geneArray = array();
            $geneExplode = explode(',', $gene);
            foreach ($geneExplode as $t) {
                $geneArray[] = "'" . $t . "'";
            }
            $searchGene = implode(",", $geneArray);
            $query = $query . " AND ts.generefgene IN ($searchGene)";
        }

        if (!empty($exonicFunction)) {
            $exonicFunctionArray = array();
            $exonicFunctionExplode = explode(',', $exonicFunction);
            foreach ($exonicFunctionExplode as $t) {
                $exonicFunctionArray[] = "'" . $t . "'";
            }
            $searchExonicFunction = implode(",", $exonicFunctionArray);
            $query = $query . " AND ts.exonicfuncrefgene IN ($searchExonicFunction)";
        }

        if (!empty($snpName)) {
            $snpArray = array();
            $snpExplode = explode(',', $snpName);
            foreach ($snpExplode as $t) {
                $snpArray[] = "'" . $t . "'";
            }
            $searchSnp = implode(",", $snpArray);
            $query = $query . " AND ts.snp IN ($searchSnp)";
        }

        $querys = $this->db->query($query);
        $result = $querys->result();
        return $result;
    }

    public function searchIndel($sampleId, $chromosome, $startPosition, $endPosition, $location, $gene, $exonicFunction, $snpName)
    {

        $query = "SELECT count(*)
                FROM $this->table_indel ti, $this->table_mainTable tm,$this->table_sample tse
                WHERE ti.pagrindineslentelesid=tm.id AND tm.pacientasid=tse.pacientasid";

        if (!empty($sampleId)) {
            $sampleIdArray = array();
            $sampleIdExplode = explode(',', $sampleId);
            foreach ($sampleIdExplode as $t) {
                $sampleIdArray[] = "'" . $t . "'";
            }
            $searchSampleId = implode(",", $sampleIdArray);
            $query = $query . " AND tse.trumpinys IN ($searchSampleId)";
        }

        if (!empty($chromosome)) {
            $chromosomeArray = array();
            $chromosomeExplode = explode(',', $chromosome);
            foreach ($chromosomeExplode as $t) {
                $chromosomeArray[] = "'" . $t . "'";
            }
            $searchChromosome = implode(",", $chromosomeArray);
            $query = $query . " AND ti.chr IN ($searchChromosome)";
        }

        if (!empty($startPosition)) {
            $startPositionArray = array();
            $startPositionExplode = explode(',', $startPosition);
            foreach ($startPositionExplode as $t) {
                $startPositionArray[] = "'" . $t . "'";
            }
            $searchStartPosition = implode(",", $startPositionArray);
            $query = $query . " AND ti.start IN ($searchStartPosition)";
        }

        if (!empty($endPosition)) {
            $endPositionArray = array();
            $endPositionExplode = explode(',', $endPosition);
            foreach ($endPositionExplode as $t) {
                $endPositionArray[] = "'" . $t . "'";
            }
            $searchEndPosition = implode(",", $endPositionArray);
            $query = $query . " AND ti.ends IN ($searchEndPosition)";
        }

        if (!empty($location)) {
            $locationArray = array();
            $locationExplode = explode(',', $location);
            foreach ($locationExplode as $t) {
                $locationArray[] = "'" . $t . "'";
            }
            $searchLocation = implode(",", $locationArray);
            $query = $query . " AND ti.funcrefgene IN ($searchLocation)";
        }

        if (!empty($gene)) {
            $geneArray = array();
            $geneExplode = explode(',', $gene);
            foreach ($geneExplode as $t) {
                $geneArray[] = "'" . $t . "'";
            }
            $searchGene = implode(",", $geneArray);
            $query = $query . " AND ti.generefgene IN ($searchGene)";
        }

        if (!empty($exonicFunction)) {
            $exonicFunctionArray = array();
            $exonicFunctionExplode = explode(',', $exonicFunction);
            foreach ($exonicFunctionExplode as $t) {
                $exonicFunctionArray[] = "'" . $t . "'";
            }
            $searchExonicFunction = implode(",", $exonicFunctionArray);
            $query = $query . " AND ti.exonicfuncrefgene IN ($searchExonicFunction)";
        }

        if (!empty($snpName)) {
            $snpArray = array();
            $snpExplode = explode(',', $snpName);
            foreach ($snpExplode as $t) {
                $snpArray[] = "'" . $t . "'";
            }
            $searchSnp = implode(",", $snpArray);
            $query = $query . " AND ti.snp IN ($searchSnp)";
        }

        $querys = $this->db->query($query);
        $result = $querys->result();
        return $result;
    }

    public function searchFinalReport($sampleId, $chromosome, $startPosition, $location, $gene, $exonicFunction, $snpName)
    {
        $query = "SELECT count(*)
                FROM $this->table_finalReport tfr, $this->table_mainTable tm,$this->table_sample tse
                WHERE tfr.pagrindineslentelesid=tm.id AND tm.pacientasid=tse.pacientasid";

        if (!empty($sampleId)) {
            $sampleIdArray = array();
            $sampleIdExplode = explode(',', $sampleId);
            foreach ($sampleIdExplode as $t) {
                $sampleIdArray[] = "'" . $t . "'";
            }
            $searchSampleId = implode(",", $sampleIdArray);
            $query = $query . " AND tse.trumpinys IN ($searchSampleId)";
        }

        if (!empty($chromosome)) {
            $chromosomeArray = array();
            $chromosomeExplode = explode(',', $chromosome);
            foreach ($chromosomeExplode as $t) {
                $chromosomeArray[] = "'" . $t . "'";
            }
            $searchChromosome = implode(",", $chromosomeArray);
            $query = $query . " AND tfr.chr IN ($searchChromosome)";
        }

        if (!empty($startPosition)) {
            $startPositionArray = array();
            $startPositionExplode = explode(',', $startPosition);
            foreach ($startPositionExplode as $t) {
                $startPositionArray[] = "'" . $t . "'";
            }
            $searchStartPosition = implode(",", $startPositionArray);
            $query = $query . " AND tfr.position     IN ($searchStartPosition)";
        }

        if (!empty($location)) {
            $locationArray = array();
            $locationExplode = explode(',', $location);
            foreach ($locationExplode as $t) {
                $locationArray[] = "'" . $t . "'";
            }
            $searchLocation = implode(",", $locationArray);
            $query = $query . " AND tfr.inexon IN ($searchLocation)";
        }

        if (!empty($gene)) {
            $geneArray = array();
            $geneExplode = explode(',', $gene);
            foreach ($geneExplode as $t) {
                $geneArray[] = "'" . $t . "'";
            }
            $searchGene = implode(",", $geneArray);
            $query = $query . " AND tfr.genes IN ($searchGene)";
        }

        if (!empty($exonicFunction)) {
            $exonicFunctionArray = array();
            $exonixFunctionExplode = explode(',', $exonicFunction);
            foreach ($exonixFunctionExplode as $t) {
                $exonicFunctionArray[] = "'" . $t . "'";
            }
            $searchExonicFunction = implode(",", $exonicFunctionArray);
            $query = $query . " AND tfr.mutations IN ($searchExonicFunction)";
        }

        if (!empty($snpName)) {
            $snpArray = array();
            $snpNameExplode = explode(',', $snpName);
            foreach ($snpNameExplode as $t) {
                $snpArray[] = "'" . $t . "'";
            }
            $searchSnpName = implode(",", $snpArray);
            $query = $query . " AND tfr.snpname IN ($searchSnpName)";
        }

        $querys = $this->db->query($query);
        $result = $querys->result();
        return $result;
    }

    public function searchQuantiSnp($sampleId, $chromosome, $startPosition, $endPosition, $length, $copyNumber)
    {
        $query = "SELECT count(*)
                FROM $this->table_quantiSnp tqs, $this->table_mainTable tm,$this->table_sample tse
                WHERE tqs.pagrindineslentelesid=tm.id AND tm.pacientasid=tse.pacientasid";

        if (!empty($sampleId)) {
            $sampleIdArray = array();
            $sampleIdExplode = explode(',', $sampleId);
            foreach ($sampleIdExplode as $t) {
                $sampleIdArray[] = "'" . $t . "'";
            }
            $searchSampleId = implode(",", $sampleIdArray);
            $query = $query . " AND tse.trumpinys IN ($searchSampleId)";
        }

        if (!empty($chromosome)) {
            $chromosomeArray = array();
            $chromosomeExplode = explode(',', $chromosome);
            foreach ($chromosomeExplode as $t) {
                $chromosomeArray[] = "'" . $t . "'";
            }
            $searchChromosome = implode(",", $chromosomeArray);
            $query = $query . " AND tqs.chromosome IN ($searchChromosome)";
        }

        if (!empty($startPosition)) {
            $startPositionArray = array();
            $startPositionExplode = explode(',', $startPosition);
            foreach ($startPositionExplode as $t) {
                $startPositionArray[] = "'" . $t . "'";
            }
            $searchStartPosition = implode(",", $startPositionArray);
            $query = $query . " AND tqs.startposition IN ($searchStartPosition)";
        }

        if (!empty($endPosition)) {
            $endPositionArray = array();
            $endPositionExplode = explode(',', $endPosition);
            foreach ($endPositionExplode as $t) {
                $endPositionArray[] = "'" . $t . "'";
            }
            $searchEndPosition = implode(",", $endPositionArray);
            $query = $query . " AND tqs.endposition IN ($searchEndPosition)";
        }

        if (!empty($length)) {
            $lengthArray = array();
            $lengthExplode = explode(',', $length);
            foreach ($lengthExplode as $t) {
                $lengthArray[] = "'" . $t . "'";
            }
            $searchLength = implode(",", $lengthArray);
            $query = $query . " AND tqs.length IN ($searchLength)";
        }

        if (!empty($copyNumber)) {
            $copyNumbeArray = array();
            $copyNumberExplode = explode(',', $copyNumber);
            foreach ($copyNumberExplode as $t) {
                $copyNumbeArray[] = "'" . $t . "'";
            }
            $searchCopyNumber = implode(",", $copyNumbeArray);
            $query = $query . " AND tqs.copynumber IN ($searchCopyNumber)";
        }


        $querys = $this->db->query($query);
        $result = $querys->result();
        return $result;
    }

    public function searchPennCNV($sampleId, $chromosome, $startPosition, $endPosition, $length, $copyNumber)
    {
        $query = "SELECT count(*)
                FROM $this->table_pennCnv tpc, $this->table_mainTable tm,$this->table_sample tse
                WHERE tpc.pagrindineslentelesid=tm.id AND tm.pacientasid=tse.pacientasid";

        if (!empty($sampleId)) {
            $sampleIdArray = array();
            $sampleIdExplode = explode(',', $sampleId);
            foreach ($sampleIdExplode as $t) {
                $sampleIdArray[] = "'" . $t . "'";
            }
            $searchSampleId = implode(",", $sampleIdArray);
            $query = $query . " AND tse.trumpinys IN ($searchSampleId)";
        }
        if (!empty($chromosome)) {
            $chromosomeArray = array();
            $chromosomeExplode = explode(',', $chromosome);
            foreach ($chromosomeExplode as $t) {
                $chromosomeArray[] = "'" . $t . "'";
            }
            $searchChromosome = implode(",", $chromosomeArray);
            $query = $query . " AND tpc.chromosome IN ($searchChromosome)";
        }

        if (!empty($startPosition)) {
            $startPositionArray = array();
            $startPositionExplode = explode(',', $startPosition);
            foreach ($startPositionExplode as $t) {
                $startPositionArray[] = "'" . $t . "'";
            }
            $searchStartPosition = implode(",", $startPositionArray);
            $query = $query . " AND tpc.startposition IN ($searchStartPosition)";
        }

        if (!empty($endPosition)) {
            $endPositionArray = array();
            $endPositionExplode = explode(',', $endPosition);
            foreach ($endPositionExplode as $t) {
                $endPositionArray[] = "'" . $t . "'";
            }
            $searchEndPosition = implode(",", $endPositionArray);
            $query = $query . " AND tpc.endposition IN ($searchEndPosition)";
        }

        if (!empty($length)) {
            $lengthArray = array();
            $lengthExplode = explode(',', $length);
            foreach ($lengthExplode as $t) {
                $lengthArray[] = "'" . $t . "'";
            }
            $searchLength = implode(",", $lengthArray);
            $query = $query . " AND tpc.length IN ($searchLength)";
        }

        if (!empty($copyNumber)) {
            $copyNumbeArray = array();
            $copyNumberExplode = explode(',', $copyNumber);
            foreach ($copyNumberExplode as $t) {
                $copyNumbeArray[] = "'" . $t . "'";
            }
            $searchCopyNumber = implode(",", $copyNumbeArray);
            $query = $query . " AND tpc.copynumber IN ($searchCopyNumber)";
        }

        $querys = $this->db->query($query);
        $result = $querys->result();
        return $result;
    }

    public function searchCNVPart($sampleId, $chromosome, $startPosition, $endPosition, $copyNumber)
    {
        $query = "SELECT count(*)
                FROM $this->table_cnvPart tcp, $this->table_mainTable tm,$this->table_sample tse
                WHERE tcp.pagrindineslentelesid=tm.id AND tm.pacientasid=tse.pacientasid";

        if (!empty($sampleId)) {
            $sampleIdArray = array();
            $sampleIdExplode = explode(',', $sampleId);
            foreach ($sampleIdExplode as $t) {
                $sampleIdArray[] = "'" . $t . "'";
            }
            $searchSampleId = implode(",", $sampleIdArray);
            $query = $query . " AND tse.trumpinys IN ($searchSampleId)";
        }

        if (!empty($chromosome)) {
            $chromosomeArray = array();
            $chromosomeExplode = explode(',', $chromosome);
            foreach ($chromosomeExplode as $t) {
                $chromosomeArray[] = "'" . $t . "'";
            }
            $searchChromosome = implode(",", $chromosomeArray);
            $query = $query . " AND tcp.chromosome IN ($searchChromosome)";
        }

        if (!empty($startPosition)) {
            $startPositionArray = array();
            $startPositionExplode = explode(',', $startPosition);
            foreach ($startPositionExplode as $t) {
                $startPositionArray[] = "'" . $t . "'";
            }
            $searchStartPosition = implode(",", $startPositionArray);
            $query = $query . " AND tcp.startposition IN ($searchStartPosition)";
        }

        if (!empty($endPosition)) {
            $endPositionArray = array();
            $endPositionExplode = explode(',', $endPosition);
            foreach ($endPositionExplode as $t) {
                $endPositionArray[] = "'" . $t . "'";
            }
            $searchEndPosition = implode(",", $endPositionArray);
            $query = $query . " AND tcp.endposition IN ($searchEndPosition)";
        }

        if (!empty($copyNumber)) {
            $copyNumbeArray = array();
            $copyNumberExplode = explode(',', $copyNumber);
            foreach ($copyNumberExplode as $t) {
                $copyNumbeArray[] = "'" . $t . "'";
            }
            $searchCopyNumber = implode(",", $copyNumbeArray);
            $query = $query . " AND tcp.copynumber IN ($searchCopyNumber)";
        }

        $querys = $this->db->query($query);
        $result = $querys->result();
        return $result;
    }
}

?>
