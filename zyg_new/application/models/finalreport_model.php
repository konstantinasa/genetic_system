<?php

Class Finalreport_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    protected $table_mainTable = 'pagrindinelentele';
    protected $table_finalReport = 'finalreport';
    protected $table_sample = 'meginys';

    function search($query_array, $limit, $offset, $sort_by, $sort_order)
    {

        $sort_order = ($sort_order == 'desc') ? 'desc' : 'asc';
        $sort_columns = array('trumpinys', 'chr', 'position', 'snpname', 'allele1design', 'allele2design', 'genes', 'inexon', 'mutations');
        $sort_by = (in_array($sort_by, $sort_columns)) ? $sort_by : 'trumpinys';

        // results query
        $q = $this->db->select('trumpinys,chr,position,snpname,allele1design,allele2design,genes,inexon,mutations')
            ->from($this->table_finalReport)
            ->join($this->table_mainTable, 'finalreport.pagrindineslentelesid=pagrindinelentele.id')
            ->join($this->table_sample, 'pagrindinelentele.pacientasid=meginys.pacientasid')
            ->limit($limit, $offset)
            ->order_by($sort_by, $sort_order);


        if (strlen($query_array['trumpinys'])) {
            $q->where('trumpinys', $query_array['trumpinys']);
        }

        $ret['rows'] = $q->get()->result();

        // count query
        $q = $this->db->select('COUNT(*) as count', FALSE)
            ->from($this->table_finalReport)
            ->join($this->table_mainTable, 'finalreport.pagrindineslentelesid=pagrindinelentele.id')
            ->join($this->table_sample, 'pagrindinelentele.pacientasid=meginys.pacientasid');

        if (strlen($query_array['trumpinys'])) {
            $q->where('trumpinys', $query_array['trumpinys']);
        }

        $tmp = $q->get()->result();

        $ret['num_rows'] = $tmp[0]->count;

        return $ret;
    }

    function delete($data)
    {
        if ($data != null) {
            $query = $this->db->query("SELECT mt.id
                                   FROM $this->table_mainTable mt,$this->table_sample ts
                                   WHERE mt.pacientasid=ts.pacientasid AND  ts.trumpinys LIKE '%$data%'");
            $dataId = $query->result();
            $id = $dataId[0]->id;
            $this->db->delete($this->table_finalReport, array('pagrindineslentelesid' => $id));
        }
    }
}

?>
