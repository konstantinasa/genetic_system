<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo lang('script_heading'); ?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <?php echo form_open("script/scripts_copyFile"); ?>
                            <div class="form-group">
                                <label class="help-block"> <?php echo lang('script_copyFile_information'); ?></label>
                                <button type="submit" class="btn btn-primary btn-xs">
                                    <?php echo lang('script_copyFile_button'); ?>
                                </button>
                                <?php echo form_close(); ?>
                                <hr>
                            </div>
                            <?php echo form_open("script/scripts_rmFile"); ?>
                            <div class="form-group">
                                <label class="help-block"> <?php echo lang('script_rmFile_information'); ?></label>
                                <button type="submit" class="btn btn-primary btn-xs">
                                    <?php echo lang('script_rmFile_button'); ?>
                                </button>
                                <?php echo form_close(); ?>
                                <hr>
                            </div>
                        </div>
                        <!-- /.col-lg-12 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->