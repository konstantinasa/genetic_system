<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo lang('cnvPart_heading'); ?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <!-- /.col-lg-12-->
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-lg-12">
                        <?php echo form_open("cnvpart/search"); ?>
                        <div class="form-group">
                            <?php
                            echo form_label(lang('genetic_sampleId', 'trumpinys'));
                            echo "<select class=\"form-control\" id=\"trumpinys\" name=\"trumpinys\">";
                            foreach ($sampleid_options as $key => $value) {
                                if ($trumpinys == null) {
                                    echo "<option value=\"$key\">" . $key . "</option>";
                                } else {
                                    if ($key == $trumpinys) {
                                        $s = "selected='selected'";
                                    } else {
                                        $s = "";
                                    }
                                    echo "<option value='$key' $s>$key</option>";
                                }
                            }
                            echo "</select>"; ?>
                        </div>
                        <button type="submit" formaction="<?php echo base_url('cnvpart/search') ?>"
                                class="btn btn-primary"><i
                                class="fa fa-search"></i> <?php echo lang('genetic_search'); ?></button>
                        <button type="submit" formaction="<?php echo base_url('cnvpart/cnvPartCsv') ?>"
                                class="btn btn-primary"><?php echo lang('genetic_csv'); ?></button>
                        <button type="submit" formaction="<?php echo base_url('cnvpart/cnvPartDelete') ?>"
                                class="btn btn-danger"><?php echo lang('genetic_delete'); ?></button>
                        <br>
                        <br>
                        <?php echo form_close(); ?>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <p><?php echo lang('allRecords'), $num_results; ?></p>
                                <thead>
                                <?php foreach ($fields as $field_name => $field_display): ?>
                                    <th <?php if ($sort_by == $field_name) echo "class=\"sort_$sort_order\"" ?>>
                                        <?php echo anchor("cnvpart/display/$query_id/$field_name/" .
                                            (($sort_order == 'asc' && $sort_by == $field_name) ? 'desc' : 'asc'),
                                            $field_display); ?>
                                    </th>
                                <?php endforeach; ?>
                                </thead>

                                <tbody>
                                <?php foreach ($cnvparts as $cnvpart): ?>
                                    <tr>
                                        <?php foreach ($fields as $field_name => $field_display): ?>
                                            <td style="word-wrap: break-word;min-width: 90px;max-width: 90px;white-space:normal;">
                                                <?php echo $cnvpart->$field_name; ?>
                                            </td>
                                        <?php endforeach; ?>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>

                            </table>

                            <?php if (strlen($pagination)): ?>
                                <div>
                                    <center><?php echo $pagination; ?> </center>
                                </div>
                            <?php endif; ?>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->
                <!-- /.row -->
            </div>
            <!-- /#page-wrapper -->