<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo lang('create_main_heading'); ?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <p class="help-block"><?php echo lang('create_main_subheading'); ?></p>
                            <?php echo validation_errors(); ?>
                            <?php echo form_open("main/create_main"); ?>
                            <div class="form-group">
                                <div class="select2-bootstrap-prepend">
                                    <label><?php echo lang('create_main_patient_label', 'patient'); ?></label>
                                    <?php
                                    echo '<select id="dropDownSearch" name="patient">';
                                    echo '<option value=""></option>';
                                    foreach($resultsAboutPatient as $value){
                                        echo '<option value="' . $value->id . '">' . $value->pacientas . '</option>';
                                    }
                                    echo '</select>';
                                    ?>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">
                                <?php echo lang('create_main_submit_btn'); ?>
                            </button>
                            <?php echo form_close(); ?>
                        </div>
                        <!-- /.col-lg-12 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->