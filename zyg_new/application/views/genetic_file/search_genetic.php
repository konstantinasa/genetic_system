<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo lang('geneticSearch_heading'); ?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <?php echo validation_errors(); ?>
                            <?php echo form_open(); ?>
<!--                            <div class="form-group">-->
<!--                                <div class="select2-bootstrap-prepend">-->
<!--                                    --><?php //echo form_label(lang('geneticSearch_sampleId', 'sampleId')); ?>
<!--                                    --><?php //$dbms = isset($_POST['sampleId[]']); ?>
<!--                                    <select multiple id="dropDownSearchs" name="sampleId[]">-->
<!--                                        --><?php //foreach ($sampleId as $row): ?>
<!--                                            <option value="--><?php //echo $row->trumpinys; ?><!--"-->
<!--                                                --><?php //if ($row->trumpinys == set_value($dbms)) echo "selected=\"selected\""; ?>
<!--                                                > --><?php //echo $row->trumpinys; ?><!--</option>-->
<!--                                        --><?php //endforeach; ?>
<!--                                    </select>-->
<!--                                </div>-->
<!--                            </div>-->
                            <div class="form-group">
                                <label>   <?php echo lang('geneticSearch_sampleId', 'sampleId'); ?></label>
                                <?php
                                $sampleId = array(
                                    'class' => 'form-control',
                                    'name' => 'sampleId',
                                    'id' => 'sampleId',
                                    'type' => 'text',
                                    'placeholder' => "Pvz.: LTG_528",
                                    'value' => $this->form_validation->set_value('sampleId')
                                );
                                echo form_input($sampleId); ?>
                            </div>
                            <div class="form-group">
                                <label>   <?php echo lang('geneticSearch_chromosome', 'chromosome'); ?></label>
                                <?php
                                $chromosome = array(
                                    'class' => 'form-control',
                                    'name' => 'chromosome',
                                    'id' => 'chromosome',
                                    'type' => 'text',
                                    'placeholder' => "Pvz.: 1",
                                    'value' => $this->form_validation->set_value('chromosome')
                                );
                                echo form_input($chromosome); ?>
                            </div>
                            <div class="form-group">
                                <label>   <?php echo lang('geneticSearch_start_position', 'startPosition'); ?></label>
                                <?php
                                $startPosition = array(
                                    'class' => 'form-control',
                                    'name' => 'startPosition',
                                    'id' => 'startPosition',
                                    'type' => 'text',
                                    'placeholder' => "Pvz.: 82154",
                                    'value' => $this->form_validation->set_value('startPosition')
                                );
                                echo form_input($startPosition); ?>
                            </div>
                            <div class="form-group">
                                <label>   <?php echo lang('geneticSearch_end_position', 'endPosition'); ?></label>
                                <?php
                                $endPosition = array(
                                    'class' => 'form-control',
                                    'name' => 'endPosition',
                                    'id' => 'endPosition',
                                    'type' => 'text',
                                    'placeholder' => "Pvz.: 82154",
                                    'value' => $this->form_validation->set_value('endPosition')
                                );
                                echo form_input($endPosition); ?>
                            </div>
                            <div class="form-group">
                                <label>   <?php echo lang('geneticSearch_location', 'location'); ?></label>
                                <?php
                                $location = array(
                                    'class' => 'form-control',
                                    'name' => 'location',
                                    'id' => 'location',
                                    'type' => 'text',
                                    'placeholder' => "Pvz.: intronic",
                                    'value' => $this->form_validation->set_value('location')
                                );
                                echo form_input($location); ?>
                            </div>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>   <?php echo lang('geneticSearch_gene', 'gene'); ?></label>
                                <?php
                                $gene = array(
                                    'class' => 'form-control',
                                    'name' => 'gene',
                                    'id' => 'gene',
                                    'type' => 'text',
                                    'placeholder' => "Pvz.: CFAP74",
                                    'value' => $this->form_validation->set_value('gene')
                                );
                                echo form_input($gene); ?>

                            </div>
                            <div class="form-group">
                                <label>   <?php echo lang('geneticSearch_exonicFunction', 'exonicFunction'); ?></label>
                                <?php $exonicFunction = array(
                                    'class' => 'form-control',
                                    'name' => 'exonicFunction',
                                    'id' => 'exonicFunction',
                                    'type' => 'text',
                                    'placeholder' => "Pvz.: EXON",
                                    'value' => $this->form_validation->set_value('exonicFunction')
                                );
                                echo form_input($exonicFunction); ?>
                            </div>
                            <div class="form-group">
                                <label>   <?php echo lang('geneticSearch_snpName', 'snpName'); ?></label>
                                <?php
                                $snpName = array(
                                    'class' => 'form-control',
                                    'name' => 'snpName',
                                    'id' => 'snpName',
                                    'type' => 'text',
                                    'placeholder' => "Pvz.: rs10082305",
                                    'value' => $this->form_validation->set_value('snpName')
                                );
                                echo form_input($snpName); ?>
                            </div>


                            <div class="form-group">
                                <label>   <?php echo lang('geneticSearch_length', 'length'); ?></label>
                                <?php $length = array(
                                    'class' => 'form-control',
                                    'name' => 'length',
                                    'id' => 'length',
                                    'type' => 'text',
                                    'placeholder' => "Pvz.: 15",
                                    'value' => $this->form_validation->set_value('length')
                                );
                                echo form_input($length); ?>

                            </div>
                            <div class="form-group">
                                <label>   <?php echo lang('geneticSearch_copyNumber', 'copyNumber'); ?></label>
                                <?php $copyNumber = array(
                                    'class' => 'form-control',
                                    'name' => 'copyNumber',
                                    'id' => 'copyNumber',
                                    'type' => 'text',
                                    'placeholder' => "Pvz.: 1",
                                    'value' => $this->form_validation->set_value('copyNumber')
                                );
                                echo form_input($copyNumber); ?>
                            </div>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                        <div class="col-lg-12">
                            <button type="submit" name="submit"
                                    formaction="<?php echo base_url('searchgenetic/SearchGenetics') ?>"
                                    class="btn btn-primary"><i
                                    class="fa fa-search"></i> <?php echo lang('geneticSearch_search'); ?>
                            </button>
                            <button type="submit" name="submit1"
                                    formaction="<?php echo base_url('searchgenetic/searchgeneticsCSV') ?>"
                                    class="btn btn-primary"><?php echo lang('geneticSearch_csv'); ?></button>
                            <button type="submit" name="submit1"
                                    formaction="<?php echo base_url('searchgenetic/searchgeneticsCSVMerger') ?>"
                                    class="btn btn-primary"><?php echo lang('geneticSearch_csvMerger'); ?></button>
                            <button type="submit" name="submit"
                                    formaction="<?php echo base_url('searchgenetic/ResetSearchGenetics') ?>"
                                    class="btn btn-danger"><i
                                    class="fa fa-eraser"></i> <?php echo lang('geneticSearch_reset'); ?>
                            </button>
                            </br>
                            <?php echo form_close();
                            if (isset($_POST['submit'])) { ?>
                            </br>
                            <div class="form-group">
                                <label>   <?php echo lang('geneticSearch_result'); ?></label>
                            </div>
                            <div class="form-group">
                            <?php
                                if ($resultSearchQuantiSnp) {
                                    foreach ($resultSearchQuantiSnp as $resultSearchQuantiSnps) {
                                        echo lang('geneticSearch_quantisnp') . " - " . $resultSearchQuantiSnps->count;
                                    }
                                } ?>
                            </div>
                            <div class="form-group">
                            <?php
                                if ($resultSearchPennCnv) {
                                    foreach ($resultSearchPennCnv as $resultSearchPennCnvs) {
                                        echo lang('geneticSearch_penncnv') . " - " . $resultSearchPennCnvs->count;
                                    }
                                } ?>
                            </div>
                            <div class="form-group">
                             <?php
                                if ($resultSearchCNVPart) {
                                    foreach ($resultSearchCNVPart as $resultSearchCNVParts) {
                                        echo lang('geneticSearch_cnvpart') . " - " . $resultSearchCNVParts->count;
                                    }
                                } ?>
                            </div>
                            <div class="form-group">
                                <?php
                                if ($resultSearchFinalReport) {
                                    foreach ($resultSearchFinalReport as $resultSearchFinalReports) {
                                        echo lang('geneticSearch_finalreport') . " - " . $resultSearchFinalReports->count;
                                    }
                                } ?>
                            </div>
                            <div class="form-group">
                                  <?php
                                if ($resultSearchIndel) {
                                    foreach ($resultSearchIndel as $resultSearchIndels) {
                                        echo lang('geneticSearch_indel') . " - " . $resultSearchIndels->count;
                                    }
                                } ?>
                            </div>
                            <div class="form-group">
                                <?php
                                if ($resultSearchSnp) {
                                    foreach ($resultSearchSnp as $resultSearchSnps) {
                                        echo lang('geneticSearch_snp') . " - " . $resultSearchSnps->count;
                                    }
                                } ?>
                            </div>
                            <?php }
                            if (isset($_POST['submit1'])) { ?>
                            </br>
                            <div class="form-group">
                                <label>   <?php echo lang('geneticSearchCSV_result'); ?></label>
                            </div>
                            <div class="form-group">
                                <?php
                                if ($resultSearchQuantiSnp) {
                                    foreach ($resultSearchQuantiSnp as $resultSearchQuantiSnps) {
                                        echo lang('geneticSearch_quantisnp') . " - " . $resultSearchQuantiSnps->count;
                                    }
                                } ?>
                            </div>
                            <div class="form-group">
                                <?php
                                if ($resultSearchPennCnv) {
                                    foreach ($resultSearchPennCnv as $resultSearchPennCnvs) {
                                        echo lang('geneticSearch_penncnv') . " - " . $resultSearchPennCnvs->count;
                                    }
                                } ?>
                            </div>
                            <div class="form-group">
                                <?php
                                if ($resultSearchCNVPart) {
                                    foreach ($resultSearchCNVPart as $resultSearchCNVParts) {
                                        echo lang('geneticSearch_cnvpart') . " - " . $resultSearchCNVParts->count;
                                    }
                                } ?>
                            </div>
                            <div class="form-group">
                                <?php
                                if ($resultSearchFinalReport) {
                                    foreach ($resultSearchFinalReport as $resultSearchFinalReports) {
                                        echo lang('geneticSearch_finalreport') . " - " . $resultSearchFinalReports->count;
                                    }
                                } ?>
                            </div>
                            <div class="form-group">
                                <?php
                                if ($resultSearchIndel) {
                                    foreach ($resultSearchIndel as $resultSearchIndels) {
                                        echo lang('geneticSearch_indel') . " - " . $resultSearchIndels->count;
                                    }
                                } ?>
                            </div>
                            <div class="form-group">
                                <?php
                                if ($resultSearchSnp) {
                                    foreach ($resultSearchSnp as $resultSearchSnps) {
                                        echo lang('geneticSearch_snp') . " - " . $resultSearchSnps->count;
                                    }
                                } ?>
                            </div>
                            <?php } ?>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->