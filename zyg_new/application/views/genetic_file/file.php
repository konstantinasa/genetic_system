<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo lang('file_heading'); ?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <a class="btn btn-default" href="<?php echo base_url('file/fileUpload'); ?>"><i
                    class="fa fa-upload fa-fw"></i><?php echo lang('include_file_upload') ?></a>
            </br>
            </br>
        </div>
        <!-- /.col-lg-12-->
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-lg-12">
                        <?php echo form_open("file/search"); ?>
                        <div class="form-group">
                            <?php
                            echo form_label(lang('file_sampleId', 'trumpinys'));
                            echo "<select class=\"form-control\" id=\"trumpinys\" name=\"trumpinys\">";
                            foreach ($sampleid_options as $key => $value) {
                                if ($trumpinys == null) {
                                    echo "<option value=\"$key\">" . $key . "</option>";
                                } else {
                                    if ($key == $trumpinys) {
                                        $s = "selected='selected'";
                                    } else {
                                        $s = "";
                                    }
                                    echo "<option value='$key' $s>$key</option>";
                                }
                            }
                            echo "</select>"; ?>
                        </div>
                        <button type="submit" class="btn btn-primary">
                            <?php echo lang('file_search'); ?>
                        </button>
                        <br>
                        <br>
                        <?php echo form_close(); ?>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <p><?php echo lang('allRecords'), $num_results; ?></p>
                                <thead>
                                <?php foreach ($fields as $field_name => $field_display): ?>
                                    <th <?php if ($sort_by == $field_name) echo "class=\"sort_$sort_order\"" ?>>
                                        <?php echo anchor("file/display/$query_id/$field_name/" .
                                            (($sort_order == 'asc' && $sort_by == $field_name) ? 'desc' : 'asc'),
                                            $field_display); ?>
                                    </th>
                                <?php endforeach; ?>
                                <th><?php echo lang('file_action'); ?></th>
                                </thead>

                                <tbody>
                                <?php foreach ($files as $file): ?>
                                    <tr>
                                        <?php foreach ($fields as $field_name => $field_display): ?>
                                            <td>
                                                <?php echo $file->$field_name; ?>
                                            </td>
                                        <?php endforeach; ?>
                                        <td>
                                            <?php echo "<a class=\"btn btn-primary\" href=" . base_url('file/runScript/' . $file->id . '-' . $file->failopavadinimas) . ">"; ?><?php echo lang('file_script') ?></a>
                                            &nbsp; &nbsp;
                                            <?php echo "<a class=\"btn btn-danger\" href=" . base_url('file/deleteFile/' . $file->failasid . '-' . $file->failopavadinimas) . ">"; ?><?php echo lang('file_delete') ?></a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>

                            </table>

                            <?php if (strlen($pagination)): ?>
                                <div>
                                    <center><?php echo $pagination; ?> </center>
                                </div>
                            <?php endif; ?>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->
                <!-- /.row -->
            </div>
            <!-- /.col-lg-6 -->
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->