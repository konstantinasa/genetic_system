<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-2">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php echo lang('forgot_password_heading'); ?></h3>
                </div>
                <div class="panel-body">
                    <?php echo validation_errors(); ?>
                    <?php echo form_open("auth/forgot_password"); ?>
                    <fieldset>
                        <div class="form-group">
                            <label><?php echo sprintf(lang('forgot_password_subheading'), $identity_label); ?></label>
                            <?php echo form_input($email); ?>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-lg btn-success btn-block" type="submit"
                                    name="sumbit"><?php echo lang('forgot_password_submit_btn'); ?></button>
                                 <span class="pull-right">
                        </div>
                    </fieldset>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>