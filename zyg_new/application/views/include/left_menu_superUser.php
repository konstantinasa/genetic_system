<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li>
                <a href="<?php echo base_url('main/mains'); ?>"><?php echo lang('include_mainTable') ?></a>
            </li>
            <li>
                <a href="<?php echo base_url('file/display'); ?>"><?php echo lang('include_file') ?></a>
            </li>
            <li>
                <a href="<?php echo base_url('file/fileDownload'); ?>"><?php echo lang('include_file_download') ?></a>
            </li>
            <li>
                <a href="<?php echo base_url('script/scripts'); ?>"><?php echo lang('include_script') ?></a>
            </li>
            <li>
                <a href="#"><?php echo lang("include_genetic_file") ?><span
                        class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="<?php echo base_url('quantisnp/display'); ?>"> <?php echo lang('include_quantiSnp') ?></a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('penncnv/display'); ?>"> <?php echo lang('include_pennCnv') ?></a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('cnvpart/display'); ?>"> <?php echo lang('include_cnvpart') ?></a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('finalreport/display'); ?>"> <?php echo lang('include_finalReport') ?></a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('snp/display'); ?>"> <?php echo lang('include_snp') ?></a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('indel/display'); ?>"> <?php echo lang('include_indel') ?></a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <li>
                <a href="<?php echo base_url('searchgenetic/searchgenetics'); ?>"><?php echo lang('include_search') ?></a>
            </li>
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->
</nav>