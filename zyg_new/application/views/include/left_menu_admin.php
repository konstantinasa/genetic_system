<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li>
                <a href="#"><i class="fa fa-gear fa-fw"></i> <?php echo lang("include_create_user_group") ?><span
                        class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="<?php echo base_url('auth/index'); ?>"> <?php echo lang('include_user_list') ?></a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('auth/group'); ?>"><?php echo lang('include_group_list') ?></a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->
</nav>