<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Script extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->database();
        $this->load->helper(array('url', 'language'));
        $this->load->model('script_model');
        $this->lang->load('include');
        $this->lang->load('script');
        $this->lang->load('auth');
    }

    protected $groupSU = 'superUser';

    function Scripts()
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->in_group($this->groupSU)) {
            redirect('auth', 'refresh');
        } else {
            $templateDataSU = 'template/template_superUser';
            $data = array('firstFolder' => "genetic_file",
                'main_content' => "script");
            $data['username'] = $this->session->userdata('username');
            $data['resultsSampleId'] = $this->script_model->sampleId_options();
            $data['resultsFile'] = $this->script_model->file_options();
            $this->_render_page($templateDataSU, $data);
        }
    }

    function scripts_copyFile()
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->in_group($this->groupSU)) {
            redirect('auth', 'refresh');
        } else {
            $this->script_model->copyFile();
            redirect('script/Scripts', 'refresh');
        }
    }

    function scripts_RmFile()
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->in_group($this->groupSU)) {
            redirect('auth', 'refresh');
        } else {
            $this->script_model->rmFile();
            redirect('script/Scripts', 'refresh');
        }
    }

    function _render_page($templateData, $data = null, $render = false)
    {

        $this->viewdata = (empty($data)) ? $this->data : $data;

        $view_html = $this->load->view($templateData, $this->viewdata, $render);

        if (!$render)
            return $view_html;
    }

}
