<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Penncnv extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->database();
        $this->load->helper(array('url', 'language'));
        $this->load->library("pagination");
        $this->load->model('penncnv_model');
        $this->load->model('sampleid_model');
        $this->load->model('script_model');
        $this->lang->load('include');
        $this->lang->load('genetic_file');
        $this->lang->load('auth');
    }

    protected $groupSU = 'superUser';

    function display($query_id = 0, $sort_by = 'sampleId', $sort_order = 'asc', $offset = 0)
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->in_group($this->groupSU)) {
            redirect('auth', 'refresh');
        } else {
            $templateDataSU = 'template/template_superUser';
            $data = array('firstFolder' => "genetic_file",
                'main_content' => "penncnv");
            $data['username'] = $this->session->userdata('username');
            $limit = 500;
            $data['fields'] = array(
                'trumpinys' => $this->lang->line('pennCnv_sampleId'),
                'chromosome' => $this->lang->line('pennCnv_chromosome'),
                'startposition' => $this->lang->line('pennCnv_startPosition'),
                'endposition' => $this->lang->line('pennCnv_endPosition'),
                'noprobes' => $this->lang->line('pennCnv_noProbes'),
                'length' => $this->lang->line('pennCnv_length'),
                'state' => $this->lang->line('pennCnv_state'),
                'copynumber' => $this->lang->line('pennCnv_copyNumber'),
                'startprobeid' => $this->lang->line('pennCnv_startProbe'),
                'endprobeid' => $this->lang->line('pennCnv_endProbe'),
                'confidence' => $this->lang->line('pennCnv_confidence')
            );

            $this->sampleid_model->load_query($query_id);

            $query_array = array(
                'trumpinys' => $this->input->get('trumpinys')
            );

            $data['query_id'] = $query_id;

            $results = $this->penncnv_model->search($query_array, $limit, $offset, $sort_by, $sort_order);

            $data['pennCnvs'] = $results['rows'];
            $data['num_results'] = $results['num_rows'];

            $config = array();
            $config['base_url'] = site_url("penncnv/display/$query_id/$sort_by/$sort_order");
            $config['total_rows'] = $data['num_results'];
            $config['per_page'] = $limit;
            $config['uri_segment'] = 6;
            $this->pagination->initialize($config);
            $data['pagination'] = $this->pagination->create_links();

            $data['sort_by'] = $sort_by;
            $data['sort_order'] = $sort_order;

            $data['sampleid_options'] = $this->sampleid_model->sampleid_options();
            $data['trumpinys'] = $query_array['trumpinys'];

            $this->_render_page($templateDataSU, $data);
        }
    }

    function search()
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->in_group($this->groupSU)) {
            redirect('auth', 'refresh');
        } else {
            $query_array = array(
                'trumpinys' => $this->input->post('trumpinys'),
            );
            $query_id = $this->sampleid_model->save_query($query_array);

            redirect("penncnv/display/$query_id");
        }
    }

    function pennCnvCsv()
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->in_group($this->groupSU)) {
            redirect('auth', 'refresh');
        } else {
            $sampleName = $this->input->post("trumpinys");
            $this->script_model->exportPennCnv($sampleName);
            redirect('penncnv/display', 'refresh');
        }
    }

    function pennCnvDelete()
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->in_group($this->groupSU)) {
            redirect('auth', 'refresh');
        } else {
            $sampleName = $this->input->post("trumpinys");
            $this->penncnv_model->delete($sampleName);
            redirect('penncnv/display', 'refresh');
        }
    }

    function _render_page($templateData, $data = null, $render = false)
    {

        $this->viewdata = (empty($data)) ? $this->data : $data;

        $view_html = $this->load->view($templateData, $this->viewdata, $render);

        if (!$render) return $view_html;
    }


}