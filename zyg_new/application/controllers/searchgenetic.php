<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Searchgenetic extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->database();
        $this->load->helper(array('url', 'language'));
        $this->load->model('sampleid_model');
        $this->load->model('searchgenetic_model');
        $this->load->model('searchgeneticexport_model');
        $this->lang->load('include');
        $this->lang->load('search');
        $this->lang->load('auth');
    }

    protected $groupSU = 'superUser';

    public function SearchGenetics()
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->in_group($this->groupSU)) {
            redirect('auth', 'refresh');
        } else {
            $templateDataSU = 'template/template_superUser';
            $data = array('firstFolder' => "genetic_file",
                'main_content' => "search_genetic");
            $data['username'] = $this->session->userdata('username');
          //  $data['sampleId'] = $this->sampleid_model->getSampleId();
            $this->form_validation->set_rules('sampleId', $this->lang->line('geneticSearch_sampleId'), '');
            $this->form_validation->set_rules('chromosome', $this->lang->line('geneticSearch_chromosome'), '');
            $this->form_validation->set_rules('startPosition', $this->lang->line('geneticSearch_start_position'), '');
            $this->form_validation->set_rules('endPosition', $this->lang->line('geneticSearch_end_position'), '');
            $this->form_validation->set_rules('location', $this->lang->line('geneticSearch_location'), '');
            $this->form_validation->set_rules('gene', $this->lang->line('geneticSearch_gene'), '');
            $this->form_validation->set_rules('exonicFunction', $this->lang->line('geneticSearch_exonicFunction'), '');
            $this->form_validation->set_rules('snpName', $this->lang->line('geneticSearch_snpName'), '');
            $this->form_validation->set_rules('length', $this->lang->line('geneticSearch_length'), '');
            $this->form_validation->set_rules('copyNumber', $this->lang->line('geneticSearch_copyNumber'), '');
            if ($this->form_validation->run() == true) {
                $sampleId = $this->input->post('sampleId');
                $chromosome = $this->input->post('chromosome');
                $startPosition = $this->input->post('startPosition');
                $endPosition = $this->input->post('endPosition');
                $location = $this->input->post('location');
                $gene = $this->input->post('gene');
                $exonicFunction = $this->input->post('exonicFunction');
                $snpName = $this->input->post('snpName');
                $length = $this->input->post('length');
                $copyNumber = $this->input->post('copyNumber');

                if ($sampleId != null || $chromosome != null || $startPosition != null || $endPosition != null || $location != null || $gene != null || $exonicFunction != null || $snpName != null) {
                    $data["resultSearchSnp"] = $this->searchgenetic_model->searchSnp($sampleId, $chromosome, $startPosition, $endPosition, $location, $gene, $exonicFunction, $snpName);
                } else {
                    $data["resultSearchSnp"] = 0;
                }
                if ($sampleId != null || $chromosome != null || $startPosition != null || $endPosition != null || $location != null || $gene != null || $exonicFunction != null || $snpName != null) {
                    $data["resultSearchIndel"] = $this->searchgenetic_model->searchIndel($sampleId, $chromosome, $startPosition, $endPosition, $location, $gene, $exonicFunction, $snpName);
                } else {
                    $data["resultSearchIndel"] = 0;
                }
                if ($sampleId != null || $chromosome != null || $startPosition != null || $location != null || $gene != null || $exonicFunction != null || $snpName != null) {
                    $data["resultSearchFinalReport"] = $this->searchgenetic_model->searchFinalReport($sampleId, $chromosome, $startPosition, $location, $gene, $exonicFunction, $snpName);
                } else {
                    $data["resultSearchFinalReport"] = 0;
                }
                if ($sampleId != null || $chromosome != null || $startPosition != null || $endPosition != null || $length != null || $copyNumber != null) {
                    $data["resultSearchQuantiSnp"] = $this->searchgenetic_model->searchQuantiSnp($sampleId, $chromosome, $startPosition, $endPosition, $length, $copyNumber);
                } else {
                    $data["resultSearchQuantiSnp"] = 0;
                }
                if ($sampleId != null || $chromosome != null || $startPosition != null || $endPosition != null || $length != null || $copyNumber != null) {
                    $data["resultSearchPennCnv"] = $this->searchgenetic_model->searchPennCNV($sampleId, $chromosome, $startPosition, $endPosition, $length, $copyNumber);
                } else {
                    $data["resultSearchPennCnv"] = 0;
                }
                if ($sampleId != null || $chromosome != null || $startPosition != null || $endPosition != null || $copyNumber != null) {
                    $data["resultSearchCNVPart"] = $this->searchgenetic_model->searchCNVPart($sampleId, $chromosome, $startPosition, $endPosition, $copyNumber);
                } else {
                    $data["resultSearchCNVPart"] = 0;
                }
                $this->_render_page($templateDataSU, $data);
            } else {
                $this->_render_page($templateDataSU, $data);
            }
        }
    }

    public function ResetSearchGenetics()
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->in_group($this->groupSU)) {
            redirect('auth', 'refresh');
        } else {
            redirect('searchgenetic/searchgenetics', 'refresh');
        }
    }

    public function searchgeneticsCSV()
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->in_group($this->groupSU)) {
            redirect('auth', 'refresh');
        } else {
            $templateDataSU = 'template/template_superUser';
            $data = array('firstFolder' => "genetic_file",
                'main_content' => "search_genetic");
            $data['username'] = $this->session->userdata('username');
            //$data['sampleId'] = $this->sampleid_model->getSampleId();
            $this->form_validation->set_rules('sampleId', $this->lang->line('geneticSearch_sampleId'), '');
            $this->form_validation->set_rules('chromosome', $this->lang->line('geneticSearch_chromosome'), '');
            $this->form_validation->set_rules('startPosition', $this->lang->line('geneticSearch_start_position'), '');
            $this->form_validation->set_rules('endPosition', $this->lang->line('geneticSearch_end_position'), '');
            $this->form_validation->set_rules('location', $this->lang->line('geneticSearch_location'), '');
            $this->form_validation->set_rules('gene', $this->lang->line('geneticSearch_gene'), '');
            $this->form_validation->set_rules('exonicFunction', $this->lang->line('geneticSearch_exonicFunction'), '');
            $this->form_validation->set_rules('snpName', $this->lang->line('geneticSearch_snpName'), '');
            $this->form_validation->set_rules('length', $this->lang->line('geneticSearch_length'), '');
            $this->form_validation->set_rules('copyNumber', $this->lang->line('geneticSearch_copyNumber'), '');
            if ($this->form_validation->run() == true) {
                $sampleId = $this->input->post('sampleId');
                $chromosome = $this->input->post('chromosome');
                $startPosition = $this->input->post('startPosition');
                $endPosition = $this->input->post('endPosition');
                $location = $this->input->post('location');
                $gene = $this->input->post('gene');
                $exonicFunction = $this->input->post('exonicFunction');
                $snpName = $this->input->post('snpName');
                $length = $this->input->post('length');
                $copyNumber = $this->input->post('copyNumber');

                if ($sampleId != null || $chromosome != null || $startPosition != null || $endPosition != null || $location != null || $gene != null || $exonicFunction != null || $snpName != null) {
                    $snp = $this->searchgeneticexport_model->snpSearchExport($sampleId, $chromosome, $startPosition, $endPosition, $location, $gene, $exonicFunction, $snpName);
                    $data["resultSearchSnp"] = null;
                } else {
                    $snp = null;
                    $data["resultSearchSnp"] = null;
                }
                if ($sampleId != null || $chromosome != null || $startPosition != null || $endPosition != null || $location != null || $gene != null || $exonicFunction != null || $snpName != null) {
                    $indel = $this->searchgeneticexport_model->indelSearchExport($sampleId, $chromosome, $startPosition, $endPosition, $location, $gene, $exonicFunction, $snpName);
                    $data["resultSearchIndel"] = null;
                } else {
                    $indel = null;
                    $data["resultSearchIndel"] = null;
                }
                if ($sampleId != null || $chromosome != null || $startPosition != null || $location != null || $gene != null || $exonicFunction != null || $snpName != null) {
                    $finalReport = $this->searchgeneticexport_model->finalReportSearchExport($sampleId, $chromosome, $startPosition, $location, $gene, $exonicFunction, $snpName);
                    $data["resultSearchFinalReport"] = null;
                } else {
                    $finalReport = null;
                    $data["resultSearchFinalReport"] = null;
                }
                if ($sampleId != null || $chromosome != null || $startPosition != null || $endPosition != null || $length != null || $copyNumber != null) {
                    $quantiSnp = $this->searchgeneticexport_model->quantiSnpSearchExport($sampleId, $chromosome, $startPosition, $endPosition, $length, $copyNumber);
                    $data["resultSearchQuantiSnp"] = null;
                } else {
                    $quantiSnp = null;
                    $data["resultSearchQuantiSnp"] = null;
                }
                if ($sampleId != null || $chromosome != null || $startPosition != null || $endPosition != null || $length != null || $copyNumber != null) {
                    $PennCnv = $this->searchgeneticexport_model->PennCNVSearchExport($sampleId, $chromosome, $startPosition, $endPosition, $length, $copyNumber);
                    $data["resultSearchPennCnv"] = null;
                } else {
                    $PennCnv = null;
                    $data["resultSearchPennCnv"] = null;
                }
                if ($sampleId != null || $chromosome != null || $startPosition != null || $endPosition != null || $copyNumber != null) {
                    $CNVPart = $this->searchgeneticexport_model->CNVPartSearchExport($sampleId, $chromosome, $startPosition, $endPosition, $copyNumber);
                    $data["resultSearchCNVPart"] = null;
                } else {
                    $CNVPart = null;
                    $data["resultSearchCNVPart"] = null;
                }
                $this->_render_page($templateDataSU, $data);
            } else {
                $this->_render_page($templateDataSU, $data);
            }
        }
    }

    public function searchgeneticsCSVMerger()
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->in_group($this->groupSU)) {
            redirect('auth', 'refresh');
        } else {
            $templateDataSU = 'template/template_superUser';
            $data = array('firstFolder' => "genetic_file",
                'main_content' => "search_genetic");
            $data['username'] = $this->session->userdata('username');
           // $data['sampleId'] = $this->sampleid_model->getSampleId();
            $this->form_validation->set_rules('sampleId', $this->lang->line('geneticSearch_sampleId'), '');
            $this->form_validation->set_rules('chromosome', $this->lang->line('geneticSearch_chromosome'), '');
            $this->form_validation->set_rules('startPosition', $this->lang->line('geneticSearch_start_position'), '');
            $this->form_validation->set_rules('endPosition', $this->lang->line('geneticSearch_end_position'), '');
            $this->form_validation->set_rules('location', $this->lang->line('geneticSearch_location'), '');
            $this->form_validation->set_rules('gene', $this->lang->line('geneticSearch_gene'), '');
            $this->form_validation->set_rules('exonicFunction', $this->lang->line('geneticSearch_exonicFunction'), '');
            $this->form_validation->set_rules('snpName', $this->lang->line('geneticSearch_snpName'), '');
            $this->form_validation->set_rules('length', $this->lang->line('geneticSearch_length'), '');
            $this->form_validation->set_rules('copyNumber', $this->lang->line('geneticSearch_copyNumber'), '');
            if ($this->form_validation->run() == true) {
                $sampleId = $this->input->post('sampleId');
                $chromosome = $this->input->post('chromosome');
                $startPosition = $this->input->post('startPosition');
                $endPosition = $this->input->post('endPosition');
                $location = $this->input->post('location');
                $gene = $this->input->post('gene');
                $exonicFunction = $this->input->post('exonicFunction');
                $snpName = $this->input->post('snpName');
                $length = $this->input->post('length');
                $copyNumber = $this->input->post('copyNumber');

                if ($sampleId != null || $chromosome != null || $startPosition != null || $endPosition != null || $location != null || $gene != null || $exonicFunction != null || $snpName != null) {
                    $snp = $this->searchgeneticexport_model->snpSearchExport($sampleId, $chromosome, $startPosition, $endPosition, $location, $gene, $exonicFunction, $snpName);
                    $data["resultSearchSnp"] = null;
                } else {
                    $snp = null;
                    $data["resultSearchSnp"] = null;
                }
                if ($sampleId != null || $chromosome != null || $startPosition != null || $endPosition != null || $location != null || $gene != null || $exonicFunction != null || $snpName != null) {
                    $indel = $this->searchgeneticexport_model->indelSearchExport($sampleId, $chromosome, $startPosition, $endPosition, $location, $gene, $exonicFunction, $snpName);
                    $data["resultSearchIndel"] = null;
                } else {
                    $indel = null;
                    $data["resultSearchIndel"] = null;
                }
                if ($sampleId != null || $chromosome != null || $startPosition != null || $location != null || $gene != null || $exonicFunction != null || $snpName != null) {
                    $finalReport = $this->searchgeneticexport_model->finalReportSearchExport($sampleId, $chromosome, $startPosition, $location, $gene, $exonicFunction, $snpName);
                    $data["resultSearchFinalReport"] = null;
                } else {
                    $finalReport = null;
                    $data["resultSearchFinalReport"] = null;
                }
                if ($sampleId != null || $chromosome != null || $startPosition != null || $endPosition != null || $length != null || $copyNumber != null) {
                    $quantiSnp = $this->searchgeneticexport_model->quantiSnpSearchExport($sampleId, $chromosome, $startPosition, $endPosition, $length, $copyNumber);
                    $data["resultSearchQuantiSnp"] = null;
                } else {
                    $quantiSnp = null;
                    $data["resultSearchQuantiSnp"] = null;
                }
                if ($sampleId != null || $chromosome != null || $startPosition != null || $endPosition != null || $length != null || $copyNumber != null) {
                    $PennCnv = $this->searchgeneticexport_model->PennCNVSearchExport($sampleId, $chromosome, $startPosition, $endPosition, $length, $copyNumber);
                    $data["resultSearchPennCnv"] = null;
                } else {
                    $PennCnv = null;
                    $data["resultSearchPennCnv"] = null;
                }
                if ($sampleId != null || $chromosome != null || $startPosition != null || $endPosition != null || $copyNumber != null) {
                    $CNVPart = $this->searchgeneticexport_model->CNVPartSearchExport($sampleId, $chromosome, $startPosition, $endPosition, $copyNumber);
                    $data["resultSearchCNVPart"] = null;
                } else {
                    $CNVPart = null;
                    $data["resultSearchCNVPart"] = null;
                }
                if ($snp != null || $indel != null || $finalReport != null || $quantiSnp != null || $PennCnv != null || $CNVPart != null) {
                    $this->searchgeneticexport_model->joinSearch($snp, $indel, $finalReport, $quantiSnp, $PennCnv, $CNVPart);
                }
                $this->_render_page($templateDataSU, $data);
            } else {
                $this->_render_page($templateDataSU, $data);
            }
        }
    }

    function _render_page($templateData, $data = null, $render = false)
    {
        $this->viewdata = (empty($data)) ? $this->data : $data;

        $view_html = $this->load->view($templateData, $this->viewdata, $render);

        if (!$render) return $view_html;
    }
}

?>