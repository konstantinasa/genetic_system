<?php defined('BASEPATH') OR exit('No direct script access allowed');

class File extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->database();
        $this->load->helper(array('url', 'language'));
        $this->load->helper('form_helper');
        $this->load->library("pagination");
        $this->load->model('file_model');
        $this->load->model('sampleid_model');
        $this->load->model('script_model');
        $this->lang->load('include');
        $this->lang->load('file');
        $this->lang->load('auth');
    }

    protected $groupSU = 'superUser';
    protected $uploadDir = '/var/www/html/zyg_new/upload_file';

    function display($query_id = 0, $sort_by = 'sampleId', $sort_order = 'asc', $offset = 0)
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->in_group($this->groupSU)) {
            redirect('auth', 'refresh');
        } else {

            $templateDataSU = 'template/template_superUser';
            $data = array('firstFolder' => "genetic_file",
                'main_content' => "file");
            $data['username'] = $this->session->userdata('username');
            $limit = 120;
            $data['fields'] = array(
                'trumpinys' => $this->lang->line('file_sampleId'),
                'failopavadinimas' => $this->lang->line('file_fileName'),
                'ikelimodata' => $this->lang->line('file_insertTime')
            );

            $this->sampleid_model->load_query($query_id);

            $query_array = array(
                'trumpinys' => $this->input->get('trumpinys')
            );
            $data['query_id'] = $query_id;

            $results = $this->file_model->search($query_array, $limit, $offset, $sort_by, $sort_order);

            $data['files'] = $results['rows'];
            $data['num_results'] = $results['num_rows'];

            $config = array();
            $config['base_url'] = site_url("file/display/$query_id/$sort_by/$sort_order");
            $config['total_rows'] = $data['num_results'];
            $config['per_page'] = $limit;
            $config['uri_segment'] = 6;
            $this->pagination->initialize($config);
            $data['pagination'] = $this->pagination->create_links();

            $data['sort_by'] = $sort_by;
            $data['sort_order'] = $sort_order;

            $data['sampleid_options'] = $this->sampleid_model->sampleid_options();
            $data['trumpinys'] = $query_array['trumpinys'];

            $this->_render_page($templateDataSU, $data);
        }
    }

    function search()
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->in_group($this->groupSU)) {
            redirect('auth', 'refresh');
        } else {
            $query_array = array(
                'trumpinys' => $this->input->post('trumpinys'),
            );
            $query_id = $this->sampleid_model->save_query($query_array);

            redirect("file/display/$query_id");
        }
    }

    function fileUpload()
    {

        if (!$this->ion_auth->logged_in() || !$this->ion_auth->in_group($this->groupSU)) {
            redirect('auth', 'refresh');
        } else {
            $templateDataSU = 'template/template_superUser';
            $data = array('firstFolder' => "genetic_file",
                'main_content' => "file_upload");
            $data['username'] = $this->session->userdata('username');
            $data['resultsSampleId'] = $this->file_model->getAllSample();
            $config['upload_path'] = $this->uploadDir;
            $config['overwrite'] = FALSE;
            $config['allowed_types'] = 'txt|csv';
            $this->load->library('upload', $config);
            $this->form_validation->set_rules('sampleId', 'Sample ID', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->_render_page($templateDataSU, $data);
                return false;
            }
            if (($data_files_count = $this->upload->do_multi_upload("uploadFile"))) {
                $data_files = "";
                for ($i = 0; $i < count($data_files_count); $i++) {
                    if ($data_files_count[$i]['file_name'] != "") {
                        $data_files .= $data_files_count[$i]['file_name'] . "\n";
                    }
                }
                $dataFile['failopavadinimas'] = $data_files;
                $dataFile['pagrindineslentelesid'] = $this->input->post('sampleId');
                $this->file_model->insertFile($dataFile);
                redirect('/file/display', 'refresh');
            } else {
                echo $this->upload->display_errors();
            }
        }
    }

    function fileDownload()
    {

        if (!$this->ion_auth->logged_in() || !$this->ion_auth->in_group($this->groupSU)) {
            redirect('auth', 'refresh');
        } else {
            $templateDataSU = 'template/template_superUser';
            $data = array('firstFolder' => "genetic_file",
                'main_content' => "file_download");
            $data['username'] = $this->session->userdata('username');
            $data['resultFileDownload'] = $this->file_model->downloadFile();
            $this->_render_page($templateDataSU, $data);
        }
    }

    public function fileDownloadRm($fileName)
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->in_group($this->groupSU)) {
            redirect('auth', 'refresh');
        } else {
            unlink('downloadExport/' . $fileName);
            redirect('/file/fileDownload', 'refresh');
        }
    }

    public function deleteFile($variable)
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->in_group($this->groupSU)) {
            redirect('auth', 'refresh');
        } else {
            $variableArray = array();
            $variableExplode = explode('-', $variable);
            foreach ($variableExplode as $t) {
                $variableArray[] = $t;
            }
            $this->file_model->deleteFileFromDatabase($variableArray[0]);
            unlink('upload_file/' . $variableArray[1]);
            redirect('/file/display', 'refresh');
        }
    }

    public function runScript($variable)
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->in_group($this->groupSU)) {
            redirect('auth', 'refresh');
        } else {
            $variableArray = array();
            $variableExplode = explode('-', $variable);
	     
            foreach ($variableExplode as $t) {
                $variableArray[] = $t;
            }
            $mystringQuanti = $variableArray[1];
            $findmeQuanti = 'Quanti';
            $posQuanti = strpos($mystringQuanti, $findmeQuanti);
            if ($posQuanti === false) {
            } else {
                $this->script_quantiSnp($variableArray[1], $variableArray[0]);
            }
            $mystringPennCNV = $variableArray[1];
            $findmePennCNV = 'PennCNV';
            $posPennCNV = strpos($mystringPennCNV, $findmePennCNV);
            if ($posPennCNV === false) {
            } else {
                $this->script_pennCnv($variableArray[1], $variableArray[0]);
            }
            $mystringCNVpart = $variableArray[1];
            $findmeCNVpart = 'CNVpart';
            $posCNVpart = strpos($mystringCNVpart, $findmeCNVpart);
            if ($posCNVpart === false) {
            } else {
                $this->script_cnvPart($variableArray[1], $variableArray[0]);
            }
            $mystringFinalReport = $variableArray[1];
            $findmeFinalReport = 'FinalReport';
            $posFinalReport = strpos($mystringFinalReport, $findmeFinalReport);
            if ($posFinalReport === false) {
            } else {
                $this->script_finalReport($variableArray[1], $variableArray[0]);
            }
            $mystringINDEL = $variableArray[1];
            $findmeINDEL = 'INDEL';
            $posINDEL = strpos($mystringINDEL, $findmeINDEL);
            if ($posINDEL === false) {
            } else {
                $this->script_indel($variableArray[1], $variableArray[0]);
            }
            $mystringSNP = $variableArray[1];
            $findmeSNP = 'SNP';
            $posSNP = strpos($mystringSNP, $findmeSNP);
            if ($posSNP === false) {
            } else {
                $this->script_snp($variableArray[1], $variableArray[0]);
            }
        }
    }

    public function script_quantiSnp($fileName, $sampleId)
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->in_group($this->groupSU)) {
            redirect('auth', 'refresh');
        } else {
            $this->script_model->insertQuantiSnp($fileName, $sampleId);
            redirect('/file/display', 'refresh');
        }
    }

    function script_pennCnv($fileName, $sampleId)
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->in_group($this->groupSU)) {
            redirect('auth', 'refresh');
        } else {
            $this->script_model->insertPennCnv($fileName, $sampleId);
            redirect('/file/display', 'refresh');
        }
    }

    function script_cnvPart($fileName, $sampleId)
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->in_group($this->groupSU)) {
            redirect('auth', 'refresh');
        } else {
            $this->script_model->insertCnvPart($fileName, $sampleId);
            redirect('/file/display', 'refresh');
        }
    }

    function script_finalReport($fileName, $sampleId)
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->in_group($this->groupSU)) {
            redirect('auth', 'refresh');
        } else {
            $this->script_model->insertFinalReport($fileName, $sampleId);
            redirect('/file/display', 'refresh');
        }
    }

    function script_indel($fileName, $sampleId)
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->in_group($this->groupSU)) {
            redirect('auth', 'refresh');
        } else {
            $this->script_model->insertIndel($fileName, $sampleId);
            redirect('/file/display', 'refresh');
        }
    }

    function script_snp($fileName, $sampleId)
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->in_group($this->groupSU)) {
            redirect('auth', 'refresh');
        } else {
            $this->script_model->insertSnp($fileName, $sampleId);
            redirect('/file/display', 'refresh');
        }
    }

    function _render_page($templateData, $data = null, $render = false)
    {

        $this->viewdata = (empty($data)) ? $this->data : $data;

        $view_html = $this->load->view($templateData, $this->viewdata, $render);

        if (!$render) return $view_html;
    }

}